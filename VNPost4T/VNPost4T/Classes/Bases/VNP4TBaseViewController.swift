//
//  VNP4TBaseViewController.swift
//  
//
//  Created by AEGONA on 9/11/20.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

public class VNP4TBaseViewController : UIViewController {
    
    let disposeBag : DisposeBag! = DisposeBag()
    
    let vnp4tBundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
    
    func showNavigationBar(animated: Bool = false) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func hideNavigationBar(animated: Bool = false) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isMovingFromParent {
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        }
    }
    
    public func showErrorDialog(errorMsg: String, buttonTitle: String = "Ok"){
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        let errorDialog = VNP4TDialogError(nibName: "VNP4TDialogError", bundle: bundle)
        errorDialog.messageDialog = BehaviorRelay<String>(value: errorMsg)
        errorDialog.titleButton = BehaviorRelay<String>(value: buttonTitle)
        errorDialog.modalPresentationStyle = .overFullScreen
        errorDialog.onDoneActions = {
            errorDialog.dismiss(animated: false, completion: nil)
        }
        
        self.parent?.present(errorDialog, animated: false, completion: nil)
    }
    
    public func showAlert(title:String?, msg: String?){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Đóng", style: .default, handler: { action in
                                        switch action.style{
                                        case .default:
                                            print("default")
                                            
                                        case .cancel:
                                            print("cancel")
                                            alert.dismiss(animated: false, completion: nil)
                                            
                                        case .destructive:
                                            print("destructive")
                                            
                                            
                                        }}))
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension String {
    var localized: String {
        let bundle = Bundle(url: Bundle.main.bundleURL.appendingPathComponent("Frameworks").appendingPathComponent("VNPost4T.framework")) ?? Bundle.main
        return NSLocalizedString(self, tableName: nil, bundle: bundle, value: "", comment: "")
    }
}
