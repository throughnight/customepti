//
//  VNP4TCommon.swift
//  
//
//  Created by AEGONA on 9/11/20.
//

import Foundation
import UIKit
import FirebaseStorage
import RxSwift

let startLoadingMoreOffset: CGFloat = 20.0

func isNearTheBottomEdge(contentOffset: CGPoint, _ tableView: UITableView) -> Bool {
    return contentOffset.y + tableView.frame.size.height + startLoadingMoreOffset > tableView.contentSize.height
}

func setupRequiredLabel(with text: String, label: UILabel) {
    let normalText = text
    
    let requiredSymbolText  = " *"
    
    let attributedString = NSMutableAttributedString(string:normalText)
    
//    let attrs = [NSAttributedString.Key.foregroundColor : UIColor(hex: "ff5959")]
    let stringRequire = [NSAttributedString.Key.font : UIFont.italicSystemFont(ofSize: 18), NSAttributedString.Key.foregroundColor : UIColor.red]
    let symbolString = NSMutableAttributedString(string: requiredSymbolText, attributes:stringRequire)
    
    attributedString.append(symbolString)
    
    label.attributedText = attributedString
}

func resizeAndUploadImage(listImage: [UIImage],storage: Storage?,typeImage: String,userId: String?, completion: @escaping ([URL?],[String]) -> Void){
    var arrLink : [URL?] = []
    var arrFileName : [String] = []
    var count = 0
    listImage.forEach { (image) in
        let fileName = Date().vnp4TToString(format: "yyyy-MM-dd'T'HH:mm:ss.SSS")
        let day = Date().vnp4TToString(format: "yyyy-MM-dd")
        let nameImage = "\(typeImage)/\(userId ?? "0000-0000-0000-0000")/\(day)/\(fileName).jpg"
        let storageRef = storage?.reference().child(nameImage)
        
        guard let imageData = image.jpegData(compressionQuality: 0.9) else {
            return completion([],[])
        }
        
        storageRef?.putData(imageData, metadata: nil) { (metadata, error) in
            // 3
            if let error = error {
                assertionFailure(error.localizedDescription)
                return completion([],[])
            }

            // 4
            storageRef?.downloadURL(completion: { (url, error) in
                if let error = error {
                    assertionFailure(error.localizedDescription)
                    return completion([],[])
                }
                arrLink.append(url)
                arrFileName.append(nameImage)
                count += 1
                if count == listImage.count {
                    completion(arrLink,arrFileName)
                }
            })
        }
    }
}

func resizeAndUploadListImage(listImage: [VNP4TListImage],storage: Storage?,typeImage: String,userId: String?, completion: @escaping ([URL?],[VNP4TListImage]) -> Void){
    var arrLink : [URL?] = []
    var arrFileName : [VNP4TListImage] = []
    var count = 0
    var totalCount = 0
    listImage.forEach { (list) in
        arrFileName.append(VNP4TListImage(id: list.id, images: []))
        list.images?.forEach { (image) in
            totalCount += 1
            let fileName = Date().vnp4TToString(format: "yyyy-MM-dd'T'HH:mm:ss.SSS")
            let day = Date().vnp4TToString(format: "yyyy-MM-dd")
            let nameImage = "\(typeImage)/\(userId ?? "0000-0000-0000-0000")/\(day)/\(fileName).jpg"
            let storageRef = storage?.reference().child(nameImage)
            if image.url != nil {
                arrFileName.forEach { arrFile in
                    if (arrFile.id == list.id) {
                        count += 1
                        arrFile.images?.append(VNP4TImage(image: nil, url: getFileNameFromFullUrl(url: image.url ?? "")))
                    }
                }
            } else {
                guard let imageData = image.image?.jpegData(compressionQuality: 0.9) else {
                    return completion([],[])
                }
                storageRef?.putData(imageData, metadata: nil) { (metadata, error) in
                    // 3
                    if let error = error {
                        assertionFailure(error.localizedDescription)
                        return completion([],[])
                    }

                    // 4
                    storageRef?.downloadURL(completion: { (url, error) in
                        if let error = error {
                            assertionFailure(error.localizedDescription)
                            return completion([],[])
                        }
                        arrLink.append(url)
                        arrFileName.forEach { arrFile in
                            if (arrFile.id == list.id) {
                                arrFile.images?.append(VNP4TImage(image: nil, url: nameImage))
                            }
                        }
                        count += 1
                        if count == totalCount {
                            completion(arrLink,arrFileName)
                        }
                    })
                }
            }
        }
    }
}

func getFileNameFromFullUrl(url:String) -> String {
    var string : String = ""
    if url.contains("https") && (url.contains(VNP4TTypeSurveyUploadImage.ELECTRICAL.rawValue) ||
                                    url.contains(VNP4TTypeSurveyUploadImage.PHYSICAL.rawValue)) {
        let startIndex = url.contains(VNP4TTypeSurveyUploadImage.ELECTRICAL.rawValue) ? url.index(of: VNP4TTypeSurveyUploadImage.ELECTRICAL.rawValue) : url.index(of: VNP4TTypeSurveyUploadImage.PHYSICAL.rawValue)
        
        let endIndex = (url.index(of: "jpg") != nil) ? url.index(of: "jpg") : url.index(of: "png")
        
        
        if startIndex != nil && endIndex != nil {
            string = url.substring((startIndex?.utf16Offset(in: url) ?? 0)..<((endIndex?.utf16Offset(in: url) ?? 0) + 3))
//            string.append("jpg")
            string = string.replacingOccurrences(of: "%3A", with: ":")
            string = string.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    return string
}
