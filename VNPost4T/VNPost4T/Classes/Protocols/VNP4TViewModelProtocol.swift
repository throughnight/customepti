//
//  VNP4TViewModelProtocol.swift
//  
//
//  Created by AEGONA on 9/10/20.
//

import Foundation

protocol VNP4TViewModelProtocol {
    associatedtype Input
    associatedtype Output
    associatedtype Dependency
    
    var input      : Input? { get set }
    var output     : Output? { get set }
    var dependency : Dependency? { get set }
    
    @discardableResult
    func transform(input: Input?, dependency: Dependency?) -> Output?
}
