//
//  VNP4TViewControllerProtocol.swift
//  
//
//  Created by AEGONA on 9/10/20.
//

import Foundation

protocol VNP4TViewControllerProtocol {
        
    func setupView()
    
    func setupViewModel()
    
}

protocol ListViewControllerProtocol {
    
    func registerCell()
    
    func configDataSource()
    
}
