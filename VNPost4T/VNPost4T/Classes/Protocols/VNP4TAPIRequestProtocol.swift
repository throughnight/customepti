//
//  VNP4TAPIRequestProtocol.swift
//  
//
//  Created by AEGONA on 9/10/20.
//

import Foundation

protocol VNP4TAPIRequestProtocol {
    func toParameters() -> [String:Any]
}
