//
//  VNP4THomeVC.swift
//  
//
//  Created by AEGONA on 9/11/20.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift
import FirebaseStorage

class VNP4THomeVC: VNP4TBaseViewController {
    
    @IBOutlet weak var toolbarLabel: UILabel!
    @IBOutlet weak var iNSLabel: UILabel!
    @IBOutlet weak var iNSDescriptionLabel: UILabel!
    @IBOutlet weak var registerLabel: UILabel!
    @IBOutlet weak var historyAndReportLabel: UILabel!
    @IBOutlet weak var electricalLabel: UILabel!
    @IBOutlet weak var physicalLabel: UILabel!
    @IBOutlet weak var reportLabel: UILabel!
    @IBOutlet weak var historyLabel: UILabel!
    @IBOutlet weak var electricalView: UIStackView!
    @IBOutlet weak var physicalView: UIStackView!
    @IBOutlet weak var historyView: UIStackView!
    @IBOutlet weak var reportView: UIStackView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var buttonToolbarBack: UIButton!
    @IBOutlet weak var permisionDenyLabel: UILabel!
    
    private var loading: VNP4TLoadingProgressView!
    
    var seconds = 2
    
    var viewModel: VNP4THomeVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        viewModel.output?.userToken.subscribe(onNext: {result in
            if(result.success){
                self.contentView.isHidden = false
                VNP4TManager.shared.setAccessToken(result.data?.token ?? "")
            }
            else{
                switch result.message {
                case "USER_NOT_FOUND":
                    self.showErrorDialog(errorMsg: "4t_base_service_error_user_not_found".localized)
                case "UNAUTHORIZED":
                    self.permisionDenyLabel.isHidden = false
                default:
                    self.showErrorDialog(errorMsg: result.message)
                }
                
            }
            
        }).disposed(by: disposeBag)
        
    }
    
    @IBAction func onBack(_ sender: Any) {
        showNavigationBar(animated: false)
        navigationController?.popViewController(animated: true)
    }
}

extension VNP4THomeVC : VNP4TViewControllerProtocol{
    
    func setupViewModel() {
        
    }
    
    func setupView() {
        
        self.view.backgroundColor = UIColor(hex: "#EEF3FF")
        
        loading = VNP4TLoadingProgressView(uiScreen: UIScreen.main)
        
        toolbarLabel.text =  "4t_home_toolbar_title".localized
        iNSLabel.text = "4t_home_ins".localized
        iNSDescriptionLabel.text = "4t_home_ins_desc".localized
        registerLabel.text = "4t_home_register_title".localized
        historyAndReportLabel.text = "4t_home_report_and_history".localized
        electricalLabel.text = "4t_home_electrial_button".localized
        physicalLabel.text = "4t_home_physical_button".localized
        reportLabel.text = "4t_home_report".localized
        historyLabel.text = "4t_home_history".localized
        permisionDenyLabel.text = "4t_base_service_error_permision_deny".localized
        
        iNSLabel.textColor = UIColor(hex: "#FCB71E")
        iNSLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 18.0)
        permisionDenyLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 18.0)
        permisionDenyLabel.textColor = UIColor(hex: "#FF6C2C")
//        toolbarLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
        iNSDescriptionLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
        
        onElectricalClick()
        onPhysicalClick()
        onHistoryClick()
        onReportClick()
        
        electricalView.addBackground(color: .white)
        electricalView.layer.cornerRadius = 16
        electricalView.layer.masksToBounds = true
        physicalView.addBackground(color: .white)
        physicalView.layer.cornerRadius = 16
        physicalView.layer.masksToBounds = true
        historyView.addBackground(color: .white)
        historyView.layer.cornerRadius = 16
        historyView.layer.masksToBounds = true
        reportView.addBackground(color: .white)
        reportView.layer.cornerRadius = 16
        reportView.layer.masksToBounds = true
        
        self.contentView.isHidden = true
        self.permisionDenyLabel.isHidden = true
        bindUI()
        
    }
    
    private func bindUI(){
        viewModel.input?.isLoading.subscribe(onNext: {flag in
            self.view.addSubview(self.loading)
            if(flag==true) {
                self.loading.startAnimating()
            }else{
                self.loading.stopAnimating()
            }
        }).disposed(by: disposeBag)
    }
    
    private func handleShowMenu(){
        contentView.isHidden = true
    }
    
    private func onElectricalClick(){
        let tap = UITapGestureRecognizer()
        electricalView.addGestureRecognizer(tap)
        tap.rx.event.bind { (eventTap) in
            let registerVC = VNP4TRegisterInformationVC(nibName: "VNP4TRegisterInformationVC", bundle: self.vnp4tBundle)
            
            registerVC.viewModel = VNP4TRegisterInformationVM(
                input: VNP4TRegisterInformationVM.Input(
                    selectedSurvey: BehaviorRelay<VNP4TSurveyClassificationTypeSelected?>(value: nil),
                    isLoading: BehaviorRelay<Bool>(value: false),
                    searchTrigger: BehaviorRelay<Bool>(value: false),
                    surveyVerify: BehaviorRelay<VNP4TQuestionKey>(value: VNP4TQuestionKey(surveyId: "", answers: []))),
                dependency: VNP4TSurveyService())
            
            self.navigationController?.pushViewController(registerVC, animated: true)
        }.disposed(by: disposeBag)
        
    }
    
    private func onPhysicalClick(){
        let tap = UITapGestureRecognizer()
        physicalView.addGestureRecognizer(tap)
        tap.rx.event.bind { (eventTap) in
            self.showPhysicalOptions()
        }.disposed(by: disposeBag)
        
    }
    
    private func onHistoryClick(){
        let tap = UITapGestureRecognizer()
        historyView.addGestureRecognizer(tap)
        tap.rx.event.bind { (eventTap) in
            let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
            
            let history = VNP4TSurveyHistoryVC(nibName: "VNP4TSurveyHistoryVC", bundle: bundle)
            
            history.viewModel = VNP4TSurveyHistoryVM(input: VNP4TSurveyHistoryVM.Input(
                                                        isLoading: BehaviorRelay<Bool>(value: false),
                                                        loadTrigger: BehaviorRelay<Bool>(value: true),
                                                        dataAll: BehaviorRelay<[VNP4TSurvey]>(value: []),
                                                        dataDraft: BehaviorRelay<[VNP4TSurvey]>(value: []),
                                                        dataImage: BehaviorRelay<[VNP4TSurvey]>(value: []),
                                                        dataShow: BehaviorRelay<[VNP4TSurvey]>(value: []),
                                                        pageSize: BehaviorRelay<Int>(value: 20),
                                                        pageIndexAll: BehaviorRelay<Int>(value: 1),
                                                        pageIndexDraft: BehaviorRelay<Int>(value: 1),
                                                        pageIndexImage: BehaviorRelay<Int>(value: 1),
                                                        search: BehaviorRelay<String>(value: ""),
                                                        currentTab: BehaviorRelay<Int>(value: 0),
                                                        moreTrigger: BehaviorRelay<Bool>(value: false),
                                                        filterSurveyType: BehaviorRelay<VNP4TSurveyClassificationTypeSelected>(value: VNP4TSurveyClassificationTypeSelected()),
                                                        filterStatus: BehaviorRelay<VNP4TItemSelect>(value: VNP4TItemSelect()),
                                                        filterAction: BehaviorRelay<VNP4TItemSelect>(value: VNP4TItemSelect())),
                                                     dependency: VNP4TSurveyService())
            
            self.navigationController?.pushViewController(history, animated: true)
            
            print("onHistoryClick")
        }.disposed(by: disposeBag)
        
    }
    
    private func onReportClick(){
        let tap = UITapGestureRecognizer()
        reportView.addGestureRecognizer(tap)
        tap.rx.event.bind { (eventTap) in
            
            print("onReportClick")
            let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
            let reportVC = VNP4TReportVC(nibName: "VNP4TReportVC", bundle: bundle)
            reportVC.viewModel = VNP4TReportVM(input: VNP4TReportVM.Input(isLoading: BehaviorRelay<Bool>(value: false),
                                                                          type: BehaviorRelay<String>(value: VNP4TReportType.DAY.rawValue),
                                                                          dateFrom: BehaviorRelay<String>(value: Date().vnp4TToString(format: "dd/MM/yyyy")),
                                                                          dateTo: BehaviorRelay<String>(value: Date().vnp4TToString(format: "dd/MM/yyyy")),
                                                                          survey: BehaviorRelay<VNP4TSurveyClassificationTypeSelected>(value: VNP4TSurveyClassificationTypeSelected()),
                                                                          loadTrigger: BehaviorRelay<Bool>(value: true),
                                                                          dataReport: BehaviorRelay<[VNP4TReport]>(value: [])),
                                               dependency: VNP4TReportService())
            
            self.navigationController?.pushViewController(reportVC, animated: true)
            
        }.disposed(by: disposeBag)
        
    }
    
    public func showPhysicalOptions(){
        let physicalOption = VNP4TPhysicalOption(nibName: "VNP4TPhysicalOption", bundle: vnp4tBundle)
        let physicalVC = VNP4TPhysicalVC(nibName: "VNP4TPhysicalVC", bundle: vnp4tBundle)
        
        physicalOption.modalPresentationStyle = .overFullScreen
        
        physicalOption.onSearch  = {
            physicalVC.viewModel = VNP4TPhysicalVM(state: BehaviorRelay<Int?>(value:PhysicalOptions.SEARCH.rawValue))
            self.navigationController?.pushViewController(physicalVC, animated: true)
            physicalOption.dismiss(animated: false, completion: nil)
        }
        
        physicalOption.onTakePicture = {
            physicalVC.viewModel = VNP4TPhysicalVM(state: BehaviorRelay<Int?>(value:PhysicalOptions.TAKE_PICUTRE.rawValue))
            self.navigationController?.pushViewController(physicalVC, animated: true)
            physicalOption.dismiss(animated: false, completion: nil)
        }
        
        physicalOption.onLibrary = {
            physicalVC.viewModel = VNP4TPhysicalVM(state: BehaviorRelay<Int?>(value:PhysicalOptions.LIBRARY.rawValue))
            self.navigationController?.pushViewController(physicalVC, animated: true)
            physicalOption.dismiss(animated: false, completion: nil)
        }
        
        self.parent?.present(physicalOption, animated: false, completion: nil)
        
    }
    
}

extension UIStackView {
    func addBackground(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
}
