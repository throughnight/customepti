//
//  VNP4THomeVM.swift
//  VNPost4T
//
//  Created by m2 on 9/23/20.
//

import Foundation
import RxCocoa
import RxSwift
import RxDataSources

class VNP4THomeVM: VNP4TViewModelProtocol {
    
    typealias Dependency = VNP4TAuthService
    
    struct Input {
        let isLoading                           :BehaviorRelay<Bool>
        let token                            :BehaviorRelay<String>
    }
    
    struct Output {
        let userToken               :Observable<VNP4TResponseModel<VNP4TUserToken>>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input: Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: VNP4TAuthService?) -> Output? {
        self.input = input
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        
        let userToken = ip.token.asObservable()
            .do(onNext: { _ in
                ip.isLoading.accept(true)
            }).flatMapLatest { _token -> Observable<VNP4TResponseModel<VNP4TUserToken>> in
                return dp.getToken(token: _token)}
            .do(onNext: { _ in
                ip.isLoading.accept(false)
            })
        
        return VNP4THomeVM.Output(userToken: userToken)
    }
}
