//
//  VNP4TPhysicalImageCollectionViewCell.swift
//  VNPost4T
//
//  Created by m2 on 9/29/20.
//

import Foundation

class VNP4TPhysicalImageCollectionViewCell: UICollectionViewCell {
    
    static let identify = "VNP4TPhysicalImageCollectionViewCell"
    @IBOutlet weak var imgDelete: UIButton!
    
    var onDel : (()->())?
    
    @IBAction func onDeleteImage(_ sender: Any) {
        self.onDel?()
    }

    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var imageView: UIImageView!
   
    func isHide(){
        imgDelete.isHidden = true
    }
    
    func isShow(){
        imgDelete.isHidden = false
    }
    
    func loadImage(data: VNP4TImage?){
        if data?.image != nil {
            imageView.image = data?.image
            isShow()
        } else {
            imageView.image = nil
            imageView.load(url: URL(string: data?.url ?? "")!)
            isHide()
        }
    }
}
