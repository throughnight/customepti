//
//  VNP4TPhysical.swift
//  VNPost4T
//
//  Created by m2 on 9/28/20.
//

import Foundation
import RxSwift
import RxCocoa
import FirebaseStorage

public enum PhysicalOptions: Int{
    case SEARCH = 0
    case TAKE_PICUTRE = 1
    case LIBRARY = 2
}

class VNP4TPhysicalVC: VNP4TBaseViewController  {
    
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    
    @IBOutlet weak var surveyNameLabel: UILabel!
    @IBOutlet weak var imageCountLabel: UILabel!
    
    @IBOutlet weak var surveyTypeView: UIView!
    @IBOutlet weak var containerButtonView: UIView!
    
    @IBOutlet weak var buttonToolbarBack: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var addImageButton: UIButton!
    
    @IBOutlet weak var viewVerifySurvey: UIView!
    @IBOutlet weak var btnTitleVerify: UIButton!
    @IBOutlet weak var imgDownUp: UIImageView!
    @IBOutlet weak var viewContainVerify: UIView!
    
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    private var loading: VNP4TLoadingProgressView!
    private var physicalOption: VNP4TPhysicalOption!
    
    private let imagePickerController = UIImagePickerController()
    private var verifyVC : VNP4TSurveyQuestionsVC!
    
    var viewModel : VNP4TPhysicalVM!
    var isSearch : Bool = false
    
    var isSpend = false
    
    private var isAddImage = true
    
    @IBAction func onQRScan(_ sender: Any) {
        let scan = VNP4TScanQR(nibName: "VNP4TScanQR", bundle: self.vnp4tBundle)
        scan.delegate = self
        self.navigationController?.pushViewController(scan, animated: true)
    }
    
    @IBAction func addImage(_ sender: Any) {
        onOptionsCamera()
    }
    
    @IBAction func onBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        let listImage = self.viewModel.images.value.count
        let maxUpload = self.viewModel.maxImageUpload.value!
        
        if(listImage > maxUpload ){
            let msg = "Vui lòng không gửi quá \(maxUpload) ảnh"
            self.showAlert(title: nil, msg: msg)
            return
        }
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        let dialogOptions = VNP4TDialogOptions(nibName: "VNP4TDialogOptions", bundle: bundle)
        
        dialogOptions.modalPresentationStyle = .overFullScreen
        dialogOptions.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_submit_survey".localized)
        dialogOptions.messageDialog = BehaviorRelay<String>(value: "")
        dialogOptions.titleActionConfirm = BehaviorRelay<String?>(value: "4t_physical_submit_button".localized)
        dialogOptions.onCancelAction = {
            dialogOptions.dismiss(animated: false, completion: nil)
        }
        
        dialogOptions.onDoneActions = { [weak self] in
            guard let self = self else {return}
            dialogOptions.dismiss(animated: false, completion: nil)
            self.viewModel.uploadImage()
        }
        
        self.parent?.present(dialogOptions, animated: false, completion: nil)
    }
    
    @IBAction func onSearch(_ sender: Any) {
        let survey = self.viewModel.selectedSurvey.value
        var isPass = true
        survey?.keys.forEach({ (item) in
            if item.answerValue != "" && item.isPass == true {
                item.isPass = true
            } else {
                isPass = false
                item.isPass = false
            }
        })
        if isPass == true {
            self.viewModel.triggerVerify.accept(true)
        } else {
            self.verifyVC.tbView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        bindUI()
        
    }
    
    @IBAction func onExpend(_ sender: Any) {
        isSpend = !isSpend
        
        if isSpend {
            viewContainVerify.isHidden = false
            self.searchButton.isEnabled = true
            self.searchButton.backgroundColor = .white
            self.searchButton.layer.borderColor = UIColor(hex: "127BCA").cgColor
            self.searchButton.setTitleColor(UIColor(hex: "127BCA"), for: .normal)
            self.imgDownUp.image = UIImage(named: "ic_arrow_up_black",
            in: Bundle(url: Bundle.main.bundleURL.appendingPathComponent("Frameworks").appendingPathComponent("VNPost4T.framework")) ?? Bundle.main,
            compatibleWith: nil)
        } else {
            viewContainVerify.isHidden = true
            self.searchButton.backgroundColor = UIColor(hex: "E9E9E9")
            self.searchButton.layer.borderColor = UIColor(hex: "E9E9E9").cgColor
            self.searchButton.setTitleColor(.white, for: .normal)
            self.searchButton.isEnabled = false
            self.imgDownUp.image = UIImage(named: "ic_arrow_down_black",
            in: Bundle(url: Bundle.main.bundleURL.appendingPathComponent("Frameworks").appendingPathComponent("VNPost4T.framework")) ?? Bundle.main,
            compatibleWith: nil)
        }
    }
    
    private func clearData(){
//        viewModel.state.accept(viewModel.state.value)
        viewModel.selectedSurvey.accept(nil)
        self.surveyNameLabel.text = "4t_physical_survey_type_hint".localized
        viewModel.images.accept([])
        if (verifyVC != nil){
            self.remove(asChildViewController: verifyVC!)
        }
        self.viewModel.maxImageUpload.accept(4)
        self.containerButtonView.isHidden = true
        self.handleImageUpload(buttonAddImage: self.addImageButton, labelShowImage: self.imageCountLabel, currentImage: 0, maxImageUpload: 4)
    }
    
    private func bindUI(){
        viewModel.state
            .filter{ $0 != nil }
            .subscribe(onNext:{ state in
                switch state {
                case PhysicalOptions.SEARCH.rawValue:
                    self.isSearch = true
                    self.addImageButton.isHidden = self.isSearch
                    self.imageCountLabel.isHidden = self.isSearch
                    break
                case PhysicalOptions.TAKE_PICUTRE.rawValue:
                    self.takePhoto()
                case PhysicalOptions.LIBRARY.rawValue:
                    self.photoLibrary()
                default:
                    print("SEARCH")
                }
            }).disposed(by: disposeBag)
         
        viewModel.isLoading.subscribe(onNext: {loading in
            self.view.addSubview(self.loading)
            if(loading == true) {
                self.loading.startAnimating()
            }else{
                self.loading.stopAnimating()
            }
        }).disposed(by: disposeBag)
        
        viewModel.selectedSurvey
            .filter{ $0 != nil}
            .subscribe(onNext: { data in
                if (self.verifyVC != nil){
                    self.remove(asChildViewController: self.verifyVC!)
                }
                self.surveyNameLabel.textColor = UIColor(hex:"FFFFF")
                self.surveyNameLabel.text = "[\(String(describing: data!.code))] \(String(describing: data!.name))"
                self.containerButtonView.isHidden = false
                self.handleVerifyView()
                self.isSearch = false
                self.imageCountLabel.isHidden = self.isSearch
            }).disposed(by: disposeBag)
        
        viewModel.images
            .subscribe(onNext: { data in
                let currentImage = data.count
                let maxImageUpload = self.viewModel.maxImageUpload.value!
                
                if(!self.isSearch){
                    self.handleImageUpload(buttonAddImage: self.addImageButton, labelShowImage: self.imageCountLabel, currentImage: currentImage, maxImageUpload: maxImageUpload)
                    self.submitButton.isEnabled = (data.count > 0)
                }
                
                if (data.count > 0){
                    self.submitButton.backgroundColor = UIColor(hex: "025BBB")
                    self.collectionViewHeight.constant = 200
                    self.view.layoutIfNeeded()
                }else{
                    self.collectionViewHeight.constant = 0
                    self.view.layoutIfNeeded()
                    self.submitButton.backgroundColor = UIColor(hex: "E9E9E9")
                }
                
            }).disposed(by: disposeBag)
        
        viewModel.getDetectSurveyItemOutput?.asObservable().subscribe(onNext: { resultData in
            if(resultData.data?.data?.count == 0){
                self.showAlert(title: nil, msg: "4t_physical_survey_search_empty".localized)
            }else{
                self.viewModel.selectedSurvey.accept(resultData.data?.data?.first)
            }
        }).disposed(by: disposeBag)
        
        viewModel.getMaxImageUpload?.asObservable().subscribe(onNext: { result in
            
            let totalImage = result.data?.totalImg ?? 0
            self.viewModel.maxImageUpload.accept(totalImage)
            let currentImage = self.viewModel.images.value.count
            self.handleImageUpload(buttonAddImage: self.addImageButton, labelShowImage: self.imageCountLabel, currentImage: currentImage, maxImageUpload: totalImage)
            
        }).disposed(by: disposeBag)
        
        viewModel.verifySurveyOutput?.subscribe(onNext : {[weak self] result in
            guard let self = self else {return}
            
            if result.success == true {
                if result.data?.data?.count ?? 0 > 0 {
                    self.showAlert(title: nil, msg: "4t_register_survey_title_notify_exist".localized)
                } else {
                    self.showAlert(title: nil, msg: "4t_physical_verify_survey_not_exist_data".localized)
                }
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_dialog_error_title".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: true, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
        
        viewModel.createFSurveyOutput?.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.success == true {
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let successDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                successDialog.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_success".localized.uppercased())
                successDialog.messageDialog = BehaviorRelay<String>(value: "4t_physical_survey_message_success_1".localized + " " + (self.viewModel.selectedSurvey.value?.code ?? "") + " " + "4t_physical_survey_message_success_2".localized)
                successDialog.status = BehaviorRelay<Bool>(value: true)
                successDialog.modalPresentationStyle = .overFullScreen
                successDialog.onDoneActions = {
                    successDialog.dismiss(animated: true, completion: nil)
//                    self.navigationController?.popViewController(animated: true)
                    self.clearData()
                }
                
                self.parent?.present(successDialog, animated: false, completion: nil)
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_error".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: true, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        })
    }
    
    private func handleImageUpload(buttonAddImage: UIButton, labelShowImage: UILabel, currentImage: Int, maxImageUpload: Int){
        buttonAddImage.isHidden = (currentImage >= maxImageUpload)
        labelShowImage.text = "\(currentImage)/\(String(describing: maxImageUpload)) hình ảnh"
    }
    
    func handleVerifyView(){
        self.viewVerifySurvey.isHidden = false
        let survey = self.viewModel.selectedSurvey.value
        var dataQuestion: [VNP4TSurveyQuestion] = []
        survey?.keys.forEach({ (item) in
            dataQuestion.append(item)
        })
        
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        self.verifyVC = VNP4TSurveyQuestionsVC(nibName: "VNP4TSurveyQuestionsVC", bundle: bundle)
        self.verifyVC.viewModel = VNP4TSurveyQuestionVM(input: VNP4TSurveyQuestionVM.Input(
                                                            dataQuestions: BehaviorRelay<[VNP4TSurveyQuestion]>(value: dataQuestion),
                                                            isScroll: BehaviorRelay<Bool>(value: false)),
                                                        dependency: VNP4TSurveyService())
        self.verifyVC.onChangeValue = { item in
            survey?.keys.forEach({ (data) in
                if data.id == item.id {
                    data.answerValue = item.answerValue
                }
            })
        }
        
        self.add(asChildViewController: self.verifyVC)
        
    }
}

extension VNP4TPhysicalVC: VNP4TViewControllerProtocol{
    
    func setupViewModel() {
        
    }
    
    func setupView() {
        
        self.surveyNameLabel.text = "4t_physical_survey_type_hint".localized
        self.searchButton.setTitle("4t_physical_search_button".localized, for: .normal)
        self.submitButton.setTitle("4t_physical_submit_button".localized, for: .normal)
        
        self.imagesCollectionView.rx.setDelegate(self).disposed(by: disposeBag)
        self.imagesCollectionView.rx.setDataSource(self).disposed(by: disposeBag)
        
        self.imagesCollectionView.register(UINib(nibName: VNP4TPhysicalImageCollectionViewCell.identify, bundle: vnp4tBundle), forCellWithReuseIdentifier: VNP4TPhysicalImageCollectionViewCell.identify)
        
        self.loading = VNP4TLoadingProgressView(uiScreen: UIScreen.main)
        
        self.physicalOption = VNP4TPhysicalOption()
        
        self.imagePickerController.delegate = self
        self.imagePickerController.allowsEditing = false
        self.imagePickerController.mediaTypes = ["public.image"]
        
        self.containerButtonView.isHidden = true
        
        self.submitButton.backgroundColor = UIColor(hex: "E9E9E9")
        self.submitButton.isEnabled = false
        
        self.searchButton.backgroundColor = UIColor(hex: "E9E9E9")
        self.searchButton.layer.borderColor = UIColor(hex: "E9E9E9").cgColor
        self.searchButton.setTitleColor(.white, for: .normal)
        self.searchButton.isEnabled = false
        
        self.collectionViewHeight.constant = 0
        self.view.layoutIfNeeded()
        onSurveyTypeClick()
        
    }
    
    private func onSurveyTypeClick() {
        let tap = UITapGestureRecognizer()
        surveyTypeView.addGestureRecognizer(tap)
        tap.rx.event.bind { (eventTap) in
            self.onShowFormSurveyType()
        }.disposed(by: disposeBag)
    }
    
    private func onShowFormSurveyType(){
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        let formRegisterPicker = VNP4TFormRegisterInformationDataPickerVC(nibName: "VNP4TFormRegisterInformationDataPickerVC", bundle: bundle)
        formRegisterPicker.viewModel = VNP4TFormRegisterInformationDatePickerVM(input: VNP4TFormRegisterInformationDatePickerVM.Input(
                                                                                    searchKey: BehaviorRelay<String>(value: ""),
                                                                                    loadTrigger: BehaviorRelay<Bool>(value: true),
                                                                                    loadMoreTrigger: BehaviorRelay<Bool>(value: false),
                                                                                    nextPage: BehaviorRelay<Int>(value: 1),
                                                                                    isEnd: BehaviorRelay<Bool>(value: true),
                                                                                    surveyHistory: BehaviorRelay<[VNP4TSurveyClassificationTypeSelected]>(value: []),
                                                                                    isLoading: BehaviorRelay<Bool>(value: false),
                                                                                    collectionType:
                                                                                        BehaviorRelay<Int?>(value: 1)), dependency: VNP4TSurveyService()
        )
        formRegisterPicker.modalPresentationStyle = .overFullScreen
        formRegisterPicker.dataPickedAction = { [weak self] survey in
            guard let self = self else { return }
            
            formRegisterPicker.dismiss(animated: false, completion: nil)
            self.viewModel.selectedSurvey.accept(survey)
        }
        
        self.parent?.present(formRegisterPicker, animated: false, completion: nil)
    }
    
    private func onOptionsCamera(){
        isAddImage = true
        let imageOption = VNP4TDialogImageOption(nibName: VNP4TDialogImageOption.identifier, bundle: vnp4tBundle)
        
        imageOption.modalPresentationStyle = .overFullScreen
        
        imageOption.onTakePicture = {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.takePhoto()
            }
        }
        
        imageOption.onLibrary = {
            self.photoLibrary()
        }
        
        self.parent?.present(imageOption, animated: false, completion: nil)
        
    }
    
    private func detectQRCode(_ image: UIImage?) -> [CIFeature]? {
        if let image = image, let ciImage = CIImage.init(image: image){
            var options: [String: Any]
            let context = CIContext()
            options = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
            let qrDetector = CIDetector(ofType: CIDetectorTypeQRCode, context: context, options: options)
            if ciImage.properties.keys.contains((kCGImagePropertyOrientation as String)){
                options = [CIDetectorImageOrientation: ciImage.properties[(kCGImagePropertyOrientation as String)] ?? 1]
            } else {
                options = [CIDetectorImageOrientation: 1]
            }
            let features = qrDetector?.features(in: ciImage, options: options)
            return features
            
        }
        return nil
    }
    
    private func takePhoto(){
        self.imagePickerController.sourceType = .camera
        self.present(self.imagePickerController, animated: true, completion: nil)
    }
    
    private func photoLibrary(){
        self.imagePickerController.sourceType = .photoLibrary
        self.present(self.imagePickerController, animated: true, completion: nil)
    }
    
    
}

extension VNP4TPhysicalVC: VNP4TScanQRDelegate{
    func qrResult(qr: String?) {
        self.viewModel.qrResult.accept(qr)
    }
    
}

extension VNP4TPhysicalVC: UINavigationControllerDelegate{
    
}

extension VNP4TPhysicalVC: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.images.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell =  imagesCollectionView.dequeueReusableCell(withReuseIdentifier: VNP4TPhysicalImageCollectionViewCell.identify, for: indexPath) as! VNP4TPhysicalImageCollectionViewCell
        
        cell.imageView.image = self.viewModel.images.value[indexPath.row]
        
        cell.onDel = {
            if(self.viewModel.images.value.count == 0){
                return
            }
            var imageArray = self.viewModel.images.value
            imageArray.remove(at: indexPath.row)
            self.viewModel.images.accept(imageArray)
            self.imagesCollectionView.reloadData()
        }
        
        return cell
    }
    
}

extension VNP4TPhysicalVC: UICollectionViewDelegate{
    
}

extension VNP4TPhysicalVC: UIImagePickerControllerDelegate {
    
    private func pickerImageController(_ controller: UIImagePickerController, image: UIImage?) {
        controller.dismiss(animated: true, completion: nil)
        self.dismiss(animated: false, completion: nil)
        if(isAddImage == true && image != nil){
            isAddImage = false
            var images = viewModel.images.value
            images += [image!]
            viewModel.images.accept(images)
            self.imagesCollectionView.reloadData()
            
            if let features = detectQRCode(image), !features.isEmpty{
                for case let row as CIQRCodeFeature in features{
                    let qrText = row.messageString!
                    self.viewModel.qrResult.accept(qrText)
                    
                }
            }
        }
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        pickerImageController(picker, image: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        guard let _image = info[.originalImage] as? UIImage else {
            pickerImageController(picker,image: nil)
            return
        }
        pickerImageController(picker, image: _image)
    }
}

extension VNP4TPhysicalVC {
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChild(viewController)
        
        // Add Child View as Subview
        self.viewContainVerify.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = self.viewContainVerify.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Remove Child View From Parent
        viewController.removeFromParent()
    }
}
