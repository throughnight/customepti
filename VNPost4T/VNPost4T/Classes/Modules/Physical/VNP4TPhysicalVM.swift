//
//  VNP4TPhysicalVM.swift
//  VNPost4T
//
//  Created by m2 on 9/29/20.
//

import Foundation
import RxCocoa
import RxSwift

enum VNP4TPhysicalVMConfig: Int {
    case MAX_IMAGE_UPLOAD = 4
}

class VNP4TPhysicalVM {
    
    let bag = DisposeBag()
    let service: VNP4TSurveyAPIService!
    let state: BehaviorRelay<Int?>
    let serviceCommon: VNP4TCommonService!
    let images = BehaviorRelay<[UIImage]>(value: [])
    let isLoading = BehaviorRelay<Bool>(value: false)
    let selectedSurvey = BehaviorRelay<VNP4TSurveyClassificationTypeSelected?>(value: nil)
    let qrResult = BehaviorRelay<String?>(value: nil)
    let maxImageUpload = BehaviorRelay<Int?>(value: VNP4TPhysicalVMConfig.MAX_IMAGE_UPLOAD.rawValue)
    let surveySerice : VNP4TSurveyService!
    let triggerVerify = BehaviorRelay<Bool>(value : false)
    let triggerCreate = BehaviorRelay<Bool>(value: false)
    let listImageName = BehaviorRelay<[String]>(value: [])
    
    var getDetectSurveyItemOutput = Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyClassificationTypeSelected]>>>? (nil)
    var getMaxImageUpload = Observable<VNP4TResponseModel<VNP4TLimitImageUpload>>?(nil)
    
    var verifySurveyOutput = Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyQuestion]>>>?(nil)
    var createFSurveyOutput = Observable<VNP4TResponseModel<Any>>?(nil)
    
    init(state: BehaviorRelay<Int?>) {
        self.state = state
        self.service = VNP4TSurveyAPIService()
        self.serviceCommon = VNP4TCommonService()
        self.surveySerice = VNP4TSurveyService()
        
        self.getMaxImageUpload = self.getMaxImageToSend()
        self.getDetectSurveyItemOutput = self.getDetectSurveyItem()
        
        bindOutput()
    }
    
    private func bindOutput() {
        self.verifySurveyOutput = self.verifySurveyKey()
        self.createFSurveyOutput = self.createFSurvey()
    }
    
    private func getMaxImageToSend()
    -> Observable<VNP4TResponseModel<VNP4TLimitImageUpload>>? {
        return self.selectedSurvey.asObservable()
            .filter{ $0 != nil }
            .flatMap{ _item -> Observable<VNP4TResponseModel<VNP4TLimitImageUpload>> in
                return self.serviceCommon.getMaxImageToSend(id: _item?.id ?? "")
            }
    }
    
    private func getDetectSurveyItem()
    -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyClassificationTypeSelected]>>>{
        return self.qrResult.asObservable()
            .filter { $0 != nil}
            .do(onNext: { _ in
                self.isLoading.accept(true)
            }).flatMapLatest { _code ->  Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyClassificationTypeSelected]>>> in
                return self.service.getDetectSurveyItem(code: _code ?? "")
            }.do(onNext: { _ in
                self.isLoading.accept(false)
            })
    }
    
    private func verifySurveyKey() -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyQuestion]>>> {
        return self.triggerVerify.asObservable()
            .filter { $0 }
            .withLatestFrom(selectedSurvey)
            .do(onNext: { _ in
                self.isLoading.accept(true)
            })
            .flatMapLatest { (survey) -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyQuestion]>>> in
                var arrQuestion : [VNP4TQuestion] = []
                survey?.keys.forEach({ (item) in
                    arrQuestion.append(VNP4TQuestion(id: item.id, value: item.answerValue))
                })
                let key = VNP4TQuestionKey(surveyId: survey?.id ?? "", answers: arrQuestion)
                return self.surveySerice.verifySurveyKey(survey: key)
            }.do(onNext: { _ in
                self.isLoading.accept(false)
            })
    }
    
    func uploadImage() {
        Observable.just(images.value)
            .subscribe(onNext: {[weak self] images in
                guard let self = self else {return}
                self.isLoading.accept(true)
                resizeAndUploadImage(listImage: images, storage: VNP4TManager.shared.storage, typeImage: VNP4TTypeSurveyUploadImage.PHYSICAL.rawValue, userId: VNP4TManager.shared.userId!) { (url,fileName) in
                    self.listImageName.accept(fileName)
                    self.triggerCreate.accept(true)
                }
            })
    }
    
    private func createFSurvey() -> Observable<VNP4TResponseModel<Any>> {
        return self.triggerCreate.asObservable()
            .filter {$0}
            .withLatestFrom(listImageName)
            .flatMapLatest { images -> Observable<VNP4TResponseModel<Any>> in
                let survey = self.selectedSurvey.value
                return self.surveySerice.createPhysicalSurvey(data: VNP4TQuestionKey(surveyId: survey?.id ?? "", listImage: images))
            }.do(onNext: { _ in
                self.isLoading.accept(false)
            })
    }
}
