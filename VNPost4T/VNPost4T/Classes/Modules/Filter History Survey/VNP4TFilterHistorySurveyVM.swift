//
//  VNP4TFilterHistorySurveyVM.swift
//  VNPost4T
//
//  Created by AEGONA on 9/30/20.
//

import Foundation
import RxCocoa
import RxSwift

class VNP4TFilterHistorySurveyVM : VNP4TViewModelProtocol {
    
    typealias Dependency = VNP4TSurveyService
    
    struct Input {
        let itemSurveyType                  :BehaviorRelay<VNP4TSurveyClassificationTypeSelected>
        let itemStatusSurvey                :BehaviorRelay<VNP4TItemSelect>
        let itemSurveyAction                :BehaviorRelay<VNP4TItemSelect>
        let dataStatusSurvey                :BehaviorRelay<[VNP4TItemSelect]>
        let dataSurveyAction                :BehaviorRelay<[VNP4TItemSelect]>
        let currentTab                      :BehaviorRelay<Int>
    }
    
    struct Output {
        let statusResult                    :Observable<[VNP4TItemSelect]>
        let actionResult                    :Observable<[VNP4TItemSelect]>
        let surveyTypeResult                :Observable<VNP4TSurveyClassificationTypeSelected>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input: Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: VNP4TSurveyService?) -> Output? {
        self.input = input
        
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        
        let statusResult = ip.dataStatusSurvey.asObservable()
            .flatMapLatest { data -> Observable<[VNP4TItemSelect]> in
                return Observable.just(data)
            }
        
        let actionResult = ip.dataSurveyAction.asObservable()
            .flatMapLatest { data -> Observable<[VNP4TItemSelect]> in
                return Observable.just(data)
            }
        
        let typeSurveyResult = ip.itemSurveyType.asObservable()
            .flatMapLatest { data -> Observable<VNP4TSurveyClassificationTypeSelected> in
                return Observable.just(data)
            }
        
        return VNP4TFilterHistorySurveyVM.Output(statusResult: statusResult,
                                                 actionResult: actionResult,
                                                 surveyTypeResult: typeSurveyResult)
    }
}
