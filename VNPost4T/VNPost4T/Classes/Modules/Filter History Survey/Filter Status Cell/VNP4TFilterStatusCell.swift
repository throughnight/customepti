//
//  VNP4TFilterStatusCell.swift
//  VNPost4T
//
//  Created by AEGONA on 9/30/20.
//

import Foundation
import UIKit
import RxCocoa

class VNP4TFilterStatusCell: UITableViewCell {
    static let cellIdentifier = "VNP4TFilterStatusCell"
    
    @IBOutlet weak var btnValue: UIButton!
    @IBOutlet weak var checkBox: VNP4TCheckBox!
    
    var onPickItem : ((VNP4TItemSelect?)->())?

    var data : VNP4TItemSelect? {
        didSet {
            self.checkBox.borderStyle = .rounded
            self.checkBox.style = .circle
            self.btnValue.setTitle(data?.name, for: .normal)
            self.checkBox.isChecked = data?.status ?? false
            self.checkBox.isLock = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func onPick(_ sender: Any) {
        let currentData = data
        if currentData?.status == false {
            self.checkBox.isChecked = true
            currentData?.status = true
            self.onPickItem?(currentData)
        }
    }
}
