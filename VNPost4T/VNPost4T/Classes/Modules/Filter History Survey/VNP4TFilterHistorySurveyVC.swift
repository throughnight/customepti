//
//  VNP4TFilterHistorySurveyVC.swift
//  VNPost4T
//
//  Created by AEGONA on 9/30/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

class VNP4TFilterHistorySurveyVC: VNP4TBaseViewController {
    
    @IBOutlet weak var viewBackgroundBar: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbTitleTypeSurvey: UILabel!
    @IBOutlet weak var viewStatusType: UIView!
    @IBOutlet weak var viewStatusAction: UIView!
    @IBOutlet weak var viewStatusConstraints: NSLayoutConstraint!
    @IBOutlet weak var viewActionsConstraints: NSLayoutConstraint!
    @IBOutlet weak var btnValueSurveyType: UIButton!
    
    var viewModel : VNP4TFilterHistorySurveyVM!
    
    private var statusVC : VNP4TFilterStatusVC!
    private var actionVC : VNP4TFilterStatusVC!
    
    var onFilter : ((VNP4TSurveyClassificationTypeSelected?,VNP4TItemSelect?,VNP4TItemSelect?)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupViewModel()
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onResetFilter(_ sender: Any) {
        self.viewModel.input?.itemSurveyType.accept(VNP4TSurveyClassificationTypeSelected())
        self.viewModel.input?.itemSurveyAction.accept(VNP4TItemSelect())
        self.viewModel.input?.itemStatusSurvey.accept(VNP4TItemSelect())
        self.actionVC?.resetData()
        self.statusVC.resetData()
    }
    
    @IBAction func onPickSurveyType(_ sender: Any) {
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        let formRegisterPicker = VNP4TFormRegisterInformationDataPickerVC(nibName: "VNP4TFormRegisterInformationDataPickerVC", bundle: bundle)
        formRegisterPicker.viewModel = VNP4TFormRegisterInformationDatePickerVM(input: VNP4TFormRegisterInformationDatePickerVM.Input(
                                                                                    searchKey: BehaviorRelay<String>(value: ""),
                                                                                    loadTrigger: BehaviorRelay<Bool>(value: true),
                                                                                    loadMoreTrigger: BehaviorRelay<Bool>(value: false),
                                                                                    nextPage: BehaviorRelay<Int>(value: 1),
                                                                                    isEnd: BehaviorRelay<Bool>(value: true),
                                                                                    surveyHistory: BehaviorRelay<[VNP4TSurveyClassificationTypeSelected]>(value: []),
                                                                                    isLoading: BehaviorRelay<Bool>(value: false),
                                                                                    collectionType: BehaviorRelay<Int?>(value: nil)),
                                                                                dependency: VNP4TSurveyService()
        )
        formRegisterPicker.modalPresentationStyle = .overFullScreen
        formRegisterPicker.dataPickedAction = { [weak self] survey in
            guard let self = self else { return }
            
            formRegisterPicker.dismiss(animated: true, completion: nil)
            self.viewModel.input?.itemSurveyType.accept(survey)
        }
        
        self.parent?.present(formRegisterPicker, animated: true, completion: nil)
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onFilter(_ sender: Any) {
        self.onFilter?(self.viewModel.input?.itemSurveyType.value, self.viewModel.input?.itemStatusSurvey.value, self.viewModel.input?.itemSurveyAction.value)
        self.navigationController?.popViewController(animated: true)
    }
}

extension VNP4TFilterHistorySurveyVC : VNP4TViewControllerProtocol {
    func setupView() {
        self.viewBackgroundBar.VNP4TsetGradientBackground(colorStart: VNP4TConstants.navigationBarMainColor, colorEnd: VNP4TConstants.navigationBarSubColor)
        
        self.lbTitleTypeSurvey.text = "4t_register_survey_type".localized
        self.btnValueSurveyType.setTitle("4t_register_survey_type_placeholder".localized, for: .normal)
        
        var statusType : [VNP4TItemSelect] = []
        statusType.append(VNP4TItemSelect(id: -1, name: "4t_status_all".localized, status: false))
        statusType.append(VNP4TItemSelect(id: 0, name: "4t_status_collection".localized, status: false))
        statusType.append(VNP4TItemSelect(id: 1, name: "4t_status_re_collection".localized, status: false))
        
        var actionStatus : [VNP4TItemSelect] = []
        actionStatus.append(VNP4TItemSelect(id: -1, name: "4t_status_all".localized, status: false))
        actionStatus.append(VNP4TItemSelect(id: 0, name: "4t_status_new".localized, status: false))
        actionStatus.append(VNP4TItemSelect(id: 1, name: "4t_status_processing".localized, status: false))
        actionStatus.append(VNP4TItemSelect(id: 5, name: "4t_status_approve".localized, status: false))
        actionStatus.append(VNP4TItemSelect(id: 6, name: "4t_status_deny".localized, status: false))
        
        self.viewModel.input?.dataStatusSurvey.accept(statusType)
        self.viewModel.input?.dataSurveyAction.accept(actionStatus)
        
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        self.statusVC = VNP4TFilterStatusVC(nibName: "VNP4TFilterStatusVC", bundle: bundle)
        self.statusVC.viewModel = VNP4TFilterStatusVM(input: VNP4TFilterStatusVM.Input(data: BehaviorRelay<[VNP4TItemSelect]>(value: statusType),
                                                                                       title : BehaviorRelay<String>(value: "4t_filter_history_title_status_type_survey".localized),
                                                                                       itemInit: BehaviorRelay<VNP4TItemSelect?>(value: self.viewModel.input?.itemStatusSurvey.value)),
                                                      dependency: VNP4TSurveyService())
        self.statusVC.onPickItem = { item in
            self.viewModel.input?.itemStatusSurvey.accept(item ?? VNP4TItemSelect())
        }
        self.add(asChildViewController: self.statusVC, addView: self.viewStatusType)
        
        if self.viewModel.input?.currentTab.value == 0 {
            self.actionVC = VNP4TFilterStatusVC(nibName: "VNP4TFilterStatusVC", bundle: bundle)
            self.actionVC.viewModel = VNP4TFilterStatusVM(input: VNP4TFilterStatusVM.Input(data: BehaviorRelay<[VNP4TItemSelect]>(value: actionStatus),
                                                                                           title: BehaviorRelay<String>(value: "4t_filter_history_title_action_type_survey".localized),
                                                                                           itemInit: BehaviorRelay<VNP4TItemSelect?>(value: self.viewModel.input?.itemSurveyAction.value)), dependency: VNP4TSurveyService())
            self.actionVC.onPickItem = { item in
                self.viewModel.input?.itemSurveyAction.accept(item ?? VNP4TItemSelect())
            }
            self.add(asChildViewController: self.actionVC, addView: self.viewStatusAction)
        }
        
    }
    
    func setupViewModel() {
        if let ip = self.viewModel.input {
            bindingInput(input: ip)
        }
        
        if let op = self.viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input : VNP4TFilterHistorySurveyVM.Input){
        let dataType = self.viewModel.input?.itemSurveyType.value
        if dataType?.id != "" {
            self.btnValueSurveyType.setTitle(dataType?.name, for: .normal)
        }
    }
    
    private func bindingOutput(output : VNP4TFilterHistorySurveyVM.Output){
        output.statusResult.subscribe(onNext : { [weak self] result in
            guard let self = self else {return}
            if result.count > 0 {
                let height = (result.count * 60) + 60
//                self.viewStatusType.layer.frame.size.height = CGFloat(height)
                self.statusVC.viewModel.input?.data.accept(result)
                self.viewStatusConstraints.constant = CGFloat(height)
            }
        }).disposed(by: disposeBag)
        
        output.actionResult.subscribe(onNext : { [weak self] result in
            guard let self = self else {return}
            if result.count > 0 {
                let height = (result.count * 60) + 60
//                self.viewStatusAction.layer.frame.size.height = CGFloat(height)
                self.actionVC?.viewModel.input?.data.accept(result)
                self.viewActionsConstraints.constant = CGFloat(height)
            }
        }).disposed(by: disposeBag)
        
        output.surveyTypeResult.subscribe(onNext : { [weak self] result in
            guard let self = self else {return}
            if result.id != "" {
                self.btnValueSurveyType.setTitle(result.name, for: .normal)
            } else {
                self.btnValueSurveyType.setTitle("4t_register_survey_type_placeholder".localized, for: .normal)
            }
        }).disposed(by: disposeBag)
    }
}

extension VNP4TFilterHistorySurveyVC {
    private func add(asChildViewController viewController: UIViewController, addView : UIView) {
        // Add Child View Controller
        addChild(viewController)
        
        // Add Child View as Subview
        addView.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = addView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Remove Child View From Parent
        viewController.removeFromParent()
    }
}
