//
//  VNP4TFilterStatusVC.swift
//  VNPost4T
//
//  Created by AEGONA on 9/30/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

typealias VNP4TFilterStatusSource = RxTableViewSectionedReloadDataSource<FilterSurveySection>

class VNP4TFilterStatusVC: VNP4TBaseViewController {
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var tbView: UITableView!
    
    var dataSource : VNP4TFilterStatusSource!
    
    var viewModel : VNP4TFilterStatusVM!
    
    var onPickItem : ((VNP4TItemSelect?)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        configDataSource()
        setupView()
        setupViewModel()
    }
    
    func resetData(){
        self.viewModel.input?.data.value.forEach({ (item) in
            item.status = false
        })
        self.tbView.reloadData()
    }
}

extension VNP4TFilterStatusVC : VNP4TViewControllerProtocol {
    func setupView() {
        
    }
    
    func setupViewModel() {
        if let ip = self.viewModel.input {
            bindingInput(input: ip)
        }
        
        if let op = self.viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input: VNP4TFilterStatusVM.Input){
        self.lbTitle.text = input.title.value
        
        let value = self.viewModel.input?.itemInit.value
        if value?.id != nil {
            self.viewModel.input?.data.value.forEach({ item in
                if item.id == value?.id {
                    item.status = true
                }
            })
            self.tbView.reloadData()
        }
    }
    
    private func bindingOutput(output : VNP4TFilterStatusVM.Output){
        output.dataSections.drive(self.tbView.rx.items(dataSource: self.dataSource)).disposed(by: disposeBag)
    }
}

extension VNP4TFilterStatusVC : ListViewControllerProtocol {
    func registerCell() {
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        tbView.register(UINib(nibName: VNP4TFilterStatusCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TFilterStatusCell.cellIdentifier)
    }
    
    func configDataSource() {
        let ds = RxTableViewSectionedReloadDataSource<FilterSurveySection>(
          configureCell: { dataSource, tableView, indexPath, data in
              let cell : VNP4TFilterStatusCell = tableView.dequeueReusableCell(withIdentifier: VNP4TFilterStatusCell.cellIdentifier, for: indexPath) as! VNP4TFilterStatusCell
                     
                     cell.data = data
                     cell.onPickItem = { item in
                         self.handleResetList(item: item ?? data)
                         self.onPickItem?(item)
                     }
                     return cell
        })
        
        dataSource = ds
    }
    
    private func handleResetList(item : VNP4TItemSelect){
        let data = self.viewModel.input?.data.value
        data?.forEach({ itemFilter in
            if itemFilter.id == item.id {
                itemFilter.status = true
            } else {
                itemFilter.status = false
            }
        })
        self.viewModel.input?.data.accept(data ?? [])
    }
}
