//
//  VNP4TFilterStatusVM.swift
//  VNPost4T
//
//  Created by AEGONA on 9/30/20.
//

import Foundation
import RxCocoa
import RxSwift
import RxDataSources

typealias FilterSurveySection = SectionModel<String,VNP4TItemSelect>

class VNP4TFilterStatusVM: VNP4TViewModelProtocol {
    typealias Dependency = VNP4TSurveyService
    
    struct Input {
        let data                        :BehaviorRelay<[VNP4TItemSelect]>
        let title                       :BehaviorRelay<String>
        let itemInit                    :BehaviorRelay<VNP4TItemSelect?>
    }
    
    struct Output {
        let dataSections                :Driver<[FilterSurveySection]>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input : Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: VNP4TSurveyService?) -> Output? {
        self.input = input
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        
        let dataSection = ip.data.asDriver().map { data -> [FilterSurveySection] in
            return [FilterSurveySection(model: "Filter", items: data)]
        }
        
        return VNP4TFilterStatusVM.Output(dataSections: dataSection)
    }
}
