//
//  VNP4TCreateElectricSurveyVC.swift
//  
//
//  Created by AEGONA on 9/15/20.
//

import Foundation
import UIKit
import RxCocoa
import StepIndicator
import ObjectMapper

class VNP4TCreateElectricSurveyVC: VNP4TBaseViewController {
    
    @IBOutlet weak var navigationBarBackground: UIView!
    @IBOutlet weak var stepView: StepIndicatorView!
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var lbTitleToolbar: UILabel!
    @IBOutlet weak var btnSubmitDraft: UIButton!
    
    @IBOutlet weak var viewPrev: UIView!
    @IBOutlet weak var viewNext: UIView!
    
    @IBOutlet weak var textNextView: UILabel!
    @IBOutlet weak var nextImage: UIImageView!
    
    var viewModel : VNP4TCreateElectricSurveyVM!
    
    private var dataVC : [VNP4TSurveyQuestionsVC] = []
    
    private var dataQuestions = [Int : [VNP4TSurveyQuestion]]()
    
    private var loading: VNP4TLoadingProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loading = VNP4TLoadingProgressView(uiScreen: UIScreen.main)
        self.view.addSubview(loading)
        setupView()
        setupViewModel()
        onNext()
        onPrev()
    }
    
    @IBAction func onBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSaveDraft(_ sender: Any) {
        
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        let dialogOptions = VNP4TDialogOptions(nibName: "VNP4TDialogOptions", bundle: bundle)
        dialogOptions.modalPresentationStyle = .overFullScreen
        dialogOptions.titleDialog = BehaviorRelay<String>(value: "4t_create_electrict_survey_title_submit".localized)
        dialogOptions.messageDialog = BehaviorRelay<String>(value: "")
        dialogOptions.titleActionConfirm = BehaviorRelay<String?>(value: "4t_save".localized)
        
        dialogOptions.onCancelAction = {
            dialogOptions.dismiss(animated: false, completion: nil)
        }
        
        dialogOptions.onDoneActions = { [weak self] in
            guard let self = self else { return }
            
            dialogOptions.dismiss(animated: false, completion: nil)
            self.viewModel.uploadImage()
        }
        self.parent?.present(dialogOptions, animated: false, completion: nil)
    }
    
    private func onNext(){
        let tap = UITapGestureRecognizer()
        viewNext.addGestureRecognizer(tap)
        tap.rx.event.bind { (eventTap) in
            let currentPosition = self.viewModel.input?.currentPosition.value
            var pass = true
            if currentPosition != nil {
                let surveys = self.viewModel.input?.surveyQuestions.value
                if surveys != nil {
                    surveys!.forEach { survey in
                        if survey.step == currentPosition {
                            if survey.required {
                                if survey.isPass == false {
                                    pass = false
                                } else {
                                    if survey.answerValue == "" {
                                        if survey.step != 0 && survey.dataType == VNP4TQuestionType.PHOTO.rawValue {
                                            if survey.listImage.count > 0 {
                                                survey.isPass = true
                                            } else {
                                                survey.isPass = false
                                            }
                                        } else {
                                            pass = false
                                            survey.isPass = false
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if pass {
                        if currentPosition == (self.viewModel.input?.totalStep.value ?? 0) - 1 {
                            var dataSurvey : [VNP4TSurveyQuestion] = []
                            //                            dataSurvey += surveys ?? []
                            var listImage : [VNP4TListImage] = []
                            surveys?.forEach({ (dataItem) in
                                if dataItem.dataType == VNP4TQuestionType.PHOTO.rawValue {
                                    listImage.append(VNP4TListImage(id: dataItem.id, images: dataItem.listImage))
                                }
                                let item = Mapper().toJSON(dataItem)
                                dataSurvey.append(VNP4TSurveyQuestion(JSON: item) ?? dataItem)
                            })
                            let bundle = Bundle.init(identifier: VNP4TConstants.bundleIdentifier)
                            let summaryVC = VNP4TSummaryElectrictSurveyVC(nibName: "VNP4TSummaryElectrictSurveyVC", bundle: bundle)
                            dataSurvey.forEach { (item) in
                                if item.dataType == VNP4TQuestionType.PHOTO.rawValue {
                                    listImage.forEach { rowItem in
                                        if (rowItem.id == item.id) {
                                            item.listImage = rowItem.images ?? []
                                        }
                                    }
                                }
                            }
                            summaryVC.viewModel = VNP4TSummaryElectrictSurveyVM(
                                input: VNP4TSummaryElectrictSurveyVM.Input(
                                    isLoading: BehaviorRelay<Bool>(value: false),
                                    keys: BehaviorRelay<VNP4TSurveyClassificationTypeSelected>(value: self.viewModel.input?.surveyType.value ?? VNP4TSurveyClassificationTypeSelected()),
                                    questions: BehaviorRelay<[VNP4TSurveyQuestion]>(value: dataSurvey),
                                    dataQuestionsShow: BehaviorRelay<[VNP4TSummarySurvey]>(value: []),
                                    collectionType: BehaviorRelay<Int>(value: self.viewModel.input?.collectionType.value ?? 0),
                                    submitTrigger: BehaviorRelay<Bool>(value: false),
                                    dataSubmit: BehaviorRelay<VNP4TQuestionKey>(value: VNP4TQuestionKey())),
                                dependency: VNP4TSurveyService())
                            
                            self.navigationController?.pushViewController(summaryVC, animated: true)
                        } else {
                            self.viewModel.input?.currentPosition.accept(currentPosition! + 1)
                        }
                    } else {
                        self.dataVC[currentPosition!].tbView.reloadData()
                        self.showErrorDialog()
                    }
                }
            }
        }.disposed(by: disposeBag)
        
    }
    
    private func onPrev(){
        let tap = UITapGestureRecognizer()
        viewPrev.addGestureRecognizer(tap)
        tap.rx.event.bind { (eventTap) in
            if self.viewModel.input?.currentPosition.value ?? 0 > 0 {
                self.viewModel.input?.currentPosition.accept((self.viewModel.input?.currentPosition.value ?? 0 ) - 1)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }.disposed(by: disposeBag)
        
    }
}

extension VNP4TCreateElectricSurveyVC : VNP4TViewControllerProtocol{
    func setupView() {
        navigationBarBackground.VNP4TsetGradientBackground(colorStart: VNP4TConstants.navigationBarMainColor, colorEnd: VNP4TConstants.navigationBarSubColor)
        
        self.lbTitleToolbar.text = "4t_create_electrict_survey_title".localized
        self.btnSubmitDraft.setTitle("4t_create_electrict_survey_title_draft".localized, for: .normal)
        
        self.viewNext.VNP4TSetViewBackgroundGradientIsBlue()
        
    }
    
    func setupViewModel() {
        if let ip = viewModel.input {
            bindingInput(input: ip)
        }
        if let op = viewModel.output {
            bindingOutput(output: op)
        }
        
    }
    
    private func bindingInput(input : VNP4TCreateElectricSurveyVM.Input){
        
        if input.dataSearch.value.count > 0 {
            let questionData = input.dataSearch.value
            questionData.forEach({ question in
                if question.step == 0 {
                    if question.type == VNP4TQuestionCustomerType.DOB.rawValue ||  question.type == VNP4TQuestionCustomerType.ISSUE_ID_NUMBER_DATE.rawValue {
                        question.answerValue = question.answerValue.vnp4TFormatDateInput() ?? question.answerValue
                    }
                } else {
                    if question.dataType == VNP4TQuestionType.DATE.rawValue {
                        question.answerValue = question.answerValue.vnp4TFormatDateInput() ?? question.answerValue
                    }
                    if question.dataType == VNP4TQuestionType.PHOTO.rawValue {
                        question.convertURL()
                    }
                }
                self.viewModel.input?.survey.value.keys.forEach({ (key) in
                    if question.id == key.id {
                        question.answerValue = key.answerValue
                        question.key = true
                    }
                })
            })
            
            self.viewModel.input?.surveyQuestions.accept(questionData)
            
            var hashStep = [Int : Int]()
            var hashData = [Int : [VNP4TSurveyQuestion]]()
            questionData.forEach({ (question) in
                hashStep[question.step] = 1
                var listData = hashData[question.step] ?? []
                listData += [question]
                hashData[question.step] = listData
            })
            self.dataQuestions = hashData
            self.viewModel.input?.totalStep.accept(hashStep.count)
            self.renderUIView()
        } else {
            self.viewModel.input?.questionTrigger.accept(true)
        }
        
        input.isLoading.subscribe { [weak self] isLoading in
            guard let self = self else {return}
            
            if isLoading.element ?? false {
                self.loading.startAnimating()
            } else {
                self.loading.stopAnimating()
            }
        }
        
    }
    
    private func bindingOutput(output : VNP4TCreateElectricSurveyVM.Output){
        output.currentPositionResult.subscribe(onNext: { position in
            self.stepView.currentStep = position
            if position == (self.viewModel.input?.totalStep.value ?? 0) - 1 {
                self.nextImage.isHidden = true
                self.textNextView.text =  "4t_create_electrict_survey_done".localized
            } else {
                self.nextImage.isHidden = false
                self.textNextView.text =  "4t_create_electrict_survey_next".localized
            }
            self.renderContainView(position: position)
        }).disposed(by: disposeBag)
        
        output.surveyQuestionsResult.subscribe(onNext: { [weak self] result in
            guard let self = self else { return }
            
            if result.success == true {
                let questionList = result.data
                if questionList?.data?.count ?? 0 > 0 {
                    questionList?.data?.forEach({ question in
                        self.viewModel.input?.survey.value.keys.forEach({ (key) in
                            if question.id == key.id {
                                question.answerValue = key.answerValue
                                question.key = true
                            }
                        })
                    })
                }
                
                self.viewModel.input?.surveyQuestions.accept(questionList?.data ?? [])
                
                var hashStep = [Int : Int]()
                var hashData = [Int : [VNP4TSurveyQuestion]]()
                questionList?.data?.forEach({ (question) in
                    hashStep[question.step] = 1
                    var listData = hashData[question.step] ?? []
                    listData += [question]
                    hashData[question.step] = listData
                })
                self.dataQuestions = hashData
                self.viewModel.input?.totalStep.accept(hashStep.count)
                self.renderUIView()
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_error".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: false, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
        
        output.submitResult.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            
            if result.statusCode == 200 {
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let successDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                successDialog.titleDialog = BehaviorRelay<String>(value: "4t_create_electrict_survey_draft_success".localized.uppercased())
                successDialog.messageDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_message_success_1".localized + " " + (self.viewModel.input?.survey.value.code ?? "") + " " + "4t_summary_electrict_survey_message_success_save_draft_2".localized)
                successDialog.status = BehaviorRelay<Bool>(value: true)
                successDialog.modalPresentationStyle = .overFullScreen
                successDialog.onDoneActions = {
                    successDialog.dismiss(animated: false, completion: nil)
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                    self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                }
                
                self.parent?.present(successDialog, animated: false, completion: nil)
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_error".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: false, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
        
    }
    
    private func renderUIView(){
        self.viewModel.input?.totalStep.subscribe(onNext: { (total) in
            self.stepView.numberOfSteps = total
            
            var stepItem = -1
            for i in 0..<total {
                let itemQuestionStep = BehaviorRelay<[VNP4TSurveyQuestion]>(value: [])
                stepItem = self.getStepItem(index: stepItem) ?? stepItem
                itemQuestionStep.accept(self.dataQuestions[i] ?? [])
                let vc = VNP4TSurveyQuestionsVC(nibName: "VNP4TSurveyQuestionsVC", bundle: Bundle(identifier: VNP4TConstants.bundleIdentifier))
                vc.viewModel = VNP4TSurveyQuestionVM(
                    input: VNP4TSurveyQuestionVM.Input(
                        dataQuestions: BehaviorRelay<[VNP4TSurveyQuestion]>(value: itemQuestionStep.value),
                        isScroll: BehaviorRelay<Bool>(value: true)),
                    dependency: VNP4TSurveyService())
                vc.onChangeValue = { item in
                    let surveyQuestion = self.viewModel.input?.surveyQuestions.value
                    if surveyQuestion != nil {
                        surveyQuestion!.forEach({ (data) in
                            if data.id == item.id {
                                data.answerValue = item.answerValue
                                data.key = item.key
                            }
                        })
                    }
                }
                self.dataVC.append(vc)
            }
            self.viewModel.input?.currentPosition.accept(0)
        }).disposed(by: disposeBag)
    }
    
    private func getStepItem(index: Int) -> Int? {
        var indexItem = index
        indexItem += 1
        if self.dataQuestions[indexItem]?.count ?? 0 > 0 {
            return indexItem
        } else {
            self.getStepItem(index: indexItem)
        }
        return nil
    }
    
    private func renderContainView(position : Int){
        let total = self.viewModel.input?.totalStep.value
        if total != nil {
            if self.dataVC.count > 0 {
                for i in 0..<total! {
                    if i == position {
                        self.add(asChildViewController: self.dataVC[i])
                    } else {
                        self.remove(asChildViewController: self.dataVC[i])
                    }
                }
            }
        }
    }
    
    private func showErrorDialog(){
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        let errorDialog = VNP4TDialogError(nibName: "VNP4TDialogError", bundle: bundle)
        errorDialog.messageDialog = BehaviorRelay<String>(value: "4t_create_electrict_survey_error_empty".localized)
        errorDialog.modalPresentationStyle = .overFullScreen
        errorDialog.onDoneActions = {
            errorDialog.dismiss(animated: false, completion: nil)
        }
        
        self.parent?.present(errorDialog, animated: false, completion: nil)
    }
}

extension VNP4TCreateElectricSurveyVC {
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChild(viewController)
        
        // Add Child View as Subview
        self.containView.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = self.containView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Remove Child View From Parent
        viewController.removeFromParent()
    }
}
