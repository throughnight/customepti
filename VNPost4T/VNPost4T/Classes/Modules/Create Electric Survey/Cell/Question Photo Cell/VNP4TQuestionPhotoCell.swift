//
//  VNP4TQuestionPhotoCell.swift
//  
//
//  Created by AEGONA on 9/16/20.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

class VNP4TQuestionPhotoCell: UITableViewCell{
    
    static let cellIdentifier = "VNP4TQuestionPhotoCell"
    
    private let imagePickerController = UIImagePickerController()
    
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var btnAddImage: UIButton!
    @IBOutlet weak var lbRequire: UILabel!
    @IBOutlet weak var imgEmpty: UIImageView!
    
    weak var viewController: UIViewController?
    
    var onChangeData : ((VNP4TSurveyQuestion?)->())?
    
    let images = BehaviorRelay<[VNP4TImage]>(value: [])
    let vnp4tBundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
    
    private var isAdd = true
    
    var data : VNP4TSurveyQuestion? {
        didSet{
            lbTitle.text = data?.question
            if data?.required ?? false {
                lbRequire.isHidden = false
            } else {
                lbRequire.isHidden = true
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupCollectionView()
        images.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            let currentData = self.data
            currentData?.listImage = result
            if result.count > 0 {
                self.imgEmpty.isHidden = true
                currentData?.isPass = true
            } else {
                self.imgEmpty.isHidden = false
                currentData?.isPass = false
            }
            if self.data?.key ?? false == false {
                if self.data?.length ?? 0 <= result.count {
                    self.btnAddImage.isHidden = true
                } else {
                    self.btnAddImage.isHidden = false
                }
            }
            self.lbTitle.text = "\(self.data?.question ?? "") (\(result.count )/\(self.data?.length ?? 4))"
            self.onChangeData?(currentData)
        })
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func onPick(_ sender: UIButton) {
        onOptionsCamera()
    }
    
    private func setupCollectionView(){
        
        self.imagesCollectionView.delegate = self
        self.imagesCollectionView.dataSource = self
        self.imagesCollectionView.register(UINib(nibName: VNP4TPhysicalImageCollectionViewCell.identify, bundle: vnp4tBundle), forCellWithReuseIdentifier: VNP4TPhysicalImageCollectionViewCell.identify)
        
        self.imagePickerController.delegate = self
        self.imagePickerController.allowsEditing = false
        self.imagePickerController.mediaTypes = ["public.image"]
        
    }
    
    private func onOptionsCamera(){
        isAdd = true
        let imageOption = VNP4TDialogImageOption(nibName: VNP4TDialogImageOption.identifier, bundle: vnp4tBundle)
        
        imageOption.modalPresentationStyle = .overFullScreen
        
        imageOption.onTakePicture = {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.takePhoto()
            }
        }
        
        imageOption.onLibrary = {
            self.photoLibrary()
        }
        
        self.viewController?.present(imageOption, animated: false, completion: nil)
        
    }
    
    private func takePhoto(){
        self.imagePickerController.sourceType = .camera
        self.viewController?.present(self.imagePickerController, animated: true, completion: nil)
    }
    
    private func photoLibrary(){
        self.imagePickerController.sourceType = .photoLibrary
        self.viewController?.present(self.imagePickerController, animated: true, completion: nil)
    }
}

extension VNP4TQuestionPhotoCell: UIImagePickerControllerDelegate{
    
    private func pickerImageController(_ controller: UIImagePickerController, image: UIImage?) {
        controller.dismiss(animated: true, completion: nil)
        if(isAdd == true && image != nil){
            isAdd = false
            var images = self.images.value
            let item = VNP4TImage()
            item.image = image
            images += [item]
            self.images.accept(images)
            self.imagesCollectionView.reloadData()
        }
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        pickerImageController(picker, image: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        guard let _image = info[.originalImage] as? UIImage else {
            pickerImageController(picker,image: nil)
            return
        }
        pickerImageController(picker, image: _image)
    }
}

extension VNP4TQuestionPhotoCell: UINavigationControllerDelegate{
    
}

extension VNP4TQuestionPhotoCell: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  imagesCollectionView.dequeueReusableCell(withReuseIdentifier: VNP4TPhysicalImageCollectionViewCell.identify, for: indexPath) as! VNP4TPhysicalImageCollectionViewCell
        
        let imageData = images.value[indexPath.row]
        if imageData.image != nil {
            cell.imageView.image = imageData.image!
        } else {
            cell.imageView.load(url: URL(string: imageData.url ?? "")!)
        }
        
        if data?.key == true {
            cell.isHide()
        }
        
        cell.onDel = {
            var imageArray = self.images.value
            if(imageArray.count == 0){
                return
            }
            //
            self.imagesCollectionView.reloadItems(at: [indexPath])
            DispatchQueue.main.async {
                imageArray.remove(at: indexPath.row)
                self.images.accept(imageArray)
                self.imagesCollectionView.reloadData()
            }
            //            self.imagesCollectionView.reloadItems(at: [indexPath])
            //            self.imagesCollectionView.reloadData()
        }
        
        return cell
    }
    
}

extension VNP4TQuestionPhotoCell: UIScrollViewDelegate{
    
}

extension VNP4TQuestionPhotoCell: UICollectionViewDelegate{
    
}
