//
//  VNP4TQuestionAddressCell.swift
//  
//
//  Created by AEGONA on 9/16/20.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift
import ObjectMapper

class VNP4TQuestionAddressCell: UITableViewCell{
    static let cellIdentifier = "VNP4TQuestionAddressCell"
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbRequire: UILabel!
    @IBOutlet weak var lbError: UILabel!
    
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    let disposeBag : DisposeBag! = DisposeBag()
    
    var onPickAddress : (()->())?
    
    var data : VNP4TSurveyQuestion? {
        didSet{
            lbTitle.text = data?.question
            if data?.required ?? false {
                lbRequire.isHidden = false
            } else {
                lbRequire.isHidden = true
            }
            valueLabel.text = data?.answerValue
            if data?.answerValue != "" {
                valueLabel.text = VNP4TAddress(JSONString: data?.answerValue ?? "")?.getFullAddress()
            } else {
                if data?.defaultValue != "" {
                    valueLabel.text = data?.defaultValue
                }
            }
            if data?.key ?? false {
                self.containerView.backgroundColor = UIColor(hex: "E9E9E9")
            }
            self.updateUI(boolean: data?.isPass ?? false)
        }
    }
    
  
    
    private func updateUI(boolean: Bool){
        if boolean {
            self.lbError.isHidden = true
            self.containerView.layer.borderWidth = 1
            self.containerView.layer.borderColor = UIColor(hex: "DBDBDB").cgColor
        } else{
            self.lbError.isHidden = false
            self.lbError.text = "4t_message_error_empty".localized
            self.containerView.layer.borderWidth = 1
            self.containerView.layer.borderColor = UIColor.red.cgColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.onPick()
        self.containerView.layer.borderWidth = 1
        self.containerView.layer.borderColor = UIColor(hex: "DBDBDB").cgColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func onPick(){
        let tap = UITapGestureRecognizer()
        containerView.addGestureRecognizer(tap)
        tap.rx.event.bind { (eventTap) in
            if self.data?.key ?? false == false {
                self.onPickAddress?()
            }
        }.disposed(by: disposeBag)
        
    }
    
}
