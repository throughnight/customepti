//
//  VNP4TQuestionListCell.swift
//  
//
//  Created by AEGONA on 9/16/20.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

class VNP4TQuestionListCell: UITableViewCell{
    static let cellIdentifier = "VNP4TQuestionListCell"
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbError: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    @IBOutlet weak var containerValueView: UIView!
    
    let disposeBag : DisposeBag! = DisposeBag()
    
    var onPickItems : (()->())?
    var onChangeValue : ((VNP4TSurveyQuestion?)->())?
    
    var data : VNP4TSurveyQuestion? {
        didSet{
            lbTitle.text = data?.question
            valueLabel.text = data?.answerValue
            if data?.required ?? false {
                setupRequiredLabel(with: data?.question ?? "", label: self.lbTitle)
            }
            self.updateUI(boolean: data?.isPass ?? true)
            if data?.answerValue != "" {
                valueLabel.text = data?.answerValue
            } else {
                if data?.defaultValue != "" {
                    valueLabel.text = data?.defaultValue
                    let currentData = data
                    currentData?.isPass = true
                    currentData?.answerValue = data?.defaultValue ?? ""
                    self.onChangeValue?(currentData)
                }
            }
            if data?.key ?? false {
                self.containerValueView.layer.borderWidth = 1
                self.containerValueView.layer.backgroundColor = UIColor(hex: "E9E9E9").cgColor
                self.containerValueView.layer.borderColor = UIColor(hex: "DBDBDB").cgColor
            }
        }
    }
    
    private func updateUI(boolean: Bool){
        if boolean {
            self.lbError.isHidden = true
            self.containerValueView.layer.borderWidth = 1
            self.containerValueView.layer.borderColor = UIColor(hex: "DBDBDB").cgColor
        } else{
            self.lbError.isHidden = false
            self.lbError.text = "4t_message_error_empty".localized
            self.containerValueView.layer.borderWidth = 1
            self.containerValueView.layer.borderColor = UIColor.red.cgColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lbError.isHidden = false
        self.lbError.text = "4t_message_error_empty".localized
        self.onPick()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func onPick(){
        let tap = UITapGestureRecognizer()
        containerValueView.addGestureRecognizer(tap)
        tap.rx.event.bind { (eventTap) in
            if self.data?.key ?? false == false {
            self.onPickItems?()
            self.updateUI(boolean: true)
            }
        }.disposed(by: disposeBag)
        
    }

}
