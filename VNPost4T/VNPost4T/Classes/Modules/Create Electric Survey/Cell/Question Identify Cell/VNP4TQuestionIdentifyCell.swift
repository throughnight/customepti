//
//  VNP4TQuestionIdentifyCell.swift
//  
//
//  Created by AEGONA on 9/16/20.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

class VNP4TQuestionIdentifyCell: UITableViewCell{
    static let cellIdentifier = "VNP4TQuestionIdentifyCell"
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var tfValue: UITextField!
    @IBOutlet weak var lbRequire: UILabel!
    @IBOutlet weak var lbError: UILabel!
    
    var onChangeValue : ((VNP4TSurveyQuestion?)->())?
    
    var data : VNP4TSurveyQuestion? {
        didSet{
            let currentData = data
            lbTitle.text = data?.question
            tfValue.rx.controlEvent(.editingChanged)
                .subscribe(onNext: { text in
                    if self.data?.required ?? false {
                        self.updateUI(boolean: self.tfValue.text?.count ?? 0 == 9 || self.tfValue.text?.count ?? 0 == 12)
                        if self.tfValue.text?.count ?? 0 == 9 || self.tfValue.text?.count ?? 0 == 12 {
                            currentData?.isPass = true
                        } else {
                            currentData?.isPass = false
                        }
                    }
                    currentData?.answerValue = self.tfValue.text ?? ""
                    self.onChangeValue?(currentData)
                })
            if data?.required ?? false {
                lbRequire.isHidden = false
            } else {
                lbRequire.isHidden = true
            }
            self.updateUI(boolean: data?.isPass ?? true)
            if data?.key ?? false {
                tfValue.isEnabled = false
                tfValue.backgroundColor = UIColor(hex: "E9E9E9")
            }
            if data?.answerValue != "" {
                tfValue.text = data?.answerValue
            } else {
                if data?.defaultValue != "" {
                    tfValue.text = data?.defaultValue
                } else {
                    if data?.placeholder != "" {
                        tfValue.placeholder = data?.placeholder
                    }
                }
            }
        }
    }
    
    private func updateUI(boolean: Bool){
        if boolean {
            self.lbError.isHidden = true
            self.tfValue.layer.borderWidth = 1
            self.tfValue.layer.borderColor = UIColor(hex: "DBDBDB").cgColor
        } else{
            self.lbError.isHidden = false
            self.lbError.text = "4t_identify_message_error".localized
            self.tfValue.layer.borderWidth = 1
            self.tfValue.layer.borderColor = UIColor.red.cgColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
