//
//  VNP4TQuestionGenderCell.swift
//  
//
//  Created by AEGONA on 9/16/20.
//

import Foundation
import UIKit
import RxCocoa

class VNP4TQuestionGenderCell: UITableViewCell{
    static let cellIdentifier = "VNP4TQuestionGenderCell"
    
    @IBOutlet weak var lbTitle: UILabel!
    
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var lbRequire: UILabel!
    
    var onChangeValue : ((String?)->())?
    
    var data : VNP4TSurveyQuestion? {
        didSet{
            lbTitle.text = data?.question
            if data?.required ?? false {
                lbRequire.isHidden = false
            } else {
                lbRequire.isHidden = true
            }
            if data?.answerValue != "" {
                handleChangeOptions(type: data?.answerValue ?? "" == "true" ? 1 : 0)
            } else {
                if data?.defaultValue != "" {
                    handleChangeOptions(type: data?.defaultValue ?? "" == "true" ? 1 : 0)
                } else {
                    handleChangeOptions(type: 1)
                    self.onChangeValue?("true")
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func handleChangeOptions(type: Int){
        //0 is female 1 is male
        if type == 0 {
            btnMale.backgroundColor = .white
            btnMale.setTitleColor(UIColor.gray, for: .normal)
            btnFemale.backgroundColor = UIColor(hex: "3692F4")
            btnFemale.setTitleColor(UIColor.white, for: .normal)
        } else {
            btnMale.backgroundColor = UIColor(hex: "3692F4")
            btnMale.setTitleColor(UIColor.white, for: .normal)
            btnFemale.backgroundColor = .white
            btnFemale.setTitleColor(UIColor.gray, for: .normal)
        }
    }
    
    @IBAction func onSelectFemade(_ sender: Any) {
        if data?.key ?? false == false {
            handleChangeOptions(type: 0)
            self.onChangeValue?("false")
        }
    }

    @IBAction func onSelectMade(_ sender: Any) {
        if data?.key ?? false == false {
            handleChangeOptions(type: 1)
            self.onChangeValue?("true")
        }
    }
}
