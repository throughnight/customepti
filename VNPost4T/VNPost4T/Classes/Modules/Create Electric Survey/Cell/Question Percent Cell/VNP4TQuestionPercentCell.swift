//
//  VNP4TQuestionPercentCell.swift
//  
//
//  Created by AEGONA on 9/16/20.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

class VNP4TQuestionPercentCell: UITableViewCell{
    static let cellIdentifier = "VNP4TQuestionPercentCell"
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var tfValue: UITextField!
    @IBOutlet weak var lbError: UILabel!
    
    var onChangeValue : ((VNP4TSurveyQuestion?)->())?
    
    var data : VNP4TSurveyQuestion? {
        didSet{
            let currentData = data
            
            tfValue.rx.controlEvent(.editingChanged)
            .subscribe(onNext: { _ in
                if Int(self.tfValue.text ?? "0") ?? 0 > 100 {
                    self.tfValue.text = "100"
                }
                if self.data?.required ?? false {
                    self.updateUI(boolean: self.tfValue.text?.count ?? 0 > 0)
                    if self.tfValue.text?.count ?? 0 > 0 {
                        currentData?.isPass = true
                    } else {
                        currentData?.isPass = false
                    }
                }
                currentData?.answerValue = self.tfValue.text ?? ""
                self.onChangeValue?(currentData)
            })
          
            self.updateUI(boolean: data?.isPass ?? true)
         
            if data?.key ?? false {
                tfValue.isEnabled = false
                tfValue.backgroundColor = UIColor(hex: "E9E9E9")
            }
            
            if data?.answerValue != "" {
                tfValue.text = data?.answerValue
            } else {
                if data?.defaultValue != "" {
                    tfValue.text = data?.defaultValue
                } else {
                    if data?.placeholder != "" {
                        tfValue.placeholder = data?.placeholder
                    }
                }
            }
            
            self.lbTitle.renderTitleUpdateLimitInput(data: currentData!, tfValue: tfValue)

        }
    }
    
    private func updateUI(boolean: Bool){
        if boolean {
            self.lbError.isHidden = true
            self.tfValue.layer.borderWidth = 1
            self.tfValue.layer.borderColor = UIColor(hex: "DBDBDB").cgColor
        } else{
            self.lbError.isHidden = false
            self.lbError.text = "4t_message_error_empty".localized
            self.tfValue.layer.borderWidth = 1
            self.tfValue.layer.borderColor = UIColor.red.cgColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tfValue.setRightIcon(UIImage(named: "ic_percent_black",
                                         in: Bundle(url: Bundle.main.bundleURL.appendingPathComponent("Frameworks").appendingPathComponent("VNPost4T.framework")) ?? Bundle.main,
                                         compatibleWith: nil)!)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
