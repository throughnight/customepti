//
//  VNP4TQuestionDateCell.swift
//  
//
//  Created by AEGONA on 9/16/20.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

class VNP4TQuestionDateCell: UITableViewCell, VNP4TDateInputDelegate{
    func dateDidChange(dateTextField: VNP4TDateInput) {
        let currentData = data
        if self.data?.required ?? false {
            if self.tfValue.text?.count ?? 0 == 10 {
                currentData?.isPass = true
            } else {
                currentData?.isPass = false
            }
            self.updateUI(boolean: self.tfValue.text?.count ?? 0 == 10)
        }
        currentData?.answerValue = self.tfValue.text ?? ""
        self.onChangeValue?(currentData)
    }
    
    static let cellIdentifier = "VNP4TQuestionDateCell"
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbRequire: UILabel!
    @IBOutlet weak var lbError: UILabel!
    @IBOutlet weak var tfValue: VNP4TDateInput!
    
    var onChangeValue : ((VNP4TSurveyQuestion?)->())?
    
    var data : VNP4TSurveyQuestion? {
        didSet{
            tfValue.dateFormat = .dayMonthYear
            tfValue.separator = "/"
            tfValue.backgroundColor = .white
            lbTitle.text = data?.question
            if data?.required ?? false {
                lbRequire.isHidden = false
            } else {
                lbRequire.isHidden = true
            }
            self.updateUI(boolean: data?.isPass ?? true)
            tfValue.customDelegate = self
            if data?.answerValue != "" {
                tfValue.text = data?.answerValue.vnp4TFormatDateInput()
            } else {
                if data?.defaultValue != "" {
                    tfValue.text = data?.defaultValue.vnp4TFormatDateInput() ?? data?.defaultValue
                    let oldData = data
                    oldData?.answerValue = data?.defaultValue ?? ""
                    oldData?.isPass = true
                    self.onChangeValue?(oldData)
                }
            }
            if data?.key ?? false {
                tfValue.isEnabled = false
                tfValue.backgroundColor = UIColor(hex: "E9E9E9")
            }
        }
    }
    
    @IBAction func onPickDate(_ sender: Any) {
        if data?.key ?? false == false {
            let minDate = Calendar.current.date(byAdding: .year, value: -80, to: Date())
            if data?.answerValue != "" {
                VNP4TPicker.selectDate(selectedDate: data?.answerValue.vnp4TToDate(format: "yyyy-MM-dd") ?? data?.answerValue.vnp4TToDate(format: "dd/MM/yyyy") ?? Date(), minDate: minDate) { (selectedDate) in
                    self.tfValue.text = selectedDate.vnp4TToString(format: "dd/MM/yyyy")
                    let currentData = self.data
                    currentData?.isPass = true
                    currentData?.answerValue = selectedDate.vnp4TToString(format: "dd/MM/yyyy")
                    self.updateUI(boolean: self.tfValue.text?.count ?? 0 == 10)
                    self.onChangeValue?(currentData)
                }
            } else {
                var year = 0
                if data?.step == 0 && data?.type == VNP4TQuestionCustomerType.DOB.rawValue {
                    year = -25
                }
                let day = Calendar.current.date(byAdding: .year, value: year, to: Date())
                VNP4TPicker.selectDate(selectedDate: day ?? Date(), minDate: minDate) { (selectedDate) in
                    self.tfValue.text = selectedDate.vnp4TToString(format: "dd/MM/yyyy")
                    let currentData = self.data
                    currentData?.isPass = true
                    currentData?.answerValue = selectedDate.vnp4TToString(format: "dd/MM/yyyy")
                    self.updateUI(boolean: self.tfValue.text?.count ?? 0 == 10)
                    self.onChangeValue?(currentData)
                }
            }
            
        }
    }
    
    private func updateUI(boolean: Bool){
        if boolean {
            self.lbError.isHidden = true
            self.tfValue.layer.borderWidth = 1
            self.tfValue.layer.borderColor = UIColor(hex: "DBDBDB").cgColor
        } else{
            self.lbError.isHidden = false
            self.lbError.text = "4t_message_error_empty".localized
            self.tfValue.layer.borderWidth = 1
            self.tfValue.layer.borderColor = UIColor.red.cgColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
