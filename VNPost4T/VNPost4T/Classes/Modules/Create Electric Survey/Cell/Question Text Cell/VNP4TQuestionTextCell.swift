//
//  VNP4TQuestionTextCell.swift
//  
//
//  Created by AEGONA on 9/16/20.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

class VNP4TQuestionTextCell: UITableViewCell{
    static let cellIdentifier = "VNP4TQuestionTextCell"
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var tfValue: UITextField!
    @IBOutlet weak var lbRequire: UILabel!
    @IBOutlet weak var lbError: UILabel!
    
    @IBOutlet weak var containerView: UIStackView!
    var onChangeValue : ((VNP4TSurveyQuestion?)->())?
    
    var data : VNP4TSurveyQuestion? {
        didSet{
            tfValue.rx.controlEvent(.editingChanged)
                .subscribe(onNext: { (text) in
                    let currentData = self.data
                    if self.data?.required ?? false {
                        self.updateUI(boolean: self.tfValue.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0)
                        if self.tfValue.text?.count ?? 0 > 0 {
                            currentData?.isPass = true
                        } else {
                            currentData?.isPass = false
                        }
                    }
                    if self.tfValue.text != self.data?.answerValue {
                        currentData?.answerValue = self.tfValue.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
                        self.onChangeValue?(currentData)
                    }
                })
            
            if data?.key ?? false {
                tfValue.isEnabled = false
                tfValue.backgroundColor = UIColor(hex: "E9E9E9")
            } else {
                tfValue.isEnabled = true
                tfValue.backgroundColor = UIColor(hex: "fff")
            }
            if data?.answerValue != "" {
                tfValue.text = data?.answerValue
            } else {
                if data?.defaultValue != "" {
                    tfValue.text = data?.defaultValue
                } else {
                    if data?.placeholder != "" {
                        tfValue.placeholder = data?.placeholder
                    }  else {
                        tfValue.text = " "
                    }
                }
            }
            
            self.updateUI(boolean: data?.isPass ?? false)
            
            self.lbTitle.renderTitleUpdateLimitInput(data: self.data!, tfValue: tfValue)
        }
    }
    
    private func updateUI(boolean: Bool){
        if boolean {
            self.lbError.isHidden = true
            self.tfValue.layer.borderWidth = 1
            self.tfValue.layer.borderColor = UIColor(hex: "DBDBDB").cgColor
        } else{
            self.lbError.isHidden = false
            self.lbError.text = "4t_message_error_empty".localized
            self.tfValue.layer.borderWidth = 1
            self.tfValue.layer.borderColor = UIColor.red.cgColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
