//
//  VNP4TQuestionBooleanCell.swift
//  
//
//  Created by AEGONA on 9/16/20.
//

import Foundation
import UIKit
import RxCocoa

class VNP4TQuestionBooleanCell: UITableViewCell{
    static let cellIdentifier = "VNP4TQuestionBooleanCell"
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbYes: UILabel!
    @IBOutlet weak var lbNo: UILabel!
    @IBOutlet weak var lbError: UILabel!
    
    @IBOutlet weak var cbQuestionYes: VNP4TCheckBox!
    @IBOutlet weak var cbQuestionNo: VNP4TCheckBox!
    
    var onChangeValue : ((VNP4TSurveyQuestion?)->())?
    
    var data : VNP4TSurveyQuestion? {
        didSet{
            lbTitle.text = data?.question
            if data?.required ?? false {
                setupRequiredLabel(with: data?.question ?? "", label: self.lbTitle)
            }
            handleUI()
            if data?.key ?? false {
                cbQuestionYes.isLock = true
                cbQuestionNo.isLock = true
            }
            
        }
    }
    
    private func handleUI(){
        cbQuestionYes.borderStyle = .rounded
        cbQuestionYes.borderStyle = .roundedSquare(radius: 12)
        cbQuestionYes.style = .circle
        cbQuestionYes.isChecked = false
        
        cbQuestionNo.borderStyle = .rounded
        cbQuestionNo.borderStyle = .roundedSquare(radius: 12)
        cbQuestionNo.style = .circle
        cbQuestionNo.isChecked = false

        let currentData = data
        
        if data?.answerValue != "" {
            data?.answerValue == "true" ? handleCheckBox(status: true) : handleCheckBox(status: false)
        } else {
            if data?.defaultValue != "" {
                data?.defaultValue == "true" ? handleCheckBox(status: true) : handleCheckBox(status: false)
                currentData?.isPass = true
                currentData?.answerValue = data?.defaultValue == "true" ? "true" : "false"
                self.onChangeValue?(currentData)
            }
        }
        
        cbQuestionYes.onChangeValue = { isCheck in
            if isCheck {
                self.cbQuestionNo.isChecked = false
                currentData?.isPass = true
                currentData?.answerValue = "true"
                self.onChangeValue?(currentData)
            } else {
                currentData?.isPass = true
                currentData?.answerValue = "false"
                self.onChangeValue?(currentData)
            }
        }
        
        cbQuestionNo.onChangeValue = { isCheck in
            if isCheck {
                self.cbQuestionYes.isChecked = false
                currentData?.isPass = true
                currentData?.answerValue = "false"
                self.onChangeValue?(currentData)
            } else {
                currentData?.isPass = true
                currentData?.answerValue = "true"
                self.onChangeValue?(currentData)
            }
        }
        
        handleError(status: data?.isPass ?? false)
        
    }
    
    private func handleCheckBox(status: Bool){
        if status {
            self.cbQuestionYes.isChecked = true
            self.cbQuestionNo.isChecked = false
        } else {
            self.cbQuestionYes.isChecked = false
            self.cbQuestionNo.isChecked = true
        }
    }
    
    private func handleError(status: Bool){
        if status {
            self.lbError.isHidden = true
        } else {
            self.lbError.isHidden = false
            self.lbError.text = "4t_create_electrict_survey_error_empty".localized
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
