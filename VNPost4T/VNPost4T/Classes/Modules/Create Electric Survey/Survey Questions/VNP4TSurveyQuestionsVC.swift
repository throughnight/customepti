//
//  VNP4TSurveyQuestionsVC.swift
//  
//
//  Created by AEGONA on 9/15/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

typealias VNP4TSurveyQuestionSource = RxTableViewSectionedReloadDataSource<QuestionSection>

class VNP4TSurveyQuestionsVC: VNP4TBaseViewController {
    
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var tbView: UITableView!
    
    var dataSource : VNP4TSurveyQuestionSource!
    
    var viewModel : VNP4TSurveyQuestionVM!
    
    var onChangeValue : ((VNP4TSurveyQuestion)->())?
    
    var imagePicker: ImagePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        configDataSource()
        setupView()
        setupViewModel()
        tbView.allowsSelection = false
    }
}

extension VNP4TSurveyQuestionsVC : VNP4TViewControllerProtocol{
    func setupView() {
        if self.viewModel.input?.isScroll.value == false {
            self.tbView.isScrollEnabled = false
        }
    }
    
    func setupViewModel() {
        if let ip = self.viewModel.input {
            bindingInput(input: ip)
        }
        
        if let op = self.viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input: VNP4TSurveyQuestionVM.Input){
        
    }
    
    private func bindingOutput(output : VNP4TSurveyQuestionVM.Output){
        output.questionSections.drive(self.tbView.rx.items(dataSource: self.dataSource)).disposed(by: disposeBag)
    }
}

extension VNP4TSurveyQuestionsVC : ListViewControllerProtocol {
    func registerCell() {
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        tbView.register(UINib(nibName: VNP4TQuestionTextCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TQuestionTextCell.cellIdentifier)
        tbView.register(UINib(nibName: VNP4TQuestionBooleanCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TQuestionBooleanCell.cellIdentifier)
        tbView.register(UINib(nibName: VNP4TQuestionFullnameCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TQuestionFullnameCell.cellIdentifier)
        tbView.register(UINib(nibName: VNP4TQuestionDateCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TQuestionDateCell.cellIdentifier)
        tbView.register(UINib(nibName: VNP4TQuestionGenderCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TQuestionGenderCell.cellIdentifier)
        tbView.register(UINib(nibName: VNP4TQuestionIdentifyCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TQuestionIdentifyCell.cellIdentifier)
        tbView.register(UINib(nibName: VNP4TQuestionAddressCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TQuestionAddressCell.cellIdentifier)
        tbView.register(UINib(nibName: VNP4TQuestionListCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TQuestionListCell.cellIdentifier)
        tbView.register(UINib(nibName: VNP4TQuestionPhoneCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TQuestionPhoneCell.cellIdentifier)
        tbView.register(UINib(nibName: VNP4TQuestionEmailCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TQuestionEmailCell.cellIdentifier)
        tbView.register(UINib(nibName: VNP4TQuestionNumberCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TQuestionNumberCell.cellIdentifier)
        tbView.register(UINib(nibName: VNP4TQuestionDoubleCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TQuestionDoubleCell.cellIdentifier)
        tbView.register(UINib(nibName: VNP4TQuestionPercentCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TQuestionPercentCell.cellIdentifier)
        tbView.register(UINib(nibName: VNP4TQuestionPhotoCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TQuestionPhotoCell.cellIdentifier)
    }
    
    func configDataSource() {
        let ds : VNP4TSurveyQuestionSource = VNP4TSurveyQuestionSource(configureCell: {(dataSource, tv, indexPath, question) in
            if question.step == 0 {
                switch question.type {
                case VNP4TQuestionCustomerType.FULLNAME.rawValue:
                    let cell : VNP4TQuestionFullnameCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionFullnameCell.cellIdentifier, for: indexPath) as! VNP4TQuestionFullnameCell
                    cell.data = question
                    cell.onChangeValue = { data in
                        self.onChangeValue?(data ?? question)
                    }
                    return cell
                case VNP4TQuestionCustomerType.DOB.rawValue, VNP4TQuestionCustomerType.ISSUE_ID_NUMBER_DATE.rawValue:
                    let cell : VNP4TQuestionDateCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionDateCell.cellIdentifier, for: indexPath) as! VNP4TQuestionDateCell
                    cell.data = question
                    cell.onChangeValue = { data in
                        self.onChangeValue?(data ?? question)
                    }
                    return cell
                case VNP4TQuestionCustomerType.GENDER.rawValue:
                    let cell : VNP4TQuestionGenderCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionGenderCell.cellIdentifier, for: indexPath) as! VNP4TQuestionGenderCell
                    
                    if question.answerValue == "" {
                        if question.defaultValue == "" {
                            question.answerValue = "true"
                            question.isPass = true
                        } else {
                            question.answerValue = question.defaultValue
                            question.isPass = true
                        }
                        self.onChangeValue?(question)
                    }
                    
                    cell.data = question
                    
                    cell.onChangeValue = { gender in
                        let currentQuestion = question
                        currentQuestion.answerValue = gender ?? "true"
                        currentQuestion.isPass = true
                        self.onChangeValue?(currentQuestion)
                    }
                    
                    return cell
                case VNP4TQuestionCustomerType.ID_NUMBER.rawValue:
                    let cell : VNP4TQuestionIdentifyCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionIdentifyCell.cellIdentifier, for: indexPath) as! VNP4TQuestionIdentifyCell
                    cell.data = question
                    cell.onChangeValue = { data in
                        self.onChangeValue?(data ?? question)
                    }
                    return cell
                case VNP4TQuestionCustomerType.PASSPORT.rawValue,VNP4TQuestionCustomerType.ORTHER.rawValue,VNP4TQuestionCustomerType.ISSUE_ID_NUMBER_PLACE.rawValue,VNP4TQuestionCustomerType.JOB_TITLE.rawValue:
                    let cell : VNP4TQuestionTextCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionTextCell.cellIdentifier, for: indexPath) as! VNP4TQuestionTextCell
                    cell.data = question
                    cell.onChangeValue = { data in
                        self.onChangeValue?(data ?? question)
                    }
                    return cell
                case VNP4TQuestionCustomerType.TEMPORARY_ADDRESS.rawValue,VNP4TQuestionCustomerType.PERMANENT_ADDRESS.rawValue:
                    let cell : VNP4TQuestionAddressCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionAddressCell.cellIdentifier, for: indexPath) as! VNP4TQuestionAddressCell
                    if question.answerValue != "" {
                        question.address = VNP4TAddress(JSONString: question.answerValue)
                    }
                    cell.data = question
                    cell.onPickAddress = {
                        let addressPick = VNP4TDialogAddress(nibName: "VNP4TDialogAddress", bundle: self.vnp4tBundle)
                        addressPick.viewModel = VNP4TDialogAddressVM(
                            input: VNP4TDialogAddressVM.Input(
                                initValue: BehaviorRelay<VNP4TAddress>(value: question.address ?? VNP4TAddress()),
                                triggerDistrict: BehaviorRelay<Bool>(value: question.address?.province?.id != "" ? true : false),
                                district: BehaviorRelay<VNP4TQuestionValidations>(value: question.address?.district ?? VNP4TQuestionValidations()),
                                triggerWard: BehaviorRelay<Bool>(value: question.address?.district?.id != "" ? true : false),
                                ward: BehaviorRelay<VNP4TQuestionValidations>(value: question.address?.ward ?? VNP4TQuestionValidations()),
                                province: BehaviorRelay<VNP4TQuestionValidations>(value: question.address?.province ?? VNP4TQuestionValidations()),
                                isLoading: BehaviorRelay<Bool>(value: false),
                                provinces: BehaviorRelay<[VNP4TQuestionValidations]>(value: []),
                                districts: BehaviorRelay<[VNP4TQuestionValidations]>(value: []),
                                wards: BehaviorRelay<[VNP4TQuestionValidations]>(value: []),
                                address: BehaviorRelay<String>(value: question.address?.address ?? ""),
                                removeData: BehaviorRelay<Bool>(value: false)),
                            dependency: VNP4TCommonService())
                        addressPick.onSelectAddress = { result in
                            let currentQuestion = question
                            currentQuestion.address = result
                            currentQuestion.answerValue = currentQuestion.convertAddressToJson()
                            currentQuestion.isPass = true
                            self.onChangeValue?(currentQuestion)
                            self.tbView.reloadItemsAtIndexPaths([indexPath], animationStyle: .fade)
                        }
                        addressPick.modalPresentationStyle = .overFullScreen
                        addressPick.onActionPick = { (data,index) in
                            var title = ""
                            switch index {
                            case 1:
                                title = "Chọn tỉnh/thành"
                                break
                            case 2:
                                title = "Chọn quận/huyện"
                                break
                            case 3:
                                title = "Chọn phường/xã"
                                break
                            default:
                                title = "Chọn tỉnh/thành"
                            }
                            addressPick.dismiss(animated: false, completion: nil)
                        
                            let optionsPick = VNP4TDialogSearchList(nibName: "VNP4TDialogSearchList", bundle: self.vnp4tBundle)
                            optionsPick.viewModel = VNP4TDialogSearchListVM(
                                input: VNP4TDialogSearchListVM.Input(
                                    dataDialog: BehaviorRelay<[VNP4TQuestionValidations]>(value: data),
                                    dataSearch: BehaviorRelay<[VNP4TQuestionValidations]>(value: data),
                                    isSearch: BehaviorRelay<Bool>(value: true),
                                    seachText: BehaviorRelay<String>(value: ""),
                                    titleDialog: BehaviorRelay<String>(value: title.uppercased()),
                                    initValue: BehaviorRelay<String>(value: "")),
                                dependency: VNP4TCommonService())
                            optionsPick.dataPickedAction = { item in
                                print("Tick \(item.name)")
                                optionsPick.dismiss(animated: false, completion: nil)
                                switch index {
                                case 1:
                                    question.address?.province = item
                                    addressPick.viewModel.input?.province.accept(item)
                                    addressPick.viewModel.input?.triggerDistrict.accept(true)
                                    break
                                case 2:
                                    question.address?.district = item
                                    addressPick.viewModel.input?.district.accept(item)
                                    addressPick.viewModel.input?.triggerDistrict.accept(false)
                                    addressPick.viewModel.input?.triggerWard.accept(true)
                                    break
                                case 3:
                                    question.address?.ward = item
                                    addressPick.viewModel.input?.ward.accept(item)
                                    addressPick.viewModel.input?.triggerDistrict.accept(false)
                                    addressPick.viewModel.input?.triggerWard.accept(false)
                                    break
                                default:
                                    question.address?.province = item
                                    addressPick.viewModel.input?.province.accept(item)
                                    addressPick.viewModel.input?.triggerDistrict.accept(true)
                                    addressPick.viewModel.input?.triggerWard.accept(false)
                                }
                                self.parent?.present(addressPick, animated: false, completion: nil)
                            }
                            optionsPick.actionClose = {
                                optionsPick.dismiss(animated: false) {
                                    self.parent?.present(addressPick, animated: false, completion: nil)
                                }
                            }
                            optionsPick.modalPresentationStyle = .overFullScreen
                            self.parent?.present(optionsPick, animated: false, completion: nil)
                        }
                        self.parent?.present(addressPick, animated: false, completion: nil)
                    }
                    return cell
                case VNP4TQuestionCustomerType.ACADEMIC_LEVEL.rawValue,VNP4TQuestionCustomerType.MARITAL_STATUS.rawValue:
                    let cell : VNP4TQuestionListCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionListCell.cellIdentifier, for: indexPath) as! VNP4TQuestionListCell
                    cell.data = question
                    cell.onChangeValue = { data in
                        self.onChangeValue?(data ?? question)
                        self.tbView.reloadItemsAtIndexPaths([indexPath], animationStyle: .fade)
                    }
                    cell.onPickItems = {
                        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                        let optionsPick = VNP4TDialogSearchList(nibName: "VNP4TDialogSearchList", bundle: bundle)
                        optionsPick.viewModel = VNP4TDialogSearchListVM(
                            input: VNP4TDialogSearchListVM.Input(
                                dataDialog: BehaviorRelay<[VNP4TQuestionValidations]>(value: question.validations),
                                dataSearch: BehaviorRelay<[VNP4TQuestionValidations]>(value: question.validations),
                                isSearch: BehaviorRelay<Bool>(value: false),
                                seachText: BehaviorRelay<String>(value: ""),
                                titleDialog: BehaviorRelay<String>(value: question.question.uppercased()),
                                initValue: BehaviorRelay<String>(value: question.answerValue)),
                            dependency: VNP4TCommonService())
                        optionsPick.dataPickedAction = { item in
                            let currentQuestion = question
                            currentQuestion.answerValue = item.name
                            self.onChangeValue?(currentQuestion)
                            optionsPick.dismiss(animated: false) {
                                let dataList = self.viewModel.input?.dataQuestions.value
                                if dataList != nil {
                                    dataList!.forEach { (itemQuestion) in
                                        if itemQuestion.id == currentQuestion.id {
                                            itemQuestion.answerValue = currentQuestion.answerValue
                                            itemQuestion.isPass = true
                                        }
                                    }
                                    //                                    self.tbView.reloadData()
                                    self.tbView.reloadItemsAtIndexPaths([indexPath], animationStyle: .fade)
                                }
                            }
                        }
                        optionsPick.modalPresentationStyle = .overFullScreen
                        self.parent?.present(optionsPick, animated: false, completion: nil)
                    }
                    return cell
                case VNP4TQuestionCustomerType.PHONE_NUMBER.rawValue:
                    let cell : VNP4TQuestionPhoneCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionPhoneCell.cellIdentifier, for: indexPath) as! VNP4TQuestionPhoneCell
                    cell.data = question
                    cell.onChangeValue = { data in
                        self.onChangeValue?(data ?? question)
                    }
                    return cell
                case VNP4TQuestionCustomerType.EMAIL.rawValue:
                    let cell : VNP4TQuestionEmailCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionEmailCell.cellIdentifier, for: indexPath) as! VNP4TQuestionEmailCell
                    cell.data = question
                    cell.onChangeValue = { data in
                        self.onChangeValue?(data ?? question)
                    }
                    return cell
                default:
                    let cell : VNP4TQuestionTextCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionTextCell.cellIdentifier, for: indexPath) as! VNP4TQuestionTextCell
//                    cell.data = question
//                    cell.onChangeValue = { data in
//                        self.onChangeValue?(data ?? question)
//                    }
                    return cell
                }
            } else {
                switch question.dataType {
                case VNP4TQuestionType.BOOLEAN.rawValue:
                    let cell : VNP4TQuestionBooleanCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionBooleanCell.cellIdentifier, for: indexPath) as! VNP4TQuestionBooleanCell
                    cell.data = question
                    cell.onChangeValue = { data in
                        self.onChangeValue?(data ?? question)
                    }
                    return cell
                case VNP4TQuestionType.TEXT.rawValue:
                    let cell : VNP4TQuestionTextCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionTextCell.cellIdentifier, for: indexPath) as! VNP4TQuestionTextCell
                    cell.data = question
                    cell.onChangeValue = { data in
                        print(data?.toJSONString() ?? "")
                        self.onChangeValue?(data ?? question)
                    }
                    return cell
                case VNP4TQuestionType.NUMBER.rawValue:
                    let cell : VNP4TQuestionNumberCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionNumberCell.cellIdentifier, for: indexPath) as! VNP4TQuestionNumberCell
                    cell.data = question
                    cell.onChangeValue = { data in
                        self.onChangeValue?(data ?? question)
                    }
                    return cell
                case VNP4TQuestionType.DOUBLE.rawValue:
                    let cell : VNP4TQuestionDoubleCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionDoubleCell.cellIdentifier, for: indexPath) as! VNP4TQuestionDoubleCell
                    cell.data = question
                    cell.onChangeValue = { data in
                        self.onChangeValue?(data ?? question)
                    }
                    return cell
                case VNP4TQuestionType.DATE.rawValue:
                    let cell : VNP4TQuestionDateCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionDateCell.cellIdentifier, for: indexPath) as! VNP4TQuestionDateCell
                    cell.data = question
                    cell.onChangeValue = { data in
                        self.onChangeValue?(data ?? question)
                    }
                    return cell
                case VNP4TQuestionType.LIST.rawValue:
                    let cell : VNP4TQuestionListCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionListCell.cellIdentifier, for: indexPath) as! VNP4TQuestionListCell
                    cell.data = question
                    cell.onChangeValue = { data in
                        self.onChangeValue?(data ?? question)
                        self.tbView.reloadItemsAtIndexPaths([indexPath], animationStyle: .fade)
                    }
                    cell.onPickItems = {
                        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                        let optionsPick = VNP4TDialogSearchList(nibName: "VNP4TDialogSearchList", bundle: bundle)
                        optionsPick.viewModel = VNP4TDialogSearchListVM(
                            input: VNP4TDialogSearchListVM.Input(
                                dataDialog: BehaviorRelay<[VNP4TQuestionValidations]>(value: question.validations),
                                dataSearch: BehaviorRelay<[VNP4TQuestionValidations]>(value: question.validations),
                                isSearch: BehaviorRelay<Bool>(value: false),
                                seachText: BehaviorRelay<String>(value: ""),
                                titleDialog: BehaviorRelay<String>(value: question.question.uppercased()),
                                initValue: BehaviorRelay<String>(value: question.answerValue)),
                            dependency: VNP4TCommonService())
                        optionsPick.dataPickedAction = { item in
                            let currentQuestion = question
                            currentQuestion.answerValue = item.name
                            self.onChangeValue?(currentQuestion)
                            optionsPick.dismiss(animated: false) {
                                let dataList = self.viewModel.input?.dataQuestions.value
                                if dataList != nil {
                                    dataList?.forEach { (itemQuestion) in
                                        if itemQuestion.id == currentQuestion.id {
                                            itemQuestion.answerValue = currentQuestion.answerValue
                                        }
                                    }
                                    self.tbView.reloadData()
                                }
                            }
                        }
                        optionsPick.modalPresentationStyle = .overFullScreen
                        self.parent?.present(optionsPick, animated: false, completion: nil)
                    }
                    return cell
                case VNP4TQuestionType.PERCENTAGE.rawValue:
                    let cell : VNP4TQuestionPercentCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionPercentCell.cellIdentifier, for: indexPath) as! VNP4TQuestionPercentCell
                    cell.data = question
                    cell.onChangeValue = { data in
                        self.onChangeValue?(data ?? question)
                    }
                    return cell
                case VNP4TQuestionType.PHOTO.rawValue:
                    let cell : VNP4TQuestionPhotoCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionPhotoCell.cellIdentifier, for: indexPath) as! VNP4TQuestionPhotoCell
                    cell.data = question
                    cell.images.accept(question.listImage)
                    cell.viewController = self
                    cell.onChangeData = { data in
                        self.onChangeValue?(data ?? question)
                    }
                    
                    self.imagePicker = ImagePicker(presentationController: self, delegate: self, questionId: question.id )
                    return cell
                default:
                    let cell : VNP4TQuestionTextCell = tv.dequeueReusableCell(withIdentifier: VNP4TQuestionTextCell.cellIdentifier, for: indexPath) as! VNP4TQuestionTextCell
                    cell.data = question
//                    cell.onChangeValue = { data in
//                        self.onChangeValue?(data ?? question)
//                    }
                    return cell
                }
            }
        })
        
        dataSource = ds
    }
}

extension VNP4TSurveyQuestionsVC: ImagePickerDelegate {
    func didSelect(image: UIImage?, questionId: String?) {
        print("IMAGE \(image)")
    }
}
