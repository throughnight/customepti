//
//  VNP4TSurveyQuestionsVM.swift
//  
//
//  Created by AEGONA on 9/16/20.
//

import Foundation
import RxCocoa
import RxSwift
import RxDataSources

typealias QuestionSection = SectionModel<String,VNP4TSurveyQuestion>

class VNP4TSurveyQuestionVM: VNP4TViewModelProtocol{
    
    typealias Dependency = VNP4TSurveyService
    
    struct Input {
        let dataQuestions                   :BehaviorRelay<[VNP4TSurveyQuestion]>
        let isScroll                        :BehaviorRelay<Bool>
//        let loadTrigger                     :BehaviorRelay<Bool>
    }
    
    struct Output {
//        let questionsResult                 :Observable<[VNP4TSurveyQuestion]>
        let questionSections                :Driver<[QuestionSection]>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input: Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: VNP4TSurveyService?) -> Output? {
        self.input = input
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        

//        let questionsResult = ip.dataQuestions.asObservable()
//            .flatMapLatest { questions -> Observable<[VNP4TSurveyQuestion]> in
//                return Observable.just(questions)
//        }

        
        let questionSections = ip.dataQuestions.asDriver().map { question -> [QuestionSection] in
            return [QuestionSection(model: "Question", items: question)]
        }
        
        return VNP4TSurveyQuestionVM.Output(questionSections: questionSections)
    }
}
