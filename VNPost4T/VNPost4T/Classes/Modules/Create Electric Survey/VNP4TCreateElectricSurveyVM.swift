//
//  VNP4TCreateElectricSurveyVM.swift
//  
//
//  Created by AEGONA on 9/15/20.
//

import Foundation
import RxCocoa
import RxSwift


class VNP4TCreateElectricSurveyVM: VNP4TViewModelProtocol {
    
    typealias Dependency = VNP4TSurveyService
    
    struct Input {
        let surveyType                          :BehaviorRelay<VNP4TSurveyClassificationTypeSelected>
        let dataSearch                          :BehaviorRelay<[VNP4TSurveyQuestion]>
        let currentPosition                     :BehaviorRelay<Int>
        let isLoading                           :BehaviorRelay<Bool>
        let questionTrigger                     :BehaviorRelay<Bool>
        let surveyQuestions                     :BehaviorRelay<[VNP4TSurveyQuestion]>
        let survey                              :BehaviorRelay<VNP4TSurveyClassificationTypeSelected>
        let totalStep                           :BehaviorRelay<Int>
        let collectionType                      :BehaviorRelay<Int>
        let submitDraftTrigger                  :BehaviorRelay<Bool>
        let draftData                           :BehaviorRelay<VNP4TQuestionKey>
    }
    
    struct Output {
        let currentPositionResult               :Observable<Int>
        let surveyQuestionsResult               :Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyQuestion]>>>
        let submitResult                        :Observable<VNP4TResponseModel<Any>>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input: Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: VNP4TSurveyService?) -> Output? {
        self.input = input
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        
        let currentPositionResult = ip.currentPosition.asObservable().flatMapLatest { (position) -> Observable<Int> in
            return Observable.just(ip.currentPosition.value)
        }
        
        let surveyQuestionsResult = ip.questionTrigger.asObservable()
            .filter {$0}
            .withLatestFrom(ip.survey)
            .do(onNext: { _ in
                ip.isLoading.accept(true)
            }).flatMapLatest { survey -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyQuestion]>>> in
                return dp.getSurveyQuestion(surveyId: survey.id)}
        .do(onNext: { _ in
            ip.isLoading.accept(false)
        })
        
        let triggerSubmit = ip.submitDraftTrigger.asObservable()
            .filter {$0}
            .withLatestFrom(ip.draftData)
            .do(onNext: { _ in
                ip.isLoading.accept(true)
            })
            .flatMapLatest { data -> Observable<VNP4TResponseModel<Any>> in
                return dp.createElectrictSurvey(survey: data)
            }.do(onNext: { _ in
                ip.isLoading.accept(false)
            })
        
        return VNP4TCreateElectricSurveyVM.Output(
            currentPositionResult: currentPositionResult,
            surveyQuestionsResult: surveyQuestionsResult,
            submitResult: triggerSubmit)
    }
    
    func uploadImage(){
        Observable.just(input?.surveyQuestions.value)
            .do(onNext: { _ in
            self.input?.isLoading.accept(true)
        }).subscribe(onNext: {[weak self] result in
                guard let self = self else {return}
                var arrImage : [VNP4TListImage] = []
                var isPass = false
                result?.forEach({ (item) in
                    if item.dataType == VNP4TQuestionType.PHOTO.rawValue {
                        var listImg : [VNP4TImage] = []
                        if item.listImage.count > 0 {
                            item.listImage.forEach { (img) in
                                if img.image != nil {
                                    isPass = true
                                }
                                listImg.append(VNP4TImage(image: img.image, url: img.url))
                            }
                            arrImage.append(VNP4TListImage(id: item.id, images: listImg))
                        }
                    }
                })
                if isPass {
                    resizeAndUploadListImage(listImage: arrImage, storage: VNP4TManager.shared.storage, typeImage: VNP4TTypeSurveyUploadImage.ELECTRICAL.rawValue, userId: VNP4TManager.shared.userId) { (url, fileName) in
                        let draftData = self.convertDataSubmit(listImage: fileName)
                        self.input?.draftData.accept(draftData)
                        self.input?.submitDraftTrigger.accept(true)
                    }
                } else {
                    let draftData = self.convertDataSubmit(listImage: nil)
                    self.input?.draftData.accept(draftData)
                    self.input?.submitDraftTrigger.accept(true)
                }
            })
    }
    
    private func convertDataSubmit(listImage: [VNP4TListImage]?) -> VNP4TQuestionKey{
        let data = self.input?.surveyQuestions.value
        let dataSubmit = VNP4TQuestionKey()
        if data != nil {
            dataSubmit.surveyId = self.input?.survey.value.id ?? ""
            dataSubmit.collectionType = self.input?.collectionType.value ?? 0
            dataSubmit.status = VNP4TSurveySaveType.DRAFT.rawValue
            dataSubmit.answers = []
            data?.forEach({ survey in
                if survey.step == 0 {
                    if survey.type == VNP4TQuestionCustomerType.DOB.rawValue ||  survey.type == VNP4TQuestionCustomerType.ISSUE_ID_NUMBER_DATE.rawValue {
                        survey.answerValue = survey.answerValue.vnp4TFormatDateSubmit() ?? survey.answerValue
                    }
                } else {
                    if survey.dataType == VNP4TQuestionType.DATE.rawValue {
                        survey.answerValue = survey.answerValue.vnp4TFormatDateSubmit() ?? survey.answerValue
                    }
                    if survey.dataType == VNP4TQuestionType.PHOTO.rawValue {
                        var listImg : [String] = []
                        if listImage != nil {
                            listImage?.forEach({ itemImage in
                                if (survey.id == itemImage.id) {
                                    itemImage.images?.forEach({ url in
                                        listImg.append(url.url ?? "")
                                    })
                                }
                            })
                        } else {
                            survey.listImage.forEach { img in
                                listImg.append(getFileNameFromFullUrl(url: img.url ?? ""))
                            }
                        }
                        survey.answerValue = "\(listImg)"
                    }
                }
                dataSubmit.answers.append(VNP4TQuestion(id: survey.id, value: survey.answerValue, answerId: survey.answerId, step: survey.step, profileType: survey.step == 0 ? survey.type : nil))
            })
        }
        return dataSubmit
    }
}
