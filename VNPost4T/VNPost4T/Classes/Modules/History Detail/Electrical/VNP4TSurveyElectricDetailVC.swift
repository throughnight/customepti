//
//  VNP4TSurveyElectricDetailVC.swift
//  VNPost4T
//
//  Created by AEGONA on 10/1/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

typealias VNP4TSurveyElectricDetailSource = RxTableViewSectionedReloadDataSource<ElectricDetailSection>

class VNP4TSurveyElectricDetailVC : VNP4TBaseViewController {
    
    @IBOutlet weak var viewBackgroundBar: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbType: VNP4TPaddingLabel!
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var viewContainConstraintsTop: NSLayoutConstraint!
    @IBOutlet weak var viewTotal: UIView!
    @IBOutlet weak var lbTitleTotal: UILabel!
    @IBOutlet weak var lbTotalPercent: UILabel!
    @IBOutlet weak var lbStatusTotal: VNP4TPaddingLabel!
    @IBOutlet weak var viewTotalConstraintsHeight: NSLayoutConstraint!
    @IBOutlet weak var viewBackgroundPercent: UIView!
    @IBOutlet weak var viewPercent: UIView!
    @IBOutlet weak var viewPercentConstraintsWidth: NSLayoutConstraint!
    @IBOutlet weak var tbViewConstraintsBottom: NSLayoutConstraint!
    @IBOutlet weak var viewActions: UIView!
    @IBOutlet weak var btnDraft: UIButton!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var viewActionsConstraintsHeight: NSLayoutConstraint!
    
    @IBOutlet weak var actionView: UIView!
    
    var viewModel : VNP4TSurveyElectricDetailVM!
    
    var dataSource : VNP4TSurveyElectricDetailSource!
    
    private var loading: VNP4TLoadingProgressView!
    
    var onRefreshData : (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loading = VNP4TLoadingProgressView(uiScreen: UIScreen.main)
        self.view.addSubview(loading)
        
        registerCell()
        configDataSource()
        setupView()
        setupViewModel()
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSaveDraft(_ sender: Any) {
        self.viewModel.uploadImage(type: VNP4TSurveySaveType.DRAFT.rawValue)
    }
    
    @IBAction func onDelete(_ sender: Any) {
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        let dialogOptions = VNP4TDialogOptions(nibName: "VNP4TDialogOptions", bundle: bundle)
        dialogOptions.modalPresentationStyle = .overFullScreen
        dialogOptions.titleDialog = BehaviorRelay<String>(value: "4t_history_detail_delete_title".localized)
        dialogOptions.messageDialog = BehaviorRelay<String>(value: "")
        dialogOptions.titleActionConfirm =  BehaviorRelay<String?>(value: "4t_delete_survey".localized)
        dialogOptions.onCancelAction = {
            dialogOptions.dismiss(animated: false, completion: nil)
        }
        
        dialogOptions.onDoneActions = { [weak self] in
            dialogOptions.dismiss(animated: false, completion: nil)
            guard let self = self else { return }
            self.viewModel.input?.deleteTrigger.accept(true)
        }
        self.parent?.present(dialogOptions, animated: false, completion: nil)
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        var pass = true
        let data = self.viewModel.input?.dataShow.value
        data?.forEach({ itemData in
            itemData.questions?.forEach({ listQuestion in
                if listQuestion.required {
                    if listQuestion.isPass == false {
                        pass = false
                    } else {
                        if listQuestion.answerValue == "" {
                            if listQuestion.step != 0 && listQuestion.dataType == VNP4TQuestionType.PHOTO.rawValue {
                                if listQuestion.listImage.count > 0 {
                                    listQuestion.isPass = true
                                } else {
                                    listQuestion.isPass = false
                                }
                            } else {
                                pass = false
                                listQuestion.isPass = false
                            }
                        }
                    }
                }
            })
        })
        
        if pass == true {
            let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
            let dialogOptions = VNP4TDialogOptions(nibName: "VNP4TDialogOptions", bundle: bundle)
            dialogOptions.modalPresentationStyle = .overFullScreen
            dialogOptions.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_submit_survey".localized)
            dialogOptions.titleActionConfirm = BehaviorRelay<String?>(value: "4t_submit".localized)
            dialogOptions.messageDialog = BehaviorRelay<String>(value: "")
            dialogOptions.onCancelAction = {
                dialogOptions.dismiss(animated: false, completion: nil)
            }
            
            dialogOptions.onDoneActions = { [weak self] in
                dialogOptions.dismiss(animated: false, completion: nil)
                guard let self = self else { return }
                
                self.viewModel.uploadImage(type: VNP4TSurveySaveType.SUBMIT.rawValue)
            }
            self.parent?.present(dialogOptions, animated: false, completion: nil)
        } else {
            self.tbView.reloadData()
            let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
            let errorDialog = VNP4TDialogError(nibName: "VNP4TDialogError", bundle: bundle)
            errorDialog.messageDialog = BehaviorRelay<String>(value: "4t_create_electrict_survey_error_empty".localized)
            errorDialog.modalPresentationStyle = .overFullScreen
            errorDialog.onDoneActions = {
                errorDialog.dismiss(animated: true, completion: nil)
            }
            
            self.parent?.present(errorDialog, animated: false, completion: nil)
        }
    }
}

extension VNP4TSurveyElectricDetailVC : VNP4TViewControllerProtocol{
    func setupView() {
        self.tbView.rx.setDelegate(self).disposed(by: disposeBag)
        self.viewBackgroundBar.VNP4TsetGradientBackground(colorStart: VNP4TConstants.navigationBarMainColor, colorEnd: VNP4TConstants.navigationBarSubColor)
        self.viewPercent.VNP4TsetGradientBackground(colorStart: VNP4TConstants.buttonMainColor, colorEnd: VNP4TConstants.buttonSubColor, radius: 5)
        
        self.btnSend.VNP4TsetGradientBlueButtonBackground()
    }
    
    func setupViewModel() {
        if let ip = self.viewModel.input {
            bindingInput(input: ip)
        }
        if let op = self.viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input : VNP4TSurveyElectricDetailVM.Input){
        let status = input.status.value
        if status == VNP4TSurveyStatus.APPROVED.rawValue {
            self.viewContainConstraintsTop.constant = 160
            self.viewTotalConstraintsHeight.constant = 80
        } else {
            self.viewContainConstraintsTop.constant = 60
            self.viewTotalConstraintsHeight.constant = 0
            self.viewTotal.isHidden = true
            self.view.layoutIfNeeded()
        }
        
        if status == VNP4TSurveyStatus.NEW.rawValue {
            self.tbViewConstraintsBottom.constant = 120
            self.viewActionsConstraintsHeight.constant = 120
            self.viewActions.isHidden = false
            self.lbTitle.text = "4t_detail_e_survey_title".localized.uppercased()
        } else {
            self.tbViewConstraintsBottom.constant = 0
            self.viewActionsConstraintsHeight.constant = 0
            self.viewActions.isHidden = true
            self.view.layoutIfNeeded()
            self.lbTitle.text = "4t_detail_e_survey_title".localized.uppercased()
        }
        
        input.isLoading.subscribe { [weak self] isLoading in
            guard let self = self else {return}
            
            if isLoading.element ?? false {
                self.loading.startAnimating()
            } else {
                self.loading.stopAnimating()
            }
        }.disposed(by: disposeBag)
        
//        input.isShowAction.subscribe{[weak self] isShow in
//            guard let self = self else {return}
//            self.actionView.isHidden = !(isShow.element ?? false)
//        }
    }
    
    private func bindingOutput(output: VNP4TSurveyElectricDetailVM.Output){
        
        output.surveySection.drive(self.tbView.rx.items(dataSource: self.dataSource)).disposed(by: disposeBag)
        
        output.dataSurveyResult.subscribe(onNext : { [weak self] result in
            guard let self = self else {return}
//            self.viewModel.input?.isShowAction.accept(result.success)
            
            if result.success == true {
                result.data?.questions?.forEach { (item) in
                    if item.dataType == VNP4TQuestionType.PHOTO.rawValue {
                        item.convertURL()
                    }
                }
                self.viewModel.input?.surveyDetail.accept(result.data ?? VNP4TSurveyDetail())
                self.viewModel.input?.dataSurvey.accept(result.data?.questions ?? [])
                var summaryData : [VNP4TSummarySurvey] = []
                summaryData.append(self.handleConvertSurveyType())
                summaryData += self.handleConvertDataQuestion(data: result.data?.questions ?? [])
                
                self.viewModel.input?.dataShow.accept(summaryData)
                
                self.renderSurveyUI(data: result.data)
                
            } else {
                self.showErrorDialog(errorMsg: result.message, buttonTitle: "Thử lại")
            }
        }).disposed(by: disposeBag)
        
        output.draftResult.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            
            if result.success == true {
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let successDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                successDialog.titleDialog = BehaviorRelay<String>(value: "4t_create_electrict_survey_draft_success".localized.uppercased())
                successDialog.messageDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_message_success_1".localized + " " + (self.viewModel.input?.survey.value.surveySheetCode ?? "") + " " + "4t_summary_electrict_survey_message_success_save_draft_2".localized)
                successDialog.status = BehaviorRelay<Bool>(value: true)
                successDialog.modalPresentationStyle = .overFullScreen
                successDialog.onDoneActions = {
                    successDialog.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                    self.onRefreshData?()
                }
                
                self.parent?.present(successDialog, animated: false, completion: nil)
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_error".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: true, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        })
        
        output.submitResult.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            
            if result.success == true {
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let successDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                successDialog.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_success".localized.uppercased())
                successDialog.messageDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_message_success_1".localized + " " + (self.viewModel.input?.survey.value.surveySheetCode ?? "") + " " + "4t_summary_electrict_survey_message_success_2".localized)
                successDialog.status = BehaviorRelay<Bool>(value: true)
                successDialog.modalPresentationStyle = .overFullScreen
                successDialog.onDoneActions = {
                    successDialog.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                    self.onRefreshData?()
                }
                
                self.parent?.present(successDialog, animated: false, completion: nil)
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_error".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: true, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        })
        
        output.deleteResult.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            
            if result.success == true {
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let successDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                successDialog.titleDialog = BehaviorRelay<String>(value: "4t_history_detail_delete_success_title".localized.uppercased())
                successDialog.messageDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_message_success_1".localized + " " + (self.viewModel.input?.survey.value.surveySheetCode ?? "") + " " + "4t_history_detail_delete_success_message_2".localized)
                successDialog.status = BehaviorRelay<Bool>(value: true)
                successDialog.modalPresentationStyle = .overFullScreen
                successDialog.onDoneActions = {
                    successDialog.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                    self.onRefreshData?()
                }
                
                self.parent?.present(successDialog, animated: false, completion: nil)
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_error".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: true, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        })
    }
    
    private func renderSurveyUI(data : VNP4TSurveyDetail?) {
        
        if self.viewModel.input?.status.value == VNP4TSurveySaveType.DRAFT.rawValue {
            if data?.collectionType == VNP4TCollectionType.NEW.rawValue {
                self.lbType.text = "4t_summary_electrict_survey_type_new".localized
            } else {
                self.lbType.text = "4t_summary_electrict_survey_type_old".localized
            }
        } else {
            switch data?.surveySheetStatus {
            case VNP4TSurveyStatus.APPROVED.rawValue:
                self.lbType.text = "4t_status_approve".localized
                self.lbTotalPercent.text = "\(data?.percent ?? 0)%"
                self.lbStatusTotal.textColor = .white
                if data?.evaluate == VNP4TEvaluate.GOOD.rawValue {
                    self.lbStatusTotal.text = "4t_survey_approved_good_title".localized
                    self.lbStatusTotal.backgroundColor = UIColor(hex: "00B16A")
                } else {
                    self.lbStatusTotal.text = "4t_survey_approved_bad_title".localized
                    self.lbStatusTotal.backgroundColor = UIColor(hex: "F64744")
                }
                let totalPercent = (Int(self.viewBackgroundPercent.layer.frame.size.width) * (data?.percent ?? 0) ) / 100
                self.viewPercentConstraintsWidth.constant = CGFloat(totalPercent)
                break
            case VNP4TSurveyStatus.CANCEL.rawValue:
                self.lbType.text = "4t_status_deny".localized
                break
            case VNP4TSurveyStatus.PROCRESSING.rawValue:
                self.lbType.text = "4t_status_processing".localized
                break
            default:
                self.lbType.text = "4t_status_processing".localized
            }
        }
    }
    
    private func handleConvertSurveyType() -> VNP4TSummarySurvey{
        let itemType = VNP4TSummarySurvey()
        itemType.title = "4t_summary_electrict_survey_title_survey_type".localized.uppercased()
        if let type = self.viewModel.input?.survey.value {
            var typeArr : [VNP4TSurveyQuestion] = []
            let itemCode = VNP4TSurveyQuestion()
            itemCode.answerValue = type.surveySheetCode
            itemCode.key = true
            itemCode.question = "4t_summary_electrict_survey_title_survey_type_1".localized
            itemCode.required = true
            
            let itemName = VNP4TSurveyQuestion()
            itemName.answerValue = type.name
            itemName.key = true
            itemName.question = "4t_summary_electrict_survey_title_survey_type_2".localized
            itemName.required = true
            
            typeArr.append(itemCode)
            typeArr.append(itemName)
            
            itemType.questions = typeArr
        }
        return itemType
    }
    
    private func handleConvertDataQuestion(data : [VNP4TSurveyQuestion]?) -> [VNP4TSummarySurvey] {
        var array : [VNP4TSummarySurvey] = []
        if data?.count ?? 0 > 0 {
            var arrProfile : [VNP4TSurveyQuestion] = []
            var arrQuestion : [VNP4TSurveyQuestion] = []
            let status = self.viewModel.input?.status.value
            data?.forEach { question in
                if status == VNP4TSurveyStatus.NEW.rawValue {
                    if question.checkQueryRequired == true {
                        question.key = true
                    } else {
                        question.key = false
                    }
                } else {
                    question.key = true
                }
                if question.step == 0 {
                    arrProfile.append(question)
                } else {
                    arrQuestion.append(question)
                }
            }
            
            let itemProfile = VNP4TSummarySurvey()
            itemProfile.title = "4t_summary_electrict_survey_title_survey_profile".localized.uppercased()
            itemProfile.questions = arrProfile
            
            let itemQuestion = VNP4TSummarySurvey()
            itemQuestion.title = "4t_summary_electrict_survey_title_survey_sheet".localized.uppercased()
            itemQuestion.questions = arrQuestion
            
            array.append(itemProfile)
            array.append(itemQuestion)
        }
        
        return array
    }
}

extension VNP4TSurveyElectricDetailVC : ListViewControllerProtocol {
    func registerCell() {
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        tbView.register(UINib(nibName: VNP4TSummaryElectrictSurveyCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TSummaryElectrictSurveyCell.cellIdentifier)
    }
    
    func configDataSource() {
        
        let dataSource = RxTableViewSectionedReloadDataSource<ElectricDetailSection>(
            configureCell: { dataSource, tableView, indexPath, question in
                let cell : VNP4TSummaryElectrictSurveyCell = tableView.dequeueReusableCell(withIdentifier: VNP4TSummaryElectrictSurveyCell.cellIdentifier, for: indexPath) as! VNP4TSummaryElectrictSurveyCell
                cell.data = question
                
                cell.onChangeValue = { item in
                    question.questions?.forEach({ result in
                        if result.id == item.id {
                            result.answerValue = item.answerValue
                            result.isPass = true
                        }
                    })
                    self.viewModel.input?.surveyDetail.value.questions?.forEach({ data in
                        if data.id == item.id {
                            data.answerValue = item.answerValue
                            data.isPass = true
                        }
                    })
                }
                return cell
        })
        
        self.dataSource = dataSource
    }
}

extension VNP4TSurveyElectricDetailVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let index = indexPath.row
        switch index {
        case 0:
            return 230
        case 1:
            var countAddress : Int = 0
            var countH : Int = 0
            var countQ  = 0
            self.viewModel.input?.dataShow.value[1].questions?.forEach({ (item) in
                if item.type == VNP4TQuestionCustomerType.TEMPORARY_ADDRESS.rawValue || item.type == VNP4TQuestionCustomerType.PERMANENT_ADDRESS.rawValue {
                    if item.answerValue.count > 30 {
                        countAddress += 1
                    }
                } else {
                    if item.answerValue.count > 30 {
                        countH += 1
                    }
                }
                if item.question.count > 35 {
                    countQ += 1
                }
            })
            let heightView = (((self.viewModel.input?.dataShow.value[1].questions?.count ?? 0)) * 95)
            return CGFloat(heightView + (countAddress * 30) + (countH * 20) + (countQ * 10))
        case 2:
            var count: Int = 0
            var countBool : Int = 0
            var countH : Int = 0
            self.viewModel.input?.dataShow.value[2].questions?.forEach({ (item) in
                if item.dataType == VNP4TQuestionType.PHOTO.rawValue {
                    count += 1
                }
                if item.dataType == VNP4TQuestionType.BOOLEAN.rawValue {
                    countBool += 1
                }
                if item.question.count > 35 {
                    countH += 1
                }
            })
            let heightPhoto = count * 152
            let heightView = (((self.viewModel.input?.dataShow.value[2].questions?.count ?? 0)) * 108)
            let totalHeight = CGFloat(heightView + 20 + heightPhoto + (countBool * 13) + (countH * 20))
            return totalHeight
        default:
            return 230
        }
    }
}
