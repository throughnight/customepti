//
//  VNP4TSurveyElectricDetailVM.swift
//  VNPost4T
//
//  Created by AEGONA on 10/1/20.
//

import Foundation
import RxCocoa
import RxSwift
import RxDataSources
import ObjectMapper

typealias ElectricDetailSection = SectionModel<String,VNP4TSummarySurvey>

class VNP4TSurveyElectricDetailVM: VNP4TViewModelProtocol {
    
    typealias Dependency = VNP4TSurveyService
    
    struct Input {
        let status                      :BehaviorRelay<Int>
        let isLoading                   :BehaviorRelay<Bool>
        let survey                      :BehaviorRelay<VNP4TSurvey>
        let dataShow                    :BehaviorRelay<[VNP4TSummarySurvey]>
        let dataSurvey                  :BehaviorRelay<[VNP4TSurveyQuestion]>
        let surveyDetail                :BehaviorRelay<VNP4TSurveyDetail>
        let saveDraftTrigger            :BehaviorRelay<Bool>
        let dataUpdate                  :BehaviorRelay<VNP4TQuestionKey>
        let submitTrigger               :BehaviorRelay<Bool>
        let deleteTrigger               :BehaviorRelay<Bool>
        let isShowAction                :BehaviorRelay<Bool>
    }
    
    struct Output {
        let dataSurveyResult            :Observable<VNP4TResponseModel<VNP4TSurveyDetail>>
        let surveySection               :Driver<[ElectricDetailSection]>
        let draftResult                 :Observable<VNP4TResponseModel<Any>>
        let submitResult                :Observable<VNP4TResponseModel<Any>>
        let deleteResult                :Observable<VNP4TResponseModel<Any>>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input: Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: VNP4TSurveyService?) -> Output? {
        self.input = input
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        
        let detailResult = ip.survey.asObservable()
            .do(onNext: { (_) in
                ip.isLoading.accept(true)
            }).flatMapLatest { survey -> Observable<VNP4TResponseModel<VNP4TSurveyDetail>> in
                return dp.getSurveyEDetail(id: survey.id)
        }.do(onNext: { (_) in
            ip.isLoading.accept(false)
        })
        
        let surveySection = ip.dataShow.asDriver().map { data -> [ElectricDetailSection] in
            return [ElectricDetailSection(model: "EDetail", items: data)]
        }
        
        let draftResult = ip.saveDraftTrigger.asObservable()
            .filter {$0}
            .withLatestFrom(ip.dataUpdate)
            .do(onNext: { _ in
                ip.isLoading.accept(true)
        }).flatMapLatest { data -> Observable<VNP4TResponseModel<Any>> in
            return dp.updateESurvey(survey: data)
        }.do(onNext: { _ in
            ip.isLoading.accept(false)
        })
        
        let submitResult = ip.submitTrigger.asObservable()
            .filter {$0}
            .withLatestFrom(ip.dataUpdate)
            .do(onNext: { _ in
                ip.isLoading.accept(true)
        }).flatMapLatest { data -> Observable<VNP4TResponseModel<Any>> in
            return dp.updateESurvey(survey: data)
        }.do(onNext: { _ in
            ip.isLoading.accept(false)
        })
        
        let deleteResult = ip.deleteTrigger.asObservable()
            .filter {$0}
            .withLatestFrom(ip.surveyDetail)
            .do(onNext: { _ in
                ip.isLoading.accept(true)
        }).flatMapLatest { data -> Observable<VNP4TResponseModel<Any>> in
            return dp.deleteESurvey(id: data.surveySheetId ?? "")
        }.do(onNext: { _ in
            ip.isLoading.accept(false)
        })
        
        return VNP4TSurveyElectricDetailVM.Output(dataSurveyResult: detailResult,
                                                  surveySection: surveySection,
                                                  draftResult: draftResult,
                                                  submitResult: submitResult,
                                                  deleteResult: deleteResult)
    }
    
    func uploadImage(type: Int){
        Observable.just(input?.surveyDetail.value)
            .do { () in
                self.input?.isLoading.accept(true)
        }
        .subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            var listImg : [VNP4TListImage] = []
            var isPass = false
            result?.questions?.forEach({ (item) in
                var imgs : [VNP4TImage] = []
                if item.dataType == VNP4TQuestionType.PHOTO.rawValue {
                    if item.listImage.count > 0 {
                        item.listImage.forEach { (img) in
                            if img.image != nil {
                                isPass = true
                            }
                            imgs.append(VNP4TImage(image: img.image, url: img.url))
                        }
                        listImg.append(VNP4TListImage(id: item.id, images: imgs))
                    }
                }
            })
            if isPass {
                resizeAndUploadListImage(listImage: listImg, storage: VNP4TManager.shared.storage, typeImage: VNP4TTypeSurveyUploadImage.ELECTRICAL.rawValue, userId: VNP4TManager.shared.userId) { (url, fileName) in
                    let draftData = self.convertDataSubmit(type: type, listImage: fileName)
                    self.input?.dataUpdate.accept(draftData)
                    if type == VNP4TSurveySaveType.DRAFT.rawValue{
                        self.input?.saveDraftTrigger.accept(true)
                    } else {
                        self.input?.submitTrigger.accept(true)
                    }
                }
            } else {
                let draftData = self.convertDataSubmit(type: type, listImage: nil)
                self.input?.dataUpdate.accept(draftData)
                if type == VNP4TSurveySaveType.DRAFT.rawValue{
                    self.input?.saveDraftTrigger.accept(true)
                } else {
                    self.input?.submitTrigger.accept(true)
                }
            }
        })
    }
    
    private func convertDataSubmit(type: Int,listImage: [VNP4TListImage]?) -> VNP4TQuestionKey{
        let survey = self.input?.surveyDetail.value
        let data = survey?.questions
        let dataSubmit = VNP4TQuestionKey()
        
        if survey != nil {
            dataSubmit.surveyId = survey?.surveyId ?? ""
            dataSubmit.collectionType = survey?.collectionType ?? 0
            dataSubmit.surveySheetId = survey?.surveySheetId ?? ""
            if type == VNP4TSurveySaveType.DRAFT.rawValue {
                dataSubmit.status = VNP4TSurveySaveType.DRAFT.rawValue
            } else {
                dataSubmit.status = VNP4TSurveySaveType.SUBMIT.rawValue
            }
            dataSubmit.answers = []
            data?.forEach({ survey in
                var listImg : [String] = []
                if survey.step == 0 {
                    if survey.type == VNP4TQuestionCustomerType.DOB.rawValue ||  survey.type == VNP4TQuestionCustomerType.ISSUE_ID_NUMBER_DATE.rawValue {
                        survey.answerValue = survey.answerValue.vnp4TFormatDateSubmit() ?? survey.answerValue
                    }
                } else {
                    if survey.dataType == VNP4TQuestionType.DATE.rawValue {
                        survey.answerValue = survey.answerValue.vnp4TFormatDateSubmit() ?? survey.answerValue
                    }
                    if survey.dataType == VNP4TQuestionType.PHOTO.rawValue {
                        if listImage != nil {
                            listImage?.forEach({ itemImage in
                                if (survey.id == itemImage.id) {
                                    itemImage.images?.forEach({ url in
                                        listImg.append(url.url ?? "")
                                    })
                                }
                            })
                        } else {
                            survey.listImage.forEach { img in
                                listImg.append(getFileNameFromFullUrl(url: img.url ?? ""))
                            }
                        }
                        survey.answerValue = "\(listImg)"
                    }
                }
                if survey.dataType == VNP4TQuestionType.PHOTO.rawValue {
                    if listImg.count == 0 {
                        dataSubmit.answers.append(VNP4TQuestion(id: survey.id,answerId: survey.answerId, step: survey.step, profileType: survey.step == 0 ? survey.type : nil))
                    } else {
                        dataSubmit.answers.append(VNP4TQuestion(id: survey.id, value: survey.answerValue, answerId: survey.answerId, step: survey.step, profileType: survey.step == 0 ? survey.type : nil))
                    }
                } else {
                    dataSubmit.answers.append(VNP4TQuestion(id: survey.id, value: survey.answerValue, answerId: survey.answerId, step: survey.step, profileType: survey.step == 0 ? survey.type : nil))
                }
            })
        }
        return dataSubmit
    }
}
