//
//  VNP4TSurveyPhysicalDetailVM.swift
//  VNPost4T
//
//  Created by AEGONA on 10/1/20.
//

import Foundation
import RxCocoa
import RxSwift
import RxDataSources

class VNP4TSurveyPhysicalDetailVM: VNP4TViewModelProtocol {
    typealias Dependency = VNP4TSurveyService
    
    struct Input {
        let status                      :BehaviorRelay<Int>
        let isLoading                   :BehaviorRelay<Bool>
        let triggerDetail               :BehaviorRelay<Bool>
        let survey                      :BehaviorRelay<VNP4TSurvey>
        let dataDetail                  :BehaviorRelay<VNP4TSurveyDetail>
        let images                      :BehaviorRelay<[VNP4TImage]>
        let initTotalImage              :BehaviorRelay<Int>
        let imageUrl                    :BehaviorRelay<[String]>
        let triggerUpdate               :BehaviorRelay<Bool>
    }
    
    struct Output {
        let dataSurveyResult            :Observable<VNP4TResponseModel<VNP4TSurveyDetail>>
        let updateResult                :Observable<VNP4TResponseModel<Any>>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input: Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: VNP4TSurveyService?) -> Output? {
        self.input = input
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        
        let dataResult = ip.triggerDetail.asObservable()
            .filter {$0}
            .withLatestFrom(ip.survey)
            .do(onNext: { _ in
                ip.isLoading.accept(true)
            }).flatMapLatest { survey -> Observable<VNP4TResponseModel<VNP4TSurveyDetail>> in
                return dp.getSurveyPhysicalDetail(id: survey.id)
            }.do(onNext: { _ in
                ip.isLoading.accept(false)
            })
        
        let updateResult = ip.triggerUpdate.asObservable()
            .filter {$0}
            .withLatestFrom(ip.dataDetail)
            .flatMapLatest { detail -> Observable<VNP4TResponseModel<Any>> in
                var listFileName : [String] = []
                listFileName = ip.imageUrl.value
                detail.listImg?.forEach({ (img) in
                    listFileName.append(getFileNameFromFullUrl(url: img))
                })
                let data = VNP4TQuestionKey()
                data.listImages = listFileName
                return dp.updateFSurvey(id: detail.id ?? "", data: data)
            }.do(onNext: { _ in
                ip.isLoading.accept(false)
            })
        
        return VNP4TSurveyPhysicalDetailVM.Output(dataSurveyResult: dataResult,
                                                  updateResult: updateResult)
    }
    
    func uploadImage(){
        Observable.just(input?.images.value)
            .subscribe(onNext: {[weak self] images in
                guard let self = self else {return}
                var imageUpload : [UIImage] = []
                images?.forEach({ (item) in
                    if item.image != nil {
                        imageUpload.append(item.image!)
                    }
                })
                self.input?.isLoading.accept(true)
                resizeAndUploadImage(listImage: imageUpload, storage: VNP4TManager.shared.storage, typeImage: VNP4TTypeSurveyUploadImage.PHYSICAL.rawValue, userId: VNP4TManager.shared.userId!) { (url,fileName) in
                    self.input?.imageUrl.accept(fileName)
                    self.input?.triggerUpdate.accept(true)
                }
            })
    }
}
