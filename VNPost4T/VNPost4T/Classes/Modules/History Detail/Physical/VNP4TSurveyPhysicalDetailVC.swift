//
//  VNP4TSurveyPhysicalDetailVC.swift
//  VNPost4T
//
//  Created by AEGONA on 10/1/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

class VNP4TSurveyPhysicalDetailVC : VNP4TBaseViewController {
    
    @IBOutlet weak var viewActions: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbStatus: VNP4TPaddingLabel!
    @IBOutlet weak var viewBackgroundBar: UIView!
    @IBOutlet weak var viewSurveyType: UIView!
    @IBOutlet weak var lbTitleSurvey: UILabel!
    @IBOutlet weak var lbImage: UILabel!
    @IBOutlet weak var lbTitleSurveyType: UILabel!
    @IBOutlet weak var lbSurveyCode: UILabel!
    @IBOutlet weak var lbSurveyName: UILabel!
    @IBOutlet weak var tfValueSurveyCode: UITextField!
    @IBOutlet weak var tfValueSurveyName: UITextField!
    @IBOutlet weak var collectionImages: UICollectionView!
    @IBOutlet weak var btnAddImage: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnUpdateImage: UIButton!
    @IBOutlet weak var viewActionsConstraintsHeight: NSLayoutConstraint!
    
    var viewModel : VNP4TSurveyPhysicalDetailVM!
    
    var surveyTypeVC : VNP4TSummaryElectrictSurveyCell!
    
    private let imagePickerController = UIImagePickerController()
    private var physicalOption: VNP4TPhysicalOption!
    
    var onRefreshData : (()->())?
    
    private var loading: VNP4TLoadingProgressView!
    
    var isUpdate = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loading = VNP4TLoadingProgressView(uiScreen: UIScreen.main)
        self.view.addSubview(loading)
        setupView()
        setupViewModel()
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onUpdateImage(_ sender: Any) {
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        let dialogOptions = VNP4TDialogOptions(nibName: "VNP4TDialogOptions", bundle: bundle)
        dialogOptions.modalPresentationStyle = .overFullScreen
        dialogOptions.titleDialog = BehaviorRelay<String>(value: "4t_detail_physical_message_update".localized)
        dialogOptions.messageDialog = BehaviorRelay<String>(value: "")
        dialogOptions.onCancelAction = {
            dialogOptions.dismiss(animated: false, completion: nil)
        }
        
        dialogOptions.onDoneActions = { [weak self] in
            guard let self = self else {return}
            dialogOptions.dismiss(animated: false, completion: nil)
            self.viewModel.uploadImage()
        }
        
        self.parent?.present(dialogOptions, animated: false, completion: nil)
    }
    
    @IBAction func onAddImage(_ sender: Any) {
        onOptionsCamera()
    }
    
    private func onOptionsCamera(){
        
        let imageOption = VNP4TDialogImageOption(nibName: VNP4TDialogImageOption.identifier, bundle: vnp4tBundle)
        
        imageOption.modalPresentationStyle = .overFullScreen
        
        imageOption.onTakePicture = {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.takePhoto()
            }
        }
        
        imageOption.onLibrary = {
            self.photoLibrary()
        }
        
        self.parent?.present(imageOption, animated: false, completion: nil)
        
    }
    
    private func takePhoto(){
        self.imagePickerController.sourceType = .camera
        self.present(self.imagePickerController, animated: true, completion: nil)
    }
    
    private func photoLibrary(){
        self.imagePickerController.sourceType = .photoLibrary
        self.present(self.imagePickerController, animated: true, completion: nil)
    }
    
    func handleActionAdd(){
        var uiImageCount = 0
        var totalImage = 0
        if(self.isUpdate){
            self.viewModel.input?.images.value.forEach({ (img) in
                if img.image != nil {
                    uiImageCount += 1
                }
            })
            totalImage = uiImageCount
        }else{
            totalImage = self.viewModel.input?.images.value.count ?? 0
        }
        
        let totalLimit = self.viewModel.input?.dataDetail.value.limitImg ?? 0
        if self.viewModel.input?.status.value == VNP4TSurveyStatus.PROCRESSING.rawValue {
            if totalImage < totalLimit {
                self.btnAddImage.isHidden = false
            } else {
                self.btnAddImage.isHidden = true
            }
        }
        self.lbImage.text = "\(totalImage)/\(totalLimit) " + "4t_detail_physical_image".localized
        let totalInit = self.viewModel.input?.initTotalImage.value ?? 0
        if self.isUpdate {
            if totalImage <= self.viewModel.input?.dataDetail.value.limitImg ?? 0 && totalImage != 0 {
                self.btnUpdateImage.backgroundColor = UIColor(hex: "127BCA")
                self.btnUpdateImage.isEnabled = true
            } else {
                self.btnUpdateImage.backgroundColor = UIColor(hex: "E9E9E9")
                self.btnUpdateImage.isEnabled = false
            }
        } else {
            if totalInit == totalImage {
                self.btnUpdateImage.backgroundColor = UIColor(hex: "E9E9E9")
                self.btnUpdateImage.isEnabled = false
            } else {
                self.btnUpdateImage.backgroundColor = UIColor(hex: "127BCA")
                self.btnUpdateImage.isEnabled = true
            }
        }
    }
    
    
}

extension VNP4TSurveyPhysicalDetailVC : VNP4TViewControllerProtocol  {
    func setupView() {
        
        self.handleShowSurveyType()
        
        let typeSurvey = self.viewModel.input?.status.value
        switch typeSurvey {
        case VNP4TSurveyStatus.APPROVED.rawValue:
            lbStatus.text = "4t_survey_approved_good_title".localized
            viewActions.isHidden = true
            viewActionsConstraintsHeight.constant = 0
            self.btnAddImage.isHidden = true
            view.layoutIfNeeded()
            break
        case VNP4TSurveyStatus.CANCEL.rawValue:
            lbStatus.text = "4t_status_deny".localized
            viewActions.isHidden = true
            viewActionsConstraintsHeight.constant = 0
            self.btnAddImage.isHidden = true
            view.layoutIfNeeded()
            break
        case VNP4TSurveyStatus.PROCRESSING.rawValue:
            lbStatus.text = "4t_status_processing".localized
            viewActions.isHidden = false
            viewActionsConstraintsHeight.constant = 60
            self.btnAddImage.isHidden = false
            view.layoutIfNeeded()
            break
        default:
            lbStatus.text = ""
            viewActions.isHidden = true
            viewActionsConstraintsHeight.constant = 0
            self.btnAddImage.isHidden = true
            view.layoutIfNeeded()
        }
        
        self.collectionImages.register(UINib(nibName: VNP4TPhysicalImageCollectionViewCell.identify, bundle: vnp4tBundle), forCellWithReuseIdentifier: VNP4TPhysicalImageCollectionViewCell.identify)
        self.collectionImages.rx.setDataSource(self).disposed(by: disposeBag)
        
        self.physicalOption = VNP4TPhysicalOption()
        
        self.imagePickerController.delegate = self
        self.imagePickerController.allowsEditing = false
        self.imagePickerController.mediaTypes = ["public.image"]
        self.handleActionAdd()
        
        self.btnUpdateImage.backgroundColor = UIColor(hex: "E9E9E9")
        self.btnUpdateImage.isEnabled = false
        
        self.btnBack.setTitle("4t_create_electrict_survey_back".localized, for: .normal)
        self.btnUpdateImage.setTitle("4t_survey_history_image".localized, for: .normal)
    }
    
    func setupViewModel() {
        if let ip = self.viewModel.input {
            bindingInput(input: ip)
        }
        if let op = self.viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input: VNP4TSurveyPhysicalDetailVM.Input){
        input.isLoading.subscribe { [weak self] isLoading in
            guard let self = self else {return}
            
            if isLoading.element ?? false {
                self.loading.startAnimating()
            } else {
                self.loading.stopAnimating()
            }
        }
    }
    
    private func bindingOutput(output: VNP4TSurveyPhysicalDetailVM.Output){
        output.dataSurveyResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            
            if result.success == true {
                var listImage : [VNP4TImage] = []
                result.data?.listImg?.forEach({ (url) in
                    var item = VNP4TImage()
                    item.url = url
                    listImage.append(item)
                })
                self.viewModel.input?.initTotalImage.accept(result.data?.listImg?.count ?? 0)
                self.viewModel.input?.dataDetail.accept(result.data ?? VNP4TSurveyDetail())
                self.viewModel.input?.images.accept(listImage)
                self.collectionImages.reloadData()
                self.isUpdate = result.data?.statusUpdateImg ?? false
                self.handleActionAdd()
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_error".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: true, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
        
        output.updateResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.success == true {
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let successDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                successDialog.titleDialog = BehaviorRelay<String>(value: "4t_detail_physical_title_update_success".localized.uppercased())
                successDialog.messageDialog = BehaviorRelay<String>(value: "4t_physical_survey_message_success_1".localized + " " + (self.viewModel.input?.survey.value.surveySheetCode ?? "") + " " + "4t_detail_physical_message_update_success_2".localized)
                successDialog.status = BehaviorRelay<Bool>(value: true)
                successDialog.modalPresentationStyle = .overFullScreen
                successDialog.onDoneActions = {
                    successDialog.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                    self.onRefreshData?()
                }
                
                self.parent?.present(successDialog, animated: false, completion: nil)
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_error".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: true, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
    }
    
    private func handleShowSurveyType(){
        self.lbTitleSurveyType.text = "4t_summary_electrict_survey_title_survey_type".localized.uppercased()
        if let type = self.viewModel.input?.survey.value {
            self.tfValueSurveyCode.text = type.surveySheetCode
            setupRequiredLabel(with: "4t_summary_electrict_survey_title_survey_type_1".localized, label: self.lbSurveyCode)
            
            self.tfValueSurveyName.text = type.name
            setupRequiredLabel(with: "4t_summary_electrict_survey_title_survey_type_2".localized, label: self.lbSurveyName)
        }
        
    }
}

extension VNP4TSurveyPhysicalDetailVC: UINavigationControllerDelegate{
    
}

extension VNP4TSurveyPhysicalDetailVC: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.input?.images.value.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell =  collectionImages.dequeueReusableCell(withReuseIdentifier: VNP4TPhysicalImageCollectionViewCell.identify, for: indexPath) as! VNP4TPhysicalImageCollectionViewCell
        
        let data = self.viewModel.input?.images.value[indexPath.row]
    
        cell.loadImage(data: data)
        
        cell.onDel = {
            var imageArray = self.viewModel.input?.images.value
            imageArray?.remove(at: indexPath.row)
            self.viewModel.input?.images.accept(imageArray ?? [])
            self.handleActionAdd()
            self.collectionImages.reloadData()
        }
        
        return cell
    }
    
}


extension VNP4TSurveyPhysicalDetailVC: UICollectionViewDelegate{
    
}

extension VNP4TSurveyPhysicalDetailVC: UIImagePickerControllerDelegate {
    
    private func pickerImageController(_ controller: UIImagePickerController, image: UIImage?) {
        controller.dismiss(animated: true, completion: nil)
        self.dismiss(animated: false, completion: nil)
        if(image != nil){
            var listImage = self.viewModel.input?.images.value ?? []
            let item = VNP4TImage()
            item.image = image
            listImage += [item]
            self.viewModel.input?.images.accept(listImage)
            self.collectionImages.reloadData()
            self.handleActionAdd()
        }
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        pickerImageController(picker, image: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        guard let _image = info[.originalImage] as? UIImage else {
            pickerImageController(picker,image: nil)
            return
        }
        pickerImageController(picker, image: _image)
    }
}
