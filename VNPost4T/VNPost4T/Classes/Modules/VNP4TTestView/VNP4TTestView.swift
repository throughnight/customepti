//
//  VNP4TTestView.swift
//  VNPost4T
//
//  Created by m2 on 10/9/20.
//

import Foundation

class VNP4TTestView: VNP4TBaseViewController, UIScrollViewDelegate, UITableViewDelegate  {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func onClose(_ sender: Any) {
        navigationController?.popViewController(animated: false)
    }
    override func viewDidLoad() {
        setupView()
    }
    
}
extension VNP4TTestView: VNP4TViewControllerProtocol{
    
    func setupView() {
        self.tableView.delegate = self
        
        self.tableView.dataSource = self
        
        self.tableView.register(UINib(nibName: VNP4TQuestionPercentCell.cellIdentifier, bundle: vnp4tBundle), forCellReuseIdentifier: VNP4TQuestionPercentCell.cellIdentifier)
        
    }
    
    func setupViewModel() {
        
    }
    
}

extension VNP4TTestView: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : VNP4TQuestionPercentCell = tableView.dequeueReusableCell(withIdentifier: VNP4TQuestionPercentCell.cellIdentifier, for: indexPath) as! VNP4TQuestionPercentCell
        return cell
    }
    
}
