//
//  VNP4TFormRegisterInformationDataPickerVC.swift
//  
//
//  Created by AEGONA on 9/12/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

typealias VNP4TFormRegisterInformationDataPickerSource = RxTableViewSectionedReloadDataSource<SurveyListSection>

class VNP4TFormRegisterInformationDataPickerVC : VNP4TBaseViewController {
    
    @IBOutlet weak var backgroundHeaderView: UIView!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var imgEmpty: UIImageView!
    @IBOutlet weak var lbtitle: UILabel!
    @IBOutlet weak var viewParent: UIView!
    @IBOutlet weak var viewContain: UIView!
    
    private var refreshControl : UIRefreshControl!
    
    var viewModel : VNP4TFormRegisterInformationDatePickerVM!
    
    var dataSource : VNP4TFormRegisterInformationDataPickerSource!
    
    var dataPickedAction : ((VNP4TSurveyClassificationTypeSelected)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        configDataSource()
        setupView()
        setupViewModel()
        
        let tapDeadSpace = UITapGestureRecognizer(target: self, action: #selector(onClose))
        viewParent.addGestureRecognizer(tapDeadSpace)
        let tapContain = UITapGestureRecognizer(target: self, action: #selector(onContain))
        viewContain.addGestureRecognizer(tapContain)
    }
    
    @IBAction func onClosePicker(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func onClose(_ sender: Any){
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func onContain(_ sender: Any){
    }
}

extension VNP4TFormRegisterInformationDataPickerVC : VNP4TViewControllerProtocol{
    func setupView() {
        backgroundHeaderView.VNP4TsetGradientBackground(colorStart: VNP4TConstants.navigationBarMainColor, colorEnd: VNP4TConstants.navigationBarSubColor)
        
        //SETUP PULL TO REQUEST
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = .clear
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        tbView.refreshControl = refreshControl
        
        self.lbtitle.text = "4t_dialog_survey_type_title".localized
        self.tfSearch.placeholder = "4t_dialog_survey_type_placeholder_search".localized
    }
    
    @objc private func refresh() {
        self.viewModel.input?.loadTrigger.accept(true)
    }
    
    func setupViewModel() {
        if let ip = self.viewModel.input {
            bindingInput(input: ip)
        }
        if let op = self.viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input : VNP4TFormRegisterInformationDatePickerVM.Input){

        
        let loadMoreTrigger = self.tbView.rx.contentOffset.asDriver()
            .debounce(RxTimeInterval.milliseconds(300))
            .flatMap { [weak self] offset -> Driver<Bool> in
                guard let self = self else { return Driver.empty() }
                
                if (((self.viewModel.input?.nextPage.value ?? 0) * 10) - (self.viewModel.input?.surveyHistory.value.count ?? 0)) == 0 {
                    return isNearTheBottomEdge(contentOffset: offset, self.tbView) ? Driver.just(true) : Driver.empty()
                }
                return Driver.empty()
        }
        
        loadMoreTrigger.drive(input.loadMoreTrigger).disposed(by: disposeBag)
        
        tfSearch.rx.text.orEmpty.asDriver()
            .distinctUntilChanged()
            .debounce(RxTimeInterval.milliseconds(500))
            .drive(input.searchKey)
            .disposed(by: disposeBag)
        
    }
    
    private func bindingOutput(output : VNP4TFormRegisterInformationDatePickerVM.Output){
        output.surveyHistorySections.drive(self.tbView.rx.items(dataSource: self.dataSource)).disposed(by: disposeBag)
        
        output.surveyHistoryResult.subscribe(onNext: { [weak self] result in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
            }
            if result.statusCode == 200 {
                let surveys = result.data
                self.viewModel.input?.surveyHistory.accept(surveys?.data ?? [])
                
                if surveys?.data?.count ?? 0 > 0 {
                    var currentPapge = self.viewModel.input?.nextPage.value ?? 1
                    currentPapge += 1
                    self.viewModel.input?.nextPage.accept(currentPapge)
                    self.imgEmpty.isHidden = true
                } else {
                    self.imgEmpty.isHidden = true
                }
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_error".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: true, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
        
        output.moreSurveyHistoryResult.subscribe(onNext: { [weak self] result in
            guard let self = self else { return }
            
            if result.statusCode == 200 {
                let surveys = result.data
                if surveys?.data?.count ?? 0 > 0 {
                    var currentSurves = self.viewModel.input?.surveyHistory.value ?? []
                    currentSurves += surveys?.data ?? []
                    self.viewModel.input?.surveyHistory.accept(currentSurves)
                    
                    var currentPage = self.viewModel.input?.nextPage.value ?? 0
                    currentPage += 1
                    self.viewModel.input?.nextPage.accept(currentPage)
                } else {
                    self.viewModel.input?.isEnd.accept(true)
                }
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_error".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: true, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
    }
}

extension VNP4TFormRegisterInformationDataPickerVC : ListViewControllerProtocol{
    func registerCell() {
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        tbView.register(UINib(nibName: VNP4TFormRegisterInformationDataPickerCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TFormRegisterInformationDataPickerCell.cellIdentifier)
    }
    
    func configDataSource() {
        let ds : VNP4TFormRegisterInformationDataPickerSource = VNP4TFormRegisterInformationDataPickerSource(configureCell: {(dataSource, tv, indexPath, survey) in
            let cell : VNP4TFormRegisterInformationDataPickerCell = tv.dequeueReusableCell(withIdentifier: VNP4TFormRegisterInformationDataPickerCell.cellIdentifier, for: indexPath) as! VNP4TFormRegisterInformationDataPickerCell
            cell.index = indexPath.row
            cell.data = survey
            cell.onClickItem = { item in
                self.dataPickedAction?(item ?? survey)
            }
            return cell
        })
        
        dataSource = ds
    }
}
