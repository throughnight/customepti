//
//  VNP4TFormRegisterInformationDataPickerVM.swift
//  
//
//  Created by AEGONA on 9/14/20.
//

import Foundation
import RxCocoa
import RxSwift
import RxDataSources

typealias SurveyListSection = SectionModel<String,VNP4TSurveyClassificationTypeSelected>

class VNP4TFormRegisterInformationDatePickerVM: VNP4TViewModelProtocol {
    
    typealias Dependency = VNP4TSurveyService
    
    struct Input {
        let searchKey                   :BehaviorRelay<String>
        let loadTrigger                 :BehaviorRelay<Bool>
        let loadMoreTrigger             :BehaviorRelay<Bool>
        let nextPage                    :BehaviorRelay<Int>
        let isEnd                       :BehaviorRelay<Bool>
        let surveyHistory               :BehaviorRelay<[VNP4TSurveyClassificationTypeSelected]>
        let isLoading                   :BehaviorRelay<Bool>
        let collectionType              :BehaviorRelay<Int?>
    }
    
    struct Output {
        let surveyHistoryResult         :Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyClassificationTypeSelected]>>>
        let moreSurveyHistoryResult     :Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyClassificationTypeSelected]>>>
        let surveyHistorySections       :Driver<[SurveyListSection]>
    }
    
    var input           : Input?
    
    var output          : Output?
    
    var dependency      : Dependency?
    
    init(input: Input, dependency : Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: VNP4TSurveyService?) -> Output? {
        
        self.input = input
        
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        
        let loadTrigger = Observable.merge(ip.loadTrigger.asObservable().filter { $0}.withLatestFrom(ip.searchKey),ip.searchKey.asObservable())
        
        let surveyHistoryResult = loadTrigger.asObservable()
            .debounce(DispatchTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .do(onNext: { _ in
                ip.nextPage.accept(1)
                ip.isEnd.accept(false)
                ip.isLoading.accept(true)
            }).flatMapLatest { search -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyClassificationTypeSelected]>>> in
                return dp.getSurveyClassification(surveyClassification: self.input?.collectionType.value, page: 1, size: 10, search: search)}
            .do(onNext: { _ in
                ip.isLoading.accept(false)
            })
    
        let moreSurveyHistoryResult = ip.loadMoreTrigger.asObservable()
            .filter {$0}
            .withLatestFrom(ip.isEnd)
            .filter { !$0 }
            .withLatestFrom(Observable.combineLatest(ip.searchKey,ip.nextPage))
            .do(onNext: { _ in
                ip.isLoading.accept(true)
            }).flatMapLatest { (search, nextPage) -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyClassificationTypeSelected]>>> in
                return dp.getSurveyClassification(surveyClassification:  self.input?.collectionType.value, page: nextPage, size: 10, search: search)}
            .do(onNext: { _ in
                ip.isLoading.accept(false)
            })
        
        let surveyHistorySections = ip.surveyHistory.asDriver().map { surveys -> [SurveyListSection] in
            return [SurveyListSection(model: "SurveyList", items: surveys)]
        }
        
        return VNP4TFormRegisterInformationDatePickerVM.Output(
            surveyHistoryResult: surveyHistoryResult,
            moreSurveyHistoryResult: moreSurveyHistoryResult,
            surveyHistorySections: surveyHistorySections)
    }
}
