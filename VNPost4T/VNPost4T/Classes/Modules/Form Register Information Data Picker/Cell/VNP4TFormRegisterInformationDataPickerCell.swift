//
//  VNP4TFormRegisterInformationDataPickerCell.swift
//  
//
//  Created by AEGONA on 9/14/20.
//

import Foundation
import UIKit
import RxCocoa

class VNP4TFormRegisterInformationDataPickerCell: UITableViewCell{
    static let cellIdentifier = "VNP4TFormRegisterInformationDataPickerCell"
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbCode: UILabel!
    
    var onClickItem : ((VNP4TSurveyClassificationTypeSelected?)->())?
    
    var index : Int = 0 {
        didSet {
            if index % 2 == 0 {
                containerView.backgroundColor = UIColor(hex: "EEF3FF")
            } else {
                containerView.backgroundColor = UIColor.white
            }
        }
    }
    
    var data : VNP4TSurveyClassificationTypeSelected? {
        didSet{
            lbName.text = data?.name
            lbCode.text = data?.code
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapDeadSpace = UITapGestureRecognizer(target: self, action: #selector(onClick))
        containerView.addGestureRecognizer(tapDeadSpace)
    }
    
    @objc func onClick(_ sender: Any){
        self.onClickItem?(data)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
