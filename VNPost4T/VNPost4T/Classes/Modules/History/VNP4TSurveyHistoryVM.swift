//
//  VNP4TSurveyHistoryVM.swift
//  VNPost4T
//
//  Created by AEGONA on 9/29/20.
//

import Foundation
import RxCocoa
import RxSwift
import RxDataSources


typealias SurveySection = SectionModel<String,VNP4TSurvey>

class VNP4TSurveyHistoryVM: VNP4TViewModelProtocol {
    
    typealias Dependency = VNP4TSurveyService
    
    struct Input {
        let isLoading                               :BehaviorRelay<Bool>
        let loadTrigger                             :BehaviorRelay<Bool>
        let dataAll                                 :BehaviorRelay<[VNP4TSurvey]>
        let dataDraft                               :BehaviorRelay<[VNP4TSurvey]>
        let dataImage                               :BehaviorRelay<[VNP4TSurvey]>
        let dataShow                                :BehaviorRelay<[VNP4TSurvey]>
        let pageSize                                :BehaviorRelay<Int>
        let pageIndexAll                            :BehaviorRelay<Int>
        let pageIndexDraft                          :BehaviorRelay<Int>
        let pageIndexImage                          :BehaviorRelay<Int>
        let search                                  :BehaviorRelay<String>
        let currentTab                              :BehaviorRelay<Int>
        let moreTrigger                             :BehaviorRelay<Bool>
        let filterSurveyType                        :BehaviorRelay<VNP4TSurveyClassificationTypeSelected>
        let filterStatus                            :BehaviorRelay<VNP4TItemSelect>
        let filterAction                            :BehaviorRelay<VNP4TItemSelect>
    }
    
    struct Output {
        let dataResult                               :Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurvey]>>>
        let dataMoreResult                           :Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurvey]>>>
        let dataSections                             :Driver<[SurveySection]>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input: Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: VNP4TSurveyService?) -> Output? {
        self.input = input
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else { return nil }
        
        let allTrigger = Observable.merge(ip.loadTrigger.asObservable().filter {$0}.withLatestFrom(ip.search),ip.search.asObservable())
        
        let allResult = allTrigger.asObservable()
            .debounce(DispatchTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .do(onNext: { _ in
                ip.isLoading.accept(true)
                ip.pageIndexAll.accept(1)
                ip.pageIndexImage.accept(1)
                ip.pageIndexDraft.accept(1)
            }).flatMapLatest { search -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurvey]>>> in
                let currentTab = ip.currentTab.value
                let surveyId = ip.filterSurveyType.value.id != "" ? ip.filterSurveyType.value.id : nil
                let collectionType = (ip.filterStatus.value.id != nil && ip.filterStatus.value.id != -1) ? ip.filterStatus.value.id : nil
                let status = (ip.filterAction.value.id != nil && ip.filterAction.value.id != -1) ? ip.filterAction.value.id : nil
                return dp.getSurveyHistory(tab: currentTab, page: 1, size: ip.pageSize.value, search : search, surveyId: surveyId, collectionType: collectionType, surveyStatus: status)
            }.do(onNext: { (_) in
                ip.isLoading.accept(false)
            })
        
        let moreResult = ip.moreTrigger.asObservable()
            .filter {$0}
            .withLatestFrom(ip.search)
            .do(onNext: { _ in
                ip.isLoading.accept(true)
            }).flatMapLatest { search -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurvey]>>> in
                let index = ip.currentTab.value
                let page = index == 0 ? ip.pageIndexAll.value : index == 1 ? ip.pageIndexDraft.value : ip.pageIndexImage.value
                let surveyId = ip.filterSurveyType.value.id != "" ? ip.filterSurveyType.value.id : nil
                let collectionType = (ip.filterStatus.value.id != nil && ip.filterStatus.value.id != -1) ? ip.filterStatus.value.id : nil
                let status = (ip.filterAction.value.id != nil && ip.filterAction.value.id != -1) ? ip.filterAction.value.id : nil
                return dp.getSurveyHistory(tab: index, page: page + 1, size: ip.pageSize.value, search : search, surveyId: surveyId, collectionType: collectionType, surveyStatus: status)
            }.do(onNext: { _ in
                ip.isLoading.accept(false)
            })
        
        let surveySections = ip.dataShow.asDriver().map { data -> [SurveySection] in
            return [SurveySection(model: "SurveyHistory", items: data)]
        }
        
        return VNP4TSurveyHistoryVM.Output(dataResult: allResult,
                                           dataMoreResult : moreResult,
                                           dataSections: surveySections)
    }
    
}
