//
//  VNP4TSurveyHistoryCell.swift
//  VNPost4T
//
//  Created by AEGONA on 9/29/20.
//

import Foundation
import UIKit
import RxCocoa

class VNP4TSurveyHistoryCell: UITableViewCell{
    static let cellIdentifier = "VNP4TSurveyHistoryCell"
    
    @IBOutlet weak var lbType: UILabel!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbEmployee: UILabel!
    @IBOutlet weak var lbPhone: UILabel!
    @IBOutlet weak var lbStatus: UILabel!
    @IBOutlet weak var lbSurveyCode: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbSurveyStatus: UILabel!
    @IBOutlet weak var imgTick: UIImageView!
    @IBOutlet weak var viewContain: UIView!
    
    var onSelectedItem : ((VNP4TSurvey?)->())?
    
    var data : VNP4TSurvey? {
        didSet {
            lbSurveyStatus.isHidden = true
            imgTick.isHidden = true
            if data?.surveyClassifiation == VNP4TSurveyType.ELECTRICAL.rawValue {
                lbType.text = "E"
                lbEmployee.text = data?.fullName
                lbPhone.text = data?.phoneNumber
                switch data?.surveySheetStatus {
                case VNP4TSurveyStatus.NEW.rawValue:
                    lbStatus.text = "4t_survey_history_draft".localized
                    lbSurveyStatus.isHidden = true
                    break
                case VNP4TSurveyStatus.APPROVED.rawValue :
                    lbStatus.text = "4t_status_approve".localized
                    switch data?.evaluate {
                    case VNP4TEvaluate.GOOD.rawValue:
                        lbSurveyStatus.isHidden = false
                        let text = "\(Int(data?.percent ?? 0.0))% " + "4t_survey_approved_good_title".localized
                        lbSurveyStatus.text = text
                        lbSurveyStatus.textColor = .green
                        lbSurveyStatus.backgroundColor = UIColor(red: 0, green: 177, blue: 106, alpha: 0.16)
                        break
                    case VNP4TEvaluate.BAD.rawValue :
                        lbSurveyStatus.isHidden = false
                        let text = "\(Int(data?.percent ?? 0.0))% " + "4t_survey_approved_bad_title".localized
                        lbSurveyStatus.text = text
                        lbSurveyStatus.textColor = UIColor(hex: "F64744")
                        lbSurveyStatus.backgroundColor = UIColor(hex: "FDE0DF")
                        break
                    default:
                        lbSurveyStatus.isHidden = true
                    }
                case VNP4TSurveyStatus.CANCEL.rawValue:
                    lbStatus.text = "4t_status_deny".localized
                    break
                default:
                    lbStatus.text = ""
                }
            } else {
                lbType.text = "F"
                lbEmployee.text = "4t_image_physical_title".localized
                lbPhone.text = ""
                switch data?.surveySheetStatus {
                case VNP4TSurveyStatus.APPROVED.rawValue:
                    lbStatus.text = "4t_status_approve".localized
                    lbSurveyStatus.isHidden = false
                    let text = "4t_survey_approved_good_title".localized
                    lbSurveyStatus.text = text
                    lbSurveyStatus.textColor = .green
                    lbSurveyStatus.backgroundColor = UIColor(red: 0, green: 177, blue: 106, alpha: 0.16)
                    break
                case VNP4TSurveyStatus.CANCEL.rawValue:
                    lbStatus.text = "4t_status_deny".localized
                    lbSurveyStatus.isHidden = false
                    let text = "4t_survey_approved_bad_title".localized
                    lbSurveyStatus.text = text
                    lbSurveyStatus.textColor = UIColor(hex: "F64744")
                    lbSurveyStatus.backgroundColor = UIColor(hex: "FDE0DF")
                    break
                default:
                    lbStatus.text = ""
                }
            }
            
            if data?.surveySheetStatus == VNP4TSurveyStatus.PROCRESSING.rawValue {
                lbStatus.text = "4t_status_processing".localized
                imgTick.isHidden = false
            } else {
                imgTick.isHidden = true
            }
            
            lbDate.text = data?.addedStamp.substring(0..<10).vnp4TFormatDateInput()
            lbTitle.text = data?.name
            lbSurveyCode.text = data?.surveySheetCode
            
            let tapView = UITapGestureRecognizer(target: self, action: #selector(self.onClickItem))
            viewContain.addGestureRecognizer(tapView)
        }
    }
    
    @objc func onClickItem(sender: UITapGestureRecognizer){
        self.onSelectedItem?(data)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
