//
//  VNP4TSurveyHistoryVC.swift
//  VNPost4T
//
//  Created by AEGONA on 9/29/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

typealias VNP4TSurveyHistorySource = RxTableViewSectionedReloadDataSource<SurveySection>

class VNP4TSurveyHistoryVC : VNP4TBaseViewController {
    
    let maxHeaderHeight: CGFloat = 60
    let minHeaderHeight: CGFloat = 10
    var previousScrollOffset: CGFloat = 0
    
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var searchViewConstraints: NSLayoutConstraint!
    @IBOutlet weak var containViewConstraints: UIView!
    @IBOutlet weak var imgEmpty: UIImageView!
    @IBOutlet weak var lbTotal: UILabel!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var viewBackgroundBar: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var segment: UISegmentedControl!
    
    
    var viewModel : VNP4TSurveyHistoryVM!
    
    private var dataSource : VNP4TSurveyHistorySource!
    
    private var loading: VNP4TLoadingProgressView!
    
    private var refreshControl : UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        configDataSource()
        setupView()
        setupViewModel()
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onChangeTab(_ sender: UISegmentedControl) {
        viewModel.input?.filterSurveyType.accept(VNP4TSurveyClassificationTypeSelected())
        viewModel.input?.filterStatus.accept(VNP4TItemSelect())
        viewModel.input?.filterAction.accept(VNP4TItemSelect())
        if self.viewModel.input?.currentTab.value != sender.selectedSegmentIndex {
            self.tfSearch.text = ""
            self.viewModel.input?.search.accept("")
            self.viewModel.input?.currentTab.accept(sender.selectedSegmentIndex)
            self.viewModel.input?.loadTrigger.accept(true)
        }
    }
    
    @IBAction func onFilter(_ sender: Any) {
        let bundel = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        let filterVC = VNP4TFilterHistorySurveyVC(nibName: "VNP4TFilterHistorySurveyVC", bundle: bundel)
        filterVC.viewModel = VNP4TFilterHistorySurveyVM(input: VNP4TFilterHistorySurveyVM.Input(itemSurveyType: BehaviorRelay<VNP4TSurveyClassificationTypeSelected>(value: self.viewModel.input?.filterSurveyType.value ?? VNP4TSurveyClassificationTypeSelected()),
                                                                                                itemStatusSurvey: BehaviorRelay<VNP4TItemSelect>(value: self.viewModel.input?.filterStatus.value ?? VNP4TItemSelect()),
                                                                                                itemSurveyAction: BehaviorRelay<VNP4TItemSelect>(value: self.viewModel.input?.filterAction.value ?? VNP4TItemSelect()),
                                                                                                dataStatusSurvey: BehaviorRelay<[VNP4TItemSelect]>(value: []),
                                                                                                dataSurveyAction: BehaviorRelay<[VNP4TItemSelect]>(value: []),
                                                                                                currentTab: BehaviorRelay<Int>(value: self.viewModel.input?.currentTab.value ?? 0)),
                                                        dependency: VNP4TSurveyService())
        
        filterVC.onFilter = { (type,status,action) in
            self.viewModel.input?.filterSurveyType.accept(type ?? VNP4TSurveyClassificationTypeSelected())
            self.viewModel.input?.filterStatus.accept(status ?? VNP4TItemSelect())
            self.viewModel.input?.filterAction.accept(action ?? VNP4TItemSelect())
            self.viewModel.input?.loadTrigger.accept(true)
        }
        self.navigationController?.pushViewController(filterVC, animated: true)
    }
}

extension VNP4TSurveyHistoryVC : VNP4TViewControllerProtocol {
    func setupView() {
   
        self.lbTitle.text = "4t_survey_history_title".localized
        self.tfSearch.placeholder = "4t_survey_history_search_placeholder".localized
        self.segment.setTitle("4t_survey_history_all".localized, forSegmentAt: 0)
        self.segment.setTitle("4t_survey_history_draft".localized, forSegmentAt: 1)
        self.segment.setTitle("4t_survey_history_image".localized, forSegmentAt: 2)
        
        loading = VNP4TLoadingProgressView(uiScreen: UIScreen.main)
        self.view.addSubview(self.loading)
        
        self.viewBackgroundBar.VNP4TsetGradientBackground(colorStart: VNP4TConstants.navigationBarMainColor, colorEnd: VNP4TConstants.navigationBarSubColor)
        
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = .clear
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        tbView.refreshControl = refreshControl
    }
    
    @objc private func refresh() {
        self.viewModel.input?.loadTrigger.accept(true)
    }
    
    func setupViewModel() {
        if let ip = self.viewModel.input {
            bindingInput(input: ip)
        }
        if let op = self.viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input :VNP4TSurveyHistoryVM.Input){
        
        self.viewModel.input?.isLoading.subscribe { [weak self] isLoading in
            guard let self = self else {return}
            
            if isLoading.element ?? false {
                self.loading.startAnimating()
            } else {
                DispatchQueue.main.asyncAfter(wallDeadline: .now() + 0.5) {
                    self.loading.stopAnimating()
                }
            }
        }
        
        let loadMore = self.tbView.rx.contentOffset.asDriver()
            //            .debounce(RxTimeInterval.milliseconds(300))
            .flatMap { [weak self] offset -> Driver<Bool> in
                guard let self = self else { return Driver.empty() }
                
                switch self.viewModel.input?.currentTab.value {
                case 0:
                    if (((self.viewModel.input?.pageIndexAll.value ?? 0) * 20) - (self.viewModel.input?.dataAll.value.count ?? 0)) == 0 {
                        return isNearTheBottomEdge(contentOffset: offset, self.tbView) ? Driver.just(true) : Driver.empty()
                    }
                    break
                case 1:
                    if (((self.viewModel.input?.pageIndexDraft.value ?? 0) * 20) - (self.viewModel.input?.dataDraft.value.count ?? 0)) == 0 {
                        return isNearTheBottomEdge(contentOffset: offset, self.tbView) ? Driver.just(true) : Driver.empty()
                    }
                    break
                case 2:
                    if (((self.viewModel.input?.pageIndexImage.value ?? 0) * 20) - (self.viewModel.input?.dataImage.value.count ?? 0)) == 0 {
                        return isNearTheBottomEdge(contentOffset: offset, self.tbView) ? Driver.just(true) : Driver.empty()
                    }
                    break
                default:
                    print("-")
                }
                return Driver.empty()
            }
        
        loadMore.drive(input.moreTrigger).disposed(by: disposeBag)
        
        tfSearch.rx.text.orEmpty.asDriver()
            .distinctUntilChanged()
            .debounce(RxTimeInterval.milliseconds(500))
            .drive(input.search)
            .disposed(by: disposeBag)
    }
    
    private func bindingOutput(output : VNP4TSurveyHistoryVM.Output){
        output.dataSections.drive(self.tbView.rx.items(dataSource: self.dataSource)).disposed(by: disposeBag)
        
        output.dataResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
            }
            if result.success {
                self.viewModel.input?.dataShow.accept(result.data?.data ?? [])
                switch self.viewModel.input?.currentTab.value {
                case 0:
                    self.viewModel.input?.dataAll.accept(result.data?.data ?? [])
                    break
                case 1:
                    self.viewModel.input?.dataDraft.accept(result.data?.data ?? [])
                    break
                case 2:
                    self.viewModel.input?.dataImage.accept(result.data?.data ?? [])
                    break
                default:
                    print("-")
                }
                if result.data?.data?.count ?? 0 > 0 {
                    self.imgEmpty.isHidden = true
                } else {
                    self.imgEmpty.isHidden = false
                }
                let stringBold = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.black]
                let stringNomal = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)]
                
                let attributedString1 = NSMutableAttributedString(string: "\(result.data?.total ?? 0) ", attributes:stringBold)
                let attributedString2 = NSMutableAttributedString(string: "4t_survey_history_total_survey".localized, attributes:stringNomal)
                
                attributedString1.append(attributedString2)
                self.lbTotal.attributedText = attributedString1
            }
        }).disposed(by: disposeBag)
        
        output.dataMoreResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.success {
                var data = self.viewModel.input?.dataShow.value ?? []
                data += result.data?.data ?? []
                
                self.viewModel.input?.dataShow.accept(data)
                if result.data?.data?.count ?? 0 > 0 {
                    switch self.viewModel.input?.currentTab.value {
                    case 0:
                        self.viewModel.input?.dataAll.accept(data)
                        var currentPage = self.viewModel.input?.pageIndexAll.value ?? 0
                        currentPage += 1
                        self.viewModel.input?.pageIndexAll.accept(currentPage)
                        break
                    case 1:
                        self.viewModel.input?.dataDraft.accept(data)
                        var currentPage = self.viewModel.input?.pageIndexDraft.value ?? 0
                        currentPage += 1
                        self.viewModel.input?.pageIndexDraft.accept(currentPage)
                        break
                    case 2:
                        self.viewModel.input?.dataImage.accept(data)
                        var currentPage = self.viewModel.input?.pageIndexImage.value ?? 0
                        currentPage += 1
                        self.viewModel.input?.pageIndexImage.accept(currentPage)
                        break
                    default:
                        print("-")
                    }
                }
            }
        }).disposed(by: disposeBag)
        
    }
}

extension VNP4TSurveyHistoryVC : ListViewControllerProtocol {
    func registerCell() {
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        tbView.register(UINib(nibName: VNP4TSurveyHistoryCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TSurveyHistoryCell.cellIdentifier)
    }
    
    func configDataSource() {
        let ds = RxTableViewSectionedReloadDataSource<SurveySection>(
          configureCell: { dataSource, tableView, indexPath, data in
            
            let cell : VNP4TSurveyHistoryCell = tableView.dequeueReusableCell(withIdentifier: VNP4TSurveyHistoryCell.cellIdentifier, for: indexPath) as! VNP4TSurveyHistoryCell
                      
                      cell.data = data
                      cell.onSelectedItem = { item in
                          
                          let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                          if item?.surveyClassifiation == VNP4TSurveyType.ELECTRICAL.rawValue {
                              let detailVC = VNP4TSurveyElectricDetailVC(nibName: "VNP4TSurveyElectricDetailVC", bundle: bundle)
                              detailVC.viewModel = VNP4TSurveyElectricDetailVM(input: VNP4TSurveyElectricDetailVM.Input(status: BehaviorRelay<Int>(value: data.surveySheetStatus ?? 0),
                                                                                                                        isLoading: BehaviorRelay<Bool>(value: true),
                                                                                                                        survey: BehaviorRelay<VNP4TSurvey>(value: data),
                                                                                                                        dataShow: BehaviorRelay<[VNP4TSummarySurvey]>(value: []),
                                                                                                                        dataSurvey: BehaviorRelay<[VNP4TSurveyQuestion]>(value: []),
                                                                                                                        surveyDetail: BehaviorRelay<VNP4TSurveyDetail>(value: VNP4TSurveyDetail()),
                                                                                                                        saveDraftTrigger: BehaviorRelay<Bool>(value: false),
                                                                                                                        dataUpdate: BehaviorRelay<VNP4TQuestionKey>(value: VNP4TQuestionKey()),
                                                                                                                        submitTrigger: BehaviorRelay<Bool>(value: false),
                                                                                                                        deleteTrigger: BehaviorRelay<Bool>(value: false), isShowAction: BehaviorRelay<Bool>(value: false)),
                                                                               dependency: VNP4TSurveyService())
                              detailVC.onRefreshData = {
                                  self.viewModel.input?.loadTrigger.accept(true)
                              }
                              self.navigationController?.pushViewController(detailVC, animated: true)
                          } else {
                              let detailVC = VNP4TSurveyPhysicalDetailVC(nibName: "VNP4TSurveyPhysicalDetailVC", bundle: bundle)
                              detailVC.viewModel = VNP4TSurveyPhysicalDetailVM(input: VNP4TSurveyPhysicalDetailVM.Input(
                                                                                  status: BehaviorRelay<Int>(value: data.surveySheetStatus ?? 0),
                                                                                  isLoading: BehaviorRelay<Bool>(value: false),
                                                                                  triggerDetail: BehaviorRelay<Bool>(value: true),
                                                                                  survey: BehaviorRelay<VNP4TSurvey>(value: data),
                                                                                  dataDetail: BehaviorRelay<VNP4TSurveyDetail>(value: VNP4TSurveyDetail()),
                                                                                  images: BehaviorRelay<[VNP4TImage]>(value: []),
                                                                                  initTotalImage: BehaviorRelay<Int>(value: 0),
                                                                                  imageUrl: BehaviorRelay<[String]>(value: []),
                                                                                  triggerUpdate: BehaviorRelay<Bool>(value: false)),
                                                                               dependency: VNP4TSurveyService())
                              detailVC.onRefreshData = {
                                  self.viewModel.input?.loadTrigger.accept(true)
                              }
                              self.navigationController?.pushViewController(detailVC, animated: true)
                          }
                      }
                      return cell
        })
    
        dataSource = ds
    }
}

extension VNP4TSurveyHistoryVC {
    func canAnimateHeader (_ scrollView: UIScrollView) -> Bool {
        let scrollViewMaxHeight = scrollView.frame.height + self.searchViewConstraints.constant - minHeaderHeight
        return scrollView.contentSize.height > scrollViewMaxHeight
    }
    
    func setScrollPosition() {
        self.tbView.contentOffset = CGPoint(x:0, y: 0)
    }
}

extension VNP4TSurveyHistoryVC {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollDiff = (scrollView.contentOffset.y - previousScrollOffset)
        let isScrollingDown = scrollDiff > 0
        let isScrollingUp = scrollDiff < 0
        if canAnimateHeader(scrollView) {
            var newHeight = searchViewConstraints.constant
            if isScrollingDown {
                newHeight = max(minHeaderHeight, searchViewConstraints.constant - abs(scrollDiff))
            } else if isScrollingUp {
                newHeight = min(maxHeaderHeight, searchViewConstraints.constant + abs(scrollDiff))
            }
            if newHeight != searchViewConstraints.constant {
                searchViewConstraints.constant = newHeight
                setScrollPosition()
                previousScrollOffset = scrollView.contentOffset.y
            }
        }
    }
}
