//
//  VNP4TSummaryElectrictSurveyVM.swift
//  VNPost4T
//
//  Created by AEGONA on 9/24/20.
//

import Foundation
import RxCocoa
import RxSwift
import RxDataSources
import ObjectMapper

typealias SummarySurveySection = SectionModel<String,VNP4TSummarySurvey>

class VNP4TSummaryElectrictSurveyVM: VNP4TViewModelProtocol {
    
    typealias Dependency = VNP4TSurveyService
    
    struct Input {
        let isLoading                       :BehaviorRelay<Bool>
        let keys                            :BehaviorRelay<VNP4TSurveyClassificationTypeSelected>
        let questions                       :BehaviorRelay<[VNP4TSurveyQuestion]>
        let dataQuestionsShow               :BehaviorRelay<[VNP4TSummarySurvey]>
        let collectionType                  :BehaviorRelay<Int>
        let submitTrigger                   :BehaviorRelay<Bool>
        let dataSubmit                      :BehaviorRelay<VNP4TQuestionKey>
    }
    
    struct Output {
        let questionSections                :Driver<[SummarySurveySection]>
        let submitResult                    :Observable<VNP4TResponseModel<Any>>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input :Input,dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: VNP4TSurveyService?) -> Output? {
        self.input = input
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        
        let questionSections = ip.dataQuestionsShow.asDriver().map { question -> [SummarySurveySection] in
            return [SummarySurveySection(model: "Summary", items: question)]
        }
        
        let submitResult = ip.submitTrigger.asObservable()
            .filter {$0}
            .withLatestFrom(ip.dataSubmit)
            .do(onNext: { (_) in
                ip.isLoading.accept(true)
            }).flatMapLatest { data -> Observable<VNP4TResponseModel<Any>> in
                return dp.createElectrictSurvey(survey: data)
            }.do (onNext: { _ in
                ip.isLoading.accept(false)
            })
        
        return VNP4TSummaryElectrictSurveyVM.Output(
            questionSections: questionSections,
            submitResult: submitResult)
    }
    
    func uploadImage(){
        Observable.just(input?.questions.value)
            .do(onNext: { _ in
                self.input?.isLoading.accept(true)
            })
            .subscribe(onNext: {[weak self] result in
                guard let self = self else {return}
                var arrImage : [VNP4TListImage] = []
                var isPass = false
                result?.forEach({ (item) in
                    if item.dataType == VNP4TQuestionType.PHOTO.rawValue {
                        var listImg : [VNP4TImage] = []
                        if item.listImage.count > 0 {
                            item.listImage.forEach { (img) in
                                if img.image != nil {
                                    isPass = true
                                }
                                listImg.append(VNP4TImage(image: img.image, url: img.url))
                            }
                            arrImage.append(VNP4TListImage(id: item.id, images: listImg))
                        }
                    }
                })
                if isPass {
                    resizeAndUploadListImage(listImage: arrImage, storage: VNP4TManager.shared.storage, typeImage: VNP4TTypeSurveyUploadImage.ELECTRICAL.rawValue, userId: VNP4TManager.shared.userId) { (url, fileName) in
                        let draftData = self.convertDataSubmit(listImage: fileName)
                        self.input?.dataSubmit.accept(draftData)
                        self.input?.submitTrigger.accept(true)
                    }
                } else {
                    let draftData = self.convertDataSubmit(listImage: nil)
                    self.input?.dataSubmit.accept(draftData)
                    self.input?.submitTrigger.accept(true)
                }
            })
    }
    
    private func convertDataSubmit(listImage: [VNP4TListImage]?) -> VNP4TQuestionKey{
        let data = self.input?.questions.value
        let dataKeys = self.input?.keys.value
        let collectionType = self.input?.collectionType.value
        let dataSubmit = VNP4TQuestionKey()
        if data != nil {
            dataSubmit.surveyId = dataKeys != nil ? dataKeys!.id : ""
            dataSubmit.collectionType = collectionType ?? 0
            dataSubmit.status = VNP4TSurveySaveType.SUBMIT.rawValue
            dataSubmit.answers = []
            data?.forEach({ survey in
                if survey.step == 0 {
                    if survey.type == VNP4TQuestionCustomerType.DOB.rawValue ||  survey.type == VNP4TQuestionCustomerType.ISSUE_ID_NUMBER_DATE.rawValue {
                        survey.answerValue = survey.answerValue.vnp4TFormatDateSubmit() ?? survey.answerValue
                    }
                    if survey.type == VNP4TQuestionCustomerType.TEMPORARY_ADDRESS.rawValue || survey.type == VNP4TQuestionCustomerType.PERMANENT_ADDRESS.rawValue {
                        if survey.address?.province != nil {
                            survey.answerValue = survey.convertAddressToJson()
                        }
                    }
                } else {
                    if survey.dataType == VNP4TQuestionType.DATE.rawValue {
                        survey.answerValue = survey.answerValue.vnp4TFormatDateSubmit() ?? survey.answerValue
                    }
                    if survey.dataType == VNP4TQuestionType.PHOTO.rawValue {
                        var listImg : [String] = []
                        if listImage != nil {
                            listImage?.forEach({ itemImage in
                                if (survey.id == itemImage.id) {
                                    itemImage.images?.forEach({ url in
                                        listImg.append(url.url ?? "")
                                    })
                                    
                                }
                            })
                            
                        } else {
                            survey.listImage.forEach { img in
                                listImg.append(getFileNameFromFullUrl(url: img.url ?? ""))
                            }
                        }
                        survey.answerValue = "\(listImg)"
                    }
                    if survey.type == VNP4TQuestionCustomerType.TEMPORARY_ADDRESS.rawValue || survey.type == VNP4TQuestionCustomerType.PERMANENT_ADDRESS.rawValue {
                        if survey.address?.province != nil {
                            survey.answerValue = survey.convertAddressToJson()
                        }
                    }
                    if survey.dataType == VNP4TQuestionType.DOUBLE.rawValue {
                        var stringText = survey.answerValue.replacingOccurrences(of: ",", with: "")
                        stringText = stringText.replacingOccurrences(of: ".", with: "")
                        survey.answerValue = stringText
                    }
                }
                dataSubmit.answers.append(VNP4TQuestion(id: survey.id, value: survey.answerValue, answerId: survey.answerId, step: survey.step, profileType: survey.step == 0 ? survey.type : nil))
            })
        }
        return dataSubmit
    }
    
}
