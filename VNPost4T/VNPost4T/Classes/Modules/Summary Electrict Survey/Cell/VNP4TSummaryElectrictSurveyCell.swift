//
//  VNP4TSummaryElectrictSurveyCell.swift
//  VNPost4T
//
//  Created by AEGONA on 9/24/20.
//

import Foundation
import UIKit
import RxCocoa

class VNP4TSummaryElectrictSurveyCell: UITableViewCell{
    static let cellIdentifier = "VNP4TSummaryElectrictSurveyCell"
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var viewVC: UIView!
    @IBOutlet weak var viewContent: UIView!
    
    var onChangeValue : ((VNP4TSurveyQuestion)->())?
    
    var data : VNP4TSummarySurvey? {
        didSet {
            lbTitle.text = data?.title
            if data?.questions?.count ?? 0 > 0 {
                
                let vc = VNP4TSurveyQuestionsVC(nibName: "VNP4TSurveyQuestionsVC", bundle: Bundle(identifier: VNP4TConstants.bundleIdentifier))
                vc.viewModel = VNP4TSurveyQuestionVM(
                    input: VNP4TSurveyQuestionVM.Input(
                        dataQuestions: BehaviorRelay<[VNP4TSurveyQuestion]>(value: data?.questions ?? []),
                        isScroll: BehaviorRelay<Bool>(value: false)),
                    dependency: VNP4TSurveyService())
                
                vc.onChangeValue = { item in
                    self.onChangeValue?(item)
                }
                
                self.viewContainingController()?.addChild(vc)
                
                // Add Child View as Subview
                self.viewVC.addSubview(vc.view)
                
                // Configure Child View
                vc.view.frame = self.viewVC.bounds
                vc.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                
                // Notify Child View Controller
                vc.didMove(toParent: self.viewContainingController())
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
