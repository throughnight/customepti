//
//  VNP4TSummaryElectrictSurveyVC.swift
//  VNPost4T
//
//  Created by AEGONA on 9/24/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

typealias VNP4TSummaryElectrictSurveySource = RxTableViewSectionedReloadDataSource<SummarySurveySection>

class VNP4TSummaryElectrictSurveyVC: VNP4TBaseViewController {
    
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var viewBackgroundToolbar: UIView!
    @IBOutlet weak var lbCollectionType: UILabel!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSend: UIButton!
    
    var viewModel : VNP4TSummaryElectrictSurveyVM!
    
    var dataSource : VNP4TSummaryElectrictSurveySource!
    
    private var loading: VNP4TLoadingProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loading = VNP4TLoadingProgressView(uiScreen: UIScreen.main)
        self.view.addSubview(loading)
        
        registerCell()
        configDataSource()
        setupView()
        setupViewModel()
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCancel(_ sender: Any) {
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        let dialogOptions = VNP4TDialogOptions(nibName: "VNP4TDialogOptions", bundle: bundle)
        dialogOptions.modalPresentationStyle = .overFullScreen
        dialogOptions.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_cancel_survey".localized)
        dialogOptions.messageDialog = BehaviorRelay<String>(value: "")
        dialogOptions.onCancelAction = {
            dialogOptions.dismiss(animated: false, completion: nil)
        }
        
        dialogOptions.onDoneActions = { [weak self] in
            dialogOptions.dismiss(animated: false, completion: nil)
            guard let self = self else { return }
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true)
        }
        self.parent?.present(dialogOptions, animated: false, completion: nil)
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        let dialogOptions = VNP4TDialogOptions(nibName: "VNP4TDialogOptions", bundle: bundle)
        dialogOptions.modalPresentationStyle = .overFullScreen
        dialogOptions.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_submit_survey".localized)
        dialogOptions.messageDialog = BehaviorRelay<String>(value: "")
        dialogOptions.titleActionConfirm = BehaviorRelay<String?>(value: "4t_physical_submit_button".localized)
        dialogOptions.onCancelAction = {
            dialogOptions.dismiss(animated: false, completion: nil)
        }
        
        dialogOptions.onDoneActions = { [weak self] in
            dialogOptions.dismiss(animated: false, completion: nil)
            guard let self = self else { return }
            self.viewModel.uploadImage()
        }
        self.parent?.present(dialogOptions, animated: false, completion: nil)
    }
}

extension VNP4TSummaryElectrictSurveyVC : VNP4TViewControllerProtocol {
    func setupView() {
        self.tbView.rx.setDelegate(self).disposed(by: disposeBag)
        self.viewBackgroundToolbar.VNP4TsetGradientBackground(colorStart: VNP4TConstants.navigationBarMainColor, colorEnd: VNP4TConstants.navigationBarSubColor)
        if self.viewModel.input?.collectionType.value ?? 0 == VNP4TCollectionType.NEW.rawValue {
            self.lbCollectionType.text = "4t_summary_electrict_survey_type_new".localized
        } else {
            self.lbCollectionType.text = "4t_summary_electrict_survey_type_old".localized
        }
        
        self.lbTitle.text = "4t_create_electrict_survey_title".localized
        self.btnCancel.setTitle("4t_register_survey_cancel".localized, for: .normal)
        self.btnSend.setTitle("4t_physical_submit_button".localized, for: .normal)
    }
    
    func setupViewModel() {
        if let ip = self.viewModel.input {
            bindingInput(input: ip)
        }
        if let op = self.viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input : VNP4TSummaryElectrictSurveyVM.Input){
        var summaryData : [VNP4TSummarySurvey] = []
        summaryData.append(handleConvertSurveyType())
        summaryData += handleConvertDataQuestion()
        
        self.viewModel.input?.dataQuestionsShow.accept(summaryData)
        
        input.isLoading.subscribe { [weak self] isLoading in
            guard let self = self else {return}
            
            if isLoading.element ?? false {
                self.loading.startAnimating()
            } else {
                self.loading.stopAnimating()
            }
        }
    }
    
    private func bindingOutput(output: VNP4TSummaryElectrictSurveyVM.Output){
        output.questionSections.drive(self.tbView.rx.items(dataSource: self.dataSource)).disposed(by: disposeBag)

        output.submitResult.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            
            if result.statusCode == 200 {
                if result.success == true {
                    let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                    let successDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                    successDialog.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_success".localized.uppercased())
                    successDialog.messageDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_message_success_1".localized + " " + (self.viewModel.input?.keys.value.code ?? "") + " " + "4t_summary_electrict_survey_message_success_2".localized)
                    successDialog.status = BehaviorRelay<Bool>(value: true)
                    successDialog.modalPresentationStyle = .overFullScreen
                    successDialog.onDoneActions = {
                        successDialog.dismiss(animated: true, completion: nil)
                        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                    }
                    
                    self.parent?.present(successDialog, animated: false, completion: nil)
                } else {
                    let message = result.message
                    let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                    let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                    errorDialog.status = BehaviorRelay<Bool>(value: false)
                    errorDialog.modalPresentationStyle = .overFullScreen
                    errorDialog.onDoneActions = {
                        errorDialog.dismiss(animated: true, completion: nil)
                    }
                    
                    if message == VNP4TErrorCode.SURVEY_NOT_CHANGE.rawValue {
                        errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_dialog_error_title".localized.uppercased())
                        errorDialog.messageDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_error_not_change".localized)
                    } else {
                        errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_dialog_error_title".localized.uppercased())
                        errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                    }
                    self.parent?.present(errorDialog, animated: false, completion: nil)
                }
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_error".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: true, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
    }
    
    private func handleConvertSurveyType() -> VNP4TSummarySurvey {
        let itemType = VNP4TSummarySurvey()
        itemType.title = "4t_summary_electrict_survey_title_survey_type".localized.uppercased()
        if let type = self.viewModel.input?.keys.value {
            var typeArr : [VNP4TSurveyQuestion] = []
            let itemCode = VNP4TSurveyQuestion()
            itemCode.answerValue = type.code
            itemCode.key = true
            itemCode.question = "4t_summary_electrict_survey_title_survey_type_1".localized
            itemCode.required = true
            
            let itemName = VNP4TSurveyQuestion()
            itemName.answerValue = type.name
            itemName.key = true
            itemName.question = "4t_summary_electrict_survey_title_survey_type_2".localized
            itemName.required = true
            
            typeArr.append(itemCode)
            typeArr.append(itemName)
            
            itemType.questions = typeArr
        }
        return itemType
    }
    
    private func handleConvertDataQuestion() -> [VNP4TSummarySurvey] {
        var array : [VNP4TSummarySurvey] = []
        if let data = self.viewModel.input?.questions.value {
            var arrProfile : [VNP4TSurveyQuestion] = []
            var arrQuestion : [VNP4TSurveyQuestion] = []
            
            data.forEach { question in
                question.key = true
                if question.step == 0 {
                    arrProfile.append(question)
                } else {
                    arrQuestion.append(question)
                }
            }
            
            let itemProfile = VNP4TSummarySurvey()
            itemProfile.title = "4t_summary_electrict_survey_title_survey_profile".localized.uppercased()
            itemProfile.questions = arrProfile
            
            let itemQuestion = VNP4TSummarySurvey()
            itemQuestion.title = "4t_summary_electrict_survey_title_survey_sheet".localized.uppercased()
            itemQuestion.questions = arrQuestion
            
            array.append(itemProfile)
            array.append(itemQuestion)
        }
        
        return array
    }
}

extension VNP4TSummaryElectrictSurveyVC : ListViewControllerProtocol {
    func registerCell() {
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        tbView.register(UINib(nibName: VNP4TSummaryElectrictSurveyCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TSummaryElectrictSurveyCell.cellIdentifier)
    }
    
    func configDataSource() {
        let ds = RxTableViewSectionedReloadDataSource<SummarySurveySection>(
          configureCell: { dataSource, tableView, indexPath, question in
            
            let cell : VNP4TSummaryElectrictSurveyCell = tableView.dequeueReusableCell(withIdentifier: VNP4TSummaryElectrictSurveyCell.cellIdentifier, for: indexPath) as! VNP4TSummaryElectrictSurveyCell
                        cell.data = question
                        return cell
        })
        
      
        dataSource = ds
    }
}

extension VNP4TSummaryElectrictSurveyVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let index = indexPath.row
        switch index {
        case 0:
            return 230
        case 1:
            var countAddress : Int = 0
            var countH : Int = 0
            var countQ : Int = 0
            self.viewModel.input?.dataQuestionsShow.value[1].questions?.forEach({ (item) in
                if item.type == VNP4TQuestionCustomerType.TEMPORARY_ADDRESS.rawValue || item.type == VNP4TQuestionCustomerType.PERMANENT_ADDRESS.rawValue {
                    if item.answerValue.count > 30 {
                        countAddress += 1
                    }
                } else {
                    if item.answerValue.count > 30 {
                        countH += 1
                    }
                }
                if item.question.count > 35 {
                    countQ += 1
                }
            })
            let heightView = (((self.viewModel.input?.dataQuestionsShow.value[1].questions?.count ?? 0)) * 95)
            return CGFloat(heightView + (countAddress * 30) + (countH * 20) + (countQ * 10))
        case 2:
            var count: Int = 0
            var countBool : Int = 0
            var countH : Int = 0
            self.viewModel.input?.dataQuestionsShow.value[2].questions?.forEach({ (item) in
                if item.dataType == VNP4TQuestionType.PHOTO.rawValue {
                    count += 1
                }
                if item.dataType == VNP4TQuestionType.BOOLEAN.rawValue {
                    countBool += 1
                }
                if item.question.count > 30 {
                    countH += 1
                }   
            })
            let heightPhoto = count * 152
            let heightView = (((self.viewModel.input?.dataQuestionsShow.value[2].questions?.count ?? 0)) * 105)
            let totalHeight = CGFloat(heightView + 20 + heightPhoto + (countBool * 13) + (countH * 20))
            return totalHeight
        default:
            return 230
        }
    }
}
