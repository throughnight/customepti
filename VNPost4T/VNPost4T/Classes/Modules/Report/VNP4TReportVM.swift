//
//  VNP4TReportVM.swift
//  VNPost4T
//
//  Created by AEGONA on 10/2/20.
//

import Foundation
import RxCocoa
import RxSwift

class VNP4TReportVM: VNP4TViewModelProtocol {
    
    typealias Dependency = VNP4TReportService
    
    struct Input {
        let isLoading               :BehaviorRelay<Bool>
        let type                    :BehaviorRelay<String>
        let dateFrom                :BehaviorRelay<String>
        let dateTo                  :BehaviorRelay<String>
        let survey                  :BehaviorRelay<VNP4TSurveyClassificationTypeSelected>
        let loadTrigger             :BehaviorRelay<Bool>
        let dataReport              :BehaviorRelay<[VNP4TReport]>
    }
    
    struct Output {
        let reportResult            :Observable<VNP4TResponseModel<[VNP4TReport]>>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    
    init(input: Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: VNP4TReportService?) -> Output? {
        self.input = input
        
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        
        let reportResult = ip.loadTrigger.asObservable()
            .filter {$0}
            .withLatestFrom(Observable.combineLatest(ip.type,ip.dateFrom,ip.dateTo,ip.survey))
            .do(onNext: { _ in
                ip.isLoading.accept(true)
            }).flatMapLatest { (type, dateFrom, dateTo, survey) -> Observable<VNP4TResponseModel<[VNP4TReport]>> in
                let rType = type
                var rDateFrom: String? = nil
                var rDateTo: String? = nil
                var rSurveyId: String? = nil
                
                if rType == VNP4TReportType.DAY.rawValue {
                    if dateFrom != "" {
                        rDateFrom = dateFrom.vnp4TFormatDateSubmit()
                    }
                    if dateTo != "" {
                        rDateTo = dateTo.vnp4TFormatDateSubmit()
                    }
                }
                if survey.id != "" {
                    rSurveyId = survey.id
                }
                return dp.getReportMB(type: rType, dateFrom: rDateFrom, dateTo: rDateTo, surveyId: rSurveyId)
            }.do(onNext: { _ in
                ip.isLoading.accept(false)
            })
        
        return VNP4TReportVM.Output(reportResult: reportResult)
    }
}
