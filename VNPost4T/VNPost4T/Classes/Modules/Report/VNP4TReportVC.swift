//
//  VNP4TReportVC.swift
//  VNPost4T
//
//  Created by AEGONA on 10/2/20.
//

import Foundation
import UIKit
import RxCocoa
import Charts

class VNP4TReportVC : VNP4TBaseViewController {
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var lbFrom: UILabel!
    @IBOutlet weak var lbTo: UILabel!
    @IBOutlet weak var viewBackgroundBar: UIView!
    @IBOutlet weak var viewDate: UIStackView!
    @IBOutlet weak var viewDateConstraintsHeight: NSLayoutConstraint!
    @IBOutlet weak var viewContainConstraintsTop: NSLayoutConstraint!
    @IBOutlet weak var viewChart: CombinedChartView!
    @IBOutlet weak var btnValueDateFrom: UIButton!
    @IBOutlet weak var btnValueDateTo: UIButton!
    @IBOutlet weak var btnValueSurvey: UIButton!
    @IBOutlet weak var lbTitleReport: UILabel!
    @IBOutlet weak var lbDateReport: UILabel!
    @IBOutlet weak var lbTilteBonus: UILabel!
    @IBOutlet weak var lbTitleAmount: UILabel!
    @IBOutlet weak var lbBonusSub: UILabel!
    @IBOutlet weak var lbAmountSub: UILabel!
    
    var viewModel : VNP4TReportVM!
    
    private var loading: VNP4TLoadingProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loading = VNP4TLoadingProgressView(uiScreen: UIScreen.main)
        self.view.addSubview(loading)
        
        setupView()
        setupViewModel()
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onPickSurvey(_ sender: Any) {
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        let formRegisterPicker = VNP4TFormRegisterInformationDataPickerVC(nibName: "VNP4TFormRegisterInformationDataPickerVC", bundle: bundle)
        formRegisterPicker.viewModel = VNP4TFormRegisterInformationDatePickerVM(input: VNP4TFormRegisterInformationDatePickerVM.Input(
                                                                                    searchKey: BehaviorRelay<String>(value: ""),
                                                                                    loadTrigger: BehaviorRelay<Bool>(value: true),
                                                                                    loadMoreTrigger: BehaviorRelay<Bool>(value: false),
                                                                                    nextPage: BehaviorRelay<Int>(value: 1),
                                                                                    isEnd: BehaviorRelay<Bool>(value: true),
                                                                                    surveyHistory: BehaviorRelay<[VNP4TSurveyClassificationTypeSelected]>(value: []),
                                                                                    isLoading: BehaviorRelay<Bool>(value: false),
                                                                                    collectionType: BehaviorRelay<Int?>(value: nil)),
                                                                                dependency: VNP4TSurveyService()
        )
        formRegisterPicker.modalPresentationStyle = .overFullScreen
        formRegisterPicker.dataPickedAction = { [weak self] survey in
            guard let self = self else { return }
            
            formRegisterPicker.dismiss(animated: true, completion: nil)
            self.viewModel.input?.survey.accept(survey)
            self.viewModel.input?.loadTrigger.accept(true)
            self.lbTitleReport.text = survey.name
            
            self.btnValueSurvey.setTitle(survey.name, for: .normal)
            self.btnValueSurvey.setTitleColor(.white, for: .normal)
        }
        
        self.parent?.present(formRegisterPicker, animated: false, completion: nil)
    }
    
    @IBAction func onPickFrom(_ sender: Any) {
        onPickDate()
    }
    
    @IBAction func onPickTo(_ sender: Any) {
        onPickDate()
    }
    
    private func onPickDate(){
        VNP4TPicker.selectDate(title: "Từ ngày", cancelText: "Huỷ", doneText: "Ok", datePickerMode: .date, selectedDate: Date(), maxDate: Date()) { (dateFrom) in
            self.viewModel.input?.dateFrom.accept(dateFrom.vnp4TToString(format: "dd/MM/yyyy"))
            self.btnValueDateFrom.setTitle(dateFrom.vnp4TToString(format: "dd/MM/yyyy"), for: .normal)
            DispatchQueue.main.asyncAfter(wallDeadline: .now() + 0.5) {
                VNP4TPicker.selectDate(title: "Đến ngày", cancelText: "Huỷ", doneText: "Ok", datePickerMode: .date, selectedDate: Date(), minDate: dateFrom, maxDate: Date()) { (dateTo) in
                    self.viewModel.input?.dateTo.accept(dateTo.vnp4TToString(format: "dd/MM/yyyy"))
                    self.viewModel.input?.loadTrigger.accept(true)
                    self.btnValueDateTo.setTitle(dateTo.vnp4TToString(format: "dd/MM/yyyy"), for: .normal)
                    self.lbDateReport.text = "\(dateFrom.vnp4TToString(format: "dd/MM/yyyy")) - \(dateTo.vnp4TToString(format: "dd/MM/yyyy"))"
                }
            }
        }
    }
    
    @IBAction func onChangeTabDate(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            viewDate.isHidden = false
            viewDateConstraintsHeight.constant = 50
            viewContainConstraintsTop.constant = 260
            view.layoutIfNeeded()
            self.viewModel.input?.type.accept(VNP4TReportType.DAY.rawValue)
            self.viewModel.input?.dateTo.accept(Date().vnp4TToString(format: "dd/MM/yyyy"))
            self.viewModel.input?.dateFrom.accept(Date().vnp4TToString(format: "dd/MM/yyyy"))
            self.lbDateReport.text = "\(Date().vnp4TToString(format: "dd/MM/yyyy")) - \(Date().vnp4TToString(format: "dd/MM/yyyy"))"
        } else {
            viewDate.isHidden = true
            viewDateConstraintsHeight.constant = 0
            viewContainConstraintsTop.constant = 200
            view.layoutIfNeeded()
            if sender.selectedSegmentIndex == 1 {
                self.viewModel.input?.type.accept(VNP4TReportType.WEEK.rawValue)
                self.lbDateReport.text = "\(Date().startOfWeek?.vnp4TToString(format: "dd/MM/yyyy") ?? Date().vnp4TToString(format: "dd/MM/yyyy")) - \(Date().endOfWeek?.vnp4TToString(format: "dd/MM/yyyy") ?? Date().vnp4TToString(format: "dd/MM/yyyy"))"
            } else {
                self.viewModel.input?.type.accept(VNP4TReportType.MONTH.rawValue)
                self.lbDateReport.text = "Tháng \(Date().vnp4TToString(format: "MM"))"
            }
            self.viewModel.input?.dateFrom.accept("")
            self.viewModel.input?.dateTo.accept("")
        }
        self.viewModel.input?.loadTrigger.accept(true)
    }
}

extension VNP4TReportVC : VNP4TViewControllerProtocol{
    func setupView() {
        self.viewBackgroundBar.VNP4TsetGradientBackground(colorStart: VNP4TConstants.navigationBarMainColor, colorEnd: VNP4TConstants.navigationBarSubColor)
        self.segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor:UIColor(hex: "FF6C2C")], for: .selected)
        self.segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor:UIColor(red: 255, green: 255, blue: 255, alpha: 0.72)], for: .normal)
//        self.setChartData()
        self.btnValueDateFrom.setTitle(Date().vnp4TToString(format: "dd/MM/yyyy"), for: .normal)
        self.btnValueDateTo.setTitle(Date().vnp4TToString(format: "dd/MM/yyyy"), for: .normal)
        self.lbDateReport.text = "\(Date().vnp4TToString(format: "dd/MM/yyyy")) - \(Date().vnp4TToString(format: "dd/MM/yyyy"))"
        
        self.lbTitle.text = "4t_home_report".localized
        self.btnValueSurvey.setTitle("4t_report_placeholder_survey".localized, for: .normal)
        self.segment.setTitle("4t_report_day_title".localized, forSegmentAt: 0)
        self.segment.setTitle("4t_report_week_title".localized, forSegmentAt: 1)
        self.segment.setTitle("4t_report_month_title".localized, forSegmentAt: 2)
        self.lbFrom.text = "4t_report_from".localized
        self.lbTo.text = "4t_report_to".localized
        self.lbTitleReport.text = "4t_home_report".localized
        self.lbTilteBonus.text = "4t_report_title_bonus".localized
        self.lbBonusSub.text = "4t_report_title_bonus_2".localized
        self.lbTitleAmount.text = "4t_report_title_amount".localized
        self.lbAmountSub.text = "4t_report_title_amount".localized
    }
    
    func setupViewModel() {
        if let ip = self.viewModel.input {
            bindingInput(input: ip)
        }
        
        if let op = self.viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input : VNP4TReportVM.Input){
        input.isLoading.subscribe { [weak self] isLoading in
            guard let self = self else {return}
            
            if isLoading.element ?? false {
                self.loading.startAnimating()
            } else {
                self.loading.stopAnimating()
            }
        }
    }
    
    private func bindingOutput(output : VNP4TReportVM.Output){
        output.reportResult.subscribe(onNext : { [weak self] result in
            guard let self = self else {return}
            
            if result.success == true {
                let data = result.data
                var amountList : [Int] = []
                var bonusList : [Double] = []
                data?.forEach({ (item) in
                    amountList.append(item.y)
                    bonusList.append(item.y1)
                })
                self.setChartData(amountList: amountList, bonusList: bonusList)
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_error".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: true, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        })
    }
    
    func setChartData(amountList : [Int] = [0,0,0,0], bonusList : [Double] = [0.0,0.0,0.0,0.0]) {
        
        let data = CombinedChartData()
        data.lineData = generateLineData(amountList: amountList)
        data.barData = generateBarData(barArray: bonusList)

        self.viewChart.data = data
        self.viewChart.chartDescription?.enabled = false
        self.viewChart.setScaleEnabled(false)
        self.viewChart.legend.enabled = false
        
        let bonusCount = NSSet(array: bonusList).count
        let amountCount = NSSet(array: amountList).count
    
        let countValue = amountCount >= bonusCount ? amountCount : bonusCount
        
        let labelArray : [String] = [
            "4t_report_title_draft".localized + "(\(amountList[0]))",
            "4t_report_title_status_new".localized + " (\(amountList[1]))",
            "4t_report_title_status_renew".localized + " (\(amountList[2]))",
            "4t_report_title_status_fail".localized + " (\(amountList[3]))"
        ]
        
        //amount
        let rightAxis = self.viewChart.rightAxis
        rightAxis.axisMinimum = 0
        rightAxis.labelCount = countValue
        rightAxis.forceLabelsEnabled = true
        rightAxis.spaceBottom = 0
        
        if amountList.max() != 0 {
            rightAxis.axisMaximum = Double(amountList.max() != nil ? (amountList.max() ?? 0) * 2 : 0)
        } else {
            rightAxis.axisMaximum = 1
        }
        
        //bonus
        let leftAxis = self.viewChart.leftAxis
        leftAxis.axisMinimum = 0
        leftAxis.labelCount = countValue
        leftAxis.spaceBottom = 0
        leftAxis.forceLabelsEnabled = true
        leftAxis.valueFormatter = ChartFormater()
        
        if bonusList.max() != 0 {
            leftAxis.axisMaximum = bonusList.max() != nil ? ((bonusList.max() ?? 0) * 1.5) : 0
        } else {
            leftAxis.axisMaximum = 1
        }
    
        let aXis = self.viewChart.xAxis
        aXis.labelPosition = .bottom
        aXis.labelTextColor = .black
        aXis.labelCount = labelArray.count
        aXis.drawAxisLineEnabled = true
        aXis.drawGridLinesEnabled = false
        aXis.axisMinimum = -data.barData.barWidth
        aXis.axisMaximum = Double(labelArray.count) - (data.barData.barWidth / 2)
        aXis.labelRotationAngle = -30
        aXis.valueFormatter = IndexAxisValueFormatter(values: labelArray)
        aXis.labelHeight = 50
        
        self.viewChart.notifyDataSetChanged()
    }
    
    class ChartFormater : IAxisValueFormatter {
        
        func stringForValue(_ value: Double, axis: AxisBase?) -> String {
            let format = NumberFormatter()
            format.currencySymbol = ""
            format.currencyGroupingSeparator = "."
            format.usesGroupingSeparator = true
            format.numberStyle = .currency
            format.positiveFormat = "#.##"
            let divisor = pow(10.0, Double(2))
            let dataV = (value * divisor).rounded() / divisor
            return dataV.description
//            return format.string(from: NSNumber(value: value)) ?? ""
        }
    }
    
    func generateLineData(amountList : [Int]) -> LineChartData {
        
        let entries = (0..<amountList.count).map { (i) -> ChartDataEntry in
            return ChartDataEntry(x: Double(i), y: Double(amountList[i]))
        }
        
        let set = LineChartDataSet(entries: entries, label: "Line")
        set.setColor(VNP4TConstants.buttonMainColor)
        set.lineWidth = 2
        set.axisDependency = .right
        set.lineWidth = 2
        set.drawValuesEnabled = false
        set.drawCirclesEnabled = false
        set.valueFont = .systemFont(ofSize: 10)
        set.valueTextColor = VNP4TConstants.buttonMainColor
        
        return LineChartData(dataSet: set)
    }
    
    func generateBarData(barArray : [Double]) -> BarChartData {
        
        let entries1 = (0..<barArray.count).map { i -> BarChartDataEntry in
            return BarChartDataEntry(x: Double(i), y: barArray[i])
        }

        let set = BarChartDataSet(entries: entries1, label: "Bar")
        set.valueTextColor = UIColor(hex: "FA6400")
        set.setColor(UIColor(hex: "FA6400"))
        set.valueFont = .systemFont(ofSize: 10)
        set.valueFormatter = BarChartFormat()

        let data = BarChartData(dataSets: [set])
        data.barWidth = 0.3
        
        return data
    }
    
    class BarChartFormat : IValueFormatter {
        func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
            let format = NumberFormatter()
            format.currencySymbol = ""
            format.currencyGroupingSeparator = "."
            format.usesGroupingSeparator = true
            format.numberStyle = .currency
            format.positiveFormat = "#.##"
            return format.string(from: NSNumber(value: value)) ?? ""
        }
    }
}
