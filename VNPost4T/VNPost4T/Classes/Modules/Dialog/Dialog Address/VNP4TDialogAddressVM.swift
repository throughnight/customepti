//
//  VNP4TDialogAddressVM.swift
//  
//
//  Created by AEGONA on 9/17/20.
//

import Foundation
import RxCocoa
import RxSwift

class VNP4TDialogAddressVM : VNP4TViewModelProtocol{
    
    typealias Dependency = VNP4TCommonService
    
    struct Input {
        let initValue                       :BehaviorRelay<VNP4TAddress>
        let triggerDistrict                 :BehaviorRelay<Bool>
        let district                        :BehaviorRelay<VNP4TQuestionValidations>
        let triggerWard                     :BehaviorRelay<Bool>
        let ward                            :BehaviorRelay<VNP4TQuestionValidations>
        let province                        :BehaviorRelay<VNP4TQuestionValidations>
        let isLoading                       :BehaviorRelay<Bool>
        let provinces                       :BehaviorRelay<[VNP4TQuestionValidations]>
        let districts                       :BehaviorRelay<[VNP4TQuestionValidations]>
        let wards                           :BehaviorRelay<[VNP4TQuestionValidations]>
        let address                         :BehaviorRelay<String>
        let removeData                      :BehaviorRelay<Bool>
    }
    
    struct Output {
        let provinceResult                  :Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TQuestionValidations]>>>
        let districResult                   :Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TQuestionValidations]>>>
        let wardResult                      :Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TQuestionValidations]>>>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input : Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: VNP4TCommonService?) -> Output? {
        self.input = input
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        
        let provincesResult = ip.initValue.asObservable()
            .do(onNext: { _ in
                ip.isLoading.accept(true)
            }).flatMapLatest { _ -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TQuestionValidations]>>> in
                return dp.getProvinces()
            }.do(onNext: { (_) in
                ip.isLoading.accept(false)
            })
        
        let districtResult = ip.triggerDistrict.asObservable()
            .filter {$0}
            .withLatestFrom(ip.province)
            .do(onNext: { _ in
                ip.isLoading.accept(true)
                if ip.removeData.value == true {
                    ip.district.accept(VNP4TQuestionValidations())
                    ip.ward.accept(VNP4TQuestionValidations())
                }
                ip.districts.accept([])
                ip.wards.accept([])
            })
            .flatMapLatest { province -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TQuestionValidations]>>> in
                return dp.getDistricts(id: province.id)
            }
            .do(onNext: { _ in
                ip.isLoading.accept(false)
            })
        
        let wardResult = ip.triggerWard.asObservable()
            .filter {$0}
            .withLatestFrom(ip.district)
            .do(onNext: { _ in
                ip.isLoading.accept(true)
                if ip.removeData.value == true {
                    ip.ward.accept(VNP4TQuestionValidations())
                }
                ip.wards.accept([])
            })
            .flatMapLatest { district -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TQuestionValidations]>>> in
                return dp.getWards(districtId: district.id)
            }
            .do(onNext: { _ in
                ip.isLoading.accept(false)
            })
        
        return VNP4TDialogAddressVM.Output(
            provinceResult: provincesResult,
            districResult: districtResult,
            wardResult: wardResult)
    }
}
