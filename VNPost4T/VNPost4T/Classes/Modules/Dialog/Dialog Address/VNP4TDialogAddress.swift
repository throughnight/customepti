//
//  VNP4TDialogAddress.swift
//  
//
//  Created by AEGONA on 9/17/20.
//

import Foundation
import UIKit
import RxCocoa
import SwiftMessages

class VNP4TDialogAddress: VNP4TBaseViewController {
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnValueProvince: UIButton!
    @IBOutlet weak var btnValueDistrict: UIButton!
    @IBOutlet weak var btnValueWard: UIButton!
    @IBOutlet weak var tfAddress: UITextField!
    @IBOutlet weak var lbErrorProvince: UILabel!
    @IBOutlet weak var lbErrorDistrict: UILabel!
    @IBOutlet weak var lbErrorProvinceConstraintsTop: NSLayoutConstraint!
    @IBOutlet weak var lbErrorDistrictConstraintsTop: NSLayoutConstraint!
    
    var viewModel : VNP4TDialogAddressVM!
    
    var onActionPick : (([VNP4TQuestionValidations],Int)->())?
    
    var onSelectAddress : ((VNP4TAddress)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupViewModel()
    }
    
    @IBAction func onClose(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onPickProvince(_ sender: Any) {
        self.viewModel.input?.removeData.accept(true)
        self.onActionPick?(self.viewModel.input?.provinces.value ?? [],1)
    }
    
    @IBAction func onPickDistrict(_ sender: Any) {
        if self.viewModel.input?.province.value.id != "" {
            self.viewModel.input?.removeData.accept(true)
            self.onActionPick?(self.viewModel.input?.districts.value ?? [],2)
        }
    }
    
    @IBAction func onPickWard(_ sender: Any) {
        if self.viewModel.input?.district.value.id != "" {
            self.viewModel.input?.removeData.accept(true)
            self.onActionPick?(self.viewModel.input?.wards.value ?? [],3)
        }
    }
    
    @IBAction func onSelectAddress(_ sender: Any) {
        let fullAddress = VNP4TAddress()
        fullAddress.province = self.viewModel.input?.province.value ?? VNP4TQuestionValidations()
        fullAddress.district = self.viewModel.input?.district.value ?? VNP4TQuestionValidations()
        fullAddress.ward = self.viewModel.input?.ward.value ?? VNP4TQuestionValidations()
        fullAddress.address = self.tfAddress.text ?? ""
        var isPass = true
        if fullAddress.province?.id == "" {
            isPass = false
            handleError(lable: self.lbErrorProvince, btnValue: self.btnValueProvince, status: false, message: "4t_dialog_address_error_province".localized, constraint: self.lbErrorProvinceConstraintsTop)
        }
        if fullAddress.district?.id == "" {
            isPass = false
            handleError(lable: self.lbErrorDistrict, btnValue: self.btnValueDistrict, status: false, message: "4t_dialog_address_error_district".localized, constraint: self.lbErrorDistrictConstraintsTop)
        }
        if isPass {
            self.onSelectAddress?(fullAddress)
            dismiss(animated: false, completion: nil)
        }
    }
    
    func handleError(lable: UILabel, btnValue: UIButton, status: Bool, message: String, constraint: NSLayoutConstraint){
        if status {
            lable.isHidden = true
            btnValue.layer.borderWidth = 1
            btnValue.layer.borderColor = UIColor(hex: "DBDBDB").cgColor
            constraint.constant = -10
            view.layoutIfNeeded()
        } else {
            lable.isHidden = false
            lable.text = message
            btnValue.layer.borderWidth = 1
            btnValue.layer.borderColor = UIColor.red.cgColor
            constraint.constant = 10
            view.layoutIfNeeded()
        }
    }
}

extension VNP4TDialogAddress : VNP4TViewControllerProtocol {
    func setupView() {
        self.btnConfirm.VNP4TsetGradientBlueButtonBackground()
        self.headerView.vnp4TApplyGradient(withColours: [VNP4TConstants.navigationBarMainColor,VNP4TConstants.navigationBarSubColor],
                                           locations: [0,0,1,0],
                                           direction: .horizontal)
        
        self.btnConfirm.vnp4TApplyGradient(withColours: [VNP4TConstants.buttonSubColor,VNP4TConstants.buttonMainColor],
                                           locations: [0,0,1,0],
                                           direction: .horizontal)
        self.handleError(lable: self.lbErrorProvince, btnValue: self.btnValueProvince, status: true, message: "", constraint: self.lbErrorProvinceConstraintsTop)
        self.handleError(lable: self.lbErrorDistrict, btnValue: self.btnValueDistrict, status: true, message: "", constraint: self.lbErrorDistrictConstraintsTop)
    }
    
    func setupViewModel() {
        if let ip = self.viewModel.input {
            bindingInput(input: ip)
        }
        
        if let op = self.viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input: VNP4TDialogAddressVM.Input){
        
        input.province.subscribe { (result) in
            if result.element?.id != "" {
                self.handleError(lable: self.lbErrorProvince, btnValue: self.btnValueProvince, status: true, message: "", constraint: self.lbErrorProvinceConstraintsTop)
                self.btnValueProvince.setTitleColor(UIColor(hex: "#00000"), for: .normal)
                self.btnValueProvince.setTitle(result.element?.name, for: .normal)
            } else {
                self.btnValueProvince.setTitle("Lựa chọn tỉnh/thành", for: .normal)
                self.btnValueProvince.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 0.32), for: .normal)
            }
        }
        
        input.district.subscribe { (result) in
            if result.element?.id != "" {
                self.handleError(lable: self.lbErrorDistrict, btnValue: self.btnValueDistrict, status: true, message: "", constraint: self.lbErrorDistrictConstraintsTop)
                self.btnValueDistrict.setTitleColor(UIColor(hex: "#00000"), for: .normal)
                self.btnValueDistrict.setTitle(result.element?.name, for: .normal)
            } else {
                self.btnValueDistrict.setTitle("Lựa chọn quận/huyện", for: .normal)
                self.btnValueDistrict.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 0.32), for: .normal)
            }
        }
        
        input.ward.subscribe { (result) in
            if result.element?.id != "" {
                self.btnValueWard.setTitleColor(UIColor(hex: "#00000"), for: .normal)
                self.btnValueWard.setTitle(result.element?.name, for: .normal)
            } else {
                self.btnValueWard.setTitle("Lựa chọn phường/xã", for: .normal)
                self.btnValueWard.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 0.32), for: .normal)
            }
        }
        self.tfAddress.text = input.address.value
    }
    
    private func bindingOutput(output: VNP4TDialogAddressVM.Output){
        output.provinceResult.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            if result.success {
                self.viewModel.input?.provinces.accept(result.data?.data ?? [])
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_dialog_error_title".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: false, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
        
        output.districResult.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            if result.success {
                self.viewModel.input?.districts.accept(result.data?.data ?? [])
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_dialog_error_title".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: false, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
        
        output.wardResult.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            if result.success {
                self.viewModel.input?.wards.accept(result.data?.data ?? [])
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_dialog_error_title".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: false, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
    }
}
