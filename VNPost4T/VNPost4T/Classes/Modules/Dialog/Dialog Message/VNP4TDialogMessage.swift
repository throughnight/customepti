//
//  VNP4TDialogMessage.swift
//  VNPost4T
//
//  Created by AEGONA on 9/25/20.
//

import Foundation
import UIKit
import RxCocoa

class VNP4TDialogMessage: VNP4TBaseViewController {
    
    var onDoneActions : (()->())?
    
    var titleDialog : BehaviorRelay<String>!
    var messageDialog : BehaviorRelay<String>!
    var status : BehaviorRelay<Bool>!
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSubtitle: UILabel!
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var viewContain: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        let tapDeadSpace = UITapGestureRecognizer(target: self, action: #selector(onClose))
        view.addGestureRecognizer(tapDeadSpace)
        let tapContain = UITapGestureRecognizer(target: self, action: #selector(onContain))
        viewContain.addGestureRecognizer(tapContain)
    }
    
    @objc func onClose(_ sender: Any){
        dismiss(animated: false, completion: nil)
    }
    
    @objc func onContain(_ sender: Any){
    }
    
    @IBAction func onOk(_ sender: Any) {
        self.onDoneActions?()
    }
}

extension VNP4TDialogMessage : VNP4TViewControllerProtocol {
    func setupView() {
        let title = self.titleDialog.value
        self.lbTitle.text = title
        
        let message = self.messageDialog.value
        if message == "" {
            self.lbSubtitle.isHidden = true
        } else {
            self.lbSubtitle.text = message
        }
        
        if self.status.value {
            self.imgStatus.image = UIImage(named: "ic_done_green", in: Bundle(identifier: VNP4TConstants.bundleIdentifier), compatibleWith: UITraitCollection.init())
        } else {
            self.imgStatus.image = UIImage(named: "ic_error_red", in: Bundle(identifier: VNP4TConstants.bundleIdentifier), compatibleWith: UITraitCollection.init())
        }
    }
    
    func setupViewModel() {
        
    }
}
