//
//  VNP4TDialogImageOption.swift
//  VNPost4T
//
//  Created by m2 on 9/28/20.
//

import Foundation

class VNP4TDialogImageOption: VNP4TBaseViewController {
    
    static let identifier: String = String(describing: VNP4TDialogImageOption.self)
    var onTakePicture : (()->())?
    var onLibrary : (()->())?

    @IBOutlet weak var takePictureView: UIView!
    @IBOutlet weak var libraryView: UIView!
    @IBOutlet weak var toolbarLabel: UILabel!
    @IBOutlet weak var takePictureLabel: UILabel!
    @IBOutlet weak var libraryLabel: UILabel!
    @IBOutlet weak var viewContain: UIView!
    
    @IBAction func closeClick(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        onTakePictureClick()
        onLibraryClick()
        
        let tapDeadSpace = UITapGestureRecognizer(target: self, action: #selector(onClose))
        view.addGestureRecognizer(tapDeadSpace)
        let tapContain = UITapGestureRecognizer(target: self, action: #selector(onContain))
        viewContain.addGestureRecognizer(tapContain)
    }
    
    @objc func onClose(_ sender: Any){
        dismiss(animated: false, completion: nil)
    }
    
    @objc func onContain(_ sender: Any){
    }
    
    private func onTakePictureClick(){
        let tap = UITapGestureRecognizer()
        takePictureView.addGestureRecognizer(tap)
        tap.rx.event.bind { (eventTap) in
            self.dismiss(animated: false, completion: nil)
            self.onTakePicture?()
        }.disposed(by: disposeBag)
        
    }
    
    private func onLibraryClick(){
        
        let tap = UITapGestureRecognizer()
        libraryView.addGestureRecognizer(tap)
        tap.rx.event.bind { (eventTap) in
            self.dismiss(animated: false, completion: nil)
            self.onLibrary?()
        }.disposed(by: disposeBag)
        
    }
    
    private func setupUI(){
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.takePictureView.backgroundColor = UIColor(hex: "#EEF3FF")
        
        self.toolbarLabel.text = "4t_physical_dialog_option_toolbar_title".localized
        self.takePictureLabel.text = "4t_physical_dialog_option_take_picture".localized
        self.libraryLabel.text = "4t_physical_dialog_option_library".localized
        
    }
    
}
