//
//  VNP4TDialogOptions.swift
//  
//
//  Created by AEGONA on 9/16/20.
//

import Foundation
import UIKit
import RxCocoa

class VNP4TDialogOptions: VNP4TBaseViewController {
    
    var onCancelAction : (()->())?
    
    var onDoneActions : (()->())?
    
    var titleDialog : BehaviorRelay<String>!
    var messageDialog : BehaviorRelay<String>!
    var titleActionCancel: BehaviorRelay<String?>!
    var titleActionConfirm: BehaviorRelay<String?>!
    
    @IBOutlet weak var viewContain: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSubtitle: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        let tapDeadSpace = UITapGestureRecognizer(target: self, action: #selector(onClose))
        view.addGestureRecognizer(tapDeadSpace)
        let tapContain = UITapGestureRecognizer(target: self, action: #selector(onContain))
        viewContain.addGestureRecognizer(tapContain)
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.onCancelAction?()
    }
    
    @IBAction func onOk(_ sender: Any) {
        self.onDoneActions?()
    }
    
    @objc func onClose(_ sender: Any){
        dismiss(animated: false, completion: nil)
    }
    
    @objc func onContain(_ sender: Any){
    }
    
}

extension VNP4TDialogOptions : VNP4TViewControllerProtocol {
    func setupView() {
        self.btnConfirm.VNP4TsetGradientBlueButtonBackground()
        
        self.titleDialog.subscribe(onNext: { title in
            self.desginImageLeftLabel(label: self.lbTitle, title: title)
        }).disposed(by: disposeBag)
        
        self.messageDialog.subscribe(onNext: { (message) in
            if message == "" {
                self.lbSubtitle.isHidden = true
            } else {
                self.lbSubtitle.text = message
            }
        }).disposed(by: disposeBag)
        
        if titleActionCancel != nil {
            btnCancel.setTitle(titleActionCancel.value ?? "4t_register_survey_cancel".localized, for: .normal)
        } else {
            btnCancel.setTitle("4t_register_survey_cancel".localized , for: .normal)
        }
        if titleActionConfirm != nil {
            btnConfirm.setTitle(titleActionConfirm.value ?? "4t_confirm_title".localized, for: .normal)
        } else {
            btnConfirm.setTitle("4t_confirm_title".localized, for: .normal)
        }
        
    }
    
    func setupViewModel() {
        
    }
    
    func desginImageLeftLabel(label: UILabel,title: String){
        // Create Attachment
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = UIImage(named: "ic_warning",
                                        in: Bundle(url: Bundle.main.bundleURL.appendingPathComponent("Frameworks").appendingPathComponent("VNPost4T.framework")) ?? Bundle.main,
                                        compatibleWith: nil)
        // Set bound to reposition
        let imageOffsetY: CGFloat = -5.0
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
        // Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        // Initialize mutable string
        let completeText = NSMutableAttributedString(string: "")
        // Add image to mutable string
        completeText.append(attachmentString)
        // Add your text to mutable string
        let textAfterIcon = NSAttributedString(string: title)
        completeText.append(textAfterIcon)
        label.textAlignment = .center
        label.attributedText = completeText
    }
    
    
}
