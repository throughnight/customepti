//
//  VNP4TDialogSearchListVM.swift
//  
//
//  Created by AEGONA on 9/17/20.
//

import Foundation
import RxCocoa
import RxSwift
import RxDataSources

typealias DialogSearchSection = SectionModel<String,VNP4TQuestionValidations>

class VNP4TDialogSearchListVM: VNP4TViewModelProtocol {
    
    typealias Dependency = VNP4TCommonService
    
    struct Input {
        let dataDialog                      :BehaviorRelay<[VNP4TQuestionValidations]>
        let dataSearch                      :BehaviorRelay<[VNP4TQuestionValidations]>
        let isSearch                        :BehaviorRelay<Bool>
        let seachText                       :BehaviorRelay<String>
        let titleDialog                     :BehaviorRelay<String>
        let initValue                       :BehaviorRelay<String>
    }
    
    struct Output {
        let dataDialogResult                :Observable<[VNP4TQuestionValidations]>
        let dataDialogSections              :Driver<[DialogSearchSection]>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input : Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: VNP4TCommonService?) -> Output? {
        self.input = input
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        
        let dataSearchResult = ip.seachText.asObservable().flatMapLatest { (data) -> Observable<[VNP4TQuestionValidations]> in
            if ip.seachText.value.isEmpty {
                return Observable.just(ip.dataDialog.value)
            } else {
                let array : [VNP4TQuestionValidations] = ip.dataDialog.value.filter {
                    $0.name.uppercased().contains(ip.seachText.value.uppercased())
                }
                return Observable.just(array)
            }
        }
        
        let dialogSections = ip.dataSearch.asDriver().map { (data) -> [DialogSearchSection] in
            return [DialogSearchSection(model: "DialogSearch", items: data)]
        }
        
        return VNP4TDialogSearchListVM.Output(dataDialogResult: dataSearchResult, dataDialogSections: dialogSections)
    }
    
}
