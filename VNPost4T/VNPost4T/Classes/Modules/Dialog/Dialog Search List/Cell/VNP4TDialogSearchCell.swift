//
//  VNP4TDialogSearchCell.swift
//  
//
//  Created by AEGONA on 9/17/20.
//

import Foundation
import UIKit
import RxCocoa

class VNP4TDialogSearchCell: UITableViewCell{
    static let cellIdentifier = "VNP4TDialogSearchCell"
    
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var imgTick: UIImageView!
    
    var onClickItem : ((VNP4TQuestionValidations?)->())?
    
    var data : VNP4TQuestionValidations? {
        didSet{
            lbName.text = data?.name
        }
    }
    
    var initValue : String? = "" {
        didSet {
            if initValue == data?.name {
                imgTick.isHidden = false
            } else {
                imgTick.isHidden = true
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapDeadSpace = UITapGestureRecognizer(target: self, action: #selector(onClick))
        containView.addGestureRecognizer(tapDeadSpace)
    }
    
    @objc func onClick(_ sender: Any){
        self.onClickItem?(data)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
