//
//  VNP4TDialogSearchList.swift
//  
//
//  Created by AEGONA on 9/17/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

typealias VNP4TDialogSearchListSource = RxTableViewSectionedReloadDataSource<DialogSearchSection>

class VNP4TDialogSearchList: VNP4TBaseViewController {
    
    static let cellIdentifier = "VNP4TDialogSearchList"
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var viewSearchConstraints: NSLayoutConstraint!
    @IBOutlet weak var viewParent: UIView!
    @IBOutlet weak var viewContain: UIView!
    
    var viewModel : VNP4TDialogSearchListVM!
    
    var dataSource : VNP4TDialogSearchListSource!
    
    var dataPickedAction : ((VNP4TQuestionValidations)->())?
    
    var actionClose : (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        configDataSource()
        setupView()
        setupViewModel()
        
        let tapDeadSpace = UITapGestureRecognizer(target: self, action: #selector(onClose))
        viewParent.addGestureRecognizer(tapDeadSpace)
        let tapContain = UITapGestureRecognizer(target: self, action: #selector(onContain))
        viewContain.addGestureRecognizer(tapContain)
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        self.actionClose?()
    }
    
    @objc func onClose(_ sender: Any){
        self.dismiss(animated: false, completion: nil)
        self.actionClose?()
    }
    
    @objc func onContain(_ sender: Any){
    }
}

extension VNP4TDialogSearchList : VNP4TViewControllerProtocol {
    func setupView() {
//        self.headerView.VNP4TsetGradientBackground(colorStart: VNP4TConstants.navigationBarMainColor, colorEnd: VNP4TConstants.navigationBarSubColor)
        self.btnCancel.setTitle("4t_dialog_list_cancel".localized, for: .normal)
    }
    
    func setupViewModel() {
        if let ip = self.viewModel.input {
            bindingInput(input: ip)
        }
        if let op = self.viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input : VNP4TDialogSearchListVM.Input){
        tfSearch.rx.text.orEmpty.asDriver()
            .distinctUntilChanged()
            .debounce(RxTimeInterval.milliseconds(300))
            .drive(input.seachText)
            .disposed(by: disposeBag)

        self.lbTitle.text = input.titleDialog.value
        
        if input.isSearch.value == false {
            self.searchView.isHidden = true
            self.viewSearchConstraints.constant = 0
            self.view.layoutIfNeeded()
            self.searchView.setNeedsUpdateConstraints()
        }
    }
    
    private func bindingOutput(output : VNP4TDialogSearchListVM.Output){
        output.dataDialogSections.drive(self.tbView.rx.items(dataSource: self.dataSource)).disposed(by: disposeBag)
        
        output.dataDialogResult.subscribe(onNext: { [weak self] data in
            guard let self = self else {return}
            self.viewModel.input?.dataSearch.accept(data)
        }).disposed(by: disposeBag)
    }
}

extension VNP4TDialogSearchList : ListViewControllerProtocol{
    func registerCell() {
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        tbView.register(UINib(nibName: VNP4TDialogSearchCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: VNP4TDialogSearchCell.cellIdentifier)
    }
    
    func configDataSource() {
        let ds : VNP4TDialogSearchListSource = VNP4TDialogSearchListSource(configureCell: { (dataSource, tv, indexPath, data) in
            let cell : VNP4TDialogSearchCell = tv.dequeueReusableCell(withIdentifier: VNP4TDialogSearchCell.cellIdentifier, for: indexPath) as! VNP4TDialogSearchCell
            
            cell.data = data
            cell.initValue = self.viewModel.input?.initValue.value
            cell.onClickItem = { item in
                self.dataPickedAction?(item ?? data)
            }
            
            return cell
        })
        
        dataSource = ds
    }
}
