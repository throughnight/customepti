//
//  VNP4TDialogError.swift
//  VNPost4T
//
//  Created by AEGONA on 9/25/20.
//

import Foundation
import UIKit
import RxCocoa

class VNP4TDialogError: VNP4TBaseViewController {
    
    var onDoneActions : (()->())?
    
    var messageDialog : BehaviorRelay<String>!
    var titleButton : BehaviorRelay<String> = BehaviorRelay<String>(value: "Ok")
    @IBOutlet weak var viewContain: UIView!
    
    @IBOutlet weak var lbSubtitle: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        let tapDeadSpace = UITapGestureRecognizer(target: self, action: #selector(onClose))
        view.addGestureRecognizer(tapDeadSpace)
        let tapContain = UITapGestureRecognizer(target: self, action: #selector(onContain))
        viewContain.addGestureRecognizer(tapContain)
    }
    
    @IBAction func onOk(_ sender: Any) {
        self.onDoneActions?()
    }
    
    @objc func onClose(_ sender: Any){
        dismiss(animated: false, completion: nil)
    }
    
    @objc func onContain(_ sender: Any){
    }
    
}

extension VNP4TDialogError : VNP4TViewControllerProtocol {
    func setupView() {
        let message = self.messageDialog.value
        if message == "" {
            self.lbSubtitle.isHidden = true
        } else {
            self.lbSubtitle.text = message
        }
        self.btnOk.setTitle(self.titleButton.value, for: .normal)
    }
    
    func setupViewModel() {
        
    }
}
