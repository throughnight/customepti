//
//  ScanQR.swift
//  Alamofire
//
//  Created by m2 on 9/29/20.
//

import UIKit
import Foundation
import AVFoundation

protocol VNP4TScanQRDelegate {
    func qrResult(qr: String?)
}

class VNP4TScanQR: VNP4TBaseViewController {
    
    var avCaptureSession: AVCaptureSession!
    var avPreviewLayer: AVCaptureVideoPreviewLayer!
    
    @IBOutlet weak var toolbarHeaderView: UIView!
    
    @IBAction func onClose(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    var delegate: VNP4TScanQRDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        avCaptureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
            self.failed()
            return
        }
        let avVideoInput: AVCaptureDeviceInput
        
        do {
            avVideoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            self.failed()
            return
        }
        
        if (self.avCaptureSession.canAddInput(avVideoInput)) {
            self.avCaptureSession.addInput(avVideoInput)
        } else {
            self.failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (self.avCaptureSession.canAddOutput(metadataOutput)) {
            self.avCaptureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.ean8, .ean13, .pdf417, .qr]
        } else {
            self.failed()
            return
        }
        
        self.avPreviewLayer = AVCaptureVideoPreviewLayer(session: self.avCaptureSession)
        self.avPreviewLayer.frame = self.view.layer.bounds
        self.avPreviewLayer.videoGravity = .resizeAspectFill

        self.view.layer.addSublayer(self.avPreviewLayer)
        self.view.bringSubviewToFront(self.toolbarHeaderView)
        
        self.avCaptureSession.startRunning()
        
    }
    
    private func setupViews(){
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        self.toolbarHeaderView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        
    }
    
    private func failed() {
        let ac = UIAlertController(title: "Scanner not supported", message: "Please use a device with a camera. Because this device does not support scanning a code", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        avCaptureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (avCaptureSession?.isRunning == false) {
            avCaptureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (avCaptureSession?.isRunning == true) {
            avCaptureSession.stopRunning()
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
}

extension VNP4TScanQR : AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        avCaptureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
        
        dismiss(animated: true)
    }
    
    func found(code: String) {
        navigationController?.popViewController(animated: true)
        self.delegate?.qrResult(qr: code)
    }
}

