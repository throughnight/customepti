//
//  VNP4TNotificationDetailVC.swift
//  VNPost4T
//
//  Created by AEGONA on 10/5/20.
//

import Foundation
import UIKit
import RxCocoa

class VNP4TNotificationDetailVC : VNP4TBaseViewController {
    
    @IBOutlet weak var viewBackgroundBar: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbTitleNotification: UILabel!
    @IBOutlet weak var lbSubTitleNotification: UILabel!
    @IBOutlet weak var viewContainConstraintsHeight: NSLayoutConstraint!
    
    var viewModel : VNP4TNotificationDetailVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupViewModel()
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension VNP4TNotificationDetailVC : VNP4TViewControllerProtocol{
    func setupView() {
        self.viewBackgroundBar.VNP4TsetGradientBackground(colorStart: VNP4TConstants.navigationBarMainColor, colorEnd: VNP4TConstants.navigationBarSubColor)
        self.lbTitle.text = "4t_notification_detail_title".localized
        handleTitleNotification()
    }
    
    func setupViewModel() {
        if let ip = viewModel.input {
            bindingInput(input: ip)
        }
        if let op = viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input : VNP4TNotificationDetailVM.Input){
//        self.lbTitleNotification.text = self.viewModel.input?.titleNotification.value
        self.lbSubTitleNotification.text = self.viewModel.input?.messageNotification.value
    }
    
    private func bindingOutput(output : VNP4TNotificationDetailVM.Output){
        output.tokenResult.subscribe(onNext: {[weak self] result in
            guard let self = self else { return }
            if(result.success){
                VNP4TManager.shared.setAccessToken(result.data?.token ?? "")
            }
            else{
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_dialog_error_title".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: true, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
        
        output.surveyResult.subscribe(onNext: {[weak self] result in
            guard let self = self else { return }
            
            if result.success == true {
                if result.data?.data?.count ?? 0 > 0 {
                    let item : VNP4TSurvey = result.data?.data?[0] ?? VNP4TSurvey()
                    if item.surveyClassifiation == VNP4TSurveyType.ELECTRICAL.rawValue {
                        print("Detail Electric Survey")
                        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                        let detailVC = VNP4TSurveyElectricDetailVC(nibName: "VNP4TSurveyElectricDetailVC", bundle: bundle)
                        detailVC.viewModel = VNP4TSurveyElectricDetailVM(input: VNP4TSurveyElectricDetailVM.Input(status: BehaviorRelay<Int>(value: item.surveySheetStatus ?? 0),
                                                                                                                  isLoading: BehaviorRelay<Bool>(value: true),
                                                                                                                  survey: BehaviorRelay<VNP4TSurvey>(value: item),
                                                                                                                  dataShow: BehaviorRelay<[VNP4TSummarySurvey]>(value: []),
                                                                                                                  dataSurvey: BehaviorRelay<[VNP4TSurveyQuestion]>(value: []),
                                                                                                                  surveyDetail: BehaviorRelay<VNP4TSurveyDetail>(value: VNP4TSurveyDetail()),
                                                                                                                  saveDraftTrigger: BehaviorRelay<Bool>(value: false),
                                                                                                                  dataUpdate: BehaviorRelay<VNP4TQuestionKey>(value: VNP4TQuestionKey()),
                                                                                                                  submitTrigger: BehaviorRelay<Bool>(value: false),
                                                                                                                  deleteTrigger: BehaviorRelay<Bool>(value: false),
                                                                                                                  isShowAction: BehaviorRelay<Bool>(value: false)),
                                                                         dependency: VNP4TSurveyService())
                        self.navigationController?.pushViewController(detailVC, animated: true)
                    } else {
                        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                        let detailVC = VNP4TSurveyPhysicalDetailVC(nibName: "VNP4TSurveyPhysicalDetailVC", bundle: bundle)
                        detailVC.viewModel = VNP4TSurveyPhysicalDetailVM(input: VNP4TSurveyPhysicalDetailVM.Input(
                                                                            status: BehaviorRelay<Int>(value: item.surveySheetStatus ?? 0),
                                                                            isLoading: BehaviorRelay<Bool>(value: false),
                                                                            triggerDetail: BehaviorRelay<Bool>(value: true),
                                                                            survey: BehaviorRelay<VNP4TSurvey>(value: item),
                                                                            dataDetail: BehaviorRelay<VNP4TSurveyDetail>(value: VNP4TSurveyDetail()),
                                                                            images: BehaviorRelay<[VNP4TImage]>(value: []),
                                                                            initTotalImage: BehaviorRelay<Int>(value: 0),
                                                                            imageUrl: BehaviorRelay<[String]>(value: []),
                                                                            triggerUpdate: BehaviorRelay<Bool>(value: false)),
                                                                         dependency: VNP4TSurveyService())

                        self.navigationController?.pushViewController(detailVC, animated: true)
                    }
                    
                } else {
                    let message = result.message
                    let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                    let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                    errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_dialog_error_title".localized.uppercased())
                    errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                    errorDialog.status = BehaviorRelay<Bool>(value: false)
                    errorDialog.modalPresentationStyle = .overFullScreen
                    errorDialog.onDoneActions = {
                        errorDialog.dismiss(animated: true, completion: nil)
                    }
                    
                    self.parent?.present(errorDialog, animated: false, completion: nil)
                }
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_dialog_error_title".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: true, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
            
        }).disposed(by: disposeBag)
    }
    
    private func handleTitleNotification(){
        let title = self.viewModel.input?.titleNotification.value ?? ""
        if title != "" {
            if title.contains("ký") && title.contains("đã") || title.contains("yêu") {
                let code = title.split(separator: " ")[3].trimmingCharacters(in: .whitespacesAndNewlines)
                print("CODE \(code)")
                self.viewModel.input?.codeSurvey.accept(code)
                let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: title)
                attributedString.setColor(color: UIColor.blue, forText: code)
                self.lbTitleNotification.attributedText = attributedString
                
                let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(clickTitle))
                self.lbTitleNotification.isUserInteractionEnabled = true
                self.lbTitleNotification.addGestureRecognizer(tap)
            }
        }
    }
    
    @objc func clickTitle(sender: Any){
        print("Click")
        self.viewModel.input?.detailTrigger.accept(true)
    }
}

extension NSMutableAttributedString {
    func setColor(color: UIColor, forText stringValue: String) {
       let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }

}
