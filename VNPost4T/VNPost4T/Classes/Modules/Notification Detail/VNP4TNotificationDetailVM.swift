//
//  VNP4TNotificationDetailVM.swift
//  VNPost4T
//
//  Created by AEGONA on 10/5/20.
//

import Foundation
import RxCocoa
import RxSwift

class VNP4TNotificationDetailVM : VNP4TViewModelProtocol{
    typealias Dependency = VNP4TSurveyService
    
    struct Input {
        let isLoading                                :BehaviorRelay<Bool>
        let titleNotification                        :BehaviorRelay<String>
        let messageNotification                      :BehaviorRelay<String>
        let codeSurvey                               :BehaviorRelay<String>
        let detailTrigger                            :BehaviorRelay<Bool>
        let loadToken                                :BehaviorRelay<Bool>
    }
    
    struct Output {
        let surveyResult                            :Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurvey]>>>
        let tokenResult                             :Observable<VNP4TResponseModel<VNP4TUserToken>>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input: Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: VNP4TSurveyService?) -> Output? {
        self.input = input
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        
        let surveyResult = ip.detailTrigger.asObservable()
            .filter {$0}
            .withLatestFrom(ip.codeSurvey)
            .do(onNext: { _ in
                ip.isLoading.accept(true)
            }).flatMapLatest { code -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurvey]>>> in
                return dp.getSurveyHistory(tab: 0, page: 1, size: 10, search: code, surveyId: nil, collectionType: nil, surveyStatus: nil)
            }.do(onNext: { _ in
                ip.isLoading.accept(false)
            })
        
        let tokenResult = ip.loadToken.asObservable()
            .filter {$0}
            .do(onNext: { _ in
                ip.isLoading.accept(true)
            }).flatMapLatest { _ -> Observable<VNP4TResponseModel<VNP4TUserToken>> in
                return VNP4TAuthService().getToken(token: VNP4TManager.shared.accessToken ?? "")
            }.do(onNext: { _ in
                ip.isLoading.accept(false)
            })
        
        return VNP4TNotificationDetailVM.Output(surveyResult: surveyResult,
                                                tokenResult: tokenResult)
    }
}
