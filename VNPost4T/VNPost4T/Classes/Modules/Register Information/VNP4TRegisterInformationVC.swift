//
//  VNP4TRegisterInformationVC.swift
//
//
//  Created by AEGONA on 9/12/20.
//

import Foundation
import UIKit
import RxCocoa


class VNP4TRegisterInformationVC : VNP4TBaseViewController {
    
    @IBOutlet var backgroundBarView: UIView!
    @IBOutlet weak var actionView: UIView!
    @IBOutlet weak var questionView: UIView!
    @IBOutlet weak var listQuestionView: UIView!
    
    @IBOutlet weak var surveyPickerLabel: UILabel!
    @IBOutlet weak var lbTitleToolbar: UILabel!
    @IBOutlet weak var lbtitleSurveyType: UILabel!
    @IBOutlet weak var lbTitleQuestion: UILabel!
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBOutlet weak var selectSurveyView: UIView!
    
    var viewModel : VNP4TRegisterInformationVM!
    
    private var renderQuestionView : VNP4TSurveyQuestionsVC?
    
    private var loading: VNP4TLoadingProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loading = VNP4TLoadingProgressView(uiScreen: UIScreen.main)
        self.view.addSubview(loading)
        setupView()
        setupViewModel()
        onPickSurvey()
    }

    private func onPickSurvey(){
        let tap = UITapGestureRecognizer()
        selectSurveyView.addGestureRecognizer(tap)
        tap.rx.event.bind { (eventTap) in
            self.onGoFormRegister()
        }.disposed(by: disposeBag)
        
    }
    
    private func onGoFormRegister() {
        let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        let formRegisterPicker = VNP4TFormRegisterInformationDataPickerVC(nibName: "VNP4TFormRegisterInformationDataPickerVC", bundle: bundle)
        formRegisterPicker.viewModel = VNP4TFormRegisterInformationDatePickerVM(input: VNP4TFormRegisterInformationDatePickerVM.Input(
                                                                                    searchKey: BehaviorRelay<String>(value: ""),
                                                                                    loadTrigger: BehaviorRelay<Bool>(value: true),
                                                                                    loadMoreTrigger: BehaviorRelay<Bool>(value: false),
                                                                                    nextPage: BehaviorRelay<Int>(value: 1),
                                                                                    isEnd: BehaviorRelay<Bool>(value: true),
                                                                                    surveyHistory: BehaviorRelay<[VNP4TSurveyClassificationTypeSelected]>(value: []),
                                                                                    isLoading: BehaviorRelay<Bool>(value: false), collectionType: BehaviorRelay<Int?>(value: 0)),
                                                                                dependency: VNP4TSurveyService()
        )
        formRegisterPicker.modalPresentationStyle = .overFullScreen
        formRegisterPicker.dataPickedAction = { [weak self] survey in
            guard let self = self else { return }
            
            formRegisterPicker.dismiss(animated: false, completion: nil)
            self.viewModel.input?.selectedSurvey.accept(survey)
        }
        
        self.parent?.present(formRegisterPicker, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCancel(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSearch(_ sender: Any) {
        let survey = self.viewModel.input?.selectedSurvey.value
        if survey != nil {
            var isError = false
            let questionVerify = VNP4TQuestionKey(surveyId: "", answers: [])
            var ansArr : [VNP4TQuestion] = []
            survey?.keys.forEach({ key in
                if key.answerValue != "" && key.isPass == true {
                    key.isPass = true
                } else {
                    isError = true
                    key.isPass = false
                }
                ansArr.append(VNP4TQuestion(id: key.id, value: key.answerValue))
            })
            
            if isError {
                self.renderQuestionView?.tbView.reloadData()
            } else {
                questionVerify.answers = ansArr
                questionVerify.surveyId = survey?.id ?? ""
                self.viewModel.input?.surveyVerify.accept(questionVerify)
                self.viewModel.input?.searchTrigger.accept(true)
            }
        } else {
            self.onGoFormRegister()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.input?.selectedSurvey.accept(nil)
        if (renderQuestionView != nil) {
            self.remove(asChildViewController: self.renderQuestionView!)
        }
        self.surveyPickerLabel.text = "4t_register_survey_type_placeholder".localized
    }
}

extension VNP4TRegisterInformationVC : VNP4TViewControllerProtocol{
    func setupView() {
        backgroundBarView.VNP4TsetGradientBackground(colorStart: VNP4TConstants.navigationBarMainColor, colorEnd: VNP4TConstants.navigationBarSubColor)
        
        questionView.isHidden = true
        self.lbTitleToolbar.text = "4t_home_register_title".localized
        self.lbtitleSurveyType.text = "4t_register_survey_type".localized
        self.lbTitleQuestion.text = "4t_register_survey_title_question".localized
        self.btnCancel.setTitle("4t_register_survey_cancel".localized, for: .normal)
        self.btnSearch.setTitle("4t_register_survey_search".localized, for: .normal)
        
        self.btnSearch.VNP4TsetGradientBlueButtonBackground()
    }
    
    func setupViewModel() {
        if let ip = self.viewModel.input {
            bindingInput(input: ip)
        }
        
        if let op = self.viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input : VNP4TRegisterInformationVM.Input){
        input.isLoading.subscribe { [weak self] isLoading in
            guard let self = self else {return}
            
            if isLoading.element ?? false {
                self.loading.startAnimating()
            } else {
                self.loading.stopAnimating()
            }
        }
    }
    
    private func bindingOutput(output : VNP4TRegisterInformationVM.Output){
        output.selectedSurveyResult.subscribe(onNext: { survey in
            if survey != nil {
                self.actionView.isHidden = false
                self.surveyPickerLabel.textColor = UIColor(hex: "#00000")
                self.surveyPickerLabel.text = "[\(survey?.code ?? "")] \(survey?.name ?? "")"
                
                self.questionView.isHidden = false
                
                if self.renderQuestionView != nil {
                    self.remove(asChildViewController: self.renderQuestionView!)
                }
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let questionVC = VNP4TSurveyQuestionsVC(nibName: "VNP4TSurveyQuestionsVC", bundle: bundle)
                questionVC.viewModel = VNP4TSurveyQuestionVM(
                    input: VNP4TSurveyQuestionVM.Input(dataQuestions: BehaviorRelay<[VNP4TSurveyQuestion]>(value: survey?.keys ?? []),
                                                       isScroll: BehaviorRelay<Bool>(value: true)),
                    dependency: VNP4TSurveyService())
                self.renderQuestionView = questionVC
                questionVC.onChangeValue = { question in
                    let surveyQuestion = self.viewModel.input?.selectedSurvey.value
                    if surveyQuestion != nil {
                        surveyQuestion!.keys.forEach({ keySurvey in
                            if keySurvey.id == question.id {
                                keySurvey.answerValue = question.answerValue
                            }
                        })
                    }
                }
                
                self.add(asChildViewController: questionVC)
            }
        }).disposed(by: disposeBag)
        
        output.searchResult.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            
            if result.statusCode == 200 {
                let data = result.data
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let dialogOptions = VNP4TDialogOptions(nibName: "VNP4TDialogOptions", bundle: bundle)
                dialogOptions.modalPresentationStyle = .overFullScreen
                var statusType = 0
                if data?.data?.count ?? 0 > 0 {
                    dialogOptions.titleDialog = BehaviorRelay<String>(value: "4t_register_survey_title_notify_exist".localized)
                    dialogOptions.messageDialog = BehaviorRelay<String>(value: "4t_register_survey_message_notify_exist".localized)
                    statusType = VNP4TCollectionType.OLD.rawValue
                    dialogOptions.titleActionCancel = BehaviorRelay<String?>(value: "4t_verify_survey_not_use".localized)
                    dialogOptions.titleActionConfirm = BehaviorRelay<String?>(value: "4t_verify_survey_use".localized)
                } else{
                    dialogOptions.titleDialog = BehaviorRelay<String>(value: "4t_register_survey_title_notify_empty".localized)
                    dialogOptions.messageDialog = BehaviorRelay<String>(value: "4t_register_survey_message_notify_empty".localized)
                    statusType = VNP4TCollectionType.NEW.rawValue
                    dialogOptions.titleActionCancel = BehaviorRelay<String?>(value: "4t_register_survey_cancel".localized)
                    dialogOptions.titleActionConfirm = BehaviorRelay<String?>(value: "4t_verify_survey_create_survey".localized)
                }
                dialogOptions.onCancelAction = {
                    dialogOptions.dismiss(animated: false, completion: nil)
                }
                dialogOptions.onDoneActions = { [weak self] in
                    guard let self = self else { return }
                    
                    dialogOptions.dismiss(animated: false, completion: nil)
                    
                    let bundle = Bundle.init(identifier: VNP4TConstants.bundleIdentifier)
                    let createSurvey = VNP4TCreateElectricSurveyVC(nibName: "VNP4TCreateElectricSurveyVC", bundle: bundle)
                    self.viewModel.output?.selectedSurveyResult.subscribe(onNext: { survey in
                        createSurvey.viewModel = VNP4TCreateElectricSurveyVM(
                            input: VNP4TCreateElectricSurveyVM.Input(
                                surveyType: BehaviorRelay<VNP4TSurveyClassificationTypeSelected>(value: self.viewModel.input?.selectedSurvey.value ?? VNP4TSurveyClassificationTypeSelected()),
                                dataSearch: BehaviorRelay<[VNP4TSurveyQuestion]>(value: data?.data ?? []) ,
                                currentPosition: BehaviorRelay<Int>(value: 0),
                                isLoading: BehaviorRelay<Bool>(value: false),
                                questionTrigger: BehaviorRelay<Bool>(value: false),
                                surveyQuestions: BehaviorRelay<[VNP4TSurveyQuestion]>(value: []),
                                survey: BehaviorRelay<VNP4TSurveyClassificationTypeSelected>(value: survey ?? VNP4TSurveyClassificationTypeSelected()),
                                totalStep: BehaviorRelay<Int>(value: 0),
                                collectionType: BehaviorRelay<Int>(value: statusType),
                                submitDraftTrigger: BehaviorRelay<Bool>(value: false),
                                draftData: BehaviorRelay<VNP4TQuestionKey>(value: VNP4TQuestionKey())),
                            dependency: VNP4TSurveyService())
                    }).disposed(by: self.disposeBag)
                    
                    self.navigationController?.pushViewController(createSurvey, animated: true)
                }
                
                self.parent?.present(dialogOptions, animated: false, completion: nil)
            } else {
                let message = result.message
                let bundle = Bundle(identifier: VNP4TConstants.bundleIdentifier)
                let errorDialog = VNP4TDialogMessage(nibName: "VNP4TDialogMessage", bundle: bundle)
                errorDialog.titleDialog = BehaviorRelay<String>(value: "4t_summary_electrict_survey_title_error".localized.uppercased())
                errorDialog.messageDialog = BehaviorRelay<String>(value: message)
                errorDialog.status = BehaviorRelay<Bool>(value: false)
                errorDialog.modalPresentationStyle = .overFullScreen
                errorDialog.onDoneActions = {
                    errorDialog.dismiss(animated: true, completion: nil)
                }
                
                self.parent?.present(errorDialog, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
    }
}

extension VNP4TRegisterInformationVC {
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChild(viewController)
        
        // Add Child View as Subview
        self.listQuestionView.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = self.listQuestionView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Remove Child View From Parent
        viewController.removeFromParent()
    }
}
