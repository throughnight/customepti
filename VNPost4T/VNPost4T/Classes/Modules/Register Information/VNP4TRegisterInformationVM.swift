//
//  VNP4TRegisterInformationVM.swift
//  
//
//  Created by AEGONA on 9/15/20.
//

import Foundation
import RxCocoa
import RxSwift


class VNP4TRegisterInformationVM : VNP4TViewModelProtocol {
    
    typealias Dependency = VNP4TSurveyService
    
    struct Input {
        let selectedSurvey                              :BehaviorRelay<VNP4TSurveyClassificationTypeSelected?>
        let isLoading                                   :BehaviorRelay<Bool>
        let searchTrigger                               :BehaviorRelay<Bool>
        let surveyVerify                                :BehaviorRelay<VNP4TQuestionKey>
    }
    
    struct Output {
        let selectedSurveyResult                        :Observable<VNP4TSurveyClassificationTypeSelected?>
        let searchResult                                :Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyQuestion]>>>
    }
    
    var input               : Input?
    var output              : Output?
    var dependency          : Dependency?
    
    init(input: Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: VNP4TSurveyService?) -> Output? {
        self.input = input
        
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        
        let selectedSurveyResult = ip.selectedSurvey.asObservable()
            .flatMapLatest { _ -> Observable<VNP4TSurveyClassificationTypeSelected?> in
                return Observable.just(ip.selectedSurvey.value)
        }
        
        let searchResult = ip.searchTrigger.asObservable()
            .filter {$0}
            .withLatestFrom(ip.surveyVerify)
            .do(onNext: { _ in
                ip.isLoading.accept(true)
            }).flatMapLatest { (survey) -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyQuestion]>>> in
                return dp.verifySurveyKey(survey: survey)
        }.do(onNext: { _ in
            ip.isLoading.accept(false)
        })
        
        return VNP4TRegisterInformationVM.Output(selectedSurveyResult: selectedSurveyResult, searchResult: searchResult)
    }
}
