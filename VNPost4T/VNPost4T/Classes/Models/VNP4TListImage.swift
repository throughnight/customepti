//
//  VNP4TListImage.swift
//  VNPost4T
//
//  Created by AEGONA on 13/05/2021.
//

import Foundation
import ObjectMapper

class VNP4TListImage: Mappable {
    
    var id : String?
    var images : [VNP4TImage]?
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    init(id: String?, images: [VNP4TImage]?) {
        self.id = id;
        self.images = images;
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        images <- map["images"]
    }
}
