//
//  VNP4TItemSelect.swift
//  VNPost4T
//
//  Created by AEGONA on 9/30/20.
//

import Foundation
import ObjectMapper

class VNP4TItemSelect: Mappable {
    
    var id : Int?
    var name : String?
    var status : Bool = false
    
    required init?(map: Map) {
        
    }
    
    init(id: Int, name: String, status: Bool) {
        self.id = id
        self.name = name
        self.status = status
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        status <- map["status"]
    }
}
