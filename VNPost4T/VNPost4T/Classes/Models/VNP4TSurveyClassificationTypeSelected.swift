//
//  VNP4TSurveyClassificationTypeSelected.swift
//  
//
//  Created by AEGONA on 9/14/20.
//

import Foundation
import ObjectMapper


class VNP4TSurveyClassificationTypeSelected : Mappable  {
    
    var id = ""
    var code = ""
    var name = ""
    var surveyApply = 0
    var surveyClassification = 0
    var isActive = false
    var validTo = ""
    var validFrom = ""
    var addedStamp = ""
    var totalQuestions = 0
    var keys : [VNP4TSurveyQuestion] = []
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        code <- map["code"]
        name <- map["name"]
        surveyApply <- map["surveyApply"]
        surveyClassification <- map["surveyClassification"]
        isActive <- map["isActive"]
        validTo <- map["validTo"]
        validFrom <- map["validFrom"]
        addedStamp <- map["addedStamp"]
        totalQuestions <- map["totalQuestions"]
        keys <- map["keys"]
    }
}
