//
//  VNP4TAddress.swift
//  
//
//  Created by AEGONA on 9/17/20.
//

import Foundation
import ObjectMapper

class VNP4TAddress: Mappable {
    
    var province : VNP4TQuestionValidations?
    var district : VNP4TQuestionValidations?
    var ward : VNP4TQuestionValidations?
    var address : String?
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        province <- map["province"]
        district <- map["district"]
        ward <- map["ward"]
        address <- map["address"]
    }
}

extension VNP4TAddress {
    
    func getFullAddress() -> String{
        var fullAddress = ""
        if(address != nil){
            if(!address!.isEmpty){
                fullAddress += "\(address!), "
            }
        }
        if(ward != nil){
            if(!ward!.name.isEmpty){
                fullAddress += "\(ward!.name), "
            }
        }
        if(district != nil){
            if(!district!.name.isEmpty){
                fullAddress += "\(district!.name), "
            }
        }
        if(province != nil){
            if(!province!.name.isEmpty){
                fullAddress += "\(province!.name)"
            }
        }
        return fullAddress
    }
    
}
