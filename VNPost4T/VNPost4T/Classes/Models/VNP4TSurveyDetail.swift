//
//  VNP4TSurveyDetail.swift
//  VNPost4T
//
//  Created by AEGONA on 10/1/20.
//

import Foundation
import ObjectMapper

class VNP4TSurveyDetail : Mappable {
    
    var surveySheetId : String?
    var surveyId : String?
    var collectionType : Int?
    var questions : [VNP4TSurveyQuestion]?
    var surveySheetStatus :Int?
    var evaluate : Int?
    var percent : Int?
    var id : String?
    var statusUpdateImg : Bool?
    var listImg : [String]?
    var limitImg : Int?
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        surveySheetId <- map["surveySheetId"]
        surveyId <- map["surveyId"]
        collectionType <- map["collectionType"]
        questions <- map["questions"]
        surveySheetStatus <- map["surveySheetStatus"]
        evaluate <- map["evaluate"]
        percent <- map["percent"]
        id <- map["id"]
        statusUpdateImg <- map["statusUpdateImg"]
        listImg <- map["listImg"]
        limitImg <- map["limitImg"]
    }
    
}
