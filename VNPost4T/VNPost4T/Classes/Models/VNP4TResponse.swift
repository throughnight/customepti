//
//  VNP4TResponse.swift
//  
//
//  Created by AEGONA on 9/14/20.
//

import Foundation
import ObjectMapper

class VNP4TResponse<T> : Mappable {
    
    var search = ""
    var pageIndex = 0
    var pageSize = 10
    var total = 0
    var data : T?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        search <- map["search"]
        pageIndex <- map["pageIndex"]
        pageSize <- map["pageSize"]
        total <- map["total"]
        data <- map["data"]
    }
    
}
