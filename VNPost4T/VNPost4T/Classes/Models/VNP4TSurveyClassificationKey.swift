//
//  VNP4TSurveyClassificationKey.swift
//  
//
//  Created by AEGONA on 9/14/20.
//

import Foundation
import ObjectMapper

class VNP4TSurvayClassificationKey: Mappable {
    var questionId = ""
    var dataType = 0
    var percentPoint = 0
    var placeholder = ""
    var required = false
    var surveyId = ""
    var tooltip = ""
    var question = ""
    var length = 0
    var step = 0
    var type = 0
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        questionId <- map["questionId"]
        dataType <- map["dataType"]
        percentPoint <- map["percentPoint"]
        placeholder <- map["placeholder"]
        required <- map["required"]
        surveyId <- map["surveyId"]
        tooltip <- map["tooltip"]
        question <- map["question"]
        length <- map["length"]
        step <- map["step"]
        type <- map["type"]
    }
}
