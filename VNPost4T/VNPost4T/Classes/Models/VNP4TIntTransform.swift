//
//  IntTransform.swift
//  VNPost4T
//
//  Created by m2 on 9/22/20.
//

import UIKit
import ObjectMapper

class VNP4TIntTransform: TransformType {
    
    typealias Object = Int
    typealias JSON = String
    
    var defaultValue = 0
    
    public init() {
        
    }
    
    public init(_ value: Int) {
        defaultValue = value
    }
    
    func transformFromJSON(_ value: Any?) -> Int? {
        if let intValue = value as? Int {
            return intValue
        }
        if let strValue = value as? String {
            return Int(strValue)
        }
        return 0
    }
    
    func transformToJSON(_ value: Int?) -> String? {
        guard let data = value else{
            return nil
        }
        return "\(data)"
    }
    
}
