//
//  BaseResponse.swift
//  VNPost4T
//
//  Created by m2 on 9/22/20.
//

import UIKit
import ObjectMapper

class VNP4TBaseResponse: Mappable {
    
    var errorCode: Int = 0
    var message: String = ""
    var error : Array<String>? = nil
    var success : Int = 0
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        errorCode <- (map["error_code"], VNP4TIntTransform())
        message <- map["message"]
        error <- map["errors"]
        success <- map["success"]
    }

}
