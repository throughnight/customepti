//
//  VNP4TSurveyQuestion.swift
//  
//
//  Created by AEGONA on 9/15/20.
//

import Foundation
import ObjectMapper


class VNP4TSurveyQuestion : Mappable  {
    
    var id = ""
    var surveyId = ""
    var surveySheetId = ""
    var type = 0
    var placeholder = ""
    var tooltip = ""
    var question = ""
    var length = 0
    var dataType = 0
    var validations: [VNP4TQuestionValidations] = []
    var required = false
    var validation = ""
    var displayOrder = 0
    var step = 0
    var enable = false
    var description = ""
    var defaultValue = ""
    var answerValue = ""
    var imageValue = ""
    var answerId : String?
    var percentPoint = 0
    var checkQueryRequired = false
    var address : VNP4TAddress?
    var isPass = true
    var key = false
    var listImage : [VNP4TImage] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        surveyId <- map["surveyId"]
        surveySheetId <- map["surveySheetId"]
        type <- map["type"]
        placeholder <- map["placeholder"]
        tooltip <- map["tooltip"]
        question <- map["question"]
        length <- map["length"]
        dataType <- map["dataType"]
        validations <- map["validations"]
        required <- map["required"]
        validation <- map["validation"]
        displayOrder <- map["displayOrder"]
        step <- map["step"]
        enable <- map["enable"]
        description <- map["description"]
        defaultValue <- map["defaultValue"]
        answerValue <- map["answerValue"]
        imageValue <- map["imageValue"]
        answerId <- map["answerId"]
        percentPoint <- map["percentPoint"]
        checkQueryRequired <- map["checkQueryRequired"]
        address <- map["address"]
        id <- map["questionId"]
        isPass <- map["isPass"]
        key <- map["key"]
        listImage <- map["listImage"]
    }
    
    init() {
        
    }
}

extension VNP4TSurveyQuestion {
    func convertURL(){
        let data = answerValue
        if data != "" {
            var ans = answerValue.replacingOccurrences(of: "[", with: "")
            ans = ans.replacingOccurrences(of: "]", with: "")
            let dt = ans.split(separator: ",")
            var array : [VNP4TImage] = []
            dt.forEach { item in
                print("item \(item)")
                let itemImg = VNP4TImage()
                itemImg.url = String(item)
                itemImg.url = itemImg.url?.substring(1..<((itemImg.url?.count ?? 0) - 1))
                array.append(itemImg)
            }
            
            listImage = array
        }
    }
    
    func convertAddressToJson() -> String {
        var mAddress = VNP4TAddress()
        if address?.province?.id != "" {
            mAddress.province = address?.province
        }
        if address?.district?.id != "" {
            mAddress.district = address?.district
        }
        if address?.ward?.id != "" {
            mAddress.ward = address?.ward
        }
        if address?.address != "" {
            mAddress.address = address?.address
        }
        if mAddress.province != nil {
            return Mapper().toJSONString(mAddress, prettyPrint: true) ?? ""
        }
        return ""
    }
}

class VNP4TQuestionValidations : Mappable {
    
    var name = ""
    var id = ""
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        id <- map["id"]
    }
}

class VNP4TSummarySurvey : Mappable {
    
    var title = ""
    var questions : [VNP4TSurveyQuestion]?
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        questions <- map["questions"]
    }
}
