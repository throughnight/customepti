//
//  VNP4TImage.swift
//  VNPost4T
//
//  Created by AEGONA on 10/10/20.
//

import Foundation
import ObjectMapper

class VNP4TImage: Mappable {
    
    var image : UIImage?
    var url : String?
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    init(image: UIImage?, url: String?) {
        self.image = image;
        self.url = url;
    }
    
    func mapping(map: Map) {
        image <- map["image"]
        url <- map["url"]
    }
}
