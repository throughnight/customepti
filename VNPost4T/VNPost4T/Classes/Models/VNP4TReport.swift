//
//  VNP4TReport.swift
//  VNPost4T
//
//  Created by AEGONA on 10/2/20.
//

import Foundation
import ObjectMapper

class VNP4TReport : Mappable {
    
    var x: String = ""
    var y: Int = 0
    var y1: Double = 0.0
        
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        x <- map["x"]
        y <- map["y"]
        y1 <- map["y1"]
    }
}
