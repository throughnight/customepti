//
//  VNP4TSurvey.swift
//  VNPost4T
//
//  Created by AEGONA on 9/29/20.
//

import Foundation
import ObjectMapper

class VNP4TSurvey: Mappable {
    var id : String = ""
    var surveySheetCode : String = ""
    var name : String = ""
    var surveyClassifiation : Int?
    var fullName : String = ""
    var phoneNumber : String = ""
    var surveySheetStatus : Int?
    var evaluate : Int?
    var percent : Double? = 0.0
    var companyName : String = ""
    var addedStamp : String = ""
    
    func mapping(map: Map) {
        id <- map["id"]
        surveySheetCode <- map["surveySheetCode"]
        name <- map["name"]
        surveyClassifiation <- map["surveyClassifiation"]
        fullName <- map["fullName"]
        phoneNumber <- map["phoneNumber"]
        surveySheetStatus <- map["surveySheetStatus"]
        evaluate <- map["evaluate"]
        percent <- map["percent"]
        companyName <- map["companyName"]
        addedStamp <- map["addedStamp"]
        
    }
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
}
