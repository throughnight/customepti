//
//  VNP4TUserToken.swift
//  VNPost4T
//
//  Created by m2 on 9/22/20.
//

import Foundation

import UIKit
import ObjectMapper

class VNP4TUserToken: Mappable {
    
    var token: String = ""
    var userName: String = ""
    var expiresIn: Int = 0
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        token <- map["token"]
        userName <- map["userName"]
        expiresIn <- map["expiresIn"]
    }

}
