//
//  VNP4TLimitImageUpload.swift
//  VNPost4T
//
//  Created by m2 on 9/30/20.
//

import Foundation
import UIKit
import ObjectMapper

class VNP4TLimitImageUpload: Mappable {
    
    var totalImg: Int = 0
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        totalImg <- map["totalImg"]
    }

}
