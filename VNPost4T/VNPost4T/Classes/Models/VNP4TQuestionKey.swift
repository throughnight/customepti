//
//  VNP4TQuestionKey.swift
//  VNPost4T
//
//  Created by AEGONA on 9/18/20.
//

import Foundation
import ObjectMapper

typealias Codable = Decodable & Encodable

class VNP4TQuestionKey: Codable {
    var surveyId: String = ""
    var answers: [VNP4TQuestion] = []
    var collectionType : Int = 0
    var status : Int = 0
    var surveySheetId : String?
    var listImages : [String] = []
    
    enum CodingKeys: String, CodingKey {
        case surveyId, answers,collectionType,status,surveySheetId,listImages
    }
    
    init(surveyId: String,answers: [VNP4TQuestion], collectionType : Int, status : Int, surveySheetId : String?){
        self.surveyId = surveyId
        self.answers = answers
        self.collectionType = collectionType
        self.status = status
        self.surveySheetId = surveySheetId
    }
    
    init(surveyId: String,answers: [VNP4TQuestion], collectionType : Int, status : Int){
        self.surveyId = surveyId
        self.answers = answers
        self.collectionType = collectionType
        self.status = status
    }
    
    init(surveyId: String,answers: [VNP4TQuestion]) {
        self.surveyId = surveyId
        self.answers = answers
    }
    
    init(surveyId: String, listImage: [String]) {
        self.surveyId = surveyId
        self.listImages = listImage
    }
    
    init() {
        
    }
}

class VNP4TQuestion: Codable {
    
    var questionId : String = ""
    var value: String?
    var answerId : String?
    var step: Int = 0
    var profileType: Int?
    
    enum CodingKeys: String, CodingKey {
        case questionId, value, answerId, step, profileType
    }
    
    init(id: String,value : String) {
        self.questionId = id
        self.value = value
    }
    
    init(id: String,value : String,answerId : String?,step: Int, profileType : Int?){
        self.questionId = id
        self.value = value
        self.answerId = answerId
        self.step = step
        self.profileType = profileType
    }
    
    init(id: String, answerId : String?,step: Int, profileType : Int?){
        self.questionId = id
        self.answerId = answerId
        self.step = step
        self.profileType = profileType
    }
    
    init(profile : Int, questionId: String, step: Int, value: String){
        self.value = value
        self.step = step
        self.profileType = profile
        self.questionId = questionId
    }
    
    init(questionId: String, step: Int, value: String){
        self.value = value
        self.step = step
        self.questionId = questionId
    }
}
