//
//  VNP4TSurveyService.swift
//  VNPost4T
//
//  Created by AEGONA on 9/28/20.
//

import Foundation
import Moya
import Alamofire
import RxSwift

class VNP4TSurveyService: VNP4TBaseService<VNP4TAPI> {
    
    func getSurveyClassification(surveyClassification : Int?, page : Int = 0, size : Int = 10, search : String = "") -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyClassificationTypeSelected]>>>{
        
        let request = SurveyClassificationRequest(surveyClassification: surveyClassification, index: page, pageSize: size, search: search)
        
        return requestApi(request: .getSurveyClasification(request)).asObservable()
            .flatMap { (response) -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyClassificationTypeSelected]>>> in
                self.requestArray(response)
        }
    }
    
    func getSurveyQuestion(surveyId: String) -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyQuestion]>>> {
        
        let request = SurveyQuestionRequest(surveyId: surveyId)
        
        return requestApi(request: .getSurveyQuestion(request)).asObservable()
            .flatMap { (response) -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyQuestion]>>> in
                self.requestArray(response)
        }
    }
    
    func verifySurveyKey(survey: VNP4TQuestionKey) -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyQuestion]>>> {
        return requestApi(request: .verifySurveyKey(data: survey)).asObservable()
            .flatMap { (response) -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyQuestion]>>> in
                self.requestArray(response)
        }
    }
    
    func createElectrictSurvey(survey: VNP4TQuestionKey) -> Observable<VNP4TResponseModel<Any>> {
        return requestApi(request: .createElectrictSurvey(data: survey)).asObservable()
            .flatMap { (response) -> Observable<VNP4TResponseModel<Any>> in
                self.requestAny(response)
        }
    }
    
    func getSurveyHistory(tab: Int, page: Int, size : Int,search :String, surveyId: String?, collectionType: Int?, surveyStatus : Int?) -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurvey]>>> {
        let request = SurveyHistoryRequest(tabType: tab, pageIndex: page, pageSize: size,search : search, surveyId: surveyId, collectionType: collectionType, surveyStatus: surveyStatus)
        return requestApi(request: .getHistorySurvey(request)).asObservable()
            .flatMap { (response) -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurvey]>>> in
                self.requestArray(response)
            }
    }
    
    func getSurveyEDetail(id: String) -> Observable<VNP4TResponseModel<VNP4TSurveyDetail>> {
        return requestApi(request: .getSurveyEDetail(id: id)).asObservable()
            .flatMap { (response) -> Observable<VNP4TResponseModel<VNP4TSurveyDetail>> in
                self.request(response)
            }
    }
    
    func updateESurvey(survey: VNP4TQuestionKey) -> Observable<VNP4TResponseModel<Any>> {
        return requestApi(request: .updateESurvey(data: survey)).asObservable()
            .flatMap { (response) -> Observable<VNP4TResponseModel<Any>> in
                self.requestAny(response)
        }
    }
    
    func deleteESurvey(id: String) -> Observable<VNP4TResponseModel<Any>> {
        return requestApi(request: .deleteESurvey(id: id)).asObservable()
            .flatMap { (response) -> Observable<VNP4TResponseModel<Any>> in
                self.requestAny(response)
        }
    }
    
    func createPhysicalSurvey(data: VNP4TQuestionKey) -> Observable<VNP4TResponseModel<Any>> {
        return requestApi(request: .createPhysicalSurvey(data: data)).asObservable()
            .flatMap { (response) -> Observable<VNP4TResponseModel<Any>> in
                self.requestAny(response)
            }
    }
    
    func getSurveyPhysicalDetail(id: String) -> Observable<VNP4TResponseModel<VNP4TSurveyDetail>> {
        return requestApi(request: .getSurveyFDetail(id: id)).asObservable()
            .flatMap { (response) -> Observable<VNP4TResponseModel<VNP4TSurveyDetail>> in
                self.request(response)
            }
    }
    
    func updateFSurvey(id: String, data: VNP4TQuestionKey) -> Observable<VNP4TResponseModel<Any>> {
        return requestApi(request: .updateFSurvey(id: id, data: data)).asObservable()
            .flatMap { (response) -> Observable<VNP4TResponseModel<Any>> in
                self.requestAny(response)
            }
    }
}
