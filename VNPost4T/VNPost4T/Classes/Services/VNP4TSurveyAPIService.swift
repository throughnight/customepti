//
//  VNP4TSurveyService.swift
//  VNPost4T
//
//  Created by m2 on 9/30/20.
//

import Foundation
import Moya
import Alamofire
import RxSwift

class VNP4TSurveyAPIService: VNP4TBaseService<VNP4TAPISurvey> {
    
    func getDetectSurveyItem(code: String) ->Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyClassificationTypeSelected]>>>
    {
        let request = CodeRequest(code: code)
        return requestApi(request: .getDetectSurveyItemAsync(request)).asObservable()
            .flatMap { (response) -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TSurveyClassificationTypeSelected]>>> in
                self.requestArray(response)
            }
    }
}
