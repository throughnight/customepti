//
//  VNP4TAuthService.swift
//  VNPost4T
//
//  Created by m2 on 9/23/20.
//

import Foundation
import Moya
import Alamofire
import RxSwift

class VNP4TAuthService: VNP4TBaseService<VNP4TAuth> {
    func getToken(token: String) -> Observable<VNP4TResponseModel<VNP4TUserToken>> {
        let request = TokenRequest(token: token)
        return requestApi(request: .getToken(request)).asObservable().flatMap { (response) -> Observable<VNP4TResponseModel<VNP4TUserToken>> in
            self.request(response)
        }
    }
}
