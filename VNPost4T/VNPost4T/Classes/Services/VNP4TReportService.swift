//
//  VNP4TReportService.swift
//  VNPost4T
//
//  Created by AEGONA on 10/2/20.
//

import Foundation
import Moya
import Alamofire
import RxSwift

class VNP4TReportService: VNP4TBaseService<VNP4TAPI> {
    
    func getReportMB(type: String, dateFrom : String?, dateTo: String?, surveyId: String?) -> Observable<VNP4TResponseModel<[VNP4TReport]>> {
        let request = ReportMBRequest(type: type, surveyId: surveyId, dateFrom: dateFrom, dateTo: dateTo)
        return requestApi(request: .getReportMB(request)).asObservable()
            .flatMap { (response) -> Observable<VNP4TResponseModel<[VNP4TReport]>> in
                self.requestArrayBase(response)
            }
    }
}
