//
//  VNP4TSurveyCommon.swift
//  VNPost4T
//
//  Created by m2 on 9/30/20.
//

import Foundation
import Moya
import Alamofire
import RxSwift

class VNP4TCommonService: VNP4TBaseService<VNP4TAPICommon> {
        
    func getMaxImageToSend(id: String) ->Observable<VNP4TResponseModel<VNP4TLimitImageUpload>>
    {
        let request = SurveyIdRequest(id: id)
        return requestApi(request: .getMaxImageToSendAsync(request)).asObservable()
            .flatMap { (response) -> Observable<VNP4TResponseModel<VNP4TLimitImageUpload>> in
                self.request(response)
            }
    }
    
    func getProvinces() -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TQuestionValidations]>>> {
        return requestApi(request: .getProvinces).asObservable()
            .flatMap { (response) -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TQuestionValidations]>>> in
                self.requestArray(response)
        }
    }
    
    func getDistricts(id: String) -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TQuestionValidations]>>> {
        return requestApi(request: .getDistricts(id: Int(id) ?? 0)).asObservable()
            .flatMap { (response) -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TQuestionValidations]>>> in
                self.requestArray(response)
        }
    }
    
    func getWards(districtId: String) -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TQuestionValidations]>>> {
        return requestApi(request: .getWards(districtId: Int(districtId) ?? 0)).asObservable()
            .flatMap { (response) -> Observable<VNP4TResponseModel<VNP4TResponse<[VNP4TQuestionValidations]>>> in
                self.requestArray(response)
        }
    }
}
