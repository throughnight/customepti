//
//  VNP4TBaseService.swift
//  VNPost4T
//
//  Created by m2 on 9/23/20.
//

import Foundation
import ObjectMapper
import Moya
import Alamofire
import RxSwift
import Network

class DefaultAlamofireSession: Alamofire.Session {
    static let shared: DefaultAlamofireSession = {
        let configuration = URLSessionConfiguration.default
        configuration.headers = .default
        configuration.timeoutIntervalForRequest = 30 // as seconds, you can set your request timeout
        configuration.timeoutIntervalForResource = 30 // as seconds, you can set your resource timeout
        configuration.requestCachePolicy = .useProtocolCachePolicy
        return DefaultAlamofireSession(configuration: configuration)
    }()
}

struct VNP4TVerbosePlugin: PluginType {
    
    let verbose: Bool
    
    func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        
        #if DEBUG
        print("--> \(String(describing: request.method!.rawValue.description))")
        print("\(String(describing: request.url!.description))")
        print("\(request.headers.description)")
        print("\(String(describing: request.httpBody))")
        print(" --> END \(String(describing: request.method!.rawValue.description))")
        #endif
        
        return request
        
    }
    
}

enum APIError: Error {
    case invalidURL(url: String)
    case invalidResponseData(data: Any)
    case error(responseCode: Int, data: Any)
    case MissingKeyOrType(key: String)
}

class VNP4TBaseService<T: TargetType> {
    
    public let provider = MoyaProvider<T>(session: DefaultAlamofireSession.shared, plugins: [VNP4TVerbosePlugin(verbose: true)])
    
    func requestApi(request: T) -> Observable<Response> {
        provider.rx.request(request).asObservable().flatMap { (response) -> Observable<Response> in
            return Observable.just(response)
        }.catchErrorJustReturn(Response.init(statusCode: 1001, data: Data()))
    }
    
    private func process(response: Response) -> Observable<Any> {
        
        #if DEBUG
        print("--> \(response.statusCode) \(String(describing: response.request?.url?.description))")
        print("\(response.request?.headers.description)")
        print("\(String(describing: (try? response.mapJSON(failsOnEmptyData: true)) as? [String:Any]))")
        print(" --> END HTTP")
        #endif
        
        if let statusCode = response.response?.statusCode {
            switch statusCode{
            case 200:
                return Observable.just((try? response.mapJSON(failsOnEmptyData: true)) as? [String:Any] as Any)
            case 401:                 //unauthorized
                return Observable.just(VNP4TResponseModel<Any>(statusCode: response.statusCode,
                                                               success: false,
                                                               message: "4t_base_service_error_occur".localized).toJSON() as [String:Any])
            case 403:                  //accessDenied
                return Observable.just(VNP4TResponseModel<Any>(statusCode: response.statusCode,
                                                               success: false,
                                                               message: "4t_base_service_no_permission_error".localized).toJSON() as [String:Any])
            case 500:                  //serverError
                return Observable.just(VNP4TResponseModel<Any>(statusCode: response.statusCode,
                                                               success: false,
                                                               message: "4t_base_service_error_server_msg".localized).toJSON() as [String:Any])
            case 502:                   //badGateway
                return Observable.just(VNP4TResponseModel<Any>(statusCode: response.statusCode,
                                                               success: false,
                                                               message: "4t_base_service_error_server_msg".localized).toJSON() as [String:Any])
            case 504:                //gatewayTimeout
                return Observable.just(VNP4TResponseModel<Any>(statusCode: response.statusCode,
                                                               success: false,
                                                               message: "4t_base_service_error_client_timeout_msg".localized).toJSON() as [String:Any])
            default:
                return Observable.just(VNP4TResponseModel<Any>(statusCode: response.statusCode,
                                                               success: false,
                                                               message: "4t_base_service_error_occur".localized).toJSON() as [String:Any])
            }
        }
        else{
            return Observable.just(VNP4TResponseModel<Any>(statusCode: response.statusCode,
                                                           success: false,
                                                           message: "4t_base_service_error_occur".localized).toJSON() as [String:Any])
        }
        
    }
    
    func request<T: Mappable>(_ input: Response) -> Observable<VNP4TResponseModel<T>> {
        return process(response: input)
            .map { data -> VNP4TResponseModel<T> in
                if let json = data as? [String:Any],
                   let item = VNP4TResponseModel<T>(JSON: json) {
                    if(json["data"] as? [String : Any] != nil){
                        item.data = Mapper<T>().map(JSON: json["data"] as! [String : Any])
                    }
                    return item
                } else {
                    throw APIError.invalidResponseData(data: data)
                }
            }
        
    }
    
    func requestArray<T: Mappable>(_ input: Response) -> Observable<VNP4TResponseModel<VNP4TResponse<[T]>>> {
        return process(response: input)
            .map { data -> VNP4TResponseModel<VNP4TResponse<[T]>> in
                if let json = data as? [String:Any],
                   let item = VNP4TResponseModel<VNP4TResponse<[T]>>(JSON: json) {
                    if let rawData = json["data"] as? [String:Any] {
                        let itemData = VNP4TResponse<[T]>(JSON: rawData)
                        if let array = Mapper<T>().mapArray(JSONObject: rawData["data"]) {
                            itemData?.data = array
                        }
                        item.data = itemData
                    }
                    return item
                } else {
                    throw APIError.invalidResponseData(data: data)
                }
            }
    }
    

    func requestAny(_ input: Response) -> Observable<VNP4TResponseModel<Any>> {
        return process(response: input)
            .map { data -> VNP4TResponseModel<Any> in
                if let json = data as? [String:Any],
                   let item = VNP4TResponseModel<Any>(JSON: json) {
                    return item
                } else {
                    throw APIError.invalidResponseData(data: data)
                }
            }
        
    }
    
    func requestArrayBase<T: Mappable>(_ input: Response) -> Observable<VNP4TResponseModel<[T]>> {
        return process(response: input)
            .map { data -> VNP4TResponseModel<[T]> in
                if let json = data as? [String:Any],
                   let item = VNP4TResponseModel<[T]>(JSON: json) {
                    if let rawData = json["data"] as? [[String:Any]] {
                        if let array = Mapper<T>().mapArray(JSONObject: rawData) {
                            item.data = array
                        }
                    }
                    return item
                } else {
                    throw APIError.invalidResponseData(data: data)
                }
            }
    }

}

