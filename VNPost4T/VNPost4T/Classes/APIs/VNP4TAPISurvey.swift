//
//  VNP4TSurvey.swift
//  VNPost4T
//
//  Created by m2 on 9/30/20.
//

import Foundation
import Moya
import Alamofire

enum VNP4TAPISurvey {
    case getDetectSurveyItemAsync(CodeRequest)
}

extension VNP4TAPISurvey: TargetType{
    var baseURL: URL {
        return URL(string: "\(VNP4TConfiguration.shared.environment.apiDomain)/Survey/\(VNP4TConfiguration.shared.environment.apiVersion)")!
    }
    
    var path: String {
        switch self {
        case .getDetectSurveyItemAsync:
            return "/Detect_SurveyItem"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getDetectSurveyItemAsync:
            return Moya.Method.get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getDetectSurveyItemAsync(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.default)
        default:
            return .requestPlain
        }
        
    }
    
    var headers: [String : String]? {
        var defaultHeaders = ["Content-Type":"application/json"]
        if let accessToken = VNP4TManager.shared.accessToken {
            defaultHeaders["Authorization"] = "Bearer \(accessToken)"
        }
        
        return defaultHeaders
    }
    
    var url : String {
        return "\(baseURL)\(path)"
    }
    
    var encoding : ParameterEncoding {
        switch method {
        case .get:
            return URLEncoding.default
        default:
            return JSONEncoding()
        }
    }
    
}

struct CodeRequest : VNP4TAPIRequestProtocol {
    
    var code : String
    
    func toParameters() -> [String : Any] {
        return ["code" : code]
    }
}
