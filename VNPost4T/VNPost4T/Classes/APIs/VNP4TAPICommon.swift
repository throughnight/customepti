//
//  VNP4TCommon.swift
//  VNPost4T
//
//  Created by m2 on 9/30/20.
//

import Foundation
import Moya
import Alamofire

enum VNP4TAPICommon {
    case getMaxImageToSendAsync(SurveyIdRequest)
    case getProvinces
    case getDistricts(id: Int)
    case getWards(districtId: Int)
}

extension VNP4TAPICommon: TargetType{
    var baseURL: URL {
        return URL(string: "\(VNP4TConfiguration.shared.environment.apiDomain)/Common/\(VNP4TConfiguration.shared.environment.apiVersion)")!
    }
    
    var path: String {
        switch self {
        case .getMaxImageToSendAsync:
            return "/TotalImgSurvey"
        case .getProvinces:
            return "/Provinces"
        case .getDistricts(let id):
            return "/Provinces/\(id)/Districts"
        case .getWards(let districtId):
            return "/Districts/\(districtId)/Wards"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getMaxImageToSendAsync, .getProvinces, .getDistricts, .getWards:
            return Moya.Method.get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getMaxImageToSendAsync(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.default)
        default:
            return .requestPlain
        }
        
    }
    
    var headers: [String : String]? {
        var defaultHeaders = ["Content-Type":"application/json"]
        if let accessToken = VNP4TManager.shared.accessToken {
            defaultHeaders["Authorization"] = "Bearer \(accessToken)"
        }
        
        return defaultHeaders
    }
    
    var url : String {
        return "\(baseURL)\(path)"
    }
    
    var encoding : ParameterEncoding {
        switch method {
        case .get:
            return URLEncoding.default
        default:
            return JSONEncoding()
        }
    }
    
}

struct SurveyIdRequest : VNP4TAPIRequestProtocol {
    
    var id : String
    
    func toParameters() -> [String : Any] {
        return ["idSurvey" : id]
    }
}

