//
//  VNP4TAuthAPI.swift
//  VNPost4T
//
//  Created by m2 on 9/23/20.
//

import Foundation
import Moya
import Alamofire

enum VNP4TAuth {
    case getToken(TokenRequest)
}

extension VNP4TAuth: TargetType{
    var baseURL: URL {
        return URL(string: "\(VNP4TConfiguration.shared.environment.apiDomain)/Auth/\(VNP4TConfiguration.shared.environment.apiVersion)")!
    }
    
    var path: String {
        switch self {
        case .getToken:
            return "/GetTokenMobile"
        }
    }
    
    var method: Moya.Method {
        return Moya.Method.get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getToken(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        let defaultHeaders = ["Content-Type":"application/json"]
        return defaultHeaders
    }
    
    var url : String {
        return "\(baseURL)\(path)"
    }
    
    var encoding : ParameterEncoding {
        switch method {
        case .get:
            return URLEncoding.default
        default:
            return JSONEncoding()
        }
    }
    
}

struct TokenRequest : VNP4TAPIRequestProtocol {
    
    var token : String
    
    func toParameters() -> [String : Any] {
        return ["tokenMobile" : token]
    }
}
