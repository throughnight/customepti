//
//  VNP4TAPI.swift
//  
//
//  Created by AEGONA on 9/11/20.
//

import Foundation
import Moya
import Alamofire

enum VNP4TAPIResult<T> {
    
    case ok(data : T)
    case error(error : String)
    
    func result() -> T? {
        switch self {
        case .ok(data: let apiResult):
            return apiResult
        default:
            return nil
        }
    }
    
}

enum VNP4TAPI {
    case getDemoApi
    case getToken
    case getSurveyClasification(SurveyClassificationRequest)
    case getSurveyQuestion(SurveyQuestionRequest)
    case verifySurveyKey(data: VNP4TQuestionKey)
    case createElectrictSurvey(data : VNP4TQuestionKey)
    case getHistorySurvey(SurveyHistoryRequest)
    case getSurveyEDetail(id: String)
    case updateESurvey(data : VNP4TQuestionKey)
    case deleteESurvey(id : String)
    case getReportMB(ReportMBRequest)
    case createPhysicalSurvey(data : VNP4TQuestionKey)
    case getSurveyFDetail(id: String)
    case updateFSurvey(id: String, data: VNP4TQuestionKey)
}

extension VNP4TAPI : TargetType {
    
    var baseURL: URL {
        switch self {
        case .getDemoApi:
            return URL(string: VNP4TConfiguration.shared.environment.tokenDomain)!
        case .getSurveyClasification, .getSurveyQuestion, .verifySurveyKey, .createElectrictSurvey, .getHistorySurvey, .getSurveyEDetail, .updateESurvey, .deleteESurvey, .createPhysicalSurvey, .getSurveyFDetail, .updateFSurvey:
            return URL(string: "\(VNP4TConfiguration.shared.environment.apiDomain)/Survey/\(VNP4TConfiguration.shared.environment.apiVersion)")!
        case .getReportMB:
            return URL(string: "\(VNP4TConfiguration.shared.environment.apiDomain)/Report/\(VNP4TConfiguration.shared.environment.apiVersion)")!
        default:
            return URL(string: VNP4TConfiguration.shared.environment.apiDomain)!
        }
    }
    
    var path: String {
        switch self {
        case .getDemoApi:
            return "/connect/token"
        case .getToken:
            return "/Token"
        case .getSurveyClasification:
            return "/GetSurvey_Mobile"
        case .getSurveyQuestion:
            return "/Question_Mobile"
        case .verifySurveyKey:
            return "/VerifyKey_Mobile"
        case .createElectrictSurvey:
            return "/E_CreateSheet_Mobile"
        case .getHistorySurvey:
            return "/GetSurveySheetHistory_Mobile"
        case .getSurveyEDetail(let id):
            return "/SurveySheetEDetail_Mobile/\(id)"
        case .updateESurvey:
            return "/E_UpdateSheet_Mobile"
        case .deleteESurvey(let id):
            return "/DeleteNewSheet_Mobile/\(id)"
        case .getReportMB:
            return "/GetReportMobile"
        case .createPhysicalSurvey:
            return "F_CreateSheet_Mobile"
        case .getSurveyFDetail(let id):
            return "/SurveySheetFDetail_Mobile/\(id)"
        case .updateFSurvey:
            return "F_UpdateImageSurveySheet"
        }
    }
    
    var encoding : ParameterEncoding {
        switch method {
        case .get:
            return URLEncoding.default
        default:
            return JSONEncoding()
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getDemoApi, .verifySurveyKey, .createElectrictSurvey, .createPhysicalSurvey:
            return Moya.Method.post
        case .updateFSurvey, .updateESurvey:
            return Moya.Method.put
        case .deleteESurvey:
            return Moya.Method.delete
        default:
            return Moya.Method.get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getSurveyClasification(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.default)
        case .getSurveyQuestion(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.default)
        case .verifySurveyKey(let request):
            return .requestJSONEncodable(request)
        case .createElectrictSurvey(let request):
            return .requestJSONEncodable(request)
        case .getHistorySurvey(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.default)
        case .updateESurvey(let request):
            return .requestJSONEncodable(request)
        case .getReportMB(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.default)
        case .createPhysicalSurvey(let request):
            return .requestJSONEncodable(request)
        case .updateFSurvey(let id, let request):
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(request) {
                return .requestCompositeData(bodyData: encoded, urlParameters: ["id" : id])
            }
            return .requestJSONEncodable(request)
        default:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        var defaultHeaders = ["Content-Type":"application/json"]
        if let accessToken = VNP4TManager.shared.accessToken {
            defaultHeaders["Authorization"] = "Bearer \(accessToken)"
        }
        
        return defaultHeaders
    }
    
    private func parseDictionaryToString(dict: Dictionary<String, Any>) -> String {
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dict,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .utf8)
            
            return theJSONText!
        }
        
        return ""
    }
    
}

struct JsonArrayEncoding: Moya.ParameterEncoding {
    
    public static var `default`: JsonArrayEncoding { return JsonArrayEncoding() }
    
    
    /// Creates a URL request by encoding parameters and applying them onto an existing request.
    ///
    /// - parameter urlRequest: The request to have parameters applied.
    /// - parameter parameters: The parameters to apply.
    ///
    /// - throws: An `AFError.parameterEncodingFailed` error if encoding fails.
    ///
    /// - returns: The encoded request.
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: [String: Any]?) throws -> URLRequest {
        var req = try urlRequest.asURLRequest()
        let json = try JSONSerialization.data(withJSONObject: parameters!["jsonArray"]!, options: JSONSerialization.WritingOptions.prettyPrinted)
        req.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        req.httpBody = json
        return req
    }
    
}

struct SurveyClassificationRequest : VNP4TAPIRequestProtocol{
    
    var surveyClassification : Int?
    var index : Int
    var pageSize : Int
    var search : String
    
    func toParameters() -> [String : Any] {
        var params : [String:Any] = ["pageIndex" : index,
                      "pageSize" : pageSize,
                      "search" : search]
        if surveyClassification != nil {
            params["SurveyClassification"] = surveyClassification
        }
        return params
    }
}

struct SurveyQuestionRequest : VNP4TAPIRequestProtocol {
    
    var surveyId : String
    
    func toParameters() -> [String : Any] {
        return ["surveyId" : surveyId]
    }
}

struct VerifySurveyKeyRequest : VNP4TAPIRequestProtocol {
    
    var answers : [VNP4TQuestion] = []
    var surveyId = ""
    
    func toParameters() -> [String : Any] {
        return ["answers" : answers, "surveyId" : surveyId]
    }
}

struct SurveyHistoryRequest : VNP4TAPIRequestProtocol {
    var tabType: Int
    var pageIndex : Int
    var pageSize : Int
    var search : String
    var surveyId : String?
    var collectionType : Int?
    var surveyStatus : Int?
    
    func toParameters() -> [String : Any] {
        var params : [String:Any] = ["tabType" : tabType,
                      "pageIndex" : pageIndex,
                      "pageSize" : pageSize,
                      "search" : search]
        if surveyId != nil {
            params["surveyId"] = surveyId
        }
        if collectionType != nil {
            params["collectionType"] = collectionType
        }
        if surveyStatus != nil {
            params["surveyStatus"] = surveyStatus
        }
        return params
    }
}

struct ReportMBRequest : VNP4TAPIRequestProtocol {
    
    var type : String
    var surveyId : String?
    var dateFrom : String?
    var dateTo : String?
    
    func toParameters() -> [String : Any] {
        var params : [String:Any] = ["type" : type]
        if surveyId != nil {
            params["surveyId"] = surveyId
        }
        if dateFrom != nil {
            params["dateFrom"] = dateFrom
        }
        if dateTo != nil {
            params["dateTo"] = dateTo
        }
        return params
    }
    
}
