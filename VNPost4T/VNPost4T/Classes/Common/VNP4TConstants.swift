//
//  4TConstants.swift
//  
//
//  Created by AEGONA on 9/10/20.
//

import Foundation
import UIKit

// MARK: Server URl
public enum VNP4TEnvironment: String {
    
    case develop = "develop"
    case staging = "staging"
    case production = "production"
    
    var apiDomain: String {
        get {
            switch self {
            case .develop: return "https://ins02.vsit.com.vn"
            case .staging: return "https://ins01.vsit.com.vn"
            case .production: return "https://ins.vsit.com.vn"
            }
        }
    }
    
    var apiVersion : String {
        get {
            switch self {
            case .develop: return "v1.0"
            case .staging: return "v1.0"
            case .production : return "v1.0"
            }
        }
    }
    
    var tokenDomain: String {
        get {
            switch self {
            case .develop: return "https://ins02.vsit.com.vn”"
            case .staging: return "https://ins01.vsit.com.vn"
            case .production: return "https://ins.vsit.com.vn"
            }
        }
    }
    
    var bitbucket : String {
        get {
            switch self {
            case .develop: return "gs://bucket_tttt03"
            case .staging: return "gs://bucket_tttt04"
            case .production: return "gs://bucket_tttt"
            }
        }
    }
    
}

struct VNP4TConfiguration {
    
    static var shared = VNP4TConfiguration()
    
    private init() {}
    
    var environment: VNP4TEnvironment = .staging
}

// MARK: SDK  Constants
enum VNP4TConstants {
    static let bundleIdentifier = "org.cocoapods.VNPost4T"
    // MARK: - Color
    static let navigationBarMainColor = UIColor(hex: "FCB71E")
    static let navigationBarSubColor = UIColor(hex: "F07700")
    
    static let buttonMainColor = UIColor(hex: "025BBB")
    static let buttonSubColor = UIColor(hex: "3692F4")
    
    static let tabBarMainColor = UIColor(hex: "0064CB")
    static let tabBarSubColor = UIColor(hex: "2E8DF0")
    
    // MARK: - Image
    
    static let regexPhone = "^\\b(086|096|097|098|032|033|034|035|036|037|038|039|089|090|093|070|079|077|076|078|088|091|094|083|084|085|081|082|092|056|058|099|059)\\d{7}$"
}

enum VNP4TQuestionCustomerType : Int{
    case FULLNAME = 0
    case DOB = 1
    case GENDER = 2
    case ID_NUMBER = 3
    case ISSUE_ID_NUMBER_DATE = 4
    case ISSUE_ID_NUMBER_PLACE = 5
    case PASSPORT = 6
    case ORTHER = 7
    case PERMANENT_ADDRESS = 8
    case TEMPORARY_ADDRESS = 9
    case ACADEMIC_LEVEL = 10
    case PHONE_NUMBER = 11
    case EMAIL = 12
    case MARITAL_STATUS = 13
    case JOB_TITLE = 14
}

enum VNP4TQuestionType: Int {
    case TEXT = 0
    case NUMBER = 1
    case DOUBLE = 2
    case PERCENTAGE = 3
    case BOOLEAN = 4
    case DATE = 5
    case LIST = 6
    case PHOTO = 7
}

enum VNP4TCollectionType : Int {
    case NEW = 0
    case OLD = 1
}

enum VNP4TSurveySaveType : Int {
    case DRAFT = 0
    case SUBMIT = 1
}

enum VNP4TErrorCode : String {
    case SURVEY_NOT_CHANGE = "SURVEYSHEET_NOT_CHANGE"
}

enum VNP4TSurveyType : Int {
    case ELECTRICAL = 0
    case PHYSICAL = 1
}

enum VNP4TSurveyStatus : Int {
    case NEW = 0
    case PROCRESSING = 1
    case APPROVED = 5
    case CANCEL = 6
}

enum VNP4TEvaluate: Int {
    case BAD = 1
    case GOOD = 0
}

enum VNP4TReportType : String {
    case DAY = "DAY"
    case WEEK = "WEEK"
    case MONTH = "MONTH"
}

enum VNP4TTypeSurveyUploadImage : String {
    case ELECTRICAL = "E_ELECTRIC_FORM"
    case PHYSICAL = "F_PHYSICAL_FORM"
}
