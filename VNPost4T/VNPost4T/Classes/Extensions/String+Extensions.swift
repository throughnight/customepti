//
//  String+Extensions.swift
//  
//
//  Created by AEGONA on 9/10/20.
//

import Foundation

extension String {
    
    func vnp4TToDate(format: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        
        return formatter.date(from: self)
    }
    
    static func vnp4TFormatCurrency(_ inputNumber: NSNumber, symbol: String = "VND") -> String? {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.currencySymbol = symbol
        currencyFormatter.currencyGroupingSeparator = ","
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.positiveFormat = "#,##0 ¤"
        // localize to your grouping and decimal separator
        currencyFormatter.locale = Locale.current
        
        return currencyFormatter.string(from: inputNumber)
    }
    
    func vnp4TIsValidEmail() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        let result = emailTest.evaluate(with: self)
        return result
    }
    
    func vnp4TIsValidPhone() -> Bool {
        //        let phoneRegex = "^[0]{1}[19]{1}[0-9]{8,9}$"
        let phoneRegex = VNP4TConstants.regexPhone
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        let result =  phoneTest.evaluate(with: self)
        return result
    }
    
    func vnp4TIsValidIdentityNumber() -> Bool {
        let identityNumberRegex = "^[0-9]{9}$|^[0-9]{12}$"
        let identityNumberTest = NSPredicate(format: "SELF MATCHES %@", identityNumberRegex)
        let result =  identityNumberTest.evaluate(with: self)
        return result
    }
    
    func vnp4TIsValidText() -> Bool {
        let emailRegex = "[A-Za-z]"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        let result = emailTest.evaluate(with: self)
        return result
    }
    
    func vnp4TIsValidName() -> Bool {
        let nameRegex = "^[A-Za-zàáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹýÀÁÃẠẢĂẮẰẲẴẶÂẤẦẨẪẬÈÉẸẺẼÊỀẾỂỄỆĐÌÍĨỈỊÒÓÕỌỎÔỐỒỔỖỘƠỚỜỞỠỢÙÚŨỤỦƯỨỪỬỮỰỲỴỶỸÝ]*((\\s)*[A-Za-zàáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹýÀÁÃẠẢĂẮẰẲẴẶÂẤẦẨẪẬÈÉẸẺẼÊỀẾỂỄỆĐÌÍĨỈỊÒÓÕỌỎÔỐỒỔỖỘƠỚỜỞỠỢÙÚŨỤỦƯỨỪỬỮỰỲỴỶỸÝ])*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", nameRegex)
        var result = emailTest.evaluate(with: self)
        if result {
            if self.trimmingCharacters(in: .whitespacesAndNewlines).contains(" ") {
                result = true
            } else {
                result = false
            }
        }
        return result
    }
    
    func vnp4TFormatDateSubmit() -> String? {
        let date = self.vnp4TToDate(format: "dd/MM/yyyy")
        let formatString = DateFormatter()
        formatString.dateFormat = "yyyy-MM-dd"
        if date != nil {
            return formatString.string(from: date!)
        }
        return self
    }
    
    func vnp4TFormatDateInput() -> String? {
        let date = self.vnp4TToDate(format: "yyyy-MM-dd")
        let formatString = DateFormatter()
        formatString.dateFormat = "dd/MM/yyyy"
        if date != nil {
            return formatString.string(from: date!)
        }
        return self
    }
    
    func vnp4TFormatPrice(symbol: String = "VND") -> String? {
        var stringText = self.replacingOccurrences(of: ",", with: "")
        stringText = stringText.replacingOccurrences(of: ".", with: "")
        let number = NSNumber(value: Double(stringText) ?? 0)
        let currencyFormatter = NumberFormatter()
        currencyFormatter.currencySymbol = symbol
        currencyFormatter.currencyGroupingSeparator = ","
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.positiveFormat = "#,##0"
        // localize to your grouping and decimal separator
//        currencyFormatter.locale = Locale.current
        
        return currencyFormatter.string(from: number)
    }
}

extension StringProtocol {
    func index<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> Index? {
        range(of: string, options: options)?.lowerBound
    }
    func endIndex<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> Index? {
        range(of: string, options: options)?.upperBound
    }
    func indices<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> [Index] {
        var indices: [Index] = []
        var startIndex = self.startIndex
        while startIndex < endIndex,
              let range = self[startIndex...]
                .range(of: string, options: options) {
            indices.append(range.lowerBound)
            startIndex = range.lowerBound < range.upperBound ? range.upperBound :
                index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return indices
    }
    func ranges<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> [Range<Index>] {
        var result: [Range<Index>] = []
        var startIndex = self.startIndex
        while startIndex < endIndex,
              let range = self[startIndex...]
                .range(of: string, options: options) {
            result.append(range)
            startIndex = range.lowerBound < range.upperBound ? range.upperBound :
                index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
    
    func substring(_ r: Range<Int>) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
        let toIndex = self.index(self.startIndex, offsetBy: r.upperBound)
        let indexRange = Range<String.Index>(uncheckedBounds: (lower: fromIndex, upper: toIndex))
        return String(self[indexRange])
    }
}

extension String {
    subscript(value: Int) -> Character {
        self[index(at: value)]
    }
}

extension String {
    subscript(value: NSRange) -> Substring {
        self[value.lowerBound..<value.upperBound]
    }
}

extension String {
    subscript(value: CountableClosedRange<Int>) -> Substring {
        self[index(at: value.lowerBound)...index(at: value.upperBound)]
    }
    
    subscript(value: CountableRange<Int>) -> Substring {
        self[index(at: value.lowerBound)..<index(at: value.upperBound)]
    }
    
    subscript(value: PartialRangeUpTo<Int>) -> Substring {
        self[..<index(at: value.upperBound)]
    }
    
    subscript(value: PartialRangeThrough<Int>) -> Substring {
        self[...index(at: value.upperBound)]
    }
    
    subscript(value: PartialRangeFrom<Int>) -> Substring {
        self[index(at: value.lowerBound)...]
    }
}

private extension String {
    func index(at offset: Int) -> String.Index {
        index(startIndex, offsetBy: offset)
    }
}
