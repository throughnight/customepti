//
//  UILabel+Extensions.swift
//  VNPost4T
//
//  Created by m2 on 10/13/20.
//

import Foundation

extension UILabel{
    func renderTitleUpdateLimitInput(data: VNP4TSurveyQuestion, tfValue :UITextField ){
        
        let stringQuestion = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)]
        let attributedString1 = NSMutableAttributedString(string: data.question , attributes:stringQuestion)
        
        if data.required {
            let stringRequire = [NSAttributedString.Key.font : UIFont.italicSystemFont(ofSize: 18), NSAttributedString.Key.foregroundColor : UIColor.red]
            
            let attributedString3 = NSMutableAttributedString(string: " *", attributes:stringRequire)
            attributedString1.append(attributedString3)
        }
        if data.step != 0 && data.length > 0 {
            tfValue.maxLength = data.length
            
            let stringRequire = [NSAttributedString.Key.font : UIFont.italicSystemFont(ofSize: 12), NSAttributedString.Key.foregroundColor : UIColor.lightGray]
            
            let attributedString4 = NSMutableAttributedString(string: " (độ dài tối đa \(data.length) ký tự)", attributes:stringRequire)
            
            attributedString1.append(attributedString4)
        }
        self.attributedText = attributedString1
    }
    
}
