//
//  UIButton+Extensions.swift
//  VNPost4T
//
//  Created by AEGONA on 10/13/20.
//

import Foundation
import UIKit

extension UIButton
{
    func applyGradient(colors: [CGColor])
    {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func vnp4TSetButtonBlueGradient(){
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor(hex: "3692F4"), UIColor(hex: "025BBB")]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func VNP4TsetGradientBlueButtonBackground() {
        let gradientLayer = CAGradientLayer()
        let updatedFrame = self.bounds
        gradientLayer.frame = updatedFrame
        gradientLayer.colors = [UIColor(hex: "3692F4").cgColor, UIColor(hex: "025BBB").cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        self.clipsToBounds = true
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}
