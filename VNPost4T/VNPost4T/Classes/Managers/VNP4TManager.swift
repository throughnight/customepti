//
//  VNP4TManager.swift
//  
//
//  Created by AEGONA on 9/11/20.
//

import Foundation
import RxCocoa
import RxSwift
import UIKit
import IQKeyboardManagerSwift
import FirebaseStorage

public class VNP4TManager {
    
    // MARK: Singleton
    public static let shared = VNP4TManager()
    
    private init() {
        // Keyboard
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
        IQKeyboardManager.shared.toolbarManageBehaviour = .byPosition
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        storage = Storage.storage(url: VNP4TConfiguration.shared.environment.bitbucket)
    }
    
    var accessToken: String?
    
    var userName: String?
    
    var userId : String?
    
    var storage : Storage?
    
    public func setEnviroment(_ enviroment: VNP4TEnvironment) {
        VNP4TConfiguration.shared.environment = enviroment
        storage = Storage.storage(url: VNP4TConfiguration.shared.environment.bitbucket)
    }
    
    public func setAccessToken(_ token: String) {
        self.accessToken = token
    }
    
    public func setUserName(_ userName: String) {
        self.userName = userName
    }
    
    public func setUserId(id: String) {
        self.userId = id
    }
    
    
    public func onTest(_ navigationController: UINavigationController?){
//        navigationController?.pushViewController(VNP4TTestView(nibName: "VNP4TTestView", bundle: Bundle(identifier: VNP4TConstants.bundleIdentifier)), animated: true)
        let bundle4T = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        let physicalVC = VNP4TPhysicalVC(nibName: "VNP4TPhysicalVC", bundle: bundle4T)
        physicalVC.viewModel = VNP4TPhysicalVM(state: BehaviorRelay<Int?>(value:PhysicalOptions.SEARCH.rawValue))
        navigationController?.pushViewController(physicalVC, animated: true)
    }
    
    public func setUserId(_ userId: String) {
        self.userId = userId
        
    }
    
    public func open4T(_ navigationController: UINavigationController?, token: String, userId: String) {
        self.accessToken = token
        self.userId = userId
        self.storage = Storage.storage(url: VNP4TConfiguration.shared.environment.bitbucket)
        let bundle4T = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        let homeVC = VNP4THomeVC(nibName: "VNP4THomeVC", bundle: bundle4T)
        homeVC.viewModel = VNP4THomeVM(input: VNP4THomeVM.Input(
                                        isLoading: BehaviorRelay<Bool>(value: true),
                                        token: BehaviorRelay<String>(value: self.accessToken ?? "")),
                                       dependency: VNP4TAuthService())
        homeVC.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(homeVC, animated: true)
    }
    
    public func openNotificationDetail(_ navigationController: UINavigationController?, token: String, userId: String, titleNotification: String = "", messageNotification: String = ""){
        self.accessToken = token
        self.userId = userId
        
        let bundle4T = Bundle(identifier: VNP4TConstants.bundleIdentifier)
        let detailNotify = VNP4TNotificationDetailVC(nibName: "VNP4TNotificationDetailVC", bundle: bundle4T)
        detailNotify.viewModel = VNP4TNotificationDetailVM(input: VNP4TNotificationDetailVM.Input(isLoading: BehaviorRelay<Bool>(value: false),
                                                                                                  titleNotification: BehaviorRelay<String>(value: titleNotification),
                                                                                                  messageNotification: BehaviorRelay<String>(value: messageNotification),
                                                                                                  codeSurvey: BehaviorRelay<String>(value: ""),
                                                                                                  detailTrigger: BehaviorRelay<Bool>(value: false),
                                                                                                  loadToken: BehaviorRelay<Bool>(value: true)),
                                                           dependency: VNP4TSurveyService())
        detailNotify.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(detailNotify, animated: true)
    }
    
}
