#

# Be sure to run `pod lib lint VNPost4T.podspec' to ensure this is a

# valid spec before submitting.

#

# Any lines starting with a # are optional, but their use is encouraged

# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html

#


 

Pod::Spec.new do |s|

  s.name             = 'VNPost4T'

  s.version          = '0.1.1'

  s.summary          = 'VNPost4T.'


 

# This description is used to generate tags and improve search results.

#   * Think: What does it do? Why did you write it? What is the focus?

#   * Try to keep it short, snappy and to the point.

#   * Write the description between the DESC delimiters below.

#   * Finally, don't worry about the indent, CocoaPods strips it!


 

  s.description      = <<-DESC

TODO: Add long description of the pod here.

                       DESC


 

  s.homepage         = 'https://ptisaigon.beanstalkapp.com/vnp_ios_4t'

  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'

  s.license          = { :type => 'MIT', :file => 'LICENSE' }

  s.author           = { 'lu.ho@aegona.com' => 'lu.ho@aegona.com' }

  s.source           = { :git => 'https://ptisaigon.beanstalkapp.com/vnp_ios_4t.git', :tag => s.version.to_s }

  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'


 

  s.ios.deployment_target = '10.0'

  #s.source       = { :path => "." }

  s.source_files = 'VNPost4T/**/*.{swift}'

  s.resources = "VNPost4T/**/**/*.{png,jpeg,jpg,storyboard,xib,xcassets,lproj}"


 

  s.swift_versions = '5.0'

#  s.resource_bundles = {

#     'VNPost4T' => ['VNPost4T/Assets/*','VNPost4T/Firebase.h']

#  }

  

  # s.public_header_files = 'VNPost4T/**/**/*.h'

  # s.frameworks = 'UIKit', 'MapKit'

  # s.dependency 'AFNetworking', '~> 2.3'

  #!s.vendored_frameworks = 'VNPost4T/Firebase/*.{xcframework}'


 

  # s.vendored_library = './Lib/*.{a}'

  s.static_framework = true

  s.dependency "Alamofire"

  s.dependency "SDWebImage"

  s.dependency "SwiftMessages"

  s.dependency "ObjectMapper"

  s.dependency "IQKeyboardManagerSwift"

  s.dependency "SVProgressHUD"

  s.dependency "RxSwift"

  s.dependency "Moya/RxSwift"

  s.dependency "RxCocoa"

  s.dependency "RxDataSources"

  s.dependency "RxKeyboard"

  s.dependency "StepIndicator"

  s.dependency "Charts"

  s.dependency "Firebase/Storage"


 

end
