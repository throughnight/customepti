//
//  publicMethods.swift
//  VNPostTTBH
//
//  Created by AEGONA on 25/01/2022.
//

import Foundation
import UIKit

public class PublicMethods {
    public static func runModule(navigationController: UINavigationController, token: String, baseUrl: String) {
        CoreDataManager.shared.clearAllData()
        CoreDataManager.shared.saveOutsideDatas(token: token, baseUrl: baseUrl) // for calling api
        
        navigateToFirstScreen(navigationController: navigationController)
    }
    
    //===================================================
    
    private static func navigateToFirstScreen(navigationController: UINavigationController) {
        let bundle = Bundle(identifier: Configs.BundleIdentifier)
        
        // change first view here
        let firstViewControler = ElectronicRenewableViewController.init(nibName: ElectronicRenewableViewController.nibName, bundle: bundle)
        
        navigationController.pushViewController(firstViewControler, animated: true)
    }
}
