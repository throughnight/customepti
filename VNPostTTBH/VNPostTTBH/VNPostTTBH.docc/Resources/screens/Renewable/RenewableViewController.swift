//
//  RenewableViewController.swift
//  VNPostTTBH
//
//  Created by AEGONA on 25/01/2022.
//

import UIKit

class RenewableViewController: UIViewController {
    public static let nibName = "RenewableViewController"

    @IBOutlet weak var uivHeader: UIView!
    @IBOutlet weak var uivSearch: UIView!
    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnInProcessing: UIButton!
    @IBOutlet weak var btnDidProcessing: UIButton!
    @IBOutlet weak var uivAll: UIView!
    @IBOutlet weak var uivInProcessing: UIView!
    @IBOutlet weak var uivDidProcessing: UIView!
    @IBOutlet weak var uivContentAll: UIView!
    @IBOutlet weak var uivContentInprocessing: UIView!
    @IBOutlet weak var uivContentDidProcessing: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        customizeLayout()
        navigateTab(index: 0)
        addSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigation()
    }
    
    private func setupNavigation() {
        navigationController?.navigationBar.isHidden = true
    }

    private func customizeLayout() {
        uivHeader.layer.cornerRadius = 10
        uivSearch.layer.cornerRadius = 6
        uivSearch.layer.borderColor = UIColor.gray.cgColor
        uivSearch.layer.borderWidth = 1
        
        btnAll.tintColor = .gray
        btnInProcessing.tintColor = .gray
        btnDidProcessing.tintColor = .gray
        
        uivAll.backgroundColor = .clear
        uivInProcessing.backgroundColor = .clear
        uivDidProcessing.backgroundColor = .clear
    }
    
    private func addSubView() {
        let bundle = Bundle(identifier: Configs.BundleIdentifier)
        
        let subViewAll = ContractsViewController.init(nibName: ContractsViewController.nibName, bundle: bundle)
        subViewAll.view.frame = uivContentAll.bounds
        uivContentAll.addSubview(subViewAll.view)
        addChild(subViewAll)
        subViewAll.didMove(toParent: self)
    }
    
    private func navigateTab(index: Int) {
        changeStylesTab(index: index)
        
        switch index {
            case 0:
                self.view.bringSubviewToFront(uivContentAll)
            case 1:
                self.view.bringSubviewToFront(uivContentInprocessing)
            case 2:
                self.view.bringSubviewToFront(uivContentDidProcessing)
            default:
                self.view.bringSubviewToFront(uivContentAll)
        }
    }
    
    private func changeStylesTab(index: Int) {
        btnAll.tintColor = index == 0 ? .black : .gray
        btnInProcessing.tintColor = index == 1 ? .black : .gray
        btnDidProcessing.tintColor = index == 2 ? .black : .gray
        
        uivAll.backgroundColor = index == 0 ? .systemBlue : .clear
        uivInProcessing.backgroundColor = index == 1 ? .systemBlue : .clear
        uivDidProcessing.backgroundColor = index == 2 ? .systemBlue : .clear
    }
    
    @IBAction func onPressTab(_ sender: UIButton) {
        navigateTab(index: sender.tag)
    }
}
