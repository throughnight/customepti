//
//  DetailRenewableViewController.swift
//  VNPostTTBH
//
//  Created by AEGONA on 26/01/2022.
//

import UIKit

class DetailRenewableViewController: UIViewController {
    public static let nibName = "DetailRenewableViewController"

    @IBOutlet weak var uivOwnerContainer: UIStackView!
    @IBOutlet weak var btnContractInfo: UIButton!
    @IBOutlet weak var btnContractStatus: UIButton!
    @IBOutlet weak var btnContractNote: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("token: \(CoreDataManager.shared.getToken())")
        print("baseUrl: \(CoreDataManager.shared.getBaseUrl())")

        customizeLayout()
        navigateTab(index: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigation()
    }
    
    private func setupNavigation() {
        navigationController?.navigationBar.isHidden = true
    }
    
    private func customizeLayout() {
        uivOwnerContainer.layer.cornerRadius = 15
        
        btnContractInfo.backgroundColor = .clear
        btnContractStatus.backgroundColor = .clear
        btnContractNote.backgroundColor = .clear
    }
    
    private func navigateTab(index: Int) {
        changeStylesTab(index: index)
    }
    
    private func changeStylesTab(index: Int) {
        btnContractInfo.backgroundColor = index == 0 ? .systemGreen : .clear
        btnContractStatus.backgroundColor = index == 1 ? .systemGreen : .clear
        btnContractNote.backgroundColor = index == 2 ? .systemGreen : .clear
    }
    
    @IBAction func onPressTab(_ sender: UIButton) {
        navigateTab(index: sender.tag)
    }
}
