//
//  ElectronicRenewableViewController.swift
//  VNPostTTBH
//
//  Created by Dam Huy on 27/01/2022.
//

import UIKit

class ElectronicRenewableViewController: UIViewController {
    public static let nibName = "ElectronicRenewableViewController"

    @IBOutlet weak var uiv01: UIView!
    @IBOutlet weak var uiv02: UIView!
    @IBOutlet weak var uiv03: UIView!
    @IBOutlet weak var uivInner01: UIView!
    @IBOutlet weak var uivInner02: UIView!
    @IBOutlet weak var uivInner03: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        customizeLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigation()
    }
    
    private func setupNavigation() {
        navigationController?.navigationBar.isHidden = true
    }

    private func customizeLayout() {
        uiv01.layer.cornerRadius = uiv01.frame.size.width / 2
        uivInner01.layer.cornerRadius = uivInner01.frame.size.width / 2
        uiv02.layer.cornerRadius = uiv02.frame.size.width / 2
        uivInner02.layer.cornerRadius = uivInner02.frame.size.width / 2
        uiv03.layer.cornerRadius = uiv03.frame.size.width / 2
        uivInner03.layer.cornerRadius = uivInner03.frame.size.width / 2
    }
}
