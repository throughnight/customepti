//
//  ContractTableViewCell.swift
//  VNPostTTBH
//
//  Created by AEGONA on 26/01/2022.
//

import UIKit

class ContractTableViewCell: UITableViewCell {
    static let identifier = "ContractTableViewCell"
    static func nib() -> UINib {
        let bundle = Bundle(identifier: Configs.BundleIdentifier)
        return UINib(nibName: "ContractTableViewCell", bundle: bundle)
    }
    public var onPressLeft: ((String) -> Void)?
    public var onPressRight: ((String) -> Void)?

    //========================================================
    
    @IBOutlet weak var uivMainContainer: UIView!
    @IBOutlet weak var uivInnerContainer: UIStackView!
    @IBOutlet weak var uivStatusContainer: UIStackView!
    
    @IBOutlet weak var labCode: UILabel!
    @IBOutlet weak var labStatus: UILabel!
    @IBOutlet weak var labName: UILabel!
    @IBOutlet weak var labPhoneNumber: UILabel!
    @IBOutlet weak var labIdentifierCode: UILabel!
    @IBOutlet weak var labTypeContract: UILabel!
    @IBOutlet weak var labStartDate: UILabel!
    @IBOutlet weak var labEndDate: UILabel!
    @IBOutlet weak var labCounterDay: UILabel!
    @IBOutlet weak var labStatusContract: UILabel!
    
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        customizeLayout()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func customizeLayout() {
        uivMainContainer.layer.cornerRadius = 10
        uivInnerContainer.layer.cornerRadius = 10
        let rectShape = CAShapeLayer()
        rectShape.bounds = uivStatusContainer.frame
        rectShape.position = uivStatusContainer.center
        rectShape.path = UIBezierPath(roundedRect: uivStatusContainer.bounds, byRoundingCorners: [.bottomLeft], cornerRadii: CGSize(width: 10, height: 10)).cgPath
        uivStatusContainer.layer.mask = rectShape
        
        uivMainContainer.clipsToBounds = true
        uivInnerContainer.clipsToBounds = true
        
        uivMainContainer.layer.masksToBounds = false
        uivMainContainer.layer.shadowColor = UIColor.gray.cgColor
        uivMainContainer.layer.shadowOpacity = 0.3
        uivMainContainer.layer.shadowOffset = CGSize(width: 1, height: 1)
        uivMainContainer.layer.shadowRadius = 2
    }
    @IBAction func onPressLeft(_ sender: Any) {
        onPressLeft?(labCode.text!)
    }
    @IBAction func onPressRight(_ sender: Any) {
        onPressRight?(labCode.text!)
    }
}
