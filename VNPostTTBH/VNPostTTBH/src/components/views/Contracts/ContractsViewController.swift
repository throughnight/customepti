//
//  ContractsViewController.swift
//  VNPostTTBH
//
//  Created by AEGONA on 26/01/2022.
//

import UIKit

class ContractsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    static let nibName = "ContractsViewController"

    @IBOutlet weak var uivContentContainer: UIView!
    
    let tableView: UITableView = {
        let table = UITableView()
        table.register(ContractTableViewCell.nib(), forCellReuseIdentifier: ContractTableViewCell.identifier)

        return table
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        addSubView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        tableView.frame = uivContentContainer.bounds
    }

    private func addSubView() {
        uivContentContainer.addSubview(tableView)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ContractTableViewCell.identifier, for: indexPath) as! ContractTableViewCell
        
        cell.onPressLeft = { [weak self] id in
            guard let self = self else { return }
            print("bên trái nè \(id) \(self.nibName!)")
        }
        
        cell.onPressRight = { [weak self] id in
            guard let self = self else { return }
            print("bên phải nè \(id) \(self.nibName!)")
        }

        return cell
    }
}
