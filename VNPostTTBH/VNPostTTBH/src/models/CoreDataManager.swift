//
//  CoreDataManager.swift
//  VNPostTTBH
//
//  Created by AEGONA on 27/01/2022.
//

import Foundation
import CoreData

public class CoreDataManager {
    public static let shared = CoreDataManager()
    
    //=================================================
    
    let identifier: String = Configs.BundleIdentifier
    let model: String = "TTBHModel"
    
    lazy var persistentContainer: NSPersistentContainer = {
        let messageKitBundle = Bundle(identifier: self.identifier)
        let modelURL = messageKitBundle!.url(forResource: self.model, withExtension: "momd")!
        let managedObjectModel =  NSManagedObjectModel(contentsOf: modelURL)
        
        let container = NSPersistentContainer(name: self.model, managedObjectModel: managedObjectModel!)
        container.loadPersistentStores { (storeDescription, error) in
            if let err = error {
                fatalError("❌ Loading of store failed: \(err)")
            }
        }
        
        return container
    }()
    
    //=================================================
    
    public func clearAllData() {
        clearOutsideDatas()
    }
    
    public func saveOutsideDatas(token: String, baseUrl: String) {
        let context = persistentContainer.viewContext
        let outsideData = NSEntityDescription.insertNewObject(forEntityName: "OutsideDatas", into: context) as! OutsideDatas

        outsideData.baseUrl = baseUrl
        outsideData.token = token

        do {
            try context.save()
            print("✅ token and baseUrl were saved succesfuly")
        } catch let error {
            print("❌ Failed to save: \(error.localizedDescription)")
        }
    }
    
    public func clearOutsideDatas() {
        let context = persistentContainer.viewContext
        
        let fetchOutsideDatas = NSFetchRequest<OutsideDatas>(entityName: "OutsideDatas")
        do{
            let outsideDatas = try context.fetch(fetchOutsideDatas)

            for (_, outsideData) in outsideDatas.enumerated() {
                context.delete(outsideData)
            }

        } catch let err {
            print("❌ Failed to clear outsideDatas: \(err)")
        }
    }
    
    public func getToken() -> String{
        let context = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<OutsideDatas>(entityName: "OutsideDatas")

        do{
            let outsideDatas = try context.fetch(fetchRequest)

//            for (index, outsideData) in outsideDatas.enumerated() {
//                print("Token \(index): \(outsideData.token ?? "N/A")")
//            }

            return outsideDatas[0].token!
        } catch let err {
            print("❌ Failed to fetch token: \(err)")
            
            return ""
        }
    }
    
    public func getBaseUrl() -> String{
        let context = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<OutsideDatas>(entityName: "OutsideDatas")

        do{
            let outsideDatas = try context.fetch(fetchRequest)

            return outsideDatas[0].baseUrl!
        } catch let err {
            print("❌ Failed to fetch baseUrl: \(err)")
            
            return ""
        }
    }
}
