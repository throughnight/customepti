//
//  ViewController.swift
//  ExampleVNPost4T
//
//  Created by AEGONA on 9/11/20.
//  Copyright © 2020 AEGONA. All rights reserved.
//

import UIKit
import VNPostXLTT
import VNPostTTBH

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        XLTTManager.shared.setEnviroment(.staging)
//
//        XLTTManager.shared.openHome(self.navigationController, token: "74799c473fba00f9fe6019b77ffc1b2be6c775e3a65f78420af98cc892b641fb")
        
//           XLTTManager.shared.openCustomerDetail(self.navigationController)
    }
    
    @IBAction func onTest(_ sender: Any) {
        //        VNP4TManager.shared.onTest(self.navigationController)
    }
    
    //account 21
    @IBAction func go4T(_ sender: Any) {
        //        VNP4TManager.shared.setEnviroment(.staging)
        //        VNP4TManager.shared.open4T(self.navigationController, token: "8552a4145bad58ba003ac809ab3be56f2f36a26a273f94dcdecefe02ac0b9da5", userId: "7a2f6c0d-2f8f-4faf-b072-b36da8578669")
        
        XLTTManager.shared.setEnviroment(.staging)
        XLTTManager.shared.openIPA(self.navigationController, token: "96a1f2e4d5a0feac2ffabb6ed9ea5ecc7b6d191ce7d13e2fe453251c870913eb")
    }
    
    @IBAction func go16(_ sender: Any) {
        XLTTManager.shared.setEnviroment(.staging)
        XLTTManager.shared.openIPA(self.navigationController, token: "9ad5ee23fbf946686508c43298ce1110e5c670214ca9833737b5616e5c4b4b83")
        
        //        VNP4TManager.shared.setEnviroment(.staging)
        //        VNP4TManager.shared.open4T(self.navigationController, token: "6697418da2f951f7a560ae181f95e77bfe973d40a08ff9221e87b2fa1b184cfb", userId: "c26f46e1-8bcd-4fb9-80b4-397bbb750f2c")
    }
    
    @IBAction func go07(_ sender: Any) {
        XLTTManager.shared.setEnviroment(.staging)
        XLTTManager.shared.openIPA(self.navigationController, token: "d64e8a528189d3a12379dc21d946ab2bc839459620bf2e0f5e429b7f43bdee17")
        
        //        VNP4TManager.shared.setEnviroment(.staging)
        //        VNP4TManager.shared.open4T(self.navigationController, token: "f510863e64030d77130f7e74b6194a636969199f7c60231d9dfc790fed3df31d", userId: "6be5307a-a090-4df7-bbaf-1a5d26a49300")
    }
    
    @IBAction func goTTBH(_ sender: Any) {
        
        // hardcode
        let urlDevelop = "https://inp02.vsit.com.vn/"
        let urlStaging1 = "https://pti-ipa-api-test.aegona.work/"
        let urlStaging2 = "https://inp01.vsit.com.vn/"
        let urlProduction = "https://inp.vsit.com.vn/"
        let version = "v1.0"
        
        
        let baseUrl = urlDevelop + version
        let token = "token ở đây"
        
        
        VNPostTTBH.PublicMethods.runModule(navigationController: self.navigationController!, token: token, baseUrl: baseUrl)
    }
    
    @IBAction func onNotificationDetail(_ sender: Any) {
//                VNP4TManager.shared.setEnviroment(.staging)
//                VNP4TManager.shared.openNotificationDetail(self.navigationController, token: "26276464769961304f317e268d97f1c640e080f68e6db3ef633d73caf50a6aaf", userId: "7a2f6c0d-2f8f-4faf-b072-b36da8578669", titleNotification: "Phiếu đăng ký PDKTT0275 đã chuyển trạng thái từ Chờ phê duyệt sang Đã duyệt", messageNotification: "Cập nhật bởi nhân viên nhập liệu: 4t0020 vào lúc 13:25 05-10-2020")
    }
}
