
** Hướng dẫn config Firebase vào VNPost4T

1. Vào Framework(nằm trong folder VNPost4T) chọn từng framework rồi chọn target cho framework là VNPost4T
2. Vào Build Phases -> kéo Firebase.h vào public Headers ( Firebase.h nằm ở folder Resource)
3. Qua tab Build Settings -> search "Other Linker Flags" -> click the '+' button, and add "-ObjC"
4. Re-build

** Cách sử dụng Library:
- Đầu tiên cần config môi trường đang dùng cho Library VNPost4T : 
    VNP4TManager.shared.setEnviroment(.staging)
    Các môi trường hiện có: .develop, .staging, .production

- Library có 2 function chính:
    + open4T() : dùng để vào module 4T, cần truyền các params sau:
        navigationController (dùng để chuyển màn hinh)
        token (token của main app dùng để lấy token cho 4T)
        userId (userId của tài khoản đang login, dùng để upload image lên cloud)

    + openNotificationDetail() : dùng để xem chi tiết 1 thông báo, và khi click vào mã phiếu thì chuyển qua xem chi tiết của 1 phiếu trong Library VNPost4T, cần truyền các params sau:
        navigationController (dùng để chuyển màn hinh)
        token (token của main app dùng để lấy token cho 4T)
        userId (userId của tài khoản đang login, dùng để upload image lên cloud)
        titleNotification (title của notification)
        messageNotification (message notification có chứa mã phiếu)


** Cách sử dụng XLTT:
- Đầu tiên cần config môi trường đang dùng cho Library VNPostXLTT : 
    XLTTManager.shared.setEnviroment(.staging)
    Các môi trường hiện có: .develop, .staging, .production

- Library có 2 function chính:
    + openIPA() : dùng để vào module XLTT, cần truyền các params sau:
        navigationController (dùng để chuyển màn hinh)
        token (token của main app dùng để lấy token cho XLTT)
        userId (userId của tài khoản đang login)

    