//
//  XLTTPotentialService.swift
//  VNPostXLTT
//
//  Created by m2 on 10/27/20.
//

import Foundation
import Moya
import Alamofire
import RxSwift

class XLTTPotentialService: XLTTBaseService<XLTTPotentialCus> {
    func getMobileGetDataHome(type: String) -> Observable<XLTTResponseModel<XLTTChartHome>> {
        let request = TypeRequest(type: type)
        return requestApi(request: .getMobileGetDataHomeAsync(request)).asObservable().flatMap { (response) -> Observable<XLTTResponseModel<XLTTChartHome>> in
            self.request(response)
        }
    }
}
