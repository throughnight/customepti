//
//  XLTTCommonService.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/28/20.
//

import Foundation
import Moya
import Alamofire
import RxSwift

class XLTTCommonService: XLTTBaseService<XLTTAPI> {
    func getListTeamSale() -> Observable<XLTTResponseModel<[XLTTOptionsFilter]>> {
        return requestApi(request: .getListTeamSale).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<[XLTTOptionsFilter]>> in
                self.requestArrayBase(response)
        }
    }
    
    func getListProduct(search: String, index: Int, size: Int) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> {
        let request = PagingRequest(search: search, pageIndex: index, pageSize: size)
        return requestApi(request: .getListProduct(request)).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> in
                self.requestArray(response)
        }
    }
    
    func getProvinces() -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> {
        return requestApi(request: .getProvinces).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> in
                self.requestArray(response)
        }
    }
    
    func getDistricts(id: String) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> {
        return requestApi(request: .getDistrict(id: id)).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> in
                self.requestArray(response)
        }
    }
    
    func getWards(id: String) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> {
        return requestApi(request: .getWards(id: id)).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> in
                self.requestArray(response)
        }
    }
    
    func getListEmployees(teamId: String, index: Int, size: Int, search: String?) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> {
        let request = EmployeeRequest(saleTeamId: teamId, pageIndex: index, pageSize: size, search: search)
        return requestApi(request: .getEmployees(request)).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> in
                self.requestArray(response)
        }
    }
    
    func getListReason(search: String,index: Int, pageSize: Int) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> {
        let request = PagingRequest(search: search, pageIndex: index, pageSize: pageSize)
        return requestApi(request: .getListReason(request)).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> in
                self.requestArray(response)
        }
    }
}
