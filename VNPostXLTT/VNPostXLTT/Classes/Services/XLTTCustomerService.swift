//
//  XLTTCustomerService.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/26/20.
//

import Foundation
import Moya
import Alamofire
import RxSwift

class XLTTCustomerService: XLTTBaseService<XLTTAPI> {
    func getLeadCustomer(search: String, type: Int, index: Int, size: Int) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTCustomer>>> {
        let request = LeadCustomerRequest(search: search, type: type, pageIndex: index, pageSize: size)
        return requestApi(request: .getCustomer(request)).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTCustomer>>> in
                self.requestArray(response)
        }
    }
    
    func customerAction(idCustomer: String, type: Int, note: String?) -> Observable<XLTTResponseModel<Any>> {
        let request = CustomerActionRequest(id: idCustomer, type: type, note: note)
        return requestApi(request: .customerOfMeAction(request)).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<Any>> in
                self.requestAny(response)
        }
    }
    
    func customerDeny(id: String, type: Int) -> Observable<XLTTResponseModel<Any>> {
        let request = CustomerActionRequest(id: id, type: type)
        return requestApi(request: .customerOfMeDeny(request)).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<Any>> in
                self.requestAny(response)
        }
    }
    
    func getListUserAssignLead(id: String, search: String, index: Int, size: Int) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTSaleInfo>>> {
        let request = AssignLeadRequest(leadAssignId: id, search: search, pageIndex: index, pageSize: size)
        return requestApi(request: .getListUserAssign(request)).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTSaleInfo>>> in
                self.requestArray(response)
        }
    }
    
    func assignLeadUser(data: XLTTAssignModel) -> Observable<XLTTResponseModel<Any>> {
        return requestApi(request: .assignLead(data: data)).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<Any>> in
                self.requestAny(response)
        }
    }
    
    func filterCustomer(request: FilterRequest) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTCustomer>>> {
        return requestApi(request: .filterCustomer(request)).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTCustomer>>> in
                self.requestArray(response)
        }
    }
    
    func getHistory(search: String, type: Int, index: Int, size: Int) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTCustomer>>> {
        let request = LeadCustomerRequest(search: search, type: type, pageIndex: index, pageSize: size)
        return requestApi(request: .getHistory(request)).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTCustomer>>> in
                self.requestArray(response)
        }
    }
    
    func filterHistory(request: FilterRequest) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTCustomer>>> {
        return requestApi(request: .filterHistory(request)).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTCustomer>>> in
                self.requestArray(response)
        }
    }
    
    func getCustomerDetail(leadId: String, leadAssignId: String) -> Observable<XLTTResponseModel<XLTTLeadDetailModel>> {
        let request = CustomerDetailRequest(leadId: leadId, leadAssignId: leadAssignId)
        return requestApi(request: .getCustomerDetail(request)).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<XLTTLeadDetailModel>> in
                self.request(response)
        }
    }
    
    func pinCustomer(id: String, status: Bool) -> Observable<XLTTResponseModel<Any>> {
        let request = PinCustomerRequest(id: id, flag: status ? "true" : "false")
        return requestApi(request: .pinLead(request)).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<Any>> in
                self.requestAny(response)
        }
    }
    
    func addNote(data: XLTTAssignModel) -> Observable<XLTTResponseModel<Any>> {
        return requestApi(request: .addNote(data: data)).asObservable()
            .flatMap { (response) -> Observable<XLTTResponseModel<Any>> in
                self.requestAny(response)
        }
    }
}
