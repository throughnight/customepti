//
//  XLTTAuthService.swift
//  VNPostXLTT
//
//  Created by m2 on 10/27/20.
//

import Foundation
import Moya
import Alamofire
import RxSwift

class XLTTAuthService: XLTTBaseService<XLTTAuth> {
    func getToken(token: String) -> Observable<XLTTResponseModel<XLTTGetTokenMobileModel>> {
        let request = TokenRequest(token: token)
        return requestApi(request: .getToken(request)).asObservable().flatMap { (response) -> Observable<XLTTResponseModel<XLTTGetTokenMobileModel>> in
            self.request(response)
        }
    }
}
