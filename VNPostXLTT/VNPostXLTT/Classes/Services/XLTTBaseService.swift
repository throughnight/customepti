//
//  XLTTBaseService.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/26/20.
//

import Foundation
import ObjectMapper
import Moya
import Alamofire
import RxSwift
import Network

class DefaultAlamofireSession: Alamofire.Session {
    static let shared: DefaultAlamofireSession = {
        let configuration = URLSessionConfiguration.default
        configuration.headers = .default
        configuration.timeoutIntervalForRequest = 30 // as seconds, you can set your request timeout
        configuration.timeoutIntervalForResource = 30 // as seconds, you can set your resource timeout
        configuration.requestCachePolicy = .useProtocolCachePolicy
        return DefaultAlamofireSession(configuration: configuration)
    }()
}

struct XLTTVerbosePlugin: PluginType {
    
    let verbose: Bool
    
    func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        
        #if DEBUG
        print("--> \(String(describing: request.method!.rawValue.description))")
        print("\(String(describing: request.url!.description))")
        print("\(request.headers.description)")
        print("\(String(describing: request.httpBody))")
        print(" --> END \(String(describing: request.method!.rawValue.description))")
        #endif
        
        return request
        
    }
    
}

enum APIError: Error {
    case invalidURL(url: String)
    case invalidResponseData(data: Any)
    case error(responseCode: Int, data: Any)
    case MissingKeyOrType(key: String)
}

class XLTTBaseService<T: TargetType> {
    
    public let provider = MoyaProvider<T>(session: DefaultAlamofireSession.shared, plugins: [XLTTVerbosePlugin(verbose: true)])
    
    func requestApi(request: T) -> Observable<Response> {
        provider.rx.request(request).asObservable().flatMap { (response) -> Observable<Response> in
            return Observable.just(response)
        }.catchErrorJustReturn(Response.init(statusCode: 1001, data: Data()))
    }
    
    private func process(response: Response) -> Observable<Any> {
        
        #if DEBUG
        print("--> \(response.statusCode) \(String(describing: response.request?.url?.description))")
        print("\(response.request?.headers.description)")
        print("\(String(describing: (try? response.mapJSON(failsOnEmptyData: true)) as? [String:Any]))")
        print(" --> END HTTP")
        #endif
        
      if let statusCode = response.response?.statusCode {
            switch statusCode{
            case 200:
                return Observable.just((try? response.mapJSON(failsOnEmptyData: true)) as? [String:Any] as Any)
            case 401:                 //unauthorized
                return Observable.just(XLTTResponseModel<Any>(statusCode: response.statusCode,
                                                               success: false,
                                                               message: "xltt_base_service_error_occur".localized).toJSON() as [String:Any])
            case 403:                  //accessDenied
                return Observable.just(XLTTResponseModel<Any>(statusCode: response.statusCode,
                                                               success: false,
                                                               message: "xltt_base_service_no_permission_error".localized).toJSON() as [String:Any])
            case 500:                  //serverError
                return Observable.just(XLTTResponseModel<Any>(statusCode: response.statusCode,
                                                               success: false,
                                                               message: "xltt_base_service_error_server_msg".localized).toJSON() as [String:Any])
            case 502:                   //badGateway
                return Observable.just(XLTTResponseModel<Any>(statusCode: response.statusCode,
                                                               success: false,
                                                               message: "xltt_base_service_error_server_msg".localized).toJSON() as [String:Any])
            case 504:                //gatewayTimeout
                return Observable.just(XLTTResponseModel<Any>(statusCode: response.statusCode,
                                                               success: false,
                                                               message: "xltt_base_service_error_client_timeout_msg".localized).toJSON() as [String:Any])
            default:
                return Observable.just(XLTTResponseModel<Any>(statusCode: response.statusCode,
                                                               success: false,
                                                               message: "xltt_base_service_error_occur".localized).toJSON() as [String:Any])
            }
        }
        else{
            return Observable.just(XLTTResponseModel<Any>(statusCode: response.statusCode,
                                                           success: false,
                                                           message: "xltt_base_service_error_occur".localized).toJSON() as [String:Any])
        }
        
    }
    
    func request<T: Mappable>(_ input: Response) -> Observable<XLTTResponseModel<T>> {
        return process(response: input)
            .map { data -> XLTTResponseModel<T> in
                if let json = data as? [String:Any],
                   let item = XLTTResponseModel<T>(JSON: json) {
                    if(json["data"] as? [String : Any] != nil){
                        item.data = Mapper<T>().map(JSON: json["data"] as! [String : Any])
                    }
                    return item
                } else {
                    throw APIError.invalidResponseData(data: data)
                }
            }
        
    }
    
    func requestArray<T: Mappable>(_ input: Response) -> Observable<XLTTResponseModel<XLTTResponseList<T>>> {
        return process(response: input)
            .map { data -> XLTTResponseModel<XLTTResponseList<T>> in
                if let json = data as? [String:Any],
                   let item = XLTTResponseModel<XLTTResponseList<T>>(JSON: json) {
                    if let rawData = json["data"] as? [String:Any] {
                        let itemData = XLTTResponseList<T>(JSON: rawData)
                        if let array = Mapper<T>().mapArray(JSONObject: rawData["data"]) {
                            itemData?.data = array
                        }
                        item.data = itemData
                    }
                    return item
                } else {
                    throw APIError.invalidResponseData(data: data)
                }
            }
    }
    
    func requestAny(_ input: Response) -> Observable<XLTTResponseModel<Any>> {
        return process(response: input)
            .map { data -> XLTTResponseModel<Any> in
                if let json = data as? [String:Any],
                   let item = XLTTResponseModel<Any>(JSON: json) {
                    return item
                } else {
                    throw APIError.invalidResponseData(data: data)
                }
            }
    }
    
    func requestArrayBase<T: Mappable>(_ input: Response) -> Observable<XLTTResponseModel<[T]>> {
        return process(response: input)
            .map { data -> XLTTResponseModel<[T]> in
                if let json = data as? [String:Any],
                   let item = XLTTResponseModel<[T]>(JSON: json) {
                    if let rawData = json["data"] as? [[String:Any]] {
                        if let array = Mapper<T>().mapArray(JSONObject: rawData) {
                            item.data = array
                        }
                    }
                    return item
                } else {
                    throw APIError.invalidResponseData(data: data)
                }
            }
    }
}
