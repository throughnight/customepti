//
//  DialogCommentVC.swift
//  VNPostXLTT
//
//  Created by AEGONA on 11/16/20.
//

import Foundation
import UIKit
import RxCocoa

class XLTTDialogCommentVC: XLTTBaseViewController {
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var stackNew: UIStackView!
    @IBOutlet weak var stackSuccess: UIStackView!
    @IBOutlet weak var stackFailure: UIStackView!
    @IBOutlet weak var stackProcess: UIStackView!
    @IBOutlet weak var stackExpired: UIStackView!
    @IBOutlet weak var stackDeny: UIStackView!
    
    @IBOutlet weak var lbTitleNew: UILabel!
    @IBOutlet weak var lbTitleSuccess: UILabel!
    @IBOutlet weak var lbTitleFailure: UILabel!
    @IBOutlet weak var lbTitleProcess: UILabel!
    @IBOutlet weak var lbTitleExpired: UILabel!
    @IBOutlet weak var lbTitleDeny: UILabel!
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet var viewParent: UIView!
    @IBOutlet weak var viewContent: UIView!
    
    var isCustomer: BehaviorRelay<Bool>!
    
    var onDoneActions : (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnClose.setTitle("xllt_close_title".localized, for: .normal)
        lbTitle.text = "xltt_title_comment".localized
        lbTitleNew.text = "xltt_title_new".localized
        lbTitleSuccess.text = "xltt_home_chart_success".localized
        lbTitleFailure.text = "xltt_home_chart_failure".localized
        lbTitleProcess.text = "xltt_home_chart_progressing".localized
        lbTitleExpired.text = "xltt_title_status_expired".localized
        lbTitleDeny.text = "xltt_home_chart_failure_group".localized
        
        if isCustomer.value {
            stackNew.isHidden = false
            stackProcess.isHidden = false
            stackSuccess.isHidden = true
            stackFailure.isHidden = true
            stackExpired.isHidden = true
            stackDeny.isHidden = true
        } else {
            stackNew.isHidden = true
            stackProcess.isHidden = false
            stackSuccess.isHidden = false
            stackFailure.isHidden = false
            stackExpired.isHidden = false
            stackDeny.isHidden = false
        }
        
        btnClose.xlttSetButtonBlueGradient()
        
        let tapParent = UITapGestureRecognizer(target: self, action: #selector(onTapClose))
        viewParent.addGestureRecognizer(tapParent)
        let tapContent = UITapGestureRecognizer(target: self, action: #selector(onContent))
        viewContent.addGestureRecognizer(tapContent)
    }
    
    @objc func onTapClose(_ sender: Any){
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func onContent(_ sender: Any){
        
    }
    
    @IBAction func onClose(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
