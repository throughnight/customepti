//
//  XLTTAssignLeadCell.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/27/20.
//

import Foundation
import UIKit
import RxCocoa

class XLTTAssignLeadCell: UITableViewCell{
    static let cellIdentifier = "XLTTAssignLeadCell"

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbPhone: UILabel!
    @IBOutlet weak var imgTick: UIImageView!
    
    var index : Int = 0 {
        didSet {
            if index % 2 == 0 {
                bgView.backgroundColor = .white
            } else {
                bgView.backgroundColor = UIColor(hex: "EEF3FF")
            }
        }
    }
    
    var data : XLTTSaleInfo? {
        didSet {
            lbTitle.text = "\(data?.employeeCode ?? "") - \(data?.fullName ?? "")"
            lbName.text = data?.email
            lbPhone.text = data?.phoneNumber
        }
    }
    
    var itemSelect : XLTTSaleInfo? {
        didSet {
            if itemSelect?.id == data?.id {
                imgTick.isHidden = false
            } else {
                imgTick.isHidden = true
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
