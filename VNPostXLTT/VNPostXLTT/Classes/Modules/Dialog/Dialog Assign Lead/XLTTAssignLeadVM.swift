//
//  XLTTAssignLeadVM.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/27/20.
//

import Foundation
import RxCocoa
import RxSwift
import RxDataSources

typealias AssignLeadSection = SectionModel<String,XLTTSaleInfo>

class XLTTAssignLeadVM: XLTTViewModelProtocal {
    
    typealias Dependency = XLTTCustomerService
    
    struct Input {
        let search                          :BehaviorRelay<String>
        let triggerLoad                     :BehaviorRelay<Bool>
        let triggerLoadMore                 :BehaviorRelay<Bool>
        let data                            :BehaviorRelay<[XLTTSaleInfo]>
        let index                           :BehaviorRelay<Int>
        let size                            :BehaviorRelay<Int>
        let leadAssign                      :BehaviorRelay<XLTTCustomer>
        let isLoading                       :BehaviorRelay<Bool>
        let userSelect                      :BehaviorRelay<XLTTSaleInfo>
        let triggerAssign                   :BehaviorRelay<Bool>
    }
    
    struct Output {
        let dataResult                  :Observable<XLTTResponseModel<XLTTResponseList<XLTTSaleInfo>>>
        let dataMoreResult              :Observable<XLTTResponseModel<XLTTResponseList<XLTTSaleInfo>>>
        let assignResult                :Observable<XLTTResponseModel<Any>>
        let dataSection                 :Driver<[AssignLeadSection]>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input : Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: XLTTCustomerService?) -> Output? {
        self.input = input
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        
        let loadTrigger = Observable.merge(ip.triggerLoad.asObservable().filter {$0}.withLatestFrom(ip.search),ip.search.asObservable())
        
        let dataResult = loadTrigger.asObservable()
            .do { () in
                ip.isLoading.accept(true)
        }.flatMapLatest { (search) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTSaleInfo>>> in
            return dp.getListUserAssignLead(id: ip.leadAssign.value.id, search: search, index: ip.index.value, size: ip.size.value)
        }.do {() in
            ip.isLoading.accept(false)
        }
        
        let moreResult = ip.triggerLoadMore.asObservable()
            .filter {$0}
            .withLatestFrom(ip.search)
            .do { () in
                ip.isLoading.accept(true)
        }.flatMapFirst { (search) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTSaleInfo>>> in
            return dp.getListUserAssignLead(id: ip.leadAssign.value.id, search: search, index: ip.index.value, size: ip.size.value)
        }.do {() in
            ip.isLoading.accept(false)
        }
        
        let assignResult = ip.triggerAssign.asObservable()
            .filter {$0}
            .withLatestFrom(ip.userSelect)
            .do { () in
                ip.isLoading.accept(true)
        }.flatMapLatest { (data) -> Observable<XLTTResponseModel<Any>> in
            let dataAssign = XLTTAssignModel(leadId: ip.leadAssign.value.id, userId: data.id)
            return dp.assignLeadUser(data: dataAssign)
        }.do { () in
            ip.isLoading.accept(false)
        }
        
        let dialogSections = ip.data.asDriver().map { (data) -> [AssignLeadSection] in
            return [AssignLeadSection(model: "AssignLead", items: data)]
        }
        
        return XLTTAssignLeadVM.Output(dataResult: dataResult,
                                       dataMoreResult: moreResult,
                                       assignResult: assignResult,
                                       dataSection: dialogSections)
    }
    
}
