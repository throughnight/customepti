//
//  XLTTDialogMessage.swift
//  VNPost4T
//
//  Created by AEGONA on 9/25/20.
//

import Foundation
import UIKit
import RxCocoa

class XLTTDialogMessage: XLTTBaseViewController {
    
    var onDoneActions : (()->())?
    
    var titleDialog : BehaviorRelay<String>!
    var messageDialog : BehaviorRelay<String>!
    var status : BehaviorRelay<Bool>!
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSubtitle: UILabel!
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet var viewParent: UIView!
    @IBOutlet weak var viewContent: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
        let tapParent = UITapGestureRecognizer(target: self, action: #selector(onTapClose))
        viewParent.addGestureRecognizer(tapParent)
        let tapContent = UITapGestureRecognizer(target: self, action: #selector(onContent))
        viewContent.addGestureRecognizer(tapContent)
    }
    
    @IBAction func onOk(_ sender: Any) {
        self.onDoneActions?()
    }
    
    func setupView() {
        let title = self.titleDialog.value
        self.lbTitle.text = title
        
        let message = self.messageDialog.value
        if message == "" {
            self.lbSubtitle.isHidden = true
        } else {
            self.lbSubtitle.text = message
        }
        
        if self.status.value {
            self.imgStatus.image = UIImage(named: "ic_done_green", in: Bundle(identifier: XLTTConstants.bundleIdentifier), compatibleWith: UITraitCollection.init())
        } else {
            self.imgStatus.image = UIImage(named: "ic_error_red", in: Bundle(identifier: XLTTConstants.bundleIdentifier), compatibleWith: UITraitCollection.init())
        }
    }
    
    func setupViewModel() {
        
    }
    
    @objc func onTapClose(_ sender: Any){
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func onContent(_ sender: Any){
        
    }
}
