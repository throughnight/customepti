//
//  XLTTDialogSearchList.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/28/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

typealias XLTTDialogSearchListSource = RxTableViewSectionedReloadDataSource<DialogSearchSection>

class XLTTDialogSearchList: XLTTBaseViewController {
    
    static let cellIdentifier = "XLTTDialogSearchList"
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    
    var placeHolderText = "xltt_placeholder_search".localized
    
    var viewModel : XLTTDialogSearchListVM!
    
    var dataSource : XLTTDialogSearchListSource!
    
    var dataPickedAction : ((XLTTOptionsFilter?)->())?
    var onLoadMore : (()->())?
    var actionClose : (()->())?
    var onSearch : ((String?)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.XLTTSetGradientOrangeBackground()
        setupListView()
        setupViewModel()
        tfSearch.placeholder = placeHolderText
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        self.actionClose?()
    }
}

extension XLTTDialogSearchList : XLTTViewControllerProtocal {
    func setupListView() {
        
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        tbView.register(UINib(nibName: XLTTDialogSearchListCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: XLTTDialogSearchListCell.cellIdentifier)
        
        let ds : XLTTDialogSearchListSource = XLTTDialogSearchListSource(configureCell: { (dataSource, tv, indexPath, data) in
            let cell : XLTTDialogSearchListCell = tv.dequeueReusableCell(withIdentifier: XLTTDialogSearchListCell.cellIdentifier, for: indexPath) as! XLTTDialogSearchListCell
            
            if self.viewModel.input?.isEmployee.value ?? false {
                cell.dataEmployee = data
            } else {
                cell.data = data
            }
            
            cell.index = indexPath.row
            cell.itemSelect = self.viewModel.input?.initValue.value
            cell.isSingle = self.viewModel.input?.isSingle.value ?? false
            
            return cell
        })
        
        dataSource = ds
        
        tbView.rx.modelSelected(XLTTOptionsFilter.self)
            .subscribe(onNext: { [weak self] item in
                guard let self = self else { return }
                // other actions with Item object
                self.dataPickedAction?(item)
            }).disposed(by: disposeBag)
        
    }
    
    func setupViewModel() {
        if let ip = self.viewModel.input {
            bindingInput(input: ip)
        }
        if let op = self.viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input : XLTTDialogSearchListVM.Input){
        
        let loadMore = self.tbView.rx.contentOffset.asDriver()
            .flatMap { [weak self] offset -> Driver<Bool> in
                guard let self = self else { return Driver.empty() }
                return isNearTheBottomEdge(contentOffset: offset, self.tbView) ? Driver.just(true) : Driver.empty()
        }
        
        loadMore.drive(onNext: { (bool) in
            if bool {
                self.onLoadMore?()
            }
        }).disposed(by: disposeBag)
        
        self.lbTitle.text = input.titleDialog.value
        
        if input.isSearch.value == false {
            self.searchView.isHidden = true
            self.tbView.topAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
            self.view.layoutIfNeeded()
            self.searchView.setNeedsUpdateConstraints()
        } else {
            tfSearch.rx.text.orEmpty.asDriver()
                .distinctUntilChanged()
                .debounce(RxTimeInterval.milliseconds(300))
                .drive(onNext: {[weak self] result in
                    guard let self = self else { return }
                    self.onSearch?(self.tfSearch.text)
                })
                .disposed(by: disposeBag)
        }
        
    }
    
    private func bindingOutput(output : XLTTDialogSearchListVM.Output){
        output.dataDialogSections.drive(self.tbView.rx.items(dataSource: self.dataSource)).disposed(by: disposeBag)
    }
}
