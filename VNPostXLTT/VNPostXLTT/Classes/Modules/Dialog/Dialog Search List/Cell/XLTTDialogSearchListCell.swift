//
//  XLTTDialogSearchListCell.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/28/20.
//

import Foundation
import UIKit
import RxCocoa

class XLTTDialogSearchListCell: UITableViewCell{
    static let cellIdentifier = "XLTTDialogSearchListCell"

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbCode: UILabel!
    @IBOutlet weak var imgTick: UIImageView!
    
    
    var index : Int = 0 {
        didSet {
            if index % 2 == 0 {
                bgView.backgroundColor = .white
            } else {
                bgView.backgroundColor = UIColor(hex: "EEF3FF")
            }
        }
    }
    
    var data : XLTTOptionsFilter? {
        didSet {
            lbName.text = data?.name
            if data?.code != "" {
                lbCode.text = data?.code
                lbCode.isHidden = false
            } else {
                lbCode.isHidden = true
            }
        }
    }
    
    var isSingle : Bool = false {
        didSet {
            lbCode.isHidden = isSingle
        }
    }
    
    var itemSelect : XLTTOptionsFilter? {
        didSet {
            if itemSelect?.id == data?.id {
                imgTick.isHidden = false
            } else {
                if itemSelect?.id == dataEmployee?.id {
                    imgTick.isHidden = false
                } else {
                    imgTick.isHidden = true
                }
            }
        }
    }
    
    var dataEmployee : XLTTOptionsFilter? {
        didSet {
            lbName.text = "\(dataEmployee?.fullName ?? "") - \(dataEmployee?.employeeCode ?? "")"
            if dataEmployee?.email != "" {
                lbCode.text = dataEmployee?.email
                lbCode.isHidden = false
            } else {
                lbCode.isHidden = true
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
