//
//  XLTTDialogSearchListVM.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/28/20.
//

import Foundation
import RxCocoa
import RxSwift
import RxDataSources

typealias DialogSearchSection = SectionModel<String,XLTTOptionsFilter>

class XLTTDialogSearchListVM: XLTTViewModelProtocal {
    
    typealias Dependency = XLTTCommonService
    
    struct Input {
        let dataDialog                      :BehaviorRelay<[XLTTOptionsFilter]>
        let isSearch                        :BehaviorRelay<Bool>
        let titleDialog                     :BehaviorRelay<String>
        let initValue                       :BehaviorRelay<XLTTOptionsFilter>
        let isSingle                        :BehaviorRelay<Bool>
        let isEmployee                      :BehaviorRelay<Bool>
    }
    
    struct Output {
        let dataDialogSections              :Driver<[DialogSearchSection]>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input : Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: XLTTCommonService?) -> Output? {
        self.input = input
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        
        let dialogSections = ip.dataDialog.asDriver().map { (data) -> [DialogSearchSection] in
            return [DialogSearchSection(model: "DialogSearch", items: data)]
        }
        
        return XLTTDialogSearchListVM.Output(dataDialogSections: dialogSections)
    }
    
}
