//
//  XLTTDialogAddNoteVC.swift
//  VNPostXLTT
//
//  Created by AEGONA on 11/5/20.
//

import Foundation
import RxSwift
import RxCocoa

class XLTTDialogAddNoteVC : XLTTBaseViewController {
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var tfNote: UITextField!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet var viewParent: UIView!
    @IBOutlet weak var viewContent: UIView!
    
    var note: BehaviorRelay<String?> = BehaviorRelay<String?>(value: "")
    
    var onAddNote: ((String)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbTitle.text = "xltt_tab_note".localized
        tfNote.placeholder = "xltt_note_placeholder".localized
        btnCancel.setTitle("xltt_title_action_cancel".localized.uppercased(), for: .normal)
        btnConfirm.setTitle("xltt_title_confirm_save".localized.uppercased(), for: .normal)
        
        note.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            if result != nil && result?.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                self.btnConfirm.setTitleColor(.white, for: .normal)
                self.btnConfirm.backgroundColor = UIColor(hex: "127BCA")
            } else {
                self.btnConfirm.setTitleColor(UIColor(hue: 0, saturation: 0, brightness: 0, alpha: 0.2), for: .normal)
                self.btnConfirm.backgroundColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: 0.05)
            }
        }).disposed(by: disposeBag)
        
        tfNote.rx.text.asDriver()
            .distinctUntilChanged()
            .debounce(RxTimeInterval.milliseconds(300))
            .drive(self.note)
            .disposed(by: disposeBag)
        
        let tapClose = UITapGestureRecognizer(target: self, action: #selector(onTapClose))
        viewParent.addGestureRecognizer(tapClose)
        let tapContent = UITapGestureRecognizer(target: self, action: #selector(onTapContent))
        viewContent.addGestureRecognizer(tapContent)
    }
    
    @objc func onTapClose(_ sender: Any){
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func onTapContent(_ sender: Any){
        
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onConfirm(_ sender: Any) {
        if tfNote.text != "" {
            self.onAddNote?(tfNote.text ?? "")
        }
    }
}
