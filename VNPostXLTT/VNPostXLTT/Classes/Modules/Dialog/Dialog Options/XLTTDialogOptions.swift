//
//  XLTTDialogOptions.swift
//  
//
//  Created by AEGONA on 9/16/20.
//

import Foundation
import UIKit
import RxCocoa

class XLTTDialogOptions: XLTTBaseViewController {
    
    var onCancelAction : (()->())?
    
    var onDoneActions : ((String?)->())?
    
    var titleDialog : BehaviorRelay<String>!
    var messageDialog : BehaviorRelay<String>!
    var titleActionCancel: BehaviorRelay<String?>!
    var titleActionConfirm: BehaviorRelay<String?>!
    var colorMessageDialog : BehaviorRelay<UIColor?>!
    var isInput : BehaviorRelay<Bool?>!
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSubtitle: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var tfInput: UITextField!
    @IBOutlet var viewParent: UIView!
    @IBOutlet weak var viewContent: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        let tapParent = UITapGestureRecognizer(target: self, action: #selector(onTapClose))
        viewParent.addGestureRecognizer(tapParent)
        let tapContent = UITapGestureRecognizer(target: self, action: #selector(onContent))
        viewContent.addGestureRecognizer(tapContent)
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.onCancelAction?()
    }
    
    @IBAction func onOk(_ sender: Any) {
        self.onDoneActions?(tfInput.text)
    }
    func setupView() {
        self.btnConfirm.xlttSetGradientBlueButtonBackground()
        
        self.titleDialog.subscribe(onNext: { title in
            self.desginImageLeftLabel(label: self.lbTitle, title: title)
        }).disposed(by: disposeBag)
        
        self.messageDialog.subscribe(onNext: { (message) in
            if message == "" {
                self.lbSubtitle.isHidden = true
            } else {
                self.lbSubtitle.text = message
            }
        }).disposed(by: disposeBag)
        
        if titleActionCancel != nil {
            btnCancel.setTitle(titleActionCancel.value?.uppercased() ?? "xltt_title_action_cancel".localized.uppercased(), for: .normal)
        } else {
            btnCancel.setTitle("xltt_title_action_cancel".localized.uppercased() , for: .normal)
        }
        if titleActionConfirm != nil {
            btnConfirm.setTitle(titleActionConfirm.value?.uppercased() ?? "xltt_title_action_confirm".localized.uppercased(), for: .normal)
        } else {
            btnConfirm.setTitle("xltt_title_action_confirm".localized.uppercased(), for: .normal)
        }
        if colorMessageDialog != nil {
            self.lbSubtitle.textColor = self.colorMessageDialog.value
        }
        
        if self.isInput?.value ?? false {
            self.tfInput.isHidden = false
        } else {
            self.tfInput.isHidden = true
        }
    }
    
    func setupViewModel() {
        
    }
    
    func desginImageLeftLabel(label: UILabel,title: String){
        // Create Attachment
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = UIImage(named: "ic_warning",
                                        in: Bundle(url: Bundle.main.bundleURL.appendingPathComponent("Frameworks").appendingPathComponent("VNPostXLTT.framework")) ?? Bundle.main,
                                        compatibleWith: nil)
        // Set bound to reposition
        let imageOffsetY: CGFloat = -5.0
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
        // Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        // Initialize mutable string
        let completeText = NSMutableAttributedString(string: "")
        // Add image to mutable string
        completeText.append(attachmentString)
        // Add your text to mutable string
        let textAfterIcon = NSAttributedString(string: title)
        completeText.append(textAfterIcon)
        label.textAlignment = .center
        label.attributedText = completeText
    }
    
    @objc func onTapClose(_ sender: Any){
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func onContent(_ sender: Any){
        
    }
}
