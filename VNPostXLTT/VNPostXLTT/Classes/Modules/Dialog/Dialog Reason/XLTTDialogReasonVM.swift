//
//  XLTTDialogReasonVM.swift
//  VNPostXLTT
//
//  Created by AEGONA on 11/4/20.
//

import Foundation
import RxCocoa
import RxSwift
import RxDataSources

typealias ReasonSection = SectionModel<String,XLTTOptionsFilter>

class XLTTDialogReasonVM: XLTTViewModelProtocal {
    
    typealias Dependency = XLTTCommonService
    
    struct Input {
        let search                          :BehaviorRelay<String>
        let triggerLoad                     :BehaviorRelay<Bool>
        let triggerLoadMore                 :BehaviorRelay<Bool>
        let data                            :BehaviorRelay<[XLTTOptionsFilter]>
        let index                           :BehaviorRelay<Int>
        let size                            :BehaviorRelay<Int>
        let reasonSelect                    :BehaviorRelay<XLTTOptionsFilter>
        let isLoading                       :BehaviorRelay<Bool>
        let triggerReason                   :BehaviorRelay<Bool>
    }
    
    struct Output {
        let dataResult                  :Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>>
        let dataMoreResult              :Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>>
        let dataSection                 :Driver<[ReasonSection]>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input : Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: XLTTCommonService?) -> Output? {
        self.input = input
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        
        let loadTrigger = Observable.merge(ip.triggerLoad.asObservable().filter {$0}.withLatestFrom(ip.search),ip.search.asObservable())
        
        let dataResult = loadTrigger.asObservable()
            .do { () in
                ip.isLoading.accept(true)
        }.flatMapLatest { (search) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> in
            return dp.getListReason(search: search, index: ip.index.value, pageSize: ip.size.value)
        }.do {() in
            ip.isLoading.accept(false)
        }
        
        let moreResult = ip.triggerLoadMore.asObservable()
            .filter {$0}
            .withLatestFrom(ip.search)
            .do { () in
                ip.isLoading.accept(true)
        }.flatMapFirst { (search) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> in
            return dp.getListReason(search: search, index: ip.index.value, pageSize: ip.size.value)
        }.do {() in
            ip.isLoading.accept(false)
        }
        
        
        let dialogSections = ip.data.asDriver().map { (data) -> [ReasonSection] in
            return [ReasonSection(model: "Reason", items: data)]
        }
        
        return XLTTDialogReasonVM.Output(dataResult: dataResult,
                                       dataMoreResult: moreResult,
                                       dataSection: dialogSections)
    }
    
}
