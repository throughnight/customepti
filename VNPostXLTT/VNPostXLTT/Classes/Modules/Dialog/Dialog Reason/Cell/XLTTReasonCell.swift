//
//  XLTTReasonCell.swift
//  VNPostXLTT
//
//  Created by AEGONA on 11/4/20.
//

import Foundation
import UIKit
import RxCocoa

class XLTTReasonCell: UITableViewCell{
    static let cellIdentifier = "XLTTReasonCell"
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var imgTick: UIImageView!
    
    var index : Int = 0 {
        didSet {
            if index % 2 == 0 {
                bgView.backgroundColor = .white
            } else {
                bgView.backgroundColor = UIColor(hex: "EEF3FF")
            }
        }
    }
    
    var data : XLTTOptionsFilter? {
        didSet {
            lbTitle.text = "\(data?.code ?? "") - \(data?.title ?? "")"
        }
    }
    
    var itemSelect : XLTTOptionsFilter? {
        didSet {
            if itemSelect?.id == data?.id {
                imgTick.isHidden = false
            } else {
                imgTick.isHidden = true
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
