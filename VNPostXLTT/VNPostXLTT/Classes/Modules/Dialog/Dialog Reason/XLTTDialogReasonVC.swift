//
//  XLTTDialogReasonVC.swift
//  VNPostXLTT
//
//  Created by AEGONA on 11/4/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

typealias XLTTDialogReasonSource = RxTableViewSectionedReloadDataSource<ReasonSection>

class XLTTDialogReasonVC : XLTTBaseViewController {
    
    @IBOutlet weak var bgHeaderView: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var imgEmpty: UIImageView!
    
    var viewModel: XLTTDialogReasonVM!
    var viewControllerParent : UIViewController!
    
    private var dataSource: XLTTDialogReasonSource!
    private var refreshControl : UIRefreshControl!
    
    var onDone : ((String?)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupListView()
        setupViewModel()
        
        btnCancel.setTitle("xltt_title_action_cancel".localized.uppercased(), for: .normal)
        btnConfirm.setTitle("xltt_title_action_confirm".localized.uppercased(), for: .normal)
        lbTitle.text = "xltt_reason_title".localized.uppercased()
    }
    
    @IBAction func onClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onConfirm(_ sender: Any) {
        self.onDone?(self.viewModel.input?.reasonSelect.value.title)
    }
}

extension XLTTDialogReasonVC : XLTTViewControllerProtocal {
    func setupViewModel() {
        if let ip = self.viewModel.input {
            bindingInput(input: ip)
        }
        if let op = self.viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input: XLTTDialogReasonVM.Input){
        let loadMore = self.tbView.rx.contentOffset.asDriver()
            .flatMap { [weak self] offset -> Driver<Bool> in
                guard let self = self else { return Driver.empty() }
                let index = self.viewModel.input?.index.value ?? 0
                let size = self.viewModel.input?.size.value ?? 0
                let count = self.viewModel.input?.data.value.count ?? 0
                if ((index * size) - count) == 0 {
                    return isNearTheBottomEdge(contentOffset: offset, self.tbView) ? Driver.just(true) : Driver.empty()
                }
                return Driver.empty()
        }
        
        loadMore.drive(input.triggerLoadMore).disposed(by: disposeBag)
        
        tfSearch.rx.text.orEmpty.asDriver()
            .distinctUntilChanged()
            .debounce(RxTimeInterval.milliseconds(300))
            .drive(input.search)
            .disposed(by: disposeBag)
        
        input.reasonSelect.subscribe(onNext: {[weak self] result in
            guard let self = self else { return }
            if result.id != "" {
                self.btnConfirm.backgroundColor = UIColor(hex: "3692F4")
                self.btnConfirm.setTitleColor(.white, for: .normal)
                self.btnConfirm.isEnabled = true
            } else {
                self.btnConfirm.backgroundColor = UIColor(hex: "E9E9E9")
                self.btnConfirm.layer.borderColor = UIColor(hex: "E9E9E9").cgColor
                self.btnConfirm.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 0.2), for: .normal)
                self.btnConfirm.isEnabled = false
            }
        })
    }
    
    private func bindingOutput(output: XLTTDialogReasonVM.Output){
        output.dataSection.drive(self.tbView.rx.items(dataSource: self.dataSource)).disposed(by: disposeBag)
        
        output.dataResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
            }
            if result.success {
                if result.data?.data.count ?? 0 > 0 {
                    self.imgEmpty.isHidden = true
                } else {
                    self.imgEmpty.isHidden = false
                }
                self.viewModel.input?.data.accept(result.data?.data ?? [])
            } else {
                print("ERROR")
                let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
                let dialogError = XLTTDialogError(nibName: "XLTTDialogError", bundle: bundle)
                dialogError.modalPresentationStyle = .overFullScreen
                dialogError.messageDialog = BehaviorRelay<String>(value: self.getMessageError(error: result.message))
                dialogError.onDoneActions = {
                    dialogError.dismiss(animated: true, completion: nil)
                }
                self.viewControllerParent?.parent?.present(dialogError, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
        
        output.dataMoreResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.success {
                var data = self.viewModel.input?.data.value ?? []
                data += result.data?.data ?? []
                
                self.viewModel.input?.data.accept(data)
                var currentPage = self.viewModel.input?.index.value ?? 0
                currentPage += 1
                self.viewModel.input?.index.accept(currentPage)
            } else {
                print("ERROR")
            }
        }).disposed(by: disposeBag)
        
    }
    
    func setupListView() {
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = .clear
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        tbView.refreshControl = refreshControl
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        tbView.register(UINib(nibName: XLTTReasonCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: XLTTReasonCell.cellIdentifier)
        
        let ds = RxTableViewSectionedReloadDataSource<ReasonSection>(configureCell: { dataSource, tableView, indexPath, data in
            
            let cell : XLTTReasonCell = tableView.dequeueReusableCell(withIdentifier: XLTTReasonCell.cellIdentifier, for: indexPath) as! XLTTReasonCell
            cell.index = indexPath.row
            cell.data = data
            cell.itemSelect = self.viewModel.input?.reasonSelect.value
            return cell
        })
        
        dataSource = ds
        
        tbView.rx.modelSelected(XLTTOptionsFilter.self)
            .subscribe(onNext: { [weak self] item in
                guard let self = self else { return }
                self.viewModel.input?.reasonSelect.accept(item)
                self.tbView.reloadData()
            }).disposed(by: disposeBag)
    }
    
    @objc private func refresh() {
        self.viewModel.input?.triggerLoad.accept(true)
    }
}
