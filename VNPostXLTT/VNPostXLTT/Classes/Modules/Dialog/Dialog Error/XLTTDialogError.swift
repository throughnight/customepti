//
//  XLTTDialogError.swift
//  VNPost4T
//
//  Created by AEGONA on 9/25/20.
//

import Foundation
import UIKit
import RxCocoa

class XLTTDialogError: XLTTBaseViewController {
    
    var onDoneActions : (()->())?
    
    var messageDialog : BehaviorRelay<String>!
    var titleButton : BehaviorRelay<String> = BehaviorRelay<String>(value: "Ok")
    
    @IBOutlet weak var lbSubtitle: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet var viewParent: UIView!
    @IBOutlet weak var viewContent: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
        let tapParent = UITapGestureRecognizer(target: self, action: #selector(onTapClose))
        viewParent.addGestureRecognizer(tapParent)
        let tapContent = UITapGestureRecognizer(target: self, action: #selector(onContent))
        viewContent.addGestureRecognizer(tapContent)
        
    }
    
    @IBAction func onOk(_ sender: Any) {
        self.onDoneActions?()
    }
    func setupView() {
        let message = self.messageDialog.value
        if message == "" {
            self.lbSubtitle.isHidden = true
        } else {
            self.lbSubtitle.text = message
        }
        self.btnOk.setTitle(self.titleButton.value, for: .normal)
    }
    
    func setupViewModel() {
        
    }
    
    @objc func onTapClose(_ sender: Any){
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func onContent(_ sender: Any){
        
    }
}
