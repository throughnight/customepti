//
//  XLTTHistoryVC.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/29/20.
//

import Foundation
import RxCocoa
import RxSwift

class XLTTHistoryVC: XLTTBaseViewController {
    
    private let slidingTabController = XLTTUISimpleSlidingTabController()
    
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var bgHeaderView: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    
    private var allVC : XLTTHistoryTabVC!
    private var processVC : XLTTHistoryTabVC!
    private var finishVC : XLTTHistoryTabVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        bgHeaderView.XLTTSetGradientOrangeBackground()
        lbTitle.text = "xltt_home_history_label".localized.uppercased()
    }
    
    private func setupViews(){
        self.setupTab()
        
        tfSearch.rx.controlEvent(.editingChanged).debounce(RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance).subscribe(onNext: { _ in
            let currentTab = self.slidingTabController.currentPosition
            switch currentTab {
            case 0:
                if self.allVC.viewModel.input?.isFilter.value == false {
                    if self.tfSearch.text != self.allVC.viewModel.input?.search.value {
                        self.allVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                        self.allVC.viewModel.input?.triggerLoad.accept(true)
                    }
                } else {
                    if self.tfSearch.text != self.allVC.viewModel.input?.search.value {
                        self.allVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                        self.allVC.viewModel.input?.triggerFilter.accept(true)
                    }
                }
                return
            case 1:
                if self.processVC.viewModel.input?.isFilter.value == false {
                    if self.tfSearch.text != self.processVC.viewModel.input?.search.value {
                        self.processVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                        self.processVC.viewModel.input?.triggerLoad.accept(true)
                    }
                } else {
                    if self.tfSearch.text != self.processVC.viewModel.input?.search.value {
                        self.processVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                        self.processVC.viewModel.input?.triggerFilter.accept(true)
                    }
                }
                return
            case 2:
                if self.finishVC.viewModel.input?.isFilter.value == false {
                    if self.tfSearch.text != self.finishVC.viewModel.input?.search.value {
                        self.finishVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                        self.finishVC.viewModel.input?.triggerLoad.accept(true)
                    }
                } else {
                    if self.tfSearch.text != self.finishVC.viewModel.input?.search.value {
                        self.finishVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                        self.finishVC.viewModel.input?.triggerFilter.accept(true)
                    }
                }
                return
            default:
                if self.allVC.viewModel.input?.isFilter.value == false {
                    if self.tfSearch.text != self.allVC.viewModel.input?.search.value {
                        self.allVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                        self.allVC.viewModel.input?.triggerLoad.accept(true)
                    }
                } else {
                    if self.tfSearch.text != self.allVC.viewModel.input?.search.value {
                        self.allVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                        self.allVC.viewModel.input?.triggerFilter.accept(true)
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onQuestions(_ sender: Any) {
        let dialogComment = XLTTDialogCommentVC(nibName: "XLTTDialogCommentVC", bundle: vnpXLTTBundle)
        dialogComment.modalPresentationStyle = .overFullScreen
        dialogComment.isCustomer = BehaviorRelay<Bool>(value: false)
        self.parent?.present(dialogComment, animated: false, completion: nil)
    }
    
    private func setupTab(){
        self.tabView.backgroundColor = XLTTConstants.xltt_color_background
        self.tabView.addSubview(slidingTabController.view)
        
        self.slidingTabController.view.frame = self.tabView.bounds
        navigationController?.navigationBar.barTintColor = UIColor(hex: "EEF3FF")
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.barStyle = .black
        self.view.window?.makeKeyAndVisible()
        
        
        // MARK: slidingTabController
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        allVC = XLTTHistoryTabVC(nibName: "XLTTHistoryTabVC", bundle: bundle)
        allVC.viewModel = XLTTHistoryTabVM(
            input: XLTTHistoryTabVM.Input(isLoading: BehaviorRelay<Bool>(value: false),
                                          triggerLoad: BehaviorRelay<Bool>(value: true),
                                          triggerLoadMore: BehaviorRelay<Bool>(value: false),
                                          search: BehaviorRelay<String>(value: ""),
                                          index: BehaviorRelay<Int>(value: 1),
                                          pageSize: BehaviorRelay<Int>(value: 20),
                                          data: BehaviorRelay<[XLTTCustomer]>(value: []),
                                          triggerFilter: BehaviorRelay<Bool>(value: false),
                                          filter: BehaviorRelay<XLTTFilterModel>(value: XLTTFilterModel()),
                                          typeHistory: BehaviorRelay<Int>(value: XLTTHistoryType.EXCHANGED.rawValue),
                                          isFilter: BehaviorRelay<Bool>(value: false),
                                          triggerFilterLoadMore: BehaviorRelay<Bool>(value: false)),
            dependency: XLTTCustomerService())
        allVC.viewControllerParent = self
        
        processVC = XLTTHistoryTabVC(nibName: "XLTTHistoryTabVC", bundle: bundle)
        processVC.viewModel = XLTTHistoryTabVM(
            input: XLTTHistoryTabVM.Input(isLoading: BehaviorRelay<Bool>(value: false),
                                          triggerLoad: BehaviorRelay<Bool>(value: true),
                                          triggerLoadMore: BehaviorRelay<Bool>(value: false),
                                          search: BehaviorRelay<String>(value: ""),
                                          index: BehaviorRelay<Int>(value: 1),
                                          pageSize: BehaviorRelay<Int>(value: 20),
                                          data: BehaviorRelay<[XLTTCustomer]>(value: []),
                                          triggerFilter: BehaviorRelay<Bool>(value: false),
                                          filter: BehaviorRelay<XLTTFilterModel>(value: XLTTFilterModel()),
                                          typeHistory: BehaviorRelay<Int>(value: XLTTHistoryType.PROCESSING.rawValue),
                                          isFilter: BehaviorRelay<Bool>(value: false),
                                          triggerFilterLoadMore: BehaviorRelay<Bool>(value: false)),
            dependency: XLTTCustomerService())
        processVC.viewControllerParent = self
        
        finishVC = XLTTHistoryTabVC(nibName: "XLTTHistoryTabVC", bundle: bundle)
        finishVC.viewModel = XLTTHistoryTabVM(
            input: XLTTHistoryTabVM.Input(isLoading: BehaviorRelay<Bool>(value: false),
                                          triggerLoad: BehaviorRelay<Bool>(value: true),
                                          triggerLoadMore: BehaviorRelay<Bool>(value: false),
                                          search: BehaviorRelay<String>(value: ""),
                                          index: BehaviorRelay<Int>(value: 1),
                                          pageSize: BehaviorRelay<Int>(value: 20),
                                          data: BehaviorRelay<[XLTTCustomer]>(value: []),
                                          triggerFilter: BehaviorRelay<Bool>(value: false),
                                          filter: BehaviorRelay<XLTTFilterModel>(value: XLTTFilterModel()),
                                          typeHistory: BehaviorRelay<Int>(value: XLTTHistoryType.COMPLETED.rawValue),
                                          isFilter: BehaviorRelay<Bool>(value: false),
                                          triggerFilterLoadMore: BehaviorRelay<Bool>(value: false)),
            dependency: XLTTCustomerService())
        finishVC.viewControllerParent = self
        
        
        slidingTabController.addItem(item: allVC, title: "xltt_home_chart_all".localized)
        slidingTabController.addItem(item: processVC, title: "xltt_home_chart_progressing".localized)
        slidingTabController.addItem(item: finishVC, title: "xltt_title_end".localized)
        slidingTabController.setHeaderActiveColor(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.87))
        slidingTabController.setHeaderInActiveColor(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.6))
        slidingTabController.setHeaderBackgroundColor(color: UIColor(hex: "EEF3FF")) // default white
        slidingTabController.setIndicatorActiveColor(color: UIColor(hex: "127BCA"))
        slidingTabController.setStyle(style: .fixed) // default fixed
        slidingTabController.fontSize = 14
        slidingTabController.build() // build
        
        slidingTabController.onChangeTab = { tab in
            self.tfSearch.text = ""
            switch tab {
            case 0:
                self.allVC.viewModel.input?.isFilter.accept(false)
                self.allVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                self.allVC.viewModel.input?.triggerLoad.accept(true)
                return
            case 1:
                self.processVC.viewModel.input?.isFilter.accept(false)
                self.processVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                self.processVC.viewModel.input?.triggerLoad.accept(true)
                return
            case 2:
                self.finishVC.viewModel.input?.isFilter.accept(false)
                self.finishVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                self.finishVC.viewModel.input?.triggerLoad.accept(true)
                return
            default:
                self.allVC.viewModel.input?.isFilter.accept(false)
                self.allVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                self.allVC.viewModel.input?.triggerLoad.accept(true)
            }
        }
    }
}
