//
//  XLTTHistoryCell.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/29/20.
//

import Foundation
import UIKit
import RxCocoa

class XLTTHistoryCell: UITableViewCell{
    static let cellIdentifier = "XLTTHistoryCell"
    
    @IBOutlet weak var lbNameTitle: UILabel!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbPhoneTitle: UILabel!
    @IBOutlet weak var lbPhone: UILabel!
    @IBOutlet weak var lbProductNameTitle: UILabel!
    @IBOutlet weak var lbProductName: UILabel!
    @IBOutlet weak var viewDateUpdate: UIStackView!
    @IBOutlet weak var lbDateUpdateTitle: UILabel!
    @IBOutlet weak var lbDateUpdate: UILabel!
    @IBOutlet weak var viewDateTrade: UIStackView!
    @IBOutlet weak var lbDateTradeTitle: UILabel!
    @IBOutlet weak var lbDateTrade: UILabel!
    @IBOutlet weak var viewTimeEnd: UIStackView!
    @IBOutlet weak var lbTimeEndTitle: UILabel!
    @IBOutlet weak var lbTimeEnd: UILabel!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var lbEmailTitle: UILabel!
    @IBOutlet weak var lbEmail: UILabel!
    @IBOutlet weak var lbAddressTitle: UILabel!
    @IBOutlet weak var lbAddress: UILabel!
    @IBOutlet weak var viewAddress: UIStackView!
    @IBOutlet weak var viewEmail: UIStackView!
    @IBOutlet weak var viewPhone: UIStackView!
    @IBOutlet weak var viewContain: UIView!
    
    var onClickItem : ((XLTTCustomer?)->())?
    
    var data: XLTTCustomer? {
        didSet {
            lbNameTitle.text = "xltt_me_name".localized
            lbName.text = data?.customerName.uppercased()
            
            if data?.phoneNumber != "" {
                lbPhoneTitle.text = "xltt_me_phone".localized
                lbPhone.text = data?.phoneNumber
                viewPhone.isHidden = false
            } else {
                viewPhone.isHidden = true
            }
            
            if data?.email != nil && data?.email != "" {
                lbEmailTitle.text = "xltt_history_email_title".localized
                lbEmail.text = data?.email
                viewEmail.isHidden = false
            } else {
                viewEmail.isHidden = true
            }
            
            if data?.province != nil {
                lbAddressTitle.text = "xltt_history_address_title".localized
                lbAddress.text = data?.getFullAddress()
                viewAddress.isHidden = false
            } else {
                viewAddress.isHidden = true
            }
            
            lbProductNameTitle.text = "xltt_me_product".localized
            lbProductName.text = data?.product.uppercased()
            
            if data?.receivedDate != nil {
                lbDateUpdateTitle.text = "xltt_me_date_update".localized
                lbDateUpdate.text = data?.receivedDate?.xlttDateToStringCustomer()
                viewDateUpdate.isHidden = false
            } else {
                viewDateUpdate.isHidden = true
            }
            
            if data?.deliveryDate != nil {
                lbDateTradeTitle.text = "xltt_me_date_trade".localized
                lbDateTrade.text = data?.deliveryDate?.xlttDateToStringCustomer()
                viewDateTrade.isHidden = false
            } else {
                viewDateTrade.isHidden = true
            }
            
            if data?.transactionTime != nil {
                viewTimeEnd.isHidden = false
                lbTimeEndTitle.text = "xltt_me_time_trade".localized
                lbTimeEnd.text = data?.transactionTime
            } else {
                viewTimeEnd.isHidden = true
            }
            
            switch data?.status {
            case XLTTCustomerType.CUSTOMER_STATUS_CONTACT.rawValue:
                viewStatus.backgroundColor = UIColor(hex: "F7B500")
                return
            case XLTTCustomerType.CUSTOMER_STATUS_SUCCESS.rawValue:
                viewStatus.backgroundColor = UIColor(hex: "00B16A")
                return
            case XLTTCustomerType.CUSTOMER_STATUS_FAIL.rawValue:
                viewStatus.backgroundColor = UIColor(hex: "F64744")
                return
            default:
                switch data?.lead_Status {
                case XLTTCustomerDataType.CUSTOMER_STATUS2_REFUSE.rawValue:
                    viewStatus.backgroundColor = .black
                    return
                default:
                    viewStatus.backgroundColor = UIColor(hex: "7B61FF")
                }
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(onClick))
        viewContain.addGestureRecognizer(tap)
    }
    
    @objc func onClick(_ sender: Any){
        self.onClickItem?(data)
    }
}
