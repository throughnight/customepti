//
//  XLTTHomeChartVC.swift
//  VNPostXLTT
//
//  Created by m2 on 10/26/20.
//

import Foundation
import Charts
import RxCocoa
import RxSwift

class XLTTHomeChartVC: XLTTBaseViewController {
    
    var dataChart = BehaviorRelay<XLTTChartHome?>(value: nil)
    var isSupperVisor = false
    
    @IBOutlet weak var chartTitleLabael: UILabel!
    @IBOutlet weak var allValueLabel: UILabel!
    @IBOutlet weak var successValueLabel: UILabel!
    @IBOutlet weak var failureValueLabel: UILabel!
    @IBOutlet weak var processingValueLabel: UILabel!
    @IBOutlet weak var allTitleLabel: UILabel!
    @IBOutlet weak var sucessTitleLabel: UILabel!
    @IBOutlet weak var failureTitleLabel: UILabel!
    @IBOutlet weak var processingTitleLabel: UILabel!
    
    @IBOutlet weak var leftChartLabel: UILabel!
    @IBOutlet weak var rightChartLabel: UILabel!
    
    @IBOutlet weak var lineChart: LineChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }
    
    override init(nibName: String?, bundle: Bundle?) {
        super.init(nibName: nibName, bundle: bundle)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews(){
        self.view.backgroundColor = XLTTConstants.xltt_color_background
        
        chartTitleLabael.text = "xltt_home_title_report_label".localized
        
        allTitleLabel.text = "xltt_home_chart_all".localized
        sucessTitleLabel.text = "xltt_home_chart_success".localized
        failureTitleLabel.text = "xltt_home_chart_failure".localized
        processingTitleLabel.text = "xltt_home_chart_progressing".localized
        
        allValueLabel.textColor = XLTTConstants.xltt_color_blue_1
        successValueLabel.textColor = XLTTConstants.xltt_color_green_1
        failureValueLabel.textColor = XLTTConstants.xltt_color_red_1
        processingValueLabel.textColor = XLTTConstants.xltt_color_orange_1
        
        dataChart
            .filter{$0 != nil}
            .subscribe(onNext:{ result in
                var successArray : [Int] = []
                var failureArray : [Int] = []
                
                self.allValueLabel.text = String(describing: result!.all)
                self.successValueLabel.text = String(describing: result!.success)
                self.failureValueLabel.text = String(describing: result!.fail)
                self.processingValueLabel.text = String(describing: result!.processing)
                
                if(self.isSupperVisor){
                    self.sucessTitleLabel.text = "xltt_home_chart_success_group".localized
                    self.failureTitleLabel.text = "xltt_home_chart_failure_group".localized
                    self.leftChartLabel.text = "xltt_home_chart_success_group".localized
                    self.rightChartLabel.text = "xltt_home_chart_failure_group".localized
                }
                
                result?.listChart.forEach{item in
                    successArray.append(item.success)
                    failureArray.append(item.fail)
                }
                
                self.setupChart(successList: successArray, failureList: failureArray)
                print("result\(String(describing: result?.toJSON()))")
            }).disposed(by: disposeBag)
        
    }
    
    private func setupChart(successList : [Int] = [], failureList : [Int] = []){
        let data = LineChartData()
        data.addDataSet(generateLineData(data: successList, dataIsSuccess: true))
        data.addDataSet(generateLineData(data: failureList))
        
        let successCount = NSSet(array: successList).count
        let failureCount = NSSet(array: failureList).count
        
        self.lineChart.data = data
        self.lineChart.chartDescription?.enabled = false
        self.lineChart.setScaleEnabled(false)
        self.lineChart.legend.enabled = false
        self.lineChart.highlightPerTapEnabled = false
        self.lineChart.isUserInteractionEnabled = false
        
        //right
        let rightAxis = self.lineChart.rightAxis
        rightAxis.axisMinimum = 0
        rightAxis.labelCount = failureCount
        rightAxis.forceLabelsEnabled = true
        rightAxis.spaceBottom = 0
        
        if failureList.max() != 0 {
            rightAxis.axisMaximum = failureList.max() != nil ? (Double((failureList.max() ?? 0)) * 3) : 0
                * 3
        } else {
            rightAxis.axisMaximum = 1
        }
        
        //left
        let leftAxis = self.lineChart.leftAxis
        leftAxis.axisMinimum = 0
        leftAxis.labelCount = successCount
        leftAxis.spaceBottom = 0
        leftAxis.forceLabelsEnabled = true
        
        if successList.max() != 0 {
            leftAxis.axisMaximum = successList.max() != nil ? (Double((successList.max() ?? 0)) * 2) : 0
        } else {
            leftAxis.axisMaximum = 1
        }
        
        //bottom
        self.lineChart.xAxisRenderer = XLTTColoredLabelXAxisRenderer(viewPortHandler: lineChart.viewPortHandler, xAxis: lineChart.xAxis, transformer: lineChart.getTransformer(forAxis: YAxis.AxisDependency.left))
        
        self.lineChart.notifyDataSetChanged()
    }
    
    private func generateLineData(data : [Int], dataIsSuccess:Bool = false) -> LineChartDataSet {
        let entries = (0..<data.count).map { (i) -> ChartDataEntry in
            return ChartDataEntry(x: Double(i), y: Double(data[i]))
        }
        let set = LineChartDataSet(entries: entries, label: "Line")
        if(dataIsSuccess){
            set.setColor(XLTTConstants.xltt_color_green_1)
            set.axisDependency = .left
        }else{
            set.setColor(XLTTConstants.xltt_color_red_1)
            set.axisDependency = .right
        }
        set.lineWidth = 2
        set.drawValuesEnabled = false
        set.drawCirclesEnabled = false
        set.valueFont = .systemFont(ofSize: 10)
        
        return set
    }
    
    
}

