//
//  XLTTHomeVC.swift
//  VNPostXLTT
//
//  Created by m2 on 10/20/20.
//

import Foundation
import RxSwift
import RxCocoa

class XLTTHomeVC: XLTTBaseViewController {

    var viewModel : XLTTHomeVM!
    private var loading: XLTTLoadingProgressView!
    private let slidingTabController = XLTTUISimpleSlidingTabController()
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var customerView: UIView!
    @IBOutlet weak var historyView: UIView!
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var customerLabel: UILabel!
    @IBOutlet weak var historyLabel: UILabel!
    @IBOutlet weak var toolbarLabel: UILabel!
    
    @IBOutlet weak var toolbarButton: UIButton!
    
    var chartOfGroup : XLTTHomeChartVC!
    var chartOfMe : XLTTHomeChartVC!
    
    @IBAction func toolbarButoonClick(_ sender: Any) {
        self.showNavigationBar(animated: false)
        self.navigationController?.popViewController(animated: true)
    }
    
    private func customerClick(){
        let tap = UITapGestureRecognizer()
        customerView.addGestureRecognizer(tap)
        tap.rx.event.bind { (eventTap) in
            self.navigationController?.pushViewController(XLTTCustomerVC(nibName: "XLTTCustomerVC", bundle: self.vnpXLTTBundle), animated: true)
        }.disposed(by: disposeBag)
    }
    
    private func historyClick(){
        let tap = UITapGestureRecognizer()
        historyView.addGestureRecognizer(tap)
        tap.rx.event.bind { (eventTap) in
            self.navigationController?.pushViewController(XLTTHistoryVC(nibName: "XLTTHistoryVC", bundle: self.vnpXLTTBundle), animated: true)
        }.disposed(by: disposeBag)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.binUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.viewModel.requestChartOfMe.accept(true)
    }

    private func setupViews() {
        self.loading = XLTTLoadingProgressView(uiScreen: UIScreen.main)
        
        self.headerView.XLTTSetGradientOrangeBackground()
        
        self.customerLabel.text = "xltt_home_customer_label".localized
        self.historyLabel.text = "xltt_home_history_label".localized
        self.contentView.isHidden = true
        
        self.customerClick()
        self.historyClick()
    }
    
    private func forMember(chart: XLTTChartHome?){
        let chartOfMe = XLTTHomeChartVC(nibName: "XLTTHomeChartVC", bundle: vnpXLTTBundle)
        chartOfMe.dataChart.accept(chart)
        chartOfMe.view.frame = self.tabView.bounds
        self.tabView.addSubview(chartOfMe.view)
    }
    
    private func forSuppervisor(){
        
        self.tabView.backgroundColor = XLTTConstants.xltt_color_background
        self.tabView.addSubview(slidingTabController.view)
        
        self.slidingTabController.view.frame = self.tabView.bounds
        navigationController?.navigationBar.barTintColor = UIColor(hex: "EEF3FF")
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.barStyle = .black
        self.view.window?.makeKeyAndVisible()
        
        chartOfGroup = XLTTHomeChartVC(nibName: "XLTTHomeChartVC", bundle: vnpXLTTBundle)
        chartOfMe = XLTTHomeChartVC(nibName: "XLTTHomeChartVC", bundle: vnpXLTTBundle)
        
        slidingTabController.addItem(item: chartOfMe, title: "xltt_home_title_of_me".localized.uppercased())
        slidingTabController.addItem(item: chartOfGroup, title: "xltt_home_title_of_group".localized.uppercased())
        
        slidingTabController.setHeaderActiveColor(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.87))
        slidingTabController.setHeaderInActiveColor(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.6))
        slidingTabController.setHeaderBackgroundColor(color: UIColor(hex: "EEF3FF")) // default white
        slidingTabController.setIndicatorActiveColor(color: UIColor(hex: "127BCA"))
        slidingTabController.setStyle(style: .fixed) // default fixed
        slidingTabController.fontSize = 14
        slidingTabController.build()
    }
    
    private func binUI(){
        
        self.viewModel.isLoading
            .filter{$0 != nil}
            .subscribe(onNext:{ loading in
                self.view.addSubview(self.loading)
                if(loading==true) {
                    self.loading.startAnimating()
                }else{
                    self.loading.stopAnimating()
                }
            }).disposed(by: disposeBag)
        
        self.viewModel.getTokenOutput?
            .asObservable()
            .subscribe(onNext: { result in
                self.viewModel.isLoading.accept(false)
                if(result.success){
                    let profileType = result.data?.profiletype ?? 0
                    XLTTManager.shared.setAccessToken(result.data?.data?.token ?? "")
                    XLTTManager.shared.setUserProfileType(profileType)
                    self.viewModel.profileType.accept(result.data?.profiletype)
                    if result.data?.profiletype == XLTTProfileType.SUPERVISOR.rawValue{
                        self.forSuppervisor()
                    }
                    self.viewModel.requestChartOfMe.accept(true)
                    self.contentView.isHidden = false
                }else{
                    self.showErrorDialog(errorMsg: result.message)
                }
                
            }).disposed(by: disposeBag)
        
        self.viewModel.profileType
            .filter{$0 != nil}
            .subscribe(onNext:{ profiletype in
                switch profiletype {
                case XLTTProfileType.MEMBER.rawValue:
                    self.toolbarLabel.text = "xltt_home_toolbar_title_member".localized
                    break
                case XLTTProfileType.SUPERVISOR.rawValue:
                    self.toolbarLabel.text = "xltt_home_toolbar_title_supervisor".localized
                    break
                default:
                    print("ORTHER")
                }
            }).disposed(by: disposeBag)
        
        self.viewModel.getChartOfMeOutput?.asObservable().subscribe(onNext:{result in
            switch self.viewModel.profileType.value {
            case XLTTProfileType.MEMBER.rawValue:
                self.forMember(chart: result.data)
                break
            case XLTTProfileType.SUPERVISOR.rawValue:
                self.viewModel.requestChartOfGroup.accept(true)
                self.viewModel.chartOfMe.accept(result.data)
                break
            default:
                print("ORTHER")
            }
        }).disposed(by: disposeBag)
        
        self.viewModel.getChartOfGroupOutput?.asObservable().subscribe(onNext:{result in
//            self.forSuppervisor(me: self.viewModel.chartOfMe.value, group: result.data)
            self.chartOfMe.dataChart.accept(self.viewModel.chartOfMe.value)
            self.chartOfGroup.dataChart.accept(result.data)
            self.chartOfGroup.isSupperVisor = true
        }).disposed(by: disposeBag)
        
    }
}
