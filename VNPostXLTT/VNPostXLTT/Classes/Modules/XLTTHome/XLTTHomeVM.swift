//
//  XLTTHomeVM.swift
//  VNPostXLTT
//
//  Created by m2 on 10/27/20.
//

import Foundation
import RxCocoa
import RxSwift

class XLTTHomeVM {
    let tokenVNPost: BehaviorRelay<String?>
    var serviceAuth: XLTTAuthService!
    var potentialService: XLTTPotentialService!
    
    var profileType = BehaviorRelay<Int?>(value: nil)
    var requestChartOfMe = BehaviorRelay<Bool?>(value: nil)
    var requestChartOfGroup = BehaviorRelay<Bool?>(value: nil)
    var isLoading = BehaviorRelay<Bool?>(value: nil)
    var chartOfMe = BehaviorRelay<XLTTChartHome?>(value: nil)
    
    var getTokenOutput = Observable<XLTTResponseModel<XLTTGetTokenMobileModel>>?(nil)
    var getChartOfMeOutput = Observable<XLTTResponseModel<XLTTChartHome>>?(nil)
    var getChartOfGroupOutput = Observable<XLTTResponseModel<XLTTChartHome>>?(nil)
    
    init(token: String?) {
        
        self.tokenVNPost = BehaviorRelay<String?>(value: token)
        bindOutput()
    }
    
    private func bindOutput() {
        
        self.serviceAuth = XLTTAuthService()
        self.potentialService = XLTTPotentialService()
        
        self.getTokenOutput = self.getToken()
        self.getChartOfMeOutput = self.getDataChartOfMe()
        self.getChartOfGroupOutput = self.getDataChartOfGroup()
    }
    
    private func getToken()
        -> Observable<XLTTResponseModel<XLTTGetTokenMobileModel>>? {
            return self.tokenVNPost.asObservable()
                .filter{ $0 != nil }
                .do(onNext: { _ in
                    self.isLoading.accept(true)
                })
                .flatMap{ token -> Observable<XLTTResponseModel<XLTTGetTokenMobileModel>> in
                    return self.serviceAuth.getToken(token: token ?? "")
            }
    }
    
    private func getDataChartOfMe()
        -> Observable<XLTTResponseModel<XLTTChartHome>>? {
            return self.requestChartOfMe.asObservable()
                .filter{ $0 != nil }
                .flatMap{ _ -> Observable<XLTTResponseModel<XLTTChartHome>> in
                    return self.potentialService.getMobileGetDataHome(type: "ME")
            }
    }
    
    private func getDataChartOfGroup()
        -> Observable<XLTTResponseModel<XLTTChartHome>>? {
            return self.requestChartOfGroup.asObservable()
                .filter{ $0 != nil }
                .flatMap{ _ -> Observable<XLTTResponseModel<XLTTChartHome>> in
                    return self.potentialService.getMobileGetDataHome(type: "GROUP")
            }
    }
}
