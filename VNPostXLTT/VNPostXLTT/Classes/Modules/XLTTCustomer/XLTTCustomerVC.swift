//
//  XLTTCustomerVC.swift
//  VNPostXLTT
//
//  Created by m2 on 10/20/20.
//

import Foundation
import RxCocoa
import RxSwift

class XLTTCustomerVC: XLTTBaseViewController {
    
    private let slidingTabController = XLTTUISimpleSlidingTabController()
    
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var bgHeaderView: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    
    private var meVC : XLTTOfMeVC!
    private var teamVC : XLTTOfTeamVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        bgHeaderView.XLTTSetGradientOrangeBackground()
        lbTitle.text = "xltt_home_customer_label".localized.uppercased()
        tfSearch.placeholder = "xltt_placeholder_search_customer".localized
    }
    
    private func setupViews(){
        self.setupTab()
        
        tfSearch.rx.controlEvent(.editingChanged).debounce(RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance).subscribe(onNext: { _ in
            let currentTab = self.slidingTabController.currentPosition
            if currentTab == 0 {
                if self.meVC.viewModel.input?.isFilter.value == false {
                    if self.tfSearch.text != self.meVC.viewModel.input?.search.value {
                        self.meVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                        self.meVC.viewModel.input?.triggerLoad.accept(true)
                    }
                } else {
                    if self.tfSearch.text != self.meVC.viewModel.input?.search.value {
                        self.meVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                        self.meVC.viewModel.input?.triggerFilter.accept(true)
                    }
                }
                
            } else {
                if self.teamVC.viewModel.input?.isFilter.value == false {
                    if self.tfSearch.text != self.teamVC.viewModel.input?.search.value {
                        self.teamVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                        self.teamVC.viewModel.input?.triggerLoad.accept(true)
                    }
                } else {
                    if self.tfSearch.text != self.teamVC.viewModel.input?.search.value {
                        self.teamVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                        self.teamVC.viewModel.input?.triggerFilter.accept(true)
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onQuestion(_ sender: Any) {
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        let dialogComment = XLTTDialogCommentVC(nibName: "XLTTDialogCommentVC", bundle: bundle)
        dialogComment.modalPresentationStyle = .overFullScreen
        dialogComment.isCustomer = BehaviorRelay<Bool>(value: true)
        self.parent?.present(dialogComment, animated: false, completion: nil)
    }
    
    private func setupTab(){
        self.tabView.backgroundColor = XLTTConstants.xltt_color_background
        self.tabView.addSubview(slidingTabController.view)
        
        self.slidingTabController.view.frame = self.tabView.bounds
        navigationController?.navigationBar.barTintColor = UIColor(hex: "EEF3FF")
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.barStyle = .black
        self.view.window?.makeKeyAndVisible()
        
        
        // MARK: slidingTabController
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        meVC = XLTTOfMeVC(nibName: "XLTTOfMeVC", bundle: bundle)
        meVC.viewModel = XLTTOfMeVM(
            input: XLTTOfMeVM.Input(isLoading: BehaviorRelay<Bool>(value: false),
                                    triggerLoad: BehaviorRelay<Bool>(value: true),
                                    triggerLoadMore: BehaviorRelay<Bool>(value: false),
                                    search: BehaviorRelay<String>(value: ""),
                                    index: BehaviorRelay<Int>(value: 1),
                                    pageSize: BehaviorRelay<Int>(value: 20),
                                    data: BehaviorRelay<[XLTTCustomer]>(value: []),
                                    triggerAccept: BehaviorRelay<Bool>(value: false),
                                    triggerDeny: BehaviorRelay<Bool>(value: false),
                                    itemTrigger: BehaviorRelay<XLTTCustomer>(value: XLTTCustomer()),
                                    triggerFilter: BehaviorRelay<Bool>(value: false),
                                    filter: BehaviorRelay<XLTTFilterModel>(value: XLTTFilterModel()),
                                    isFilter: BehaviorRelay<Bool>(value: false),
                                    triggerFilterLoadMore: BehaviorRelay<Bool>(value: false)),
            dependency: XLTTCustomerService())
        meVC.viewControllerParent = self
        
        teamVC = XLTTOfTeamVC(nibName: "XLTTOfTeamVC", bundle: bundle)
        teamVC.viewModel = XLTTOfTeamVM(
            input: XLTTOfTeamVM.Input(isLoading: BehaviorRelay<Bool>(value: false),
                                      triggerLoad: BehaviorRelay<Bool>(value: true),
                                      triggerLoadMore: BehaviorRelay<Bool>(value: false),
                                      search: BehaviorRelay<String>(value: ""),
                                      index: BehaviorRelay<Int>(value: 1),
                                      pageSize: BehaviorRelay<Int>(value: 20),
                                      data: BehaviorRelay<[XLTTCustomer]>(value: []),
                                      triggerAccept: BehaviorRelay<Bool>(value: false),
                                      triggerDeny: BehaviorRelay<Bool>(value: false),
                                      itemTrigger: BehaviorRelay<XLTTCustomer>(value: XLTTCustomer()),
                                      triggerFilter: BehaviorRelay<Bool>(value: false),
                                      filter: BehaviorRelay<XLTTFilterModel>(value: XLTTFilterModel()),
                                      isFilter: BehaviorRelay<Bool>(value: false),
                                      triggerFilterLoadMore: BehaviorRelay<Bool>(value: false)),
            dependency: XLTTCustomerService())
        teamVC.viewControllerParent = self
        
        slidingTabController.addItem(item: meVC, title: "xltt_customer_me".localized.uppercased())
        slidingTabController.addItem(item: teamVC, title: "xltt_customer_team".localized.uppercased())
        slidingTabController.setHeaderActiveColor(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.87))
        slidingTabController.setHeaderInActiveColor(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.6))
        slidingTabController.setHeaderBackgroundColor(color: UIColor(hex: "EEF3FF")) // default white
        slidingTabController.setIndicatorActiveColor(color: UIColor(hex: "127BCA"))
        slidingTabController.setStyle(style: .fixed) // default fixed
        slidingTabController.fontSize = 14
        slidingTabController.build() // build
        
        slidingTabController.onChangeTab = { tab in
            self.tfSearch.text = ""
            if tab == 0 {
                self.meVC.viewModel.input?.isFilter.accept(false)
                self.meVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                self.meVC.viewModel.input?.triggerLoad.accept(true)
            } else {
                self.teamVC.viewModel.input?.isFilter.accept(false)
                self.teamVC.viewModel.input?.search.accept(self.tfSearch.text ?? "")
                self.teamVC.viewModel.input?.triggerLoad.accept(true)
            }
        }
    }
}
