//
//  XLTTCustomerOfTeamCell.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/27/20.
//

import Foundation
import UIKit
import RxCocoa

class XLTTCustomerOfTeamCell: UITableViewCell{
    static let cellIdentifier = "XLTTCustomerOfTeamCell"
    
    @IBOutlet weak var lbNameTitle: UILabel!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbPhoneTitle: UILabel!
    @IBOutlet weak var lbPhone: UILabel!
    @IBOutlet weak var lbProductNameTitle: UILabel!
    @IBOutlet weak var lbProductName: UILabel!
    @IBOutlet weak var viewDateUpdate: UIStackView!
    @IBOutlet weak var lbDateUpdateTitle: UILabel!
    @IBOutlet weak var lbDateUpdate: UILabel!
    @IBOutlet weak var viewLineAction: UIView!
    @IBOutlet weak var viewAction: UIStackView!
    @IBOutlet weak var imgPin: UIImageView!
    @IBOutlet weak var btnDistribution: UIButton!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnDeny: UIButton!
    @IBOutlet weak var viewContain: UIView!
    
    var onActionAccept : ((XLTTCustomer?)->())?
    var onActionDeny : ((XLTTCustomer?)->())?
    var onActionDistribution : ((XLTTCustomer?)->())?
    var onClickItem : ((XLTTCustomer?)->())?
    
    var data: XLTTCustomer? {
        didSet {
            lbNameTitle.text = "xltt_me_name".localized
            lbName.text = data?.customerName.uppercased()
            lbPhoneTitle.text = "xltt_me_phone".localized
            lbPhone.text = data?.phoneNumber
            lbProductNameTitle.text = "xltt_me_product".localized
            lbProductName.text = data?.product.uppercased()
            
            if data?.receivedDate != nil {
                lbDateUpdateTitle.text = "xltt_me_date_update".localized
                lbDateUpdate.text = data?.receivedDate?.xlttDateToStringCustomer()
                viewDateUpdate.isHidden = false
            } else {
                viewDateUpdate.isHidden = true
            }
            btnAccept.setTitle("xltt_me_accept".localized.uppercased(), for: .normal)
            btnDeny.setTitle("xltt_me_deny".localized.uppercased(), for: .normal)
            btnDistribution.setTitle("xltt_distribution_title".localized.uppercased(), for: .normal)
            if data?.flag ?? false {
                imgPin.isHidden = false
            } else {
                imgPin.isHidden = true
            }
        }
    }
    
    var isSale : Bool? {
        didSet {
            if isSale ?? false {
                btnDeny.isHidden = true
                btnDistribution.isHidden = true
            } else {
                btnDeny.isHidden = false
                btnDistribution.isHidden = false
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(onClick))
        viewContain.addGestureRecognizer(tap)
    }
    
    @objc func onClick(_ sender: Any){
        self.onClickItem?(data)
    }
    
    @IBAction func onHandingLead(_ sender: Any) {
        self.onActionAccept?(data)
    }
    
    @IBAction func onDenyLead(_ sender: Any) {
        self.onActionDeny?(data)
    }
    
    @IBAction func onDistribution(_ sender: Any) {
        self.onActionDistribution?(data)
    }
}
