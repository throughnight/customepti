//
//  XLTTOfTeamVM.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/27/20.
//

import Foundation
import RxCocoa
import RxSwift
import RxDataSources

typealias TeamSection = SectionModel<String,XLTTCustomer>

class XLTTOfTeamVM: XLTTViewModelProtocal {
    typealias Dependency = XLTTCustomerService
    
    struct Input {
        let isLoading                   :BehaviorRelay<Bool>
        let triggerLoad                 :BehaviorRelay<Bool>
        let triggerLoadMore             :BehaviorRelay<Bool>
        let search                      :BehaviorRelay<String>
        let index                       :BehaviorRelay<Int>
        let pageSize                    :BehaviorRelay<Int>
        let data                        :BehaviorRelay<[XLTTCustomer]>
        let triggerAccept               :BehaviorRelay<Bool>
        let triggerDeny                 :BehaviorRelay<Bool>
        let itemTrigger                 :BehaviorRelay<XLTTCustomer>
        let triggerFilter               :BehaviorRelay<Bool>
        let filter                      :BehaviorRelay<XLTTFilterModel>
        let isFilter                    :BehaviorRelay<Bool>
        let triggerFilterLoadMore       :BehaviorRelay<Bool>
    }
    
    struct Output {
        let dataResult                  :Observable<XLTTResponseModel<XLTTResponseList<XLTTCustomer>>>
        let dataMoreResult              :Observable<XLTTResponseModel<XLTTResponseList<XLTTCustomer>>>
        let acceptResult                :Observable<XLTTResponseModel<Any>>
        let denyResult                  :Observable<XLTTResponseModel<Any>>
        let filterResult                :Observable<XLTTResponseModel<XLTTResponseList<XLTTCustomer>>>
        let filterdataMoreResult        :Observable<XLTTResponseModel<XLTTResponseList<XLTTCustomer>>>
        let dataSections                :Driver<[MeSection]>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input: Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: Dependency?) -> Output? {
        self.input = input
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else { return nil }
        
        let dataResult = ip.triggerLoad.asObservable()
            .filter {$0}
            .withLatestFrom(ip.search)
            .do { () in
                ip.isLoading.accept(true)
                ip.index.accept(1)
        }.flatMapLatest { search -> Observable<XLTTResponseModel<XLTTResponseList<XLTTCustomer>>> in
            return dp.getLeadCustomer(search: search, type: XLTTPotenitalcus.GROUP.rawValue, index: ip.index.value, size: ip.pageSize.value)
        }.do { () in
            ip.isLoading.accept(false)
        }
        
        let moreResult = ip.triggerLoadMore.asObservable()
            .filter {$0}
            .withLatestFrom(ip.search)
            .do { () in
                ip.isLoading.accept(true)
        }.flatMapLatest { search -> Observable<XLTTResponseModel<XLTTResponseList<XLTTCustomer>>> in
            return dp.getLeadCustomer(search: search, type: XLTTPotenitalcus.GROUP.rawValue, index: ip.index.value + 1, size: ip.pageSize.value)
        }.do { () in
            ip.isLoading.accept(false)
        }
        
        let acceptResult = ip.triggerAccept.asObservable().filter {$0}.withLatestFrom(ip.itemTrigger)
            .do{ () in
                ip.isLoading.accept(true)
        }.flatMapLatest { data -> Observable<XLTTResponseModel<Any>> in
            return dp.customerAction(idCustomer: data.id, type: XLTTCustomerActionType.CONTACT.rawValue, note: nil)
        }.do { () in
            ip.isLoading.accept(false)
            ip.itemTrigger.accept(XLTTCustomer())
        }
        
        let denyResult = ip.triggerDeny.asObservable().filter {$0}.withLatestFrom(ip.itemTrigger)
            .do{ () in
                ip.isLoading.accept(true)
        }.flatMapLatest { data -> Observable<XLTTResponseModel<Any>> in
            return dp.customerDeny(id: data.id, type: XLTTPotenitalcus.GROUP.rawValue)
        }.do { () in
            ip.isLoading.accept(false)
            ip.itemTrigger.accept(XLTTCustomer())
        }
        
        let filterResult = ip.triggerFilter.asObservable().filter {$0}
            .withLatestFrom(ip.search)
            .do{ () in
                    ip.isLoading.accept(true)
            }.flatMapLatest { (search) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTCustomer>>> in
                var request = FilterRequest(type: XLTTPotenitalcus.GROUP.rawValue, pageIndex: 1, pageSize: ip.pageSize.value)
                let filter = ip.filter.value
                request.saleTeamId = filter.saleTeam?.id
                request.provinceId = filter.product?.id
                request.provinceId = filter.province?.id
                request.districtId = filter.district?.id
                request.wardId = filter.district?.id
                request.sort1 = filter.sort1
                request.sort2 = filter.sort2
                request.sort3 = filter.sort3
                if filter.status != nil && filter.status != -1 {
                   request.status = filter.status
                }
                request.search = search
                
                return dp.filterCustomer(request: request)
        }.do { () in
            ip.isLoading.accept(false)
        }
        
        let moreFilterResult = ip.triggerFilterLoadMore.asObservable()
            .filter {$0}
            .withLatestFrom(ip.search)
            .do { () in
                ip.isLoading.accept(true)
        }.flatMapLatest { search -> Observable<XLTTResponseModel<XLTTResponseList<XLTTCustomer>>> in
            var request = FilterRequest(type: XLTTPotenitalcus.GROUP.rawValue, pageIndex: ip.index.value + 1, pageSize: ip.pageSize.value)
            let filter = ip.filter.value
            request.saleTeamId = filter.saleTeam?.id
            request.provinceId = filter.product?.id
            request.provinceId = filter.province?.id
            request.districtId = filter.district?.id
            request.wardId = filter.district?.id
            request.sort1 = filter.sort1
            request.sort2 = filter.sort2
            request.sort3 = filter.sort3
            if filter.status != nil && filter.status != -1 {
               request.status = filter.status
            }
            request.search = search
            
            return dp.filterCustomer(request: request)
        }.do { () in
            ip.isLoading.accept(false)
        }
        
        let dataSections = ip.data.asDriver().map { (data) -> [MeSection] in
            return [MeSection(model: "CustomerOfTeam", items: data)]
        }
        
        return XLTTOfTeamVM.Output(dataResult: dataResult,
                                   dataMoreResult: moreResult,
                                   acceptResult: acceptResult,
                                   denyResult: denyResult,
                                   filterResult: filterResult,
                                   filterdataMoreResult: moreFilterResult,
                                   dataSections: dataSections)
    }
}
