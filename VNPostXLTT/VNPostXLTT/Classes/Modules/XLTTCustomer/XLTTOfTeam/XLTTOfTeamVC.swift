//
//  XLTTOfTeamVC.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/27/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

typealias XLTTOfTeamSource = RxTableViewSectionedReloadDataSource<TeamSection>

class XLTTOfTeamVC : XLTTBaseViewController {
    
    @IBOutlet weak var lbFilter: UILabel!
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var imgEmpty: UIImageView!
    @IBOutlet weak var lbEmpty: UILabel!
    
    var viewModel: XLTTOfTeamVM!
    var viewControllerParent : UIViewController!
    
    private var dataSource: XLTTOfTeamSource!
    private var refreshControl : UIRefreshControl!
    
    let stringBold = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor(red: 0, green: 0, blue: 0, alpha: 1)]
    let stringNomal = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor(red: 0, green: 0, blue: 0, alpha: 1)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupListView()
        setupViewModel()
        
        let attributedString1 = NSMutableAttributedString(string: "\(0) ", attributes:stringBold)
        let attributedString2 = NSMutableAttributedString(string: "xltt_me_customer".localized, attributes:stringNomal)
        
        attributedString1.append(attributedString2)
        self.lbFilter.attributedText = attributedString1
        lbEmpty.text = "xltt_title_empty".localized
    }
    
    @IBAction func onFitler(_ sender: Any) {
        let bunble = Bundle(identifier: XLTTConstants.bundleIdentifier)
        let filterVC = XLTTCustomerFilterVC(nibName: "XLTTCustomerFilterVC", bundle: bunble)
        filterVC.viewModel = XLTTCustomerFilterVM(
            input: XLTTCustomerFilterVM.Input(triggerTeam: BehaviorRelay<Bool>(value: true),
                                              itemTeam: BehaviorRelay<XLTTOptionsFilter>(value: XLTTOptionsFilter()),
                                              teamData: BehaviorRelay<[XLTTOptionsFilter]>(value: []),
                                              triggerLoadProduct: BehaviorRelay<Bool>(value: true),
                                              triggerLoadMoreProduct: BehaviorRelay<Bool>(value: false),
                                              itemProduct: BehaviorRelay<XLTTOptionsFilter>(value: XLTTOptionsFilter()),
                                              dataProduct: BehaviorRelay<[XLTTOptionsFilter]>(value: []),
                                              indexProduct: BehaviorRelay<Int>(value: 1),
                                              pageSize: BehaviorRelay<Int>(value: 20),
                                              searchProduct: BehaviorRelay<String>(value: ""),
                                              triggerProvinces: BehaviorRelay<Bool>(value: true),
                                              dataProvinces: BehaviorRelay<[XLTTOptionsFilter]>(value: []),
                                              dataSearchProvinces: BehaviorRelay<[XLTTOptionsFilter]>(value: []),
                                              searchProvince: BehaviorRelay<String>(value: ""),
                                              itemProvince: BehaviorRelay<XLTTOptionsFilter>(value: XLTTOptionsFilter()),
                                              triggerDistrict: BehaviorRelay<Bool>(value: false),
                                              dataDistricts: BehaviorRelay<[XLTTOptionsFilter]>(value: []),
                                              dataSearchDistricts: BehaviorRelay<[XLTTOptionsFilter]>(value: []),
                                              searchDistrict: BehaviorRelay<String>(value: ""),
                                              itemDistrict: BehaviorRelay<XLTTOptionsFilter>(value: XLTTOptionsFilter()),
                                              triggerWard: BehaviorRelay<Bool>(value: false),
                                              dataWards: BehaviorRelay<[XLTTOptionsFilter]>(value: []),
                                              dataSearchWards: BehaviorRelay<[XLTTOptionsFilter]>(value: []),
                                              searchWard: BehaviorRelay<String>(value: ""),
                                              itemWard: BehaviorRelay<XLTTOptionsFilter>(value: XLTTOptionsFilter()),
                                              statusOptions: BehaviorRelay<Int?>(value: nil),
                                              dateUpdate: BehaviorRelay<Int?>(value: nil),
                                              dateTrade: BehaviorRelay<Int?>(value: nil),
                                              timeEnd: BehaviorRelay<Int?>(value: nil),
                                              initData: BehaviorRelay<XLTTFilterModel>(value: self.viewModel.input?.filter.value ?? XLTTFilterModel()),
                                              triggerEmployee: BehaviorRelay<Bool>(value: false),
                                              triggerLoadMoreEmployee: BehaviorRelay<Bool>(value: false),
                                              dataEmployee: BehaviorRelay<[XLTTOptionsFilter]>(value: []),
                                              itemEmployee: BehaviorRelay<XLTTOptionsFilter>(value: XLTTOptionsFilter()),
                                              indexEmployee: BehaviorRelay<Int>(value: 1),
                                              pageSizeEmployee: BehaviorRelay<Int>(value: 20),
                                              searchEmployee: BehaviorRelay<String>(value: ""),
                                              dataStatusList: BehaviorRelay<[XLTTOptionsFilter]>(value: []),
                                              itemStatusList: BehaviorRelay<XLTTOptionsFilter>(value: XLTTOptionsFilter())),
            dependency: XLTTCommonService())
        filterVC.onFilter = { item in
            self.viewModel.input?.filter.accept(item)
            self.viewModel.input?.isFilter.accept(true)
            self.viewModel.input?.triggerFilter.accept(true)
        }
        self.viewControllerParent.navigationController?.pushViewController(filterVC, animated: true)
    }
}

extension XLTTOfTeamVC : XLTTViewControllerProtocal {
    func setupViewModel() {
        if let ip = self.viewModel.input {
            bindingInput(input: ip)
        }
        if let op = self.viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input: XLTTOfTeamVM.Input){
        let loadMore = self.tbView.rx.contentOffset.asDriver()
            .flatMap { [weak self] offset -> Driver<Bool> in
                guard let self = self else { return Driver.empty() }
                let index = self.viewModel.input?.index.value ?? 0
                let size = self.viewModel.input?.pageSize.value ?? 0
                let count = self.viewModel.input?.data.value.count ?? 0
                if ((index * size) - count) == 0 {
                    return isNearTheBottomEdge(contentOffset: offset, self.tbView) ? Driver.just(true) : Driver.empty()
                }
                return Driver.empty()
        }
        
        loadMore.drive(onNext: {[weak self] boolean in
            guard let self = self else { return }
            if self.viewModel.input?.isFilter.value == false {
                self.viewModel.input?.triggerLoadMore.accept(true)
            } else {
                self.viewModel.input?.triggerFilterLoadMore.accept(true)
            }
        }).disposed(by: disposeBag)
    }
    
    private func bindingOutput(output: XLTTOfTeamVM.Output){
        output.dataSections.drive(self.tbView.rx.items(dataSource: self.dataSource)).disposed(by: disposeBag)
        
        output.dataResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
            }
            if result.success {
                if result.data?.data.count ?? 0 > 0 {
                    self.imgEmpty.isHidden = true
                    self.lbEmpty.isHidden = true
                } else {
                    self.imgEmpty.isHidden = false
                    self.lbEmpty.isHidden = false
                }
                self.viewModel.input?.data.accept(result.data?.data ?? [])
                
                let stringBold = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor(red: 0, green: 0, blue: 0, alpha: 1)]
                let stringNomal = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)]
                
                let attributedString1 = NSMutableAttributedString(string: "\(result.data?.total ?? 0) ", attributes:stringBold)
                let attributedString2 = NSMutableAttributedString(string: "xltt_me_customer".localized, attributes:stringNomal)
                
                attributedString1.append(attributedString2)
                self.lbFilter.attributedText = attributedString1
                
            } else {
                print("ERROR")
                let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
                let dialogError = XLTTDialogError(nibName: "XLTTDialogError", bundle: bundle)
                dialogError.modalPresentationStyle = .overFullScreen
                dialogError.messageDialog = BehaviorRelay<String>(value: self.getMessageError(error: result.message))
                dialogError.onDoneActions = {
                    dialogError.dismiss(animated: true, completion: nil)
                }
                self.viewControllerParent?.parent?.present(dialogError, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
        
        output.dataMoreResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.success {
                var data = self.viewModel.input?.data.value ?? []
                data += result.data?.data ?? []
                
                self.viewModel.input?.data.accept(data)
                var currentPage = self.viewModel.input?.index.value ?? 0
                currentPage += 1
                self.viewModel.input?.index.accept(currentPage)
            } else {
                print("ERROR")
            }
        }).disposed(by: disposeBag)
        
        output.acceptResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.success {
                let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
                let dialogSuccess = XLTTDialogMessage(nibName: "XLTTDialogMessage", bundle: bundle)
                dialogSuccess.modalPresentationStyle = .overFullScreen
                dialogSuccess.titleDialog = BehaviorRelay<String>(value: "xltt_action_success".localized)
                dialogSuccess.messageDialog = BehaviorRelay<String>(value: "xltt_action_message_success".localized)
                dialogSuccess.status = BehaviorRelay<Bool>(value: true)
                dialogSuccess.onDoneActions = {
                    dialogSuccess.dismiss(animated: true, completion: nil)
                    self.viewModel.input?.triggerLoad.accept(true)
                }
                self.viewControllerParent?.parent?.present(dialogSuccess, animated: false, completion: nil)
            } else {
                let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
                let dialogError = XLTTDialogError(nibName: "XLTTDialogError", bundle: bundle)
                dialogError.modalPresentationStyle = .overFullScreen
                dialogError.messageDialog = BehaviorRelay<String>(value: self.getMessageError(error: result.message))
                dialogError.onDoneActions = {
                    dialogError.dismiss(animated: true, completion: nil)
                }
                self.viewControllerParent?.parent?.present(dialogError, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
        
        output.denyResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.success {
                let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
                let dialogSuccess = XLTTDialogMessage(nibName: "XLTTDialogMessage", bundle: bundle)
                dialogSuccess.modalPresentationStyle = .overFullScreen
                dialogSuccess.titleDialog = BehaviorRelay<String>(value: "xltt_action_success".localized)
                dialogSuccess.messageDialog = BehaviorRelay<String>(value: "xltt_action_message_success".localized)
                dialogSuccess.status = BehaviorRelay<Bool>(value: true)
                dialogSuccess.onDoneActions = {
                    dialogSuccess.dismiss(animated: true, completion: nil)
                    self.viewModel.input?.triggerLoad.accept(true)
                }
                self.viewControllerParent?.parent?.present(dialogSuccess, animated: false, completion: nil)
            } else {
                let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
                let dialogError = XLTTDialogError(nibName: "XLTTDialogError", bundle: bundle)
                dialogError.modalPresentationStyle = .overFullScreen
                dialogError.messageDialog = BehaviorRelay<String>(value: self.getMessageError(error: result.message))
                dialogError.onDoneActions = {
                    dialogError.dismiss(animated: true, completion: nil)
                }
                self.viewControllerParent?.parent?.present(dialogError, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
        
        output.filterResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
            }
            if result.success {
                if result.data?.data.count ?? 0 > 0 {
                    self.imgEmpty.isHidden = true
                    self.lbEmpty.isHidden = true
                } else {
                    self.imgEmpty.isHidden = false
                    self.lbEmpty.isHidden = false
                }
                self.viewModel.input?.data.accept(result.data?.data ?? [])
                
                let attributedString1 = NSMutableAttributedString(string: "\(result.data?.total ?? 0) ", attributes: self.stringBold)
                let attributedString2 = NSMutableAttributedString(string: "xltt_me_customer".localized, attributes: self.stringNomal)
                
                attributedString1.append(attributedString2)
                self.lbFilter.attributedText = attributedString1
                
            } else {
                print("ERROR")
                let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
                let dialogError = XLTTDialogError(nibName: "XLTTDialogError", bundle: bundle)
                dialogError.modalPresentationStyle = .overFullScreen
                dialogError.messageDialog = BehaviorRelay<String>(value: self.getMessageError(error: result.message))
                dialogError.onDoneActions = {
                    dialogError.dismiss(animated: true, completion: nil)
                }
                self.viewControllerParent?.parent?.present(dialogError, animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
        
        output.filterdataMoreResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.success {
                var data = self.viewModel.input?.data.value ?? []
                data += result.data?.data ?? []
                
                self.viewModel.input?.data.accept(data)
                var currentPage = self.viewModel.input?.index.value ?? 0
                currentPage += 1
                self.viewModel.input?.index.accept(currentPage)
            } else {
                print("ERROR")
            }
        }).disposed(by: disposeBag)
    }
    
    func setupListView() {
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = .clear
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        tbView.refreshControl = refreshControl
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        tbView.register(UINib(nibName: XLTTCustomerOfTeamCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: XLTTCustomerOfTeamCell.cellIdentifier)
        
        let ds = RxTableViewSectionedReloadDataSource<TeamSection>(configureCell: { dataSource, tableView, indexPath, data in
            
            let cell : XLTTCustomerOfTeamCell = tableView.dequeueReusableCell(withIdentifier: XLTTCustomerOfTeamCell.cellIdentifier, for: indexPath) as! XLTTCustomerOfTeamCell
            cell.data = data
            cell.isSale = XLTTManager.shared.userProfileType == XLTTProfileType.MEMBER.rawValue
            cell.onActionAccept = { item in
                let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
                let dialogAccept = XLTTDialogOptions(nibName: "XLTTDialogOptions", bundle: bundle)
                dialogAccept.modalPresentationStyle = .overFullScreen
                dialogAccept.titleDialog = BehaviorRelay<String>(value: "xltt_title_action".localized)
                dialogAccept.messageDialog = BehaviorRelay<String>(value: "xltt_title_handing".localized)
                dialogAccept.colorMessageDialog = BehaviorRelay<UIColor?>(value: UIColor(hex: "127BCA"))
                dialogAccept.onDoneActions = { _ in
                    dialogAccept.dismiss(animated: true, completion: nil)
                    self.viewModel.input?.itemTrigger.accept(item ?? XLTTCustomer())
                    self.viewModel.input?.triggerAccept.accept(true)
                }
                
                dialogAccept.onCancelAction = {
                    dialogAccept.dismiss(animated: true, completion: nil)
                }
                
                self.viewControllerParent?.parent?.present(dialogAccept, animated: false, completion: nil)
            }
            cell.onActionDeny = { item in
                let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
                let dialogDeny = XLTTDialogOptions(nibName: "XLTTDialogOptions", bundle: bundle)
                dialogDeny.modalPresentationStyle = .overFullScreen
                dialogDeny.titleDialog = BehaviorRelay<String>(value: "xltt_title_action".localized)
                dialogDeny.messageDialog = BehaviorRelay<String>(value: "xltt_home_chart_failure_group".localized)
                dialogDeny.colorMessageDialog = BehaviorRelay<UIColor?>(value: UIColor(hex: "F64744"))
                dialogDeny.onDoneActions = { _ in
                    dialogDeny.dismiss(animated: true, completion: nil)
                    self.viewModel.input?.itemTrigger.accept(item ?? XLTTCustomer())
                    self.viewModel.input?.triggerDeny.accept(true)
                }
                
                dialogDeny.onCancelAction = {
                    dialogDeny.dismiss(animated: true, completion: nil)
                }
                
                self.viewControllerParent?.parent?.present(dialogDeny, animated: false, completion: nil)
            }
            cell.onActionDistribution = { item in
                let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
                let assignDialog = XLTTAssignLeadVC(nibName: "XLTTAssignLeadVC", bundle: bundle)
                assignDialog.modalPresentationStyle = .overFullScreen
                assignDialog.viewModel = XLTTAssignLeadVM(
                    input: XLTTAssignLeadVM.Input(search: BehaviorRelay<String>(value: ""),
                                                  triggerLoad: BehaviorRelay<Bool>(value: true),
                                                  triggerLoadMore: BehaviorRelay<Bool>(value: false),
                                                  data: BehaviorRelay<[XLTTSaleInfo]>(value: []),
                                                  index: BehaviorRelay<Int>(value: 1),
                                                  size: BehaviorRelay<Int>(value: 20),
                                                  leadAssign: BehaviorRelay<XLTTCustomer>(value: item ?? XLTTCustomer()),
                                                  isLoading: BehaviorRelay<Bool>(value: false),
                                                  userSelect: BehaviorRelay<XLTTSaleInfo>(value: XLTTSaleInfo()),
                                                  triggerAssign: BehaviorRelay<Bool>(value: false)),
                    dependency: XLTTCustomerService())
                assignDialog.viewControllerParent = self.viewControllerParent
                assignDialog.onDone = {
                    self.viewModel.input?.triggerLoad.accept(true)
                }
                
                self.viewControllerParent?.parent?.present(assignDialog, animated: false, completion: nil)
            }
            
            cell.onClickItem = { item in
                let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
                let detailVC = XLTTCustomerDetailVC(nibName: "XLTTCustomerDetailVC", bundle: bundle)
                detailVC.viewModel = XLTTCustomerDetailVM(
                    input: XLTTCustomerDetailVM.Input(isLoading: BehaviorRelay<Bool>(value: false),
                                                      triggerDetail: BehaviorRelay<Bool>(value: true),
                                                      dataLead: BehaviorRelay<XLTTLeadDetailModel>(value: XLTTLeadDetailModel()),
                                                      initLead: BehaviorRelay<XLTTCustomer>(value: item ?? XLTTCustomer()),
                                                      type: BehaviorRelay<Int>(value: XLTTPotenitalcus.GROUP.rawValue),
                                                      triggerAccept: BehaviorRelay<Bool>(value: false),
                                                      triggerDeny: BehaviorRelay<Bool>(value: false),
                                                      triggerPin: BehaviorRelay<Bool>(value: false),
                                                      triggerSuccess: BehaviorRelay<Bool>(value: false),
                                                      noteSuccess: BehaviorRelay<String>(value: ""),
                                                      triggerFailure: BehaviorRelay<Bool>(value: false),
                                                      noteFailure: BehaviorRelay<String>(value: ""),
                                                      triggerAddNote: BehaviorRelay<Bool>(value: false),
                                                      note: BehaviorRelay<String>(value: ""),
                                                      isAction: BehaviorRelay<Bool>(value: true)),
                    dependency: XLTTCustomerService())
                self.viewControllerParent.navigationController?.pushViewController(detailVC, animated: true)
            }
            
            return cell
        })
        
        dataSource = ds
        tbView.allowsSelection = false
    }
    
    @objc private func refresh() {
        if self.viewModel.input?.isFilter.value ?? false {
            self.viewModel.input?.triggerFilter.accept(true)
        } else {
            self.viewModel.input?.triggerLoad.accept(true)
        }
    }
}
