//
//  XLTTCustomerFilterVC.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/28/20.
//

import Foundation
import RxCocoa
import RxSwift

class XLTTCustomerFilterVC: XLTTBaseViewController {
    
    var filterCustomer = true
    var isComplete = false
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbTitleListTeam: UILabel!
    @IBOutlet weak var lbTitleProduct: UILabel!
    @IBOutlet weak var lbTitleStatusOptions: UILabel!
    @IBOutlet weak var lbTitleLocation: UILabel!
    @IBOutlet weak var lbTitleSort: UILabel!
    @IBOutlet weak var lbTitleEmployee: UILabel!
    @IBOutlet weak var lbTitleStatusList: UILabel!
    @IBOutlet weak var viewEmployee: UIView!
    @IBOutlet weak var viewListTeam: UIView!
    @IBOutlet weak var viewProduct: UIView!
    @IBOutlet weak var viewStatusList: UIView!
    @IBOutlet weak var viewStatusOptions: UIView!
    @IBOutlet weak var lbTitleProvince: UILabel!
    @IBOutlet weak var lbTitleDistrict: UILabel!
    @IBOutlet weak var lbTitleWard: UILabel!
    @IBOutlet weak var lbTitleDateUpdate: UILabel!
    @IBOutlet weak var lbTitleDateTrade: UILabel!
    @IBOutlet weak var lbTitleTimeEnd: UILabel!
    
    @IBOutlet weak var lbValueListTeam: UILabel!
    @IBOutlet weak var lbValueProduct: UILabel!
    @IBOutlet weak var lbValueProvince: UILabel!
    @IBOutlet weak var lbValueDistrict: UILabel!
    @IBOutlet weak var lbValueWard: UILabel!
    @IBOutlet weak var lbValueEmployee: UILabel!
    @IBOutlet weak var lbValueStatusList: UILabel!
    
    @IBOutlet weak var viewTimeEnd: UIView!
    @IBOutlet weak var viewDateTrade: UIView!
    @IBOutlet weak var viewDateUpdate: UIView!
    @IBOutlet weak var viewStatusOptionsVC: UIView!
    
    @IBOutlet weak var bgHeaderView: UIView!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    
    @IBOutlet weak var viewListTeamAction: UIView!
    @IBOutlet weak var viewProductAction: UIView!
    @IBOutlet weak var viewProvinceAction: UIView!
    @IBOutlet weak var viewDistrictAction: UIView!
    @IBOutlet weak var viewWardAction: UIView!
    @IBOutlet weak var viewEmployeeAction: UIView!
    @IBOutlet weak var viewStatusListAction: UIView!
    
    private var optionsStatus = XLTTOptionsStatus()
    private var optionsDateUpdate = XLTTOptionsStatus()
    private var optionsDateTrade = XLTTOptionsStatus()
    private var optionsTimeEnd = XLTTOptionsStatus()
    
    var viewModel : XLTTCustomerFilterVM!
    
    private var dialogProduct : XLTTDialogSearchList!
    private var dialogEmployee : XLTTDialogSearchList!
    
    var onFilter : ((XLTTFilterModel)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        handleOptions()
        setupViewModel()
        handleAction()
        handleInitData()
        handleUpdateUI()
    }
    
    private func handleInitData(){
        let initData = self.viewModel.input?.initData.value
        if initData?.saleTeam != nil {
            self.viewModel.input?.itemTeam.accept(initData?.saleTeam ?? XLTTOptionsFilter())
        }
        if initData?.product != nil {
            self.viewModel.input?.itemProduct.accept(initData?.product ?? XLTTOptionsFilter())
        }
        if initData?.province != nil {
            self.viewModel.input?.itemProvince.accept(initData?.province ?? XLTTOptionsFilter())
        }
        if initData?.district != nil {
            self.viewModel.input?.itemDistrict.accept(initData?.district ?? XLTTOptionsFilter())
        }
        if initData?.ward != nil {
            self.viewModel.input?.itemWard.accept(initData?.ward ?? XLTTOptionsFilter())
        }
        if initData?.sort1 != nil {
            self.viewModel.input?.dateUpdate.accept(initData?.sort1)
            self.optionsDateUpdate.setCurrentPosition(position: initData?.sort1 ?? -1)
        }
        if initData?.sort2 != nil {
            self.viewModel.input?.dateTrade.accept(initData?.sort2)
            self.optionsDateTrade.setCurrentPosition(position: initData?.sort2 ?? -1)
        }
        if initData?.sort3 != nil {
            self.viewModel.input?.timeEnd.accept(initData?.sort3)
            self.optionsTimeEnd.setCurrentPosition(position: initData?.sort3 ?? -1)
        }
        if initData?.user != nil {
            self.viewModel.input?.itemEmployee.accept(initData?.user ?? XLTTOptionsFilter())
        }
        var dataStatusList : [XLTTOptionsFilter] = []
        if filterCustomer == false {
            dataStatusList.append(XLTTOptionsFilter(id: "0", name: "xltt_home_chart_all".localized))
            if isComplete == false {
                dataStatusList.append(XLTTOptionsFilter(id: "1", name: "xltt_home_chart_progressing".localized))
            }
            dataStatusList.append(XLTTOptionsFilter(id: "2", name: "xltt_home_chart_success".localized))
            dataStatusList.append(XLTTOptionsFilter(id: "3", name: "xltt_home_chart_failure".localized))
            dataStatusList.append(XLTTOptionsFilter(id: "4", name: "xltt_title_status_expired".localized))
            dataStatusList.append(XLTTOptionsFilter(id: "5", name: "xltt_home_chart_failure_group".localized))
            
            self.viewModel.input?.dataStatusList.accept(dataStatusList)
        }
        
        if initData?.status != nil {
            if filterCustomer {
                let value = (initData?.status ?? 0) + 1
                self.viewModel.input?.statusOptions.accept(value)
                self.optionsStatus.setCurrentPosition(position: value)
            } else {
                if (initData?.status ?? 0) == -1 {
                    self.viewModel.input?.itemStatusList.accept(dataStatusList[0])
                } else {
                    let itemStatus = dataStatusList.filter { (item) -> Bool in
                        item.id == "\(initData?.status ?? 0)"
                    }
                    self.viewModel.input?.itemStatusList.accept(itemStatus.first ?? XLTTOptionsFilter())
                }
            }
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onReset(_ sender: Any) {
        self.viewModel.input?.itemTeam.accept(XLTTOptionsFilter())
        self.viewModel.input?.itemProduct.accept(XLTTOptionsFilter())
        self.optionsStatus.setCurrentPosition(position: -1)
        self.viewModel.input?.statusOptions.accept(nil)
        self.viewModel.input?.itemProvince.accept(XLTTOptionsFilter())
        self.viewModel.input?.itemDistrict.accept(XLTTOptionsFilter())
        self.viewModel.input?.itemWard.accept(XLTTOptionsFilter())
        self.viewModel.input?.itemEmployee.accept(XLTTOptionsFilter())
        self.viewModel.input?.itemStatusList.accept(XLTTOptionsFilter())
        self.optionsDateUpdate.setCurrentPosition(position: -1)
        self.viewModel.input?.dateUpdate.accept(nil)
        self.optionsDateTrade.setCurrentPosition(position: -1)
        self.viewModel.input?.dateTrade.accept(nil)
        self.optionsTimeEnd.setCurrentPosition(position: -1)
        self.viewModel.input?.timeEnd.accept(nil)
    }
    
    @IBAction func onFilter(_ sender: Any) {
        let filter = XLTTFilterModel()
        filter.saleTeam = self.viewModel.input?.itemTeam.value.id != "" ? self.viewModel.input?.itemTeam.value : nil
        filter.product = self.viewModel.input?.itemProduct.value.id != "" ? self.viewModel.input?.itemProduct.value : nil
        filter.province = self.viewModel.input?.itemProvince.value.id != "" ? self.viewModel.input?.itemProvince.value : nil
        filter.district = self.viewModel.input?.itemDistrict.value.id != "" ? self.viewModel.input?.itemDistrict.value : nil
        filter.ward = self.viewModel.input?.itemWard.value.id != "" ? self.viewModel.input?.itemWard.value : nil
        filter.sort1 = self.viewModel.input?.dateUpdate.value
        filter.sort2 = self.viewModel.input?.dateTrade.value
        filter.sort3 = self.viewModel.input?.timeEnd.value
//        filter.user = self.viewModel.input?.itemStatusList.value.id != "" ? self.viewModel.input?.itemStatusList.value : nil
        if self.viewModel.input?.statusOptions.value != nil {
            let value = (self.viewModel.input?.statusOptions.value ?? 0) - 1
//            if value != -1 {
//               filter.status = value
//            }
            filter.status = value
        }
        filter.user = self.viewModel.input?.itemEmployee.value.id != "" ? self.viewModel.input?.itemEmployee.value : nil
        if self.viewModel.input?.itemStatusList.value.id != "" {
            var valueStatus = "0"
            if (self.viewModel.input?.itemStatusList.value.id ?? "0") == "0" {
                valueStatus = "-1"
            } else {
                valueStatus = self.viewModel.input?.itemStatusList.value.id ?? "0"
            }
            filter.status = Int(valueStatus)
        }
        self.onFilter?(filter)
        self.navigationController?.popViewController(animated: true)
    }
    
    private func handleUpdateUI(){
        self.viewModel.input?.itemTeam.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.id != "" {
                self.lbValueListTeam.text = result.name
                self.lbValueListTeam.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
            } else {
                self.lbValueListTeam.text = "xltt_list_team_placeholder".localized
                self.lbValueListTeam.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.32)
            }
        })
        
        self.viewModel.input?.itemProduct.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.id != "" {
                self.lbValueProduct.text = result.name
                self.lbValueProduct.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
            } else {
                self.lbValueProduct.text = "xltt_filter_product_placeholder".localized
                self.lbValueProduct.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.32)
            }
        })
        
        self.viewModel.input?.itemProvince.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.id != "" {
                self.lbValueProvince.text = result.name
                self.lbValueProvince.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
            } else {
                self.lbValueProvince.text = "xltt_filter_province_placeholder".localized
                self.lbValueProvince.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.32)
            }
        })
        
        self.viewModel.input?.itemDistrict.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.id != "" {
                self.lbValueDistrict.text = result.name
                self.lbValueDistrict.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
            } else {
                self.lbValueDistrict.text = "xltt_filter_district_placeholder".localized
                self.lbValueDistrict.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.32)
            }
        })
        
        self.viewModel.input?.itemWard.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.id != "" {
                self.lbValueWard.text = result.name
                self.lbValueWard.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
            } else {
                self.lbValueWard.text = "xltt_filter_ward_placeholder".localized
                self.lbValueWard.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.32)
            }
        })
        
        self.viewModel.input?.itemEmployee.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.id != "" {
                self.lbValueEmployee.text = result.fullName
                self.lbValueEmployee.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
            } else {
                self.lbValueEmployee.text = "xltt_title_employee_placeholder".localized
                self.lbValueEmployee.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.32)
            }
        })
        
        self.viewModel.input?.itemStatusList.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.id != "" {
                self.lbValueStatusList.text = result.name
                self.lbValueStatusList.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
            } else {
                self.lbValueStatusList.text = "xltt_title_status_list_placeholder".localized
                self.lbValueStatusList.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.32)
            }
        })
    }
    
    private func handleOptions(){
        if filterCustomer {
            
            //options status
            self.viewStatusOptionsVC.addSubview(optionsStatus.view)
            self.optionsStatus.view.frame = self.viewStatusOptionsVC.bounds
            optionsStatus.addItem(title: "xltt_title_all".localized)
            optionsStatus.addItem(title: "xltt_title_new".localized)
            optionsStatus.addItem(title: "xltt_title_in_process".localized)
            optionsStatus.setStyle(style: .fixed)
            optionsStatus.build()
            optionsStatus.onChangeValue = { index in
                print("STATUS \(index)")
                if index ?? 0 < 0 {
                    self.viewModel.input?.statusOptions.accept(nil)
                } else {
                    self.viewModel.input?.statusOptions.accept(index)
                }
            }
        }
        
        //options sort
        //date update
        self.viewDateUpdate.addSubview(optionsDateUpdate.view)
        self.optionsDateUpdate.view.frame = self.viewDateUpdate.bounds
        optionsDateUpdate.addItem(title: "xltt_title_last".localized)
        optionsDateUpdate.addItem(title: "xltt_title_old".localized)
        optionsDateUpdate.setStyle(style: .fixed)
        optionsDateUpdate.build()
        optionsDateUpdate.onChangeValue = { index in
            print("Date Update \(index)")
            if index ?? 0 < 0 {
                self.viewModel.input?.dateUpdate.accept(nil)
            } else {
                self.viewModel.input?.dateUpdate.accept(index)
            }
        }
        
        //date trade
        self.viewDateTrade.addSubview(optionsDateTrade.view)
        self.optionsDateTrade.view.frame = self.viewDateTrade.bounds
        optionsDateTrade.addItem(title: "xltt_title_last".localized)
        optionsDateTrade.addItem(title: "xltt_title_old".localized)
        optionsDateTrade.setStyle(style: .fixed)
        optionsDateTrade.build()
        optionsDateTrade.onChangeValue = { index in
            print("Date Trade \(index)")
            if index ?? 0 < 0 {
                self.viewModel.input?.dateTrade.accept(nil)
            } else {
                self.viewModel.input?.dateTrade.accept(index)
            }
        }
        
        //time end
        self.viewTimeEnd.addSubview(optionsTimeEnd.view)
        self.optionsTimeEnd.view.frame = self.viewTimeEnd.bounds
        optionsTimeEnd.addItem(title: "xltt_title_high_short".localized)
        optionsTimeEnd.addItem(title: "xltt_title_short_high".localized)
        optionsTimeEnd.setStyle(style: .fixed)
        optionsTimeEnd.build()
        optionsTimeEnd.onChangeValue = { index in
            print("Time End \(index)")
            if index ?? 0 < 0 {
                self.viewModel.input?.timeEnd.accept(nil)
            } else {
                self.viewModel.input?.timeEnd.accept(index)
            }
        }
        
    }
    
    private func handleAction(){
        let tapTeam = UITapGestureRecognizer(target: self, action:  #selector(onClickTeamSale))
        viewListTeamAction.addGestureRecognizer(tapTeam)
        
        let tapProduct = UITapGestureRecognizer(target: self, action:  #selector(onClickProduct))
        viewProductAction.addGestureRecognizer(tapProduct)
        
        let tapProvinces = UITapGestureRecognizer(target: self, action:  #selector(onClickProvinces))
        viewProvinceAction.addGestureRecognizer(tapProvinces)
        
        let tapDistricts = UITapGestureRecognizer(target: self, action:  #selector(onClickDistricts))
        viewDistrictAction.addGestureRecognizer(tapDistricts)
        
        let tapWards = UITapGestureRecognizer(target: self, action:  #selector(onClickWards))
        viewWardAction.addGestureRecognizer(tapWards)
        
        let tapEmployee = UITapGestureRecognizer(target: self, action:  #selector(onClickEmployee))
        viewEmployeeAction.addGestureRecognizer(tapEmployee)
        
        let tapStatusList = UITapGestureRecognizer(target: self, action:  #selector(onClickStatusList))
        viewStatusListAction.addGestureRecognizer(tapStatusList)
    }
    
    @objc func onClickTeamSale(sender : Any){
        print("TAP TEAM")
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        let dialogPick = XLTTDialogSearchList(nibName: "XLTTDialogSearchList", bundle: bundle)
        dialogPick.modalPresentationStyle = .overFullScreen
        dialogPick.viewModel = XLTTDialogSearchListVM(
            input: XLTTDialogSearchListVM.Input(dataDialog: BehaviorRelay<[XLTTOptionsFilter]>(value: self.viewModel.input?.teamData.value ?? []),
                                                isSearch: BehaviorRelay<Bool>(value: false),
                                                titleDialog: BehaviorRelay<String>(value: "xltt_list_team_placeholder".localized.uppercased()),
                                                initValue: BehaviorRelay<XLTTOptionsFilter>(value: self.viewModel.input?.itemTeam.value ?? XLTTOptionsFilter()),
                                                isSingle: BehaviorRelay<Bool>(value: true),
                                                isEmployee: BehaviorRelay<Bool>(value: false)),
            dependency: XLTTCommonService())
        dialogPick.dataPickedAction = { item in
            self.viewModel.input?.itemTeam.accept(item ?? XLTTOptionsFilter())
            dialogPick.dismiss(animated: true, completion: nil)
            if self.filterCustomer == false {
                self.viewModel.input?.itemEmployee.accept(XLTTOptionsFilter())
                self.viewModel.input?.triggerEmployee.accept(true)
            }
        }
        self.parent?.present(dialogPick, animated: false, completion: nil)
    }
    
    @objc func onClickProduct(sender : Any){
        print("TAP PRODUCT")
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        dialogProduct = XLTTDialogSearchList(nibName: "XLTTDialogSearchList", bundle: bundle)
        dialogProduct.modalPresentationStyle = .overFullScreen
        dialogProduct.placeHolderText = "xltt_placeholder_search_product".localized
        dialogProduct.viewModel = XLTTDialogSearchListVM(
            input: XLTTDialogSearchListVM.Input(dataDialog: BehaviorRelay<[XLTTOptionsFilter]>(value: self.viewModel.input?.dataProduct.value ?? []),
                                                isSearch: BehaviorRelay<Bool>(value: true),
                                                titleDialog: BehaviorRelay<String>(value: "xltt_filter_product_title".localized.uppercased()),
                                                initValue: BehaviorRelay<XLTTOptionsFilter>(value: self.viewModel.input?.itemProduct.value ?? XLTTOptionsFilter()),
                                                isSingle: BehaviorRelay<Bool>(value: false),
                                                isEmployee: BehaviorRelay<Bool>(value: false)),
            dependency: XLTTCommonService())
        dialogProduct.dataPickedAction = { item in
            self.viewModel.input?.itemProduct.accept(item ?? XLTTOptionsFilter())
            self.dialogProduct.dismiss(animated: false, completion: nil)
        }
        dialogProduct.onLoadMore = {
            let index = self.viewModel.input?.indexProduct.value ?? 0
            let size = self.viewModel.input?.pageSize.value ?? 0
            let count = self.viewModel.input?.dataProduct.value.count ?? 0
            if ((index * size) - count) == 0 {
                self.viewModel.input?.triggerLoadMoreProduct.accept(true)
            }
        }
        dialogProduct.onSearch = { search in
            self.viewModel.input?.searchProduct.accept(search ?? "")
        }
        self.parent?.present(dialogProduct, animated: false, completion: nil)
    }
    
    @objc func onClickProvinces(sender : Any){
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        let dialogProvinces = XLTTDialogSearchList(nibName: "XLTTDialogSearchList", bundle: bundle)
        dialogProvinces.modalPresentationStyle = .overFullScreen
        dialogProvinces.placeHolderText = "xltt_placeholder_search_province".localized
        dialogProvinces.viewModel = XLTTDialogSearchListVM(
            input: XLTTDialogSearchListVM.Input(dataDialog: BehaviorRelay<[XLTTOptionsFilter]>(value: self.viewModel.input?.dataSearchProvinces.value ?? []),
                                                isSearch: BehaviorRelay<Bool>(value: true),
                                                titleDialog: BehaviorRelay<String>(value: "xltt_select_provinces_title".localized.uppercased()),
                                                initValue: BehaviorRelay<XLTTOptionsFilter>(value: self.viewModel.input?.itemProvince.value ?? XLTTOptionsFilter()),
                                                isSingle: BehaviorRelay<Bool>(value: true),
                                                isEmployee: BehaviorRelay<Bool>(value: false)),
            dependency: XLTTCommonService())
        dialogProvinces.dataPickedAction = { item in
            self.viewModel.input?.itemProvince.accept(item ?? XLTTOptionsFilter())
            dialogProvinces.dismiss(animated: false, completion: nil)
            //clear data for district and ward
            self.viewModel.input?.triggerDistrict.accept(true)
            self.viewModel.input?.itemDistrict.accept(XLTTOptionsFilter())
            self.viewModel.input?.triggerWard.accept(true)
            self.viewModel.input?.itemWard.accept(XLTTOptionsFilter())
        }
        dialogProvinces.onSearch = { search in
            if search != nil && search != "" {
                let array : [XLTTOptionsFilter] = self.viewModel.input?.dataProvinces.value.filter {
                    $0.name.uppercased().contains(search!.uppercased())
                    } ?? []
                self.viewModel.input?.dataSearchProvinces.accept(array)
                dialogProvinces.viewModel.input?.dataDialog.accept(array)
            } else {
                self.viewModel.input?.dataSearchProvinces.accept(self.viewModel.input?.dataProvinces.value ?? [])
                dialogProvinces.viewModel.input?.dataDialog.accept(self.viewModel.input?.dataProvinces.value ?? [])
            }
        }
        self.parent?.present(dialogProvinces, animated: false, completion: nil)
    }
    
    @objc func onClickDistricts(sender : Any){
        if self.viewModel.input?.itemProvince.value.id != "" {
            let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
            let dialogDistricts = XLTTDialogSearchList(nibName: "XLTTDialogSearchList", bundle: bundle)
            dialogDistricts.modalPresentationStyle = .overFullScreen
            dialogDistricts.placeHolderText = "xltt_placeholder_search_district".localized
            dialogDistricts.viewModel = XLTTDialogSearchListVM(
                input: XLTTDialogSearchListVM.Input(dataDialog: BehaviorRelay<[XLTTOptionsFilter]>(value: self.viewModel.input?.dataSearchDistricts.value ?? []),
                                                    isSearch: BehaviorRelay<Bool>(value: true),
                                                    titleDialog: BehaviorRelay<String>(value: "xltt_select_districts_title".localized.uppercased()),
                                                    initValue: BehaviorRelay<XLTTOptionsFilter>(value: self.viewModel.input?.itemDistrict.value ?? XLTTOptionsFilter()),
                                                    isSingle: BehaviorRelay<Bool>(value: true),
                                                    isEmployee: BehaviorRelay<Bool>(value: false)),
                dependency: XLTTCommonService())
            dialogDistricts.dataPickedAction = { item in
                self.viewModel.input?.itemDistrict.accept(item ?? XLTTOptionsFilter())
                dialogDistricts.dismiss(animated: false, completion: nil)
                //clear data for ward
                self.viewModel.input?.triggerWard.accept(true)
                self.viewModel.input?.itemWard.accept(XLTTOptionsFilter())
            }
            dialogDistricts.onSearch = { search in
                if search != nil && search != "" {
                    let array : [XLTTOptionsFilter] = self.viewModel.input?.dataDistricts.value.filter {
                        $0.name.uppercased().contains(search!.uppercased())
                        } ?? []
                    self.viewModel.input?.dataSearchDistricts.accept(array)
                    dialogDistricts.viewModel.input?.dataDialog.accept(array)
                } else {
                    self.viewModel.input?.dataSearchDistricts.accept(self.viewModel.input?.dataDistricts.value ?? [])
                    dialogDistricts.viewModel.input?.dataDialog.accept(self.viewModel.input?.dataDistricts.value ?? [])
                }
            }
            self.parent?.present(dialogDistricts, animated: false, completion: nil)
        } else {
            XLTTMessageManager.init().showMessage(messageType: .warning, message: "xltt_error_province_empty".localized)
        }
    }
    
    @objc func onClickWards(sender : Any){
        if self.viewModel.input?.itemDistrict.value.id != "" {
            let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
            let dialogWards = XLTTDialogSearchList(nibName: "XLTTDialogSearchList", bundle: bundle)
            dialogWards.modalPresentationStyle = .overFullScreen
            dialogWards.placeHolderText = "xltt_placeholder_search_ward".localized
            dialogWards.viewModel = XLTTDialogSearchListVM(
                input: XLTTDialogSearchListVM.Input(dataDialog: BehaviorRelay<[XLTTOptionsFilter]>(value: self.viewModel.input?.dataSearchWards.value ?? []),
                                                    isSearch: BehaviorRelay<Bool>(value: true),
                                                    titleDialog: BehaviorRelay<String>(value: "xltt_select_wards_title".localized.uppercased()),
                                                    initValue: BehaviorRelay<XLTTOptionsFilter>(value: self.viewModel.input?.itemWard.value ?? XLTTOptionsFilter()),
                                                    isSingle: BehaviorRelay<Bool>(value: true),
                                                    isEmployee: BehaviorRelay<Bool>(value: false)),
                dependency: XLTTCommonService())
            dialogWards.dataPickedAction = { item in
                self.viewModel.input?.itemWard.accept(item ?? XLTTOptionsFilter())
                dialogWards.dismiss(animated: false, completion: nil)
            }
            dialogWards.onSearch = { search in
                if search != nil && search != "" {
                    let array : [XLTTOptionsFilter] = self.viewModel.input?.dataWards.value.filter {
                        $0.name.uppercased().contains(search!.uppercased())
                        } ?? []
                    self.viewModel.input?.dataSearchWards.accept(array)
                    dialogWards.viewModel.input?.dataDialog.accept(array)
                } else {
                    self.viewModel.input?.dataSearchWards.accept(self.viewModel.input?.dataWards.value ?? [])
                    dialogWards.viewModel.input?.dataDialog.accept(self.viewModel.input?.dataWards.value ?? [])
                }
            }
            self.parent?.present(dialogWards, animated: false, completion: nil)
        } else {
            XLTTMessageManager.init().showMessage(messageType: .warning, message: "xltt_error_district_empty".localized)
        }
    }
    
    @objc func onClickEmployee(sender : Any){
        if self.viewModel.input?.itemTeam.value.id != "" {
            let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
            dialogEmployee = XLTTDialogSearchList(nibName: "XLTTDialogSearchList", bundle: bundle)
            dialogEmployee.modalPresentationStyle = .overFullScreen
            dialogEmployee.viewModel = XLTTDialogSearchListVM(
                input: XLTTDialogSearchListVM.Input(dataDialog: BehaviorRelay<[XLTTOptionsFilter]>(value: self.viewModel.input?.dataEmployee.value ?? []),
                                                    isSearch: BehaviorRelay<Bool>(value: true),
                                                    titleDialog: BehaviorRelay<String>(value: "xltt_filter_product_title".localized.uppercased()),
                                                    initValue: BehaviorRelay<XLTTOptionsFilter>(value: self.viewModel.input?.itemEmployee.value ?? XLTTOptionsFilter()),
                                                    isSingle: BehaviorRelay<Bool>(value: false),
                                                    isEmployee: BehaviorRelay<Bool>(value: true)),
                dependency: XLTTCommonService())
            dialogEmployee.dataPickedAction = { item in
                self.viewModel.input?.itemEmployee.accept(item ?? XLTTOptionsFilter())
                self.dialogEmployee.dismiss(animated: false, completion: nil)
            }
            dialogEmployee.onLoadMore = {
                let index = self.viewModel.input?.indexEmployee.value ?? 0
                let size = self.viewModel.input?.pageSizeEmployee.value ?? 0
                let count = self.viewModel.input?.dataEmployee.value.count ?? 0
                if ((index * size) - count) == 0 {
                    self.viewModel.input?.triggerLoadMoreEmployee.accept(true)
                }
            }
            dialogEmployee.onSearch = { search in
                if search != "" {
                    self.viewModel.input?.searchEmployee.accept(search ?? "")
                }
            }
            self.parent?.present(dialogEmployee, animated: false, completion: nil)
        } else {
            XLTTMessageManager.init().showMessage(messageType: .warning, message: "xltt_error_team_empty".localized)
        }
    }
    
    @objc func onClickStatusList(sender : Any){
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        let dialogStatusList = XLTTDialogSearchList(nibName: "XLTTDialogSearchList", bundle: bundle)
        dialogStatusList.modalPresentationStyle = .overFullScreen
        dialogStatusList.viewModel = XLTTDialogSearchListVM(
            input: XLTTDialogSearchListVM.Input(dataDialog: BehaviorRelay<[XLTTOptionsFilter]>(value: self.viewModel.input?.dataStatusList.value ?? []),
                                                isSearch: BehaviorRelay<Bool>(value: false),
                                                titleDialog: BehaviorRelay<String>(value: "xltt_filter_product_title".localized.uppercased()),
                                                initValue: BehaviorRelay<XLTTOptionsFilter>(value: self.viewModel.input?.itemStatusList.value ?? XLTTOptionsFilter()),
                                                isSingle: BehaviorRelay<Bool>(value: true),
                                                isEmployee: BehaviorRelay<Bool>(value: false)),
            dependency: XLTTCommonService())
        dialogStatusList.dataPickedAction = { item in
            self.viewModel.input?.itemStatusList.accept(item ?? XLTTOptionsFilter())
            dialogStatusList.dismiss(animated: false, completion: nil)
        }
        self.parent?.present(dialogStatusList, animated: false, completion: nil)
    }
    
    private func initUI(){
        
        bgHeaderView.XLTTSetGradientOrangeBackground()
        btnFilter.xlttSetButtonBlueGradient()
        //title
        lbTitle.text = "xltt_home_customer_label".localized.uppercased()
        lbTitleListTeam.text = "xltt_list_team".localized.uppercased()
        lbTitleProduct.text = "xltt_filter_product".localized.uppercased()
        lbTitleStatusOptions.text = "xltt_filter_status".localized.uppercased()
        lbTitleStatusList.text = "xltt_filter_status".localized.uppercased()
        lbTitleLocation.text = "xltt_filter_location_title".localized.uppercased()
        lbTitleSort.text = "xltt_filter_sort".localized.uppercased()
        lbTitleEmployee.text = "xltt_filter_employee".localized.uppercased()
        lbTitleProvince.text = "xltt_filter_province".localized
        lbTitleDistrict.text = "xltt_filter_district".localized
        lbTitleWard.text = "xltt_filter_ward".localized
        lbTitleDateUpdate.text = "xltt_filter_date_update".localized
        lbTitleDateTrade.text = "xltt_filter_date_trade".localized
        lbTitleTimeEnd.text = "xltt_filter_time_end".localized
        btnReset.setTitle("xltt_filter_reset".localized.uppercased(), for: .normal)
        btnFilter.setTitle("xltt_filter".localized.uppercased(), for: .normal)
        //value
        lbValueListTeam.text = "xltt_list_team_placeholder".localized
        lbValueProduct.text = "xltt_filter_product_placeholder".localized
        lbValueProvince.text = "xltt_filter_province_placeholder".localized
        lbValueDistrict.text = "xltt_filter_district_placeholder".localized
        lbValueWard.text = "xltt_filter_ward_placeholder".localized
        lbValueEmployee.text = "xltt_title_employee_placeholder".localized
        lbValueStatusList.text = "xltt_title_status_list_placeholder".localized
        
        if filterCustomer {
            lbTitleEmployee.isHidden = true
            viewEmployee.isHidden = true
            lbTitleStatusList.isHidden = true
            viewStatusList.isHidden = true
            
            lbTitleStatusOptions.isHidden = false
            viewStatusOptions.isHidden = false
            
            lbTitleProduct.topAnchor.constraint(equalTo: viewListTeam.bottomAnchor, constant: 20).isActive = true
            lbTitleStatusOptions.topAnchor.constraint(equalTo: viewProduct.bottomAnchor, constant: 20).isActive = true
            
        } else {
//            lbTitleEmployee.isHidden = false
//            viewEmployee.isHidden = false
            lbTitleStatusList.isHidden = false
            viewStatusList.isHidden = false
            
            lbTitleStatusOptions.isHidden = true
            viewStatusOptions.isHidden = true
            
            if XLTTManager.shared.userProfileType == XLTTProfileType.MEMBER.rawValue {
                viewListTeam.isHidden = false
                lbTitleListTeam.isHidden = false
                viewEmployee.isHidden = true
                lbTitleEmployee.isHidden = true
                lbTitleProduct.topAnchor.constraint(equalTo: viewListTeam.bottomAnchor, constant: 20).isActive = true
                lbTitleLocation.topAnchor.constraint(equalTo: viewStatusList.bottomAnchor, constant: 15).isActive = true
            } else {
                viewListTeam.isHidden = false
                lbTitleListTeam.isHidden = false
                viewEmployee.isHidden = false
                lbTitleEmployee.isHidden = false
                lbTitleProduct.topAnchor.constraint(equalTo: viewEmployee.bottomAnchor, constant: 20).isActive = true
                lbTitleLocation.topAnchor.constraint(equalTo: viewStatusList.bottomAnchor, constant: 15).isActive = true
            }
        }
    }
}

extension XLTTCustomerFilterVC : XLTTViewControllerProtocal {
    func setupListView() {
        
    }
    
    func setupViewModel() {
        if let ip = self.viewModel.input {
            bindingInput(input: ip)
        }
        if let op = self.viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input : XLTTCustomerFilterVM.Input){
        
    }
    
    private func bindingOutput(output : XLTTCustomerFilterVM.Output){
        
        output.dataTeamResult.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            if result.success {
                self.viewModel.input?.teamData.accept(result.data ?? [])
            }
        }).disposed(by: disposeBag)
        
        output.dataProductResult.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            if result.success {
                self.viewModel.input?.dataProduct.accept(result.data?.data ?? [])
                self.dialogProduct?.viewModel.input?.dataDialog.accept(result.data?.data ?? [])
            }
        }).disposed(by: disposeBag)
        
        output.dataProductMoreResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.success {
                var data = self.viewModel.input?.dataProduct.value ?? []
                data += result.data?.data ?? []
                
                self.viewModel.input?.dataProduct.accept(data)
                self.dialogProduct?.viewModel.input?.dataDialog.accept(data)
                var currentPage = self.viewModel.input?.indexProduct.value ?? 0
                currentPage += 1
                self.viewModel.input?.indexProduct.accept(currentPage)
            } else {
                print("ERROR")
            }
        }).disposed(by: disposeBag)
        
        output.dataProvincesResult.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            if result.success {
                self.viewModel.input?.dataProvinces.accept(result.data?.data ?? [])
                self.viewModel.input?.dataSearchProvinces.accept(result.data?.data ?? [])
            }
        }).disposed(by: disposeBag)
        
        output.dataDistrictsResult.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            if result.success {
                self.viewModel.input?.dataDistricts.accept(result.data?.data ?? [])
                self.viewModel.input?.dataSearchDistricts.accept(result.data?.data ?? [])
            }
        }).disposed(by: disposeBag)
        
        output.dataWardsResult.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            if result.success {
                self.viewModel.input?.dataWards.accept(result.data?.data ?? [])
                self.viewModel.input?.dataSearchWards.accept(result.data?.data ?? [])
            }
        }).disposed(by: disposeBag)
        
        output.dataEmployeeResult.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            if result.success {
                self.viewModel.input?.dataEmployee.accept(result.data?.data ?? [])
                self.dialogEmployee?.viewModel.input?.dataDialog.accept(result.data?.data ?? [])
            }
        }).disposed(by: disposeBag)
        
        output.dataEmployeeMoreResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.success {
                var data = self.viewModel.input?.dataEmployee.value ?? []
                data += result.data?.data ?? []
                
                self.viewModel.input?.dataEmployee.accept(data)
                self.dialogEmployee?.viewModel.input?.dataDialog.accept(data)
                var currentPage = self.viewModel.input?.indexEmployee.value ?? 0
                currentPage += 1
                self.viewModel.input?.indexEmployee.accept(currentPage)
            } else {
                print("ERROR")
            }
        }).disposed(by: disposeBag)
    }
    
}
