//
//  XLTTCustomerFilterVM.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/28/20.
//

import Foundation
import RxCocoa
import RxSwift
import RxDataSources

class XLTTCustomerFilterVM: XLTTViewModelProtocal {
    
    typealias Dependency = XLTTCommonService
    
    struct Input {
        let triggerTeam                     :BehaviorRelay<Bool>
        let itemTeam                        :BehaviorRelay<XLTTOptionsFilter>
        let teamData                        :BehaviorRelay<[XLTTOptionsFilter]>
        let triggerLoadProduct              :BehaviorRelay<Bool>
        let triggerLoadMoreProduct          :BehaviorRelay<Bool>
        let itemProduct                     :BehaviorRelay<XLTTOptionsFilter>
        let dataProduct                     :BehaviorRelay<[XLTTOptionsFilter]>
        let indexProduct                    :BehaviorRelay<Int>
        let pageSize                        :BehaviorRelay<Int>
        let searchProduct                   :BehaviorRelay<String>
        let triggerProvinces                :BehaviorRelay<Bool>
        let dataProvinces                   :BehaviorRelay<[XLTTOptionsFilter]>
        let dataSearchProvinces             :BehaviorRelay<[XLTTOptionsFilter]>
        let searchProvince                  :BehaviorRelay<String>
        let itemProvince                    :BehaviorRelay<XLTTOptionsFilter>
        let triggerDistrict                 :BehaviorRelay<Bool>
        let dataDistricts                   :BehaviorRelay<[XLTTOptionsFilter]>
        let dataSearchDistricts             :BehaviorRelay<[XLTTOptionsFilter]>
        let searchDistrict                  :BehaviorRelay<String>
        let itemDistrict                    :BehaviorRelay<XLTTOptionsFilter>
        let triggerWard                     :BehaviorRelay<Bool>
        let dataWards                       :BehaviorRelay<[XLTTOptionsFilter]>
        let dataSearchWards                 :BehaviorRelay<[XLTTOptionsFilter]>
        let searchWard                      :BehaviorRelay<String>
        let itemWard                        :BehaviorRelay<XLTTOptionsFilter>
        let statusOptions                   :BehaviorRelay<Int?>
        let dateUpdate                      :BehaviorRelay<Int?>
        let dateTrade                       :BehaviorRelay<Int?>
        let timeEnd                         :BehaviorRelay<Int?>
        let initData                        :BehaviorRelay<XLTTFilterModel>
        let triggerEmployee                 :BehaviorRelay<Bool>
        let triggerLoadMoreEmployee         :BehaviorRelay<Bool>
        let dataEmployee                    :BehaviorRelay<[XLTTOptionsFilter]>
        let itemEmployee                    :BehaviorRelay<XLTTOptionsFilter>
        let indexEmployee                   :BehaviorRelay<Int>
        let pageSizeEmployee                :BehaviorRelay<Int>
        let searchEmployee                  :BehaviorRelay<String>
        let dataStatusList                  :BehaviorRelay<[XLTTOptionsFilter]>
        let itemStatusList                  :BehaviorRelay<XLTTOptionsFilter>
    }
    
    struct Output {
        let dataTeamResult                  :Observable<XLTTResponseModel<[XLTTOptionsFilter]>>
        let dataProductResult               :Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>>
        let dataProductMoreResult           :Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>>
        let dataProvincesResult             :Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>>
        let dataDistrictsResult             :Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>>
        let dataWardsResult                 :Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>>
        let dataEmployeeResult              :Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>>
        let dataEmployeeMoreResult          :Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input : Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = self.transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: XLTTCommonService?) -> Output? {
        self.input = input
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else {
            return nil
        }
        
        let teamSaleResult = ip.triggerTeam.asObservable()
            .filter {$0}
            .flatMapLatest { _ -> Observable<XLTTResponseModel<[XLTTOptionsFilter]>> in
                return dp.getListTeamSale()
        }
        
        let loadProduct = Observable.merge(ip.triggerLoadProduct.asObservable().filter {$0}.withLatestFrom(ip.searchProduct),ip.searchProduct.asObservable())
        
        let productResult = loadProduct.asObservable()
            .do {
                ip.indexProduct.accept(1)
        }.flatMapLatest { (search) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> in
            return dp.getListProduct(search: search, index: 1, size: ip.pageSize.value)
        }
        
        let productLoadMore = ip.triggerLoadMoreProduct.asObservable().filter {$0}
            .withLatestFrom(ip.searchProduct)
            .flatMapFirst { (search) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> in
                return dp.getListProduct(search: search, index: ip.indexProduct.value, size: ip.pageSize.value)
        }
        
        let provincesResult = ip.triggerProvinces.asObservable().filter {$0}
            .flatMapLatest { (_) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> in
                return dp.getProvinces()
        }
        
        let districtsResult = ip.triggerDistrict.asObservable().filter {$0}
            .flatMapLatest { (_) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> in
                return dp.getDistricts(id: ip.itemProvince.value.id)
        }
        
        let wardsResult = ip.triggerWard.asObservable().filter {$0}
            .flatMapLatest { (_) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> in
                return dp.getWards(id: ip.itemDistrict.value.id)
        }
        
        let loadEmployee = Observable.merge(ip.triggerEmployee.asObservable().filter {$0}.withLatestFrom(ip.searchEmployee),ip.searchEmployee.asObservable())
        
        let employeeResult = loadEmployee.asObservable()
            .do {
                ip.indexEmployee.accept(1)
        }.flatMapLatest { (search) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> in
            return dp.getListEmployees(teamId: ip.itemTeam.value.id, index: 1, size: ip.pageSizeEmployee.value, search: search != "" ? search : nil)
        }
        
        let employeeLoadMore = ip.triggerLoadMoreEmployee.asObservable().filter {$0}
            .withLatestFrom(ip.searchEmployee)
            .flatMapFirst { (search) -> Observable<XLTTResponseModel<XLTTResponseList<XLTTOptionsFilter>>> in
                return dp.getListEmployees(teamId: ip.itemTeam.value.id, index: ip.indexEmployee.value, size: ip.pageSizeEmployee.value, search: search != "" ? search : nil)
        }
        
        return XLTTCustomerFilterVM.Output(dataTeamResult: teamSaleResult,
                                           dataProductResult: productResult,
                                           dataProductMoreResult: productLoadMore,
                                           dataProvincesResult: provincesResult,
                                           dataDistrictsResult : districtsResult,
                                           dataWardsResult: wardsResult,
                                           dataEmployeeResult: employeeResult,
                                           dataEmployeeMoreResult: employeeLoadMore)
    }
    
}
