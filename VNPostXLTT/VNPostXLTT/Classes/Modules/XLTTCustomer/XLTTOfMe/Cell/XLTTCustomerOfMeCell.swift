//
//  XLTTCustomerOfMeCell.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/26/20.
//

import Foundation
import UIKit
import RxCocoa

class XLTTCustomerOfMeCell: UITableViewCell{
    static let cellIdentifier = "XLTTCustomerOfMeCell"
    
    @IBOutlet weak var lbNameTitle: UILabel!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbPhoneTitle: UILabel!
    @IBOutlet weak var lbPhone: UILabel!
    @IBOutlet weak var lbProductNameTitle: UILabel!
    @IBOutlet weak var lbProductName: UILabel!
    @IBOutlet weak var viewDateUpdate: UIStackView!
    @IBOutlet weak var lbDateUpdateTitle: UILabel!
    @IBOutlet weak var lbDateUpdate: UILabel!
    @IBOutlet weak var viewDateTrade: UIStackView!
    @IBOutlet weak var lbDateTradeTitle: UILabel!
    @IBOutlet weak var lbDateTrade: UILabel!
    @IBOutlet weak var viewTimeTrade: UIStackView!
    @IBOutlet weak var lbTimeTradeTitle: UILabel!
    @IBOutlet weak var lbTimeTrade: UILabel!
    @IBOutlet weak var viewTimeEnd: UIStackView!
    @IBOutlet weak var lbTimeEndTitle: UILabel!
    @IBOutlet weak var lbTimeEnd: UILabel!
    @IBOutlet weak var viewLineAction: UIView!
    @IBOutlet weak var viewAction: UIStackView!
    @IBOutlet weak var btnHanding: UIButton!
    @IBOutlet weak var btnDeny: UIButton!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var imgPin: UIImageView!
    @IBOutlet weak var viewContain: UIView!
    
    var onActionAccept : ((XLTTCustomer?)->())?
    var onActionDeny : ((XLTTCustomer?)->())?
    var onClickItem : ((XLTTCustomer?)->())?
    
    var data: XLTTCustomer? {
        didSet {
            lbNameTitle.text = "xltt_me_name".localized
            lbName.text = data?.customerName.uppercased()
            lbPhoneTitle.text = "xltt_me_phone".localized
            lbPhone.text = data?.phoneNumber
            lbProductNameTitle.text = "xltt_me_product".localized
            lbProductName.text = data?.product.uppercased()
            
            if data?.receivedDate != nil {
                lbDateUpdateTitle.text = "xltt_me_date_update".localized
                lbDateUpdate.text = data?.receivedDate?.xlttDateToStringCustomer()
                viewDateUpdate.isHidden = false
            } else {
                viewDateUpdate.isHidden = true
            }
            
            if data?.flag ?? false {
                imgPin.isHidden = false
            } else {
                imgPin.isHidden = true
            }
            if data?.deliveryDate != nil {
                lbDateTradeTitle.text = "xltt_me_date_trade".localized
                lbDateTrade.text = data?.deliveryDate?.xlttDateToStringCustomer()
                viewDateTrade.isHidden = false
            } else {
                viewDateTrade.isHidden = true
            }
            switch data?.status {
            case XLTTCustomerType.CUSTOMER_STATUS_NEW.rawValue:
                viewStatus.backgroundColor = UIColor(hex: "0969F2")
                viewLineAction.isHidden = false
                viewAction.isHidden = false
                btnHanding.setTitle("xltt_me_accept".localized.uppercased(), for: .normal)
                btnDeny.setTitle("xltt_me_deny".localized.uppercased(), for: .normal)
                if data?.transactionTime != nil {
                    lbTimeTradeTitle.text = "xltt_me_time_trade".localized
                    if data?.transactionTime?.count ?? 0 > 1 && data?.transactionTime?.contains("-") == true{
                        lbTimeTrade.text = "00:00:00"
                    } else {
                        lbTimeTrade.text = data?.transactionTime?.xlttFormatTime()
                    }
                    viewTimeTrade.isHidden = false
                } else {
                    viewTimeTrade.isHidden = true
                }
                break
            default:
                viewTimeTrade.isHidden = true
                viewStatus.backgroundColor = UIColor(hex: "F7B500")
                viewLineAction.isHidden = true
                viewAction.isHidden = true
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(onClick))
        viewContain.addGestureRecognizer(tap)
    }
    
    @objc func onClick(_ sender: Any){
        self.onClickItem?(data)
    }
    
    @IBAction func onHandingLead(_ sender: Any) {
        self.onActionAccept?(data)
    }
    
    @IBAction func onDenyLead(_ sender: Any) {
        self.onActionDeny?(data)
    }
}
