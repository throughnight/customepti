//
//  XLTTUISimpleCollectionViewController.swift
//  VNPostXLTT
//
//  Created by m2 on 10/29/20.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class XLTTUISimpleCollectionViewController: UIViewController {
    
    private let collectionPage = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
    private let collectionPageIdentifier = "COLLECTION_PAGE_IDENTIFIER2"
    
    private var items = [UIViewController]()
    
    func addItem(item: UIViewController){
        items.append(item)
    }
    
    func build(){
        // view
        view.addSubview(collectionPage)
        
        // collectionPage
//        collectionPage.translatesAutoresizingMaskIntoConstraints = false
//        collectionPage.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
//        collectionPage.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
//        collectionPage.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
//        (collectionPage.collectionViewLayout as? UICollectionViewFlowLayout)?.scrollDirection = .vertical
//        collectionPage.isPagingEnabled = true
//        collectionPage.register(UICollectionViewCell.self, forCellWithReuseIdentifier: collectionPageIdentifier)
        collectionPage.delegate = self
        collectionPage.dataSource = self
        collectionPage.reloadData()
    }
    
}

extension XLTTUISimpleCollectionViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
}

extension XLTTUISimpleCollectionViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionPageIdentifier, for: indexPath)
        let vc = items[indexPath.row]
        
        cell.addSubview(vc.view)
        
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        vc.view.topAnchor.constraint(equalTo: cell.topAnchor).isActive = true
        vc.view.leadingAnchor.constraint(equalTo: cell.leadingAnchor).isActive = true
        vc.view.trailingAnchor.constraint(equalTo: cell.trailingAnchor).isActive = true
        vc.view.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
        return cell
    }
}

