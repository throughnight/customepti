//
//  XLTTHistoryAllocationCell.swift
//  VNPostXLTT
//
//  Created by AEGONA on 11/5/20.
//

import Foundation

class XLTTHistoryAllocationCell: UITableViewCell {
    static let cellIdentifier = "XLTTHistoryAllocationCell"
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    
    var data: XLTTHistoryAllocation? {
        didSet {
            lbDate.text = data?.addedStamp.xlttDateToStringCustomer(endFormat: "dd/MM/yyyy HH:mm:ss")
            let stringNomal = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor(red: 0, green: 0, blue: 0, alpha: 1)]
            let stringBold = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor(red: 0, green: 0, blue: 0, alpha: 1)]
            switch data?.leadStatus {
            case 0:
                let attributedString1 = NSMutableAttributedString(string: "\(data?.addedUser ?? "") ", attributes:stringBold)
                let attributedString2 = NSMutableAttributedString(string: "xltt_message_action_allocation_1".localized, attributes:stringNomal)
                let attributedString3 = NSMutableAttributedString(string: " \(data?.saleUserName ?? "")", attributes:stringBold)
                attributedString1.append(attributedString2)
                attributedString1.append(attributedString3)
                self.title.attributedText = attributedString1
                return
            case 1:
                let attributedString1 = NSMutableAttributedString(string: "\(data?.saleUserName ?? "") ", attributes:stringBold)
                let attributedString2 = NSMutableAttributedString(string: "xltt_message_action_allocation_deny_to_team".localized, attributes:stringNomal)
                attributedString1.append(attributedString2)
                self.title.attributedText = attributedString1
                return
            case 2:
                let attributedString1 = NSMutableAttributedString(string: "\(data?.addedUser ?? "") ", attributes:stringBold)
                let attributedString2 = NSMutableAttributedString(string: "xltt_message_action_allocation_deny_to_system".localized, attributes:stringNomal)
                attributedString1.append(attributedString2)
                self.title.attributedText = attributedString1
                return
            case 3:
                let attributedString1 = NSMutableAttributedString(string: "\(data?.saleUserName ?? "") ", attributes:stringBold)
                let attributedString2 = NSMutableAttributedString(string: "xltt_message_action_allocation_accept".localized, attributes:stringNomal)
                attributedString1.append(attributedString2)
                self.title.attributedText = attributedString1
                return
            default:
                let attributedString1 = NSMutableAttributedString(string: "\(data?.addedUser ?? "") ", attributes:stringBold)
                let attributedString2 = NSMutableAttributedString(string: "xltt_message_action_allocation_1".localized, attributes:stringNomal)
                let attributedString3 = NSMutableAttributedString(string: " \(data?.saleUserName ?? "")", attributes:stringBold)
                attributedString1.append(attributedString2)
                attributedString1.append(attributedString3)
                self.title.attributedText = attributedString1
            }
        }
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
