//
//  XLTTHistoryAllocationVC.swift
//  VNPostXLTT
//
//  Created by AEGONA on 11/5/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

typealias HistoryAllocationSection = SectionModel<String,XLTTHistoryAllocation>
typealias XLTTHistoryAllocationSource = RxTableViewSectionedReloadDataSource<HistoryAllocationSection>

class XLTTHistoryAllocationVC : XLTTBaseViewController {
    
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var imgEmpty: UIImageView!
    @IBOutlet weak var lbEmpty: UILabel!
    
    private var dataSource: XLTTHistoryAllocationSource!
    var dataSubject : Driver<[HistoryAllocationSection]>!
    
    var data = BehaviorRelay<XLTTLeadDetailModel>(value: XLTTLeadDetailModel())
    var history = BehaviorRelay<[XLTTHistoryAllocation]>(value: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupListView()
        lbEmpty.text = "xltt_title_empty".localized
        dataSubject = history.asDriver().map({ (data) -> [HistoryAllocationSection] in
            return [HistoryAllocationSection(model: "HistoryAllocation", items: data)]
        })
        dataSubject.drive(self.tbView.rx.items(dataSource: self.dataSource)).disposed(by: disposeBag)
        
        data.subscribe(onNext: { [weak self] lead in
            guard let self = self else {return}
            
            if lead.historyAllocation.count > 0 {
                self.imgEmpty.isHidden = true
                self.lbEmpty.isHidden = true
            } else {
                self.imgEmpty.isHidden = false
                self.lbEmpty.isHidden = false
            }
            self.history.accept(lead.historyAllocation)
        }).disposed(by: disposeBag)
    }
}

extension XLTTHistoryAllocationVC : XLTTViewControllerProtocal {
    func setupViewModel() {
        
    }
    
    func setupListView() {
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        self.tbView.register(UINib(nibName: XLTTHistoryAllocationCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: XLTTHistoryAllocationCell.cellIdentifier)
        
        let ds = RxTableViewSectionedReloadDataSource<HistoryAllocationSection>(configureCell: { dataSource, tableView, indexPath, data in
            
            let cell : XLTTHistoryAllocationCell = tableView.dequeueReusableCell(withIdentifier: XLTTHistoryAllocationCell.cellIdentifier, for: indexPath) as! XLTTHistoryAllocationCell
            
            cell.data = data
            
            return cell
        })
        
        self.dataSource = ds
        self.tbView.allowsSelection = false
    }
}
