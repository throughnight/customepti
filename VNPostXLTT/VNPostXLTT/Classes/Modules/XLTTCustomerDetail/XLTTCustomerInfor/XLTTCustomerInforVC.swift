//
//  XLTTCustomerInforVC.swift
//  VNPostXLTT
//
//  Created by m2 on 10/29/20.
//

import Foundation

class XLTTCustomerInforVC: XLTTBaseViewController {
    
    @IBOutlet weak var pinView: UIButton!
    
    @IBOutlet weak var codeStack: UIStackView!
    @IBOutlet weak var emailStack: UIStackView!
    @IBOutlet weak var phoneStack: UIStackView!
    @IBOutlet weak var adressStack: UIStackView!
    @IBOutlet weak var actionStack: UIStackView!
    
    @IBOutlet weak var codeTitleLabel: UILabel!
    @IBOutlet weak var emailTitleLabel: UILabel!
    @IBOutlet weak var phoneTitleLabel: UILabel!
    @IBOutlet weak var addressTitleLabel: UILabel!
    @IBOutlet weak var customerInforTitle: UILabel!
    
    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var lbNameCustomer: UILabel!
    
    @IBOutlet weak var assignView: UIView!
    @IBOutlet weak var successView: UIView!
    @IBOutlet weak var failureView: UIView!
    
    @IBOutlet weak var assignLabel: UILabel!
    @IBOutlet weak var successLabel: UILabel!
    @IBOutlet weak var failureLabel: UILabel!
    
    @IBOutlet weak var imgSuccess: UIImageView!
    @IBOutlet weak var imgFailure: UIImageView!
    
    var onAssignLead : ((XLTTCustomer?)->())?
    var onAcceptLead : ((XLTTCustomer?)->())?
    var onDenyLead : ((XLTTCustomer?)->())?
    var onPinLead : (()->())?
    var onSuccessLead : ((XLTTCustomer?)->())?
    var onFailLead : ((XLTTCustomer?)->())?
    
    private var dataLead: XLTTCustomer?
    
    var type: Int = 0
    var status : Int = XLTTCustomerType.CUSTOMER_STATUS_NEW.rawValue
    var pin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }
    
    private func setupViews(){
        self.view.layer.cornerRadius = 4
        
        self.customerInforTitle.text = "xltt_customer_detail_infor_title".localized
        self.codeTitleLabel.text = "xltt_customer_detail_infor_code".localized
        self.emailTitleLabel.text = "xltt_customer_detail_infor_email".localized
        self.phoneTitleLabel.text = "xltt_customer_detail_infor_phone".localized
        self.addressTitleLabel.text = "xltt_customer_detail_infor_address".localized
        
        self.actionAcive()
        
        //register action click
        let tapAssign = UITapGestureRecognizer()
        assignView.addGestureRecognizer(tapAssign)
        tapAssign.rx.event.bind { (tap) in
            self.onAssignLead?(self.dataLead)
        }.disposed(by: disposeBag)
        
        let tapAccept = UITapGestureRecognizer()
        successView.addGestureRecognizer(tapAccept)
        tapAccept.rx.event.bind { (tap) in
            if self.status == XLTTCustomerType.CUSTOMER_STATUS_NEW.rawValue {
                self.onAcceptLead?(self.dataLead)
            }
            if self.status == XLTTCustomerType.CUSTOMER_STATUS_CONTACT.rawValue {
                self.onSuccessLead?(self.dataLead)
            }
        }.disposed(by: disposeBag)
        
        let tapDeny = UITapGestureRecognizer()
        failureView.addGestureRecognizer(tapDeny)
        tapDeny.rx.event.bind { (tap) in
            if self.status == XLTTCustomerType.CUSTOMER_STATUS_NEW.rawValue {
                self.onDenyLead?(self.dataLead)
            }
            if self.status == XLTTCustomerType.CUSTOMER_STATUS_CONTACT.rawValue {
                self.onFailLead?(self.dataLead)
            }
        }.disposed(by: disposeBag)
    }
    
    @IBAction func onClickPin(_ sender: Any) {
        self.onPinLead?()
    }
    
    func loadPin(isPin: Bool){
        if isPin {
            pinView.setImage(UIImage(named: "ic_pin",
            in: Bundle(url: Bundle.main.bundleURL.appendingPathComponent("Frameworks").appendingPathComponent("VNPostXLTT.framework")) ?? Bundle.main,
            compatibleWith: nil), for: .normal)
        } else {
            pinView.setImage(UIImage(named: "ic_pin_black",
            in: Bundle(url: Bundle.main.bundleURL.appendingPathComponent("Frameworks").appendingPathComponent("VNPostXLTT.framework")) ?? Bundle.main,
            compatibleWith: nil), for: .normal)
        }
    }
    
    func loadData(data: XLTTCustomer?, status: Int, isPin: Bool, isAction : Bool, hidePin: Bool){
        self.imgSuccess.isHidden = true
        self.imgFailure.isHidden = true
        self.dataLead = data
        self.status = status
        
        lbNameCustomer.text = data?.customerName
        if data?.customerName != "" {
            code.text = data?.customerCode
            codeStack.isHidden = false
        } else {
            codeStack.isHidden = true
        }
        if data?.email != "" {
            email.text = data?.email
            emailStack.isHidden = false
        } else {
            emailStack.isHidden = true
        }
        if data?.phoneNumber != "" {
            phone.text = data?.phoneNumber
            phoneStack.isHidden = false
        } else {
            phoneStack.isHidden = true
        }
        if data?.temporaryAddress != nil && data?.temporaryAddress?.getFullAddress() != "" {
            address.text = data?.temporaryAddress?.getFullAddress()
            adressStack.isHidden = false
        } else {
            adressStack.isHidden = true
        }
        if isAction {
            actionStack.isHidden = false
            pinView.isHidden = false
        } else {
            actionStack.isHidden = true
            pinView.isHidden = true
        }
        switch status {
        case XLTTCustomerType.CUSTOMER_STATUS_NEW.rawValue:
            self.actionAcive()
            self.pinView.isHidden = false
            if type == XLTTPotenitalcus.ME.rawValue {
                assignView.isHidden = true
            } else {
                if type == XLTTPotenitalcus.HISTORY.rawValue {
                    //history
                    actionStack.isHidden = true
                    pinView.isHidden = true
                } else {
                    //team
                    if XLTTManager.shared.userProfileType == XLTTProfileType.SUPERVISOR.rawValue {
                        assignView.isHidden = false
                    } else {
                        assignView.isHidden = true
                        failureView.isHidden = true
                        pinView.isHidden = true
                    }
                }
            }
            
            break
        case XLTTCustomerType.CUSTOMER_STATUS_CONTACT.rawValue:
            self.statusActive()
            self.pinView.isHidden = false
            if type == XLTTPotenitalcus.HISTORY.rawValue {
                //history
                actionStack.isHidden = true
                pinView.isHidden = true
            }
            break
        case XLTTCustomerType.CUSTOMER_STATUS_SUCCESS.rawValue:
            self.statusLead(status: true)
            self.pinView.isHidden = true
            actionStack.isHidden = false
            break
        case XLTTCustomerType.CUSTOMER_STATUS_FAIL.rawValue:
            self.statusLead(status: false)
            self.pinView.isHidden = true
            actionStack.isHidden = false
            break
        default:
            self.pinView.isHidden = true
            actionStack.isHidden = true
        }
        if hidePin {
            pinView.isHidden = true
        } else {
            pinView.isHidden = false
            self.pin = isPin
            self.loadPin(isPin: isPin)
        }
        
    }
    
    private func statusLead(status : Bool){
        self.assignView.isHidden = true
        self.successView.layer.cornerRadius = 18
        self.failureView.layer.cornerRadius = 18
        self.failureView.layer.borderWidth = 0
        self.successView.layer.borderWidth = 0
        self.successLabel.text = "xltt_customer_detail_infor_success".localized
        self.failureLabel.text = "xltt_customer_detail_infor_failure".localized
        if status {
            self.failureView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05)
            self.failureLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
            self.successView.backgroundColor = UIColor(red: 0, green: 177, blue: 106, alpha: 0.1)
            self.successLabel.textColor = UIColor(hex: "00B16A")
            self.imgSuccess.isHidden = false
        } else {
            self.successView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05)
            self.successLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
            self.failureView.backgroundColor = UIColor(hex: "FEEDEC")
            self.failureLabel.textColor = UIColor(hex: "F64744")
            self.imgFailure.isHidden = false
        }
    }
    
    private func statusActive(){
        
        self.assignView.isHidden = true
        self.successView.layer.cornerRadius = 18
        self.failureView.layer.cornerRadius = 18
        
        self.failureView.backgroundColor = XLTTConstants.xltt_color_red_1
        self.successView.backgroundColor = XLTTConstants.xltt_color_green_1
        
        self.successLabel.text = "xltt_customer_detail_infor_success".localized
        self.failureLabel.text = "xltt_customer_detail_infor_failure".localized
        
        self.failureLabel.textColor = .white
        self.successLabel.textColor = .white
        
        self.failureLabel.font = UIFont.systemFont(ofSize: 14.0)
        self.successLabel.font = UIFont.systemFont(ofSize: 14.0)
    }
    
    private func statusNoneActive(){
        
        self.assignView.isHidden = true
        self.successView.layer.cornerRadius = 16
        self.failureView.layer.cornerRadius = 16
    }
    
    private func actionAcive(){
        self.assignView.layer.cornerRadius = 4
        self.successView.layer.cornerRadius = 4
        self.failureView.layer.cornerRadius = 4
        
        self.successView.layer.borderWidth = 1.0
        self.failureView.layer.borderWidth = 1.0
        self.assignView.layer.borderWidth = 1.0
        
        self.successView.layer.borderColor = XLTTConstants.xltt_color_green_1.cgColor 
        self.failureView.layer.borderColor = XLTTConstants.xltt_color_red_1.cgColor
        self.assignView.layer.borderColor = XLTTConstants.xltt_color_blue_2.cgColor
        
        self.assignLabel.textColor = XLTTConstants.xltt_color_blue_2
        self.failureLabel.textColor = XLTTConstants.xltt_color_red_1
        self.successLabel.textColor = XLTTConstants.xltt_color_green_1
        
        self.assignLabel.text = "xltt_customer_detail_infor_action_assign".localized.uppercased()
        self.successLabel.text = "xltt_customer_detail_infor_action_accept".localized.uppercased()
        self.failureLabel.text = "xltt_customer_detail_infor_action_deny".localized.uppercased()
    }
    
}
