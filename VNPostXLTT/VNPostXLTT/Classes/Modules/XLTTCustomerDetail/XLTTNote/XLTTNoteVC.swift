//
//  XLTTNoteVC.swift
//  VNPostXLTT
//
//  Created by AEGONA on 11/4/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

typealias NoteSection = SectionModel<String,XLTTNote>
typealias XLTTNoteSource = RxTableViewSectionedReloadDataSource<NoteSection>

class XLTTNoteVC : XLTTBaseViewController {
    
    @IBOutlet weak var stackNoteView: UIStackView!
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var imgEmpty: UIImageView!
    @IBOutlet weak var lbEmpty: UILabel!
    
    private var dataSource: XLTTNoteSource!
    var dataSubject : Driver<[NoteSection]>!
    
    var data =  BehaviorRelay<XLTTLeadDetailModel>(value: XLTTLeadDetailModel())
    var notes = BehaviorRelay<[XLTTNote]>(value: [])
    var onAddNote: (()->())?
    var isHistory = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbEmpty.text = "xltt_title_empty".localized
        stackNoteView.isHidden = true
        setupListView()
        dataSubject = notes.asDriver().map({ (data) -> [NoteSection] in
            return [NoteSection(model: "Note", items: data)]
        })
        dataSubject.drive(self.tbView.rx.items(dataSource: self.dataSource)).disposed(by: disposeBag)
        
        data.subscribe(onNext: { [weak self] lead in
            guard let self = self else {return}
            
            if lead.status == XLTTCustomerType.CUSTOMER_STATUS_CONTACT.rawValue {
                if self.isHistory {
                    self.stackNoteView.isHidden = true
                    self.tbView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
                    self.view.layoutIfNeeded()
                } else {
                    self.stackNoteView.isHidden = false
                    self.stackNoteView.layoutIfNeeded()
                    self.view.layoutIfNeeded()
                }
            } else {
                if lead.historyComment.count > 0 {
                    self.stackNoteView.isHidden = true
                    self.tbView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
                    self.view.layoutIfNeeded()
                } else {
                    self.stackNoteView.isHidden = true
                    self.view.layoutIfNeeded()
                }
            }
            if lead.historyComment.count > 0 {
                self.imgEmpty.isHidden = true
                self.lbEmpty.isHidden = true
            } else {
                self.imgEmpty.isHidden = false
                self.lbEmpty.isHidden = false
            }
            self.notes.accept(lead.historyComment)
            self.tbView.reloadData()
            
        }).disposed(by: disposeBag)
        
        let tapAdd = UITapGestureRecognizer()
        stackNoteView.addGestureRecognizer(tapAdd)
        tapAdd.rx.event.bind { (tap) in
            self.onAddNote?()
        }.disposed(by: disposeBag)
    }
}

extension XLTTNoteVC: XLTTViewControllerProtocal {
    func setupListView() {
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        self.tbView.register(UINib(nibName: XLTTNoteCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: XLTTNoteCell.cellIdentifier)
        
        let ds = RxTableViewSectionedReloadDataSource<NoteSection>(configureCell: { dataSource, tableView, indexPath, data in
            
            let cell : XLTTNoteCell = tableView.dequeueReusableCell(withIdentifier: XLTTNoteCell.cellIdentifier, for: indexPath) as! XLTTNoteCell
            
            cell.data = data
            
            return cell
        })
        
        self.dataSource = ds
        self.tbView.allowsSelection = false
    }
    
    func setupViewModel() {
        
    }
}
