//
//  XLTTNoteCell.swift
//  VNPostXLTT
//
//  Created by AEGONA on 11/4/20.
//

import Foundation

class XLTTNoteCell: UITableViewCell {
    static let cellIdentifier = "XLTTNoteCell"
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var date: UILabel!
    
    var data: XLTTNote? {
        didSet {
            self.title.text = data?.note
            self.date.text = data?.addedStamp.xlttDateToStringCustomer(endFormat: "dd/MM/yyyy - HH:mm:ss")
        }
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
