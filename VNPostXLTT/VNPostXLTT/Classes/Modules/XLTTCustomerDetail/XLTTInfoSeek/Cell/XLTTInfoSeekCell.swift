//
//  XLTTInfoSeekCell.swift
//  VNPostXLTT
//
//  Created by AEGONA on 11/4/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

typealias SeekSection = SectionModel<String,XLTTQuestion>
typealias XLTTInfoSeekSource = RxTableViewSectionedReloadDataSource<SeekSection>

class XLTTInfoSeekCell: UITableViewCell {
    static let cellIdentifier = "XLTTInfoSeekCell"
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var containViewConstraintHeight: NSLayoutConstraint!
    
    var onChangeHeight : ((Int,Bool)->())?
    
    private var dataSource: XLTTInfoSeekSource!
    var dataSubject = PublishSubject<[SeekSection]>()
    private var heightContent = 60
    private var isFull = BehaviorRelay<Bool>(value: true)
    
    var disposeBag : DisposeBag! = DisposeBag()
    var data: XLTTInfoSeek? {
        didSet {
            self.dataSource = nil
            dataSubject = PublishSubject<[SeekSection]>()
            setupView()
            self.tbView.dataSource = nil
            self.dataSubject
                .bind(to: self.tbView.rx.items(dataSource: self.dataSource))
                .disposed(by: disposeBag)
            lbTitle.text = data?.surveySheetName
            if data?.questionAnswer.count ?? 0 > 0 {
                self.dataSubject.onNext([SeekSection(model: "Seek", items: data?.questionAnswer ?? [])])
            }
            handleUpdateUI()
//            self.onChangeHeight?(self.heightContent,self.isFull.value)
            if data?.isFull ?? true {
                self.imgArrow.image = UIImage(named: "ic_arrow_up_black",
                                              in: Bundle(url: Bundle.main.bundleURL.appendingPathComponent("Frameworks").appendingPathComponent("VNPostXLTT.framework")) ??
                                                Bundle.main,
                                              compatibleWith: nil)
//                self.onChangeHeight?(self.heightContent,true)
            } else {
                self.imgArrow.image = UIImage(named: "ic_arrow_down_black",
                                              in: Bundle(url: Bundle.main.bundleURL.appendingPathComponent("Frameworks").appendingPathComponent("VNPostXLTT.framework")) ?? Bundle.main,
                                              compatibleWith: nil)
//                self.onChangeHeight?(self.heightContent - 60,false)
            }
        }
    }
    
    func handleUpdateUI(){
        self.heightContent = 60
        heightContent = Int(self.tbView.contentSize.height) + 60
        containViewConstraintHeight.constant = CGFloat(self.heightContent)
        containView.layoutIfNeeded()
    }
    
    private func setupView(){
        
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        self.tbView.register(UINib(nibName: XLTTSeekCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: XLTTSeekCell.cellIdentifier)
        
        let ds = RxTableViewSectionedReloadDataSource<SeekSection>(configureCell: { dataSource, tableView, indexPath, data in
            
            let cell : XLTTSeekCell = tableView.dequeueReusableCell(withIdentifier: XLTTSeekCell.cellIdentifier, for: indexPath) as! XLTTSeekCell
            
            cell.data = data
            
            return cell
        })
        
        self.dataSource = ds
        self.tbView.allowsSelection = false
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapHeader = UITapGestureRecognizer(target: self, action: #selector(onTap))
        titleView.addGestureRecognizer(tapHeader)
        
//        isFull.subscribe(onNext: {value in
//            if value {
//                self.imgArrow.image = UIImage(named: "ic_arrow_up_black",
//                                              in: Bundle(url: Bundle.main.bundleURL.appendingPathComponent("Frameworks").appendingPathComponent("VNPostXLTT.framework")) ??
//                                                Bundle.main,
//                                              compatibleWith: nil)
//                self.onChangeHeight?(self.heightContent,true)
//            } else {
//                self.imgArrow.image = UIImage(named: "ic_arrow_down_black",
//                                              in: Bundle(url: Bundle.main.bundleURL.appendingPathComponent("Frameworks").appendingPathComponent("VNPostXLTT.framework")) ?? Bundle.main,
//                                              compatibleWith: nil)
//                self.onChangeHeight?(self.heightContent - 60,false)
//            }
//        })
    }
    
    @objc func onTap(_ sender: Any){
//        self.isFull.accept(!self.isFull.value)
        self.onChangeHeight?(self.heightContent,false)
    }
    
}
