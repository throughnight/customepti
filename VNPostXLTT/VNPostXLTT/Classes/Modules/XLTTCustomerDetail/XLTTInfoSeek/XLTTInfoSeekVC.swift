//
//  XLTTInfoSeekVC.swift
//  VNPostXLTT
//
//  Created by AEGONA on 11/4/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

typealias InfoSeekSection = SectionModel<String,XLTTInfoSeek>
typealias XLTTTabInfoSeekSource = RxTableViewSectionedReloadDataSource<InfoSeekSection>

class XLTTInfoSeekVC : XLTTBaseViewController {
    
    private var dataSource: XLTTTabInfoSeekSource!
    var dataSubject : Driver<[InfoSeekSection]>!
    
    var data = BehaviorRelay<[XLTTInfoSeek]>(value: [])
    
    var heightContent = 0
    var onChangeHeightView : ((Int)->())?
    
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var tbViewConstaints: NSLayoutConstraint!
    
    var listSelect : [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupListView()
        
        dataSubject = data.asDriver().map({ (data) -> [InfoSeekSection] in
            return [InfoSeekSection(model: "InfoSeek", items: data)]
        })
        dataSubject.drive(self.tbView.rx.items(dataSource: self.dataSource)).disposed(by: disposeBag)
        tbView.delegate = self
    }
    
    func loadData(data: [XLTTInfoSeek]){
        self.data.accept(data)
    }
    
   
}

extension XLTTInfoSeekVC : XLTTViewControllerProtocal {
    func setupListView() {
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        self.tbView.register(UINib(nibName: XLTTInfoSeekCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: XLTTInfoSeekCell.cellIdentifier)
        
        let ds = RxTableViewSectionedReloadDataSource<InfoSeekSection>(configureCell: { dataSource, tableView, indexPath, data in
            
            let cell : XLTTInfoSeekCell = tableView.dequeueReusableCell(withIdentifier: XLTTInfoSeekCell.cellIdentifier, for: indexPath) as! XLTTInfoSeekCell
            
            cell.data = data
            
            cell.onChangeHeight = { height,status in
                if !self.listSelect.contains(indexPath.row) {
                    self.listSelect.append(indexPath.row)
                    data.isFull = false
                } else {
                    let index = self.listSelect.firstIndex(of: indexPath.row)
                    if index != nil {
                        self.listSelect.remove(at: index ?? 0)
                        data.isFull = true
                    }
                }
                self.tbView.reloadRows(at: [indexPath], with: .none)
            }
            
            return cell
        })
        self.dataSource = ds
        self.tbView.allowsSelection = false
    }
    
    func setupViewModel() {
        
    }
    
    private func heightCell(data: XLTTInfoSeek?) -> Int {
        var height = 60
        if data?.questionAnswer.count ?? 0 > 0 {
            let widthView = self.view.frame.size.width
            let widthLine = (widthView / 2) - 20
            data?.questionAnswer.forEach({ (item) in
                var qNum = (item.questionName.count * 9) / Int(widthLine)
                qNum += 1
                height += qNum == 1 ? (qNum * 30) : (qNum * 20)
            })
        }
        return height
    }
}

extension XLTTInfoSeekVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.listSelect.contains(indexPath.row){
            return 60
        }
        return CGFloat(self.heightCell(data: self.data.value[indexPath.row]))
    }
}
