//
//  XLTTSeekCell.swift
//  VNPostXLTT
//
//  Created by AEGONA on 11/4/20.
//

import Foundation

class XLTTSeekCell: UITableViewCell {
    static let cellIdentifier = "XLTTSeekCell"
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var value: UILabel!
    
    var data: XLTTQuestion? {
        didSet {
            self.title.text = data?.questionName
            switch data?.dataType ?? 0 {
            case XLTTDataType.TEXT.rawValue, XLTTDataType.NUMBER.rawValue:
                self.value.text = data?.values
                break
            case XLTTDataType.BOOLEAN.rawValue:
                if data?.values == "true"{
                    self.value.text = "Đúng"
                } else {
                    self.value.text = "Sai"
                }
                break
            case XLTTDataType.DOUBLE.rawValue:
                let nValue = data?.values.replacingOccurrences(of: ".", with: "")
                if Int(nValue ?? "") != nil {
                    self.value.text = data?.values.xlttFormatPrice(symbol: "")
                } else {
                    self.value.text = data?.values
                }
                break
            case XLTTDataType.DATE.rawValue:
                self.value.text = data?.values.xlttFormatDateInput()
                break
//            case XLTTDataType.LIST.rawValue:
//
//                break
            default:
                self.value.text = data?.values
            }
        }
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
