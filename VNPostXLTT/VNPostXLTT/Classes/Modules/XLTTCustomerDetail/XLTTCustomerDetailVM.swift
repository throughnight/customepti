//
//  XLTTCustomerDetailVM.swift
//  VNPostXLTT
//
//  Created by AEGONA on 11/3/20.
//

import Foundation
import RxCocoa
import RxSwift


class XLTTCustomerDetailVM: XLTTViewModelProtocal {
    typealias Dependency = XLTTCustomerService
    
    struct Input {
        let isLoading                           :BehaviorRelay<Bool>
        let triggerDetail                       :BehaviorRelay<Bool>
        let dataLead                            :BehaviorRelay<XLTTLeadDetailModel>
        let initLead                            :BehaviorRelay<XLTTCustomer>
        let type                                :BehaviorRelay<Int>
        let triggerAccept                       :BehaviorRelay<Bool>
        let triggerDeny                         :BehaviorRelay<Bool>
        let triggerPin                          :BehaviorRelay<Bool>
        let triggerSuccess                      :BehaviorRelay<Bool>
        let noteSuccess                         :BehaviorRelay<String>
        let triggerFailure                      :BehaviorRelay<Bool>
        let noteFailure                         :BehaviorRelay<String>
        let triggerAddNote                      :BehaviorRelay<Bool>
        let note                                :BehaviorRelay<String>
        let isAction                            :BehaviorRelay<Bool>
    }
    
    struct Output {
        let detailResult                        :Observable<XLTTResponseModel<XLTTLeadDetailModel>>
        let acceptResult                        :Observable<XLTTResponseModel<Any>>
        let denyResult                          :Observable<XLTTResponseModel<Any>>
        let pinResult                           :Observable<XLTTResponseModel<Any>>
        let successResult                       :Observable<XLTTResponseModel<Any>>
        let failureResult                       :Observable<XLTTResponseModel<Any>>
        let noteResult                          :Observable<XLTTResponseModel<Any>>
    }
    
    var input: Input?
    var output: Output?
    var dependency: Dependency?
    
    init(input: Input, dependency: Dependency) {
        self.input = input
        self.dependency = dependency
        self.output = transform(input: self.input, dependency: self.dependency)
    }
    
    func transform(input: Input?, dependency: XLTTCustomerService?) -> Output? {
        self.input = input
        self.dependency = dependency
        
        guard let ip = input, let dp = dependency else { return nil }
        
        let detailResult = ip.triggerDetail.asObservable()
            .filter {$0}
            .withLatestFrom(ip.initLead)
            .do(onNext: { (_) in
                ip.isLoading.accept(true)
            }).flatMapLatest { (lead) -> Observable<XLTTResponseModel<XLTTLeadDetailModel>> in
                return dp.getCustomerDetail(leadId: lead.leadId, leadAssignId: lead.id)
        }.do(onNext: { (_) in
            ip.isLoading.accept(false)
        })
        
        let accpetResult = ip.triggerAccept.asObservable()
            .filter {$0}
            .withLatestFrom(ip.initLead)
            .do(onNext: { (_) in
                ip.isLoading.accept(true)
            }).flatMapLatest { (lead) -> Observable<XLTTResponseModel<Any>> in
                return dp.customerAction(idCustomer: lead.id, type: XLTTCustomerActionType.CONTACT.rawValue, note: nil)
        }.do(onNext: { (_) in
            ip.isLoading.accept(false)
        })
        
        let denyResult = ip.triggerDeny.asObservable()
            .filter {$0}
            .withLatestFrom(ip.initLead)
            .do(onNext: { (_) in
                ip.isLoading.accept(true)
            }).flatMapLatest { (lead) -> Observable<XLTTResponseModel<Any>> in
                return dp.customerDeny(id: lead.id, type: ip.type.value)
        }.do(onNext: { (_) in
            ip.isLoading.accept(false)
        })
        
        let pinResult = ip.triggerPin.asObservable()
            .filter {$0}
            .withLatestFrom(ip.initLead)
            .do(onNext: { (_) in
                ip.isLoading.accept(true)
            }).flatMapLatest { (lead) -> Observable<XLTTResponseModel<Any>> in
                return dp.pinCustomer(id: lead.leadId, status: !ip.dataLead.value.flag)
        }.do(onNext: { (_) in
            ip.isLoading.accept(false)
        })
        
        let successResult = ip.triggerSuccess.asObservable()
            .filter {$0}
            .withLatestFrom(ip.noteSuccess)
            .do(onNext: { (_) in
                ip.isLoading.accept(true)
            }).flatMapLatest { (note) -> Observable<XLTTResponseModel<Any>> in
                return dp.customerAction(idCustomer: ip.initLead.value.id, type: XLTTCustomerActionType.SUCCESS.rawValue, note: note)
        }.do(onNext: { (_) in
            ip.isLoading.accept(false)
            ip.noteSuccess.accept("")
        })
        
        let failResult = ip.triggerFailure.asObservable()
            .filter {$0}
            .withLatestFrom(ip.noteFailure)
            .do(onNext: { (_) in
                ip.isLoading.accept(true)
            }).flatMapLatest { (note) -> Observable<XLTTResponseModel<Any>> in
                return dp.customerAction(idCustomer: ip.initLead.value.id, type: XLTTCustomerActionType.FAIL.rawValue, note: note)
        }.do(onNext: { (_) in
            ip.isLoading.accept(false)
            ip.noteFailure.accept("")
        })
        
        let noteResult = ip.triggerAddNote.asObservable()
            .filter {$0}
            .withLatestFrom(ip.note)
            .do(onNext: { (_) in
                ip.isLoading.accept(true)
            }).flatMapLatest { (note) -> Observable<XLTTResponseModel<Any>> in
                dp.addNote(data: XLTTAssignModel(leadAssignId: ip.initLead.value.id, leadId: ip.initLead.value.leadId, content: note))
        }.do(onNext: { (_) in
            ip.isLoading.accept(false)
        })
        
        return XLTTCustomerDetailVM.Output(detailResult: detailResult,
                                           acceptResult: accpetResult,
                                           denyResult: denyResult,
                                           pinResult: pinResult,
                                           successResult: successResult,
                                           failureResult: failResult,
                                           noteResult: noteResult)
    }
}
