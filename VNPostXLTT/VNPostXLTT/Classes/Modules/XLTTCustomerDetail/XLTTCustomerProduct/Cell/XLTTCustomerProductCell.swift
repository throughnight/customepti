//
//  XLTTCustomerProductCell.swift
//  VNPostXLTT
//
//  Created by m2 on 10/29/20.
//

import Foundation

class XLTTCustomerProductCell: UITableViewCell {
    static let cellIdentifier = "XLTTCustomerProductCell"
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var value: UILabel!
    
    var data: XLTTQuestionAnswer? {
        didSet {
            self.title.text = data?.displayText
            
            switch data?.dataType ?? 0 {
            case XLTTDataType.TEXT.rawValue, XLTTDataType.NUMBER.rawValue:
                if data?.value.count ?? 0 > 0 {
                    let value = data?.value.substring(0..<1)
                    if value == "0" {
                        self.value.text = data?.value
                    } else {
                        if Int(data?.value ?? "") != nil {
                            self.value.text = data?.value.xlttFormatPrice(symbol: "")
                        } else {
                            self.value.text = data?.value
                        }
                    }
                }
                break
            case XLTTDataType.DOUBLE.rawValue:
                let nValue = data?.value.replacingOccurrences(of: ".", with: "")
                if Int(nValue ?? "") != nil {
                    self.value.text = data?.value.xlttFormatPrice(symbol: "")
                } else {
                    self.value.text = data?.value
                }
                break
                
            case XLTTDataType.BOOLEAN.rawValue:
                if data?.value == "true"{
                    self.value.text = "Đúng"
                } else {
                    self.value.text = "Sai"
                }
                break
            case XLTTDataType.DATE.rawValue:
                self.value.text = data?.value.xlttDateToStringCustomer(endFormat: "yyyy-MM-dd")
                break
            default:
                self.value.text = data?.value
            }
        }
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
