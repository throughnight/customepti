//
//  XLTTCustomerProductVC.swift
//  VNPostXLTT
//
//  Created by m2 on 10/29/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

typealias CustomerProductSection = SectionModel<String,XLTTQuestionAnswer>
typealias XLTTCustomerProductSource = RxTableViewSectionedReloadDataSource<CustomerProductSection>

class XLTTCustomerProductVC: XLTTBaseViewController {
    
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productionName: UILabel!
    
    @IBOutlet weak var contentTableView: UITableView!
    
    @IBOutlet weak var seeMoreStack: UIStackView!
    @IBOutlet weak var seeMoreLabel: UILabel!
    @IBOutlet weak var seeMoreImageView: UIImageView!
    
    var onSeeMore : ((Bool)->())?
    var isSeeMore :BehaviorRelay<Bool> = BehaviorRelay<Bool>(value: false)
    var itemLimit = 1
    
    private var dataSource: XLTTCustomerProductSource!
    var dataSubject : Driver<[CustomerProductSection]>!
    
    var infoProduct = BehaviorRelay<XLTTInfoProduct>(value: XLTTInfoProduct())
    private var products = BehaviorRelay<[XLTTQuestionAnswer]>(value: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.setupTableView()
        
        dataSubject = products.asDriver().map({ (data) -> [CustomerProductSection] in
            return [CustomerProductSection(model: "Products", items: data)]
        })
        dataSubject.drive(self.contentTableView.rx.items(dataSource: self.dataSource)).disposed(by: disposeBag)
        
        self.seeMoreClick()
        
        isSeeMore.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result {
                self.seeMoreLabel.text = "xltt_customer_detail_product_collapse".localized
                self.products.accept(self.infoProduct.value.questionAnswer)
                self.seeMoreImageView.image = UIImage(named: "ic_arrow_up_black",
                                                      in: Bundle(url: Bundle.main.bundleURL.appendingPathComponent("Frameworks").appendingPathComponent("VNPostXLTT.framework")) ?? Bundle.main,
                                                      compatibleWith: nil)
            } else {
                self.seeMoreLabel.text = "xltt_customer_detail_product_see_more".localized
                self.products.accept(Array(self.infoProduct.value.questionAnswer.prefix(self.itemLimit)))
                self.seeMoreImageView.image = UIImage(named: "ic_arrow_down_black",
                                                      in: Bundle(url: Bundle.main.bundleURL.appendingPathComponent("Frameworks").appendingPathComponent("VNPostXLTT.framework")) ?? Bundle.main,
                                                      compatibleWith: nil)
            }
        }).disposed(by: disposeBag)
        
        infoProduct.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            self.productionName.text = result.name
            if result.questionAnswer.count > self.itemLimit {
                self.seeMoreStack.isHidden = false
                self.products.accept(Array(self.infoProduct.value.questionAnswer.prefix(self.itemLimit)))
            } else {
                self.products.accept(self.infoProduct.value.questionAnswer)
                self.seeMoreStack.isHidden = true
            }
        }).disposed(by: disposeBag)
    }
    
    private func seeMoreClick(){
        let tap = UITapGestureRecognizer()
        self.seeMoreStack.addGestureRecognizer(tap)
        tap.rx.event.bind { (eventTap) in
            self.onSeeMore?(!self.isSeeMore.value)
            self.isSeeMore.accept(!self.isSeeMore.value)
        }.disposed(by: disposeBag)
    }
    
    private func setupViews(){
        self.view.layer.cornerRadius = 4
        self.productTitle.text = "xltt_customer_detail_product_title".localized
        self.seeMoreLabel.text = "xltt_customer_detail_product_see_more".localized
        
    }
    
    private func setupTableView(){
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        self.contentTableView.register(UINib(nibName: XLTTCustomerProductCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: XLTTCustomerProductCell.cellIdentifier)
        
        let ds = RxTableViewSectionedReloadDataSource<CustomerProductSection>(configureCell: { dataSource, tableView, indexPath, data in
            
            let cell : XLTTCustomerProductCell = tableView.dequeueReusableCell(withIdentifier: XLTTCustomerProductCell.cellIdentifier, for: indexPath) as! XLTTCustomerProductCell
            
            cell.data = data
            cell.selectionStyle = .none
            
            return cell
        })
        
        self.dataSource = ds
        self.contentTableView.allowsSelection = false
        
    }
    
}
