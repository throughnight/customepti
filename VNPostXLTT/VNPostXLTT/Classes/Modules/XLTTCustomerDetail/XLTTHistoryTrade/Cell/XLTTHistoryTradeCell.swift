//
//  XLTTHistoryTradeCell.swift
//  VNPostXLTT
//
//  Created by AEGONA on 11/5/20.
//

import Foundation

class XLTTHistoryTradeCell: UITableViewCell {
    static let cellIdentifier = "XLTTHistoryTradeCell"
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var lbBefore: UILabel!
    @IBOutlet weak var lbAfter: UILabel!
    @IBOutlet weak var lbReason: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var viewAfter: UIView!
    @IBOutlet weak var viewBefore: UIView!
    
    var data: XLTTStatusHistory? {
        didSet {
            let stringBold = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor(red: 0, green: 0, blue: 0, alpha: 1)]
            let stringNomal = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor(red: 0, green: 0, blue: 0, alpha: 1)]
            
            let attributedString1 = NSMutableAttributedString(string: "\(data?.addedUser ?? "") ", attributes:stringBold)
            let attributedString2 = NSMutableAttributedString(string: "xltt_message_change_status".localized, attributes:stringNomal)
            
            attributedString1.append(attributedString2)
            self.title.attributedText = attributedString1
            
            lbBefore.text = data?.saleStatusBefore
            lbAfter.text = data?.saleStatusAfter
            lbDate.text = data?.addedStamp.xlttDateToStringCustomer(endFormat: "dd/MM/yyyy - HH:mm:ss")
            if data?.historyComment != "" {
                lbReason.isHidden = false
                lbReason.text = data?.historyComment
            } else {
                lbReason.isHidden = true
            }
            if data?.saleStatusAfter != nil && data?.saleStatusAfter != "" {
                if data?.saleStatusAfter.contains("Thất bại") == true {
                    viewAfter.backgroundColor = UIColor(hex: "feedec")
                    lbAfter.textColor = .red
                    lbReason.textColor = .red
                }
                if data?.saleStatusAfter.contains("Đang xử lý") == true {
                    viewAfter.backgroundColor = UIColor(hex: "fff6f5")
                    lbAfter.textColor = UIColor(hex: "F07700")
                }
                if data?.saleStatusAfter.contains("Thành công") == true {
                    viewAfter.backgroundColor = UIColor(hex: "e5f7f0")
                    lbAfter.textColor = UIColor(hex: "00B16A")
                    lbReason.textColor = UIColor(hex: "00B16A")
                }
            }
            if data?.saleStatusBefore != nil && data?.saleStatusBefore != "" {
                if data?.saleStatusBefore.contains("Đang xử lý") == true {
                    viewBefore.backgroundColor = UIColor(hex: "fff6f5")
                    lbBefore.textColor = UIColor(hex: "F07700")
                }
            }
        }
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
