//
//  XLTTHistoryTradeVC.swift
//  VNPostXLTT
//
//  Created by AEGONA on 11/5/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift

typealias HistoryTradeSection = SectionModel<String,XLTTStatusHistory>
typealias XLTTHistoryTradeSource = RxTableViewSectionedReloadDataSource<HistoryTradeSection>

class XLTTHistoryTradeVC : XLTTBaseViewController {
    
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var imgEmpty: UIImageView!
    @IBOutlet weak var lbEmpty: UILabel!
    
    private var dataSource: XLTTHistoryTradeSource!
    var dataSubject : Driver<[HistoryTradeSection]>!
    
    var data = BehaviorRelay<XLTTLeadDetailModel>(value: XLTTLeadDetailModel())
    var history = BehaviorRelay<[XLTTStatusHistory]>(value: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupListView()
        lbEmpty.text = "xltt_title_empty".localized
        dataSubject = history.asDriver().map({ (data) -> [HistoryTradeSection] in
            return [HistoryTradeSection(model: "HistoryTrade", items: data)]
        })
        dataSubject.drive(self.tbView.rx.items(dataSource: self.dataSource)).disposed(by: disposeBag)
        
        data.subscribe(onNext: { [weak self] lead in
            guard let self = self else {return}
            
            if lead.statusHistory.count > 0 {
                self.imgEmpty.isHidden = true
                self.lbEmpty.isHidden = true
            } else {
                self.imgEmpty.isHidden = false
                self.lbEmpty.isHidden = false
            }
            self.history.accept(lead.statusHistory)
        }).disposed(by: disposeBag)
    }
}

extension XLTTHistoryTradeVC : XLTTViewControllerProtocal {
    func setupViewModel() {
        
    }
    
    func setupListView() {
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        self.tbView.register(UINib(nibName: XLTTHistoryTradeCell.cellIdentifier, bundle: bundle), forCellReuseIdentifier: XLTTHistoryTradeCell.cellIdentifier)
        
        let ds = RxTableViewSectionedReloadDataSource<HistoryTradeSection>(configureCell: { dataSource, tableView, indexPath, data in
            
            let cell : XLTTHistoryTradeCell = tableView.dequeueReusableCell(withIdentifier: XLTTHistoryTradeCell.cellIdentifier, for: indexPath) as! XLTTHistoryTradeCell
            
            cell.data = data
            
            return cell
        })
        
        self.dataSource = ds
        self.tbView.allowsSelection = false
    }
}
