//
//  XLTTCustomerDetailVC.swift
//  VNPostXLTT
//
//  Created by m2 on 10/27/20.
//

import Foundation
import UIKit
import RxCocoa
import RxDataSources
import RxSwift


class XLTTCustomerDetailVC: XLTTBaseViewController {
    private var loading: XLTTLoadingProgressView!
    var isHistory = false
    
    @IBOutlet weak var toolbarTitle: UILabel!
    
    @IBOutlet weak var customerInforView: UIView!
    @IBOutlet weak var customerInfoViewConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var productInfoView: UIView!
    @IBOutlet weak var productInfoViewConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var tabInfoView: UIView!
    @IBOutlet weak var tabInfoViewConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var viewModel: XLTTCustomerDetailVM!
    
    private var customerVC: XLTTCustomerInforVC!
    private var productVC : XLTTCustomerProductVC!
    private var infoSeekVC: XLTTInfoSeekVC!
    private var noteVC: XLTTNoteVC!
    private var historyTradeVC: XLTTHistoryTradeVC!
    private var historyAllocationVC: XLTTHistoryAllocationVC!
    
    private var dialogAddNote: XLTTDialogAddNoteVC!
    
    private let tabController = XLTTUISimpleSlidingTabController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loading = XLTTLoadingProgressView(uiScreen: UIScreen.main)
        setupTab()
        setupViewModel()
        setupViews()
        customerInfor()
        
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func setupViews() {
        if isHistory {
            toolbarTitle.text = "xltt_detail_history_title".localized
        } else {
            toolbarTitle.text = "xltt_customer_detail_toolbar_title".localized
        }
        
        self.viewModel.input?.dataLead.subscribe(onNext: { [weak self] data in
            guard let self = self else {return}
            if data.detailInfoCus != nil {
                self.customerVC.loadData(data: data.detailInfoCus, status: data.status, isPin: data.flag, isAction: self.viewModel.input?.isAction.value ?? false, hidePin : self.isHistory)
                self.updateViewCustomerInfo(data: data.detailInfoCus)
            }
            if data.infoProduct != nil {
//                self.productInfoView.isHidden = false
                self.productInfo()
                self.productVC.infoProduct.accept(data.infoProduct ?? XLTTInfoProduct())
                self.updateViewProductInfo(data: data.infoProduct, seeMore: false)
            }
            self.infoSeekVC?.loadData(data: data.infoRegister)
            self.updateViewTabInfoSeek(data: data.infoRegister)
            self.noteVC.data.accept(data)
            self.historyTradeVC.data.accept(data)
            if XLTTManager.shared.userProfileType == XLTTProfileType.SUPERVISOR.rawValue {
                self.historyAllocationVC.data.accept(data)
            }
        })
    }
    
    private func customerInfor(){
        customerVC = XLTTCustomerInforVC(nibName: "XLTTCustomerInforVC", bundle: vnpXLTTBundle)
        customerVC.type = self.viewModel.input?.type.value ?? 0
        customerVC.onAssignLead = { item in
            let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
            let assignDialog = XLTTAssignLeadVC(nibName: "XLTTAssignLeadVC", bundle: bundle)
            assignDialog.modalPresentationStyle = .overFullScreen
            assignDialog.viewModel = XLTTAssignLeadVM(
                input: XLTTAssignLeadVM.Input(search: BehaviorRelay<String>(value: ""),
                                              triggerLoad: BehaviorRelay<Bool>(value: true),
                                              triggerLoadMore: BehaviorRelay<Bool>(value: false),
                                              data: BehaviorRelay<[XLTTSaleInfo]>(value: []),
                                              index: BehaviorRelay<Int>(value: 1),
                                              size: BehaviorRelay<Int>(value: 20),
                                              leadAssign: BehaviorRelay<XLTTCustomer>(value: self.viewModel.input?.initLead.value ?? XLTTCustomer()),
                                              isLoading: BehaviorRelay<Bool>(value: false),
                                              userSelect: BehaviorRelay<XLTTSaleInfo>(value: XLTTSaleInfo()),
                                              triggerAssign: BehaviorRelay<Bool>(value: false)),
                dependency: XLTTCustomerService())
            assignDialog.viewControllerParent = self
            assignDialog.onDone = {
                self.navigationController?.popViewController(animated: true)
            }
            
            self.parent?.present(assignDialog, animated: false, completion: nil)
        }
        customerVC.onAcceptLead = { item in
            let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
            let dialogAccept = XLTTDialogOptions(nibName: "XLTTDialogOptions", bundle: bundle)
            dialogAccept.modalPresentationStyle = .overFullScreen
            dialogAccept.titleDialog = BehaviorRelay<String>(value: "xltt_title_action".localized)
            dialogAccept.messageDialog = BehaviorRelay<String>(value: "xltt_title_handing".localized)
            dialogAccept.colorMessageDialog = BehaviorRelay<UIColor?>(value: UIColor(hex: "127BCA"))
            dialogAccept.onDoneActions = { _ in
                dialogAccept.dismiss(animated: true, completion: nil)
                self.viewModel.input?.triggerAccept.accept(true)
            }
            
            dialogAccept.onCancelAction = {
                dialogAccept.dismiss(animated: true, completion: nil)
            }
            
            self.parent?.present(dialogAccept, animated: false, completion: nil)
        }
        customerVC.onDenyLead = { item in
            let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
            let dialogDeny = XLTTDialogOptions(nibName: "XLTTDialogOptions", bundle: bundle)
            dialogDeny.modalPresentationStyle = .overFullScreen
            dialogDeny.titleDialog = BehaviorRelay<String>(value: "xltt_title_action".localized)
            dialogDeny.messageDialog = BehaviorRelay<String>(value: "xltt_home_chart_failure_group".localized)
            dialogDeny.colorMessageDialog = BehaviorRelay<UIColor?>(value: UIColor(hex: "F64744"))
            dialogDeny.onDoneActions = { _ in
                dialogDeny.dismiss(animated: true, completion: nil)
                self.viewModel.input?.triggerDeny.accept(true)
            }
            
            dialogDeny.onCancelAction = {
                dialogDeny.dismiss(animated: true, completion: nil)
            }
            
            self.parent?.present(dialogDeny, animated: false, completion: nil)
        }
        customerVC.onPinLead = {
            let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
            let dialogPin = XLTTDialogOptions(nibName: "XLTTDialogOptions", bundle: bundle)
            dialogPin.modalPresentationStyle = .overFullScreen
            dialogPin.titleDialog = BehaviorRelay<String>(value: "xltt_title_pin".localized)
            dialogPin.messageDialog = BehaviorRelay<String>(value: self.viewModel.input?.dataLead.value.flag ?? false ? "xltt_un_pin_title".localized : "xltt_pin_title".localized)
            dialogPin.colorMessageDialog = BehaviorRelay<UIColor?>(value: UIColor(hex: "127BCA"))
            dialogPin.onDoneActions = { _ in
                dialogPin.dismiss(animated: true, completion: nil)
                self.viewModel.input?.triggerPin.accept(true)
            }
            
            dialogPin.onCancelAction = {
                dialogPin.dismiss(animated: true, completion: nil)
            }
            
            self.parent?.present(dialogPin, animated: false, completion: nil)
        }
        customerVC.onSuccessLead = { item in
            let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
            let dialogSuccess = XLTTDialogOptions(nibName: "XLTTDialogOptions", bundle: bundle)
            dialogSuccess.modalPresentationStyle = .overFullScreen
            dialogSuccess.titleDialog = BehaviorRelay<String>(value: "xltt_title_action".localized)
            dialogSuccess.messageDialog = BehaviorRelay<String>(value: "xltt_home_chart_success".localized)
            dialogSuccess.colorMessageDialog = BehaviorRelay<UIColor?>(value: UIColor(hex: "00B16A"))
            dialogSuccess.isInput = BehaviorRelay<Bool?>(value: true)
            dialogSuccess.onDoneActions = { text in
                dialogSuccess.dismiss(animated: true, completion: nil)
                self.viewModel.input?.noteSuccess.accept(text ?? "")
                self.viewModel.input?.triggerSuccess.accept(true)
            }
            
            dialogSuccess.onCancelAction = {
                dialogSuccess.dismiss(animated: true, completion: nil)
            }
            
            self.parent?.present(dialogSuccess, animated: false, completion: nil)
        }
        customerVC.onFailLead = { item in
            let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
            let dialogReason = XLTTDialogReasonVC(nibName: "XLTTDialogReasonVC", bundle: bundle)
            dialogReason.modalPresentationStyle = .overFullScreen
            dialogReason.viewModel = XLTTDialogReasonVM(
                input: XLTTDialogReasonVM.Input(search: BehaviorRelay<String>(value: ""),
                                                triggerLoad: BehaviorRelay<Bool>(value: true),
                                                triggerLoadMore: BehaviorRelay<Bool>(value: false),
                                                data: BehaviorRelay<[XLTTOptionsFilter]>(value: []),
                                                index: BehaviorRelay<Int>(value: 1),
                                                size: BehaviorRelay<Int>(value: 20),
                                                reasonSelect: BehaviorRelay<XLTTOptionsFilter>(value: XLTTOptionsFilter()),
                                                isLoading: BehaviorRelay<Bool>(value: false),
                                                triggerReason: BehaviorRelay<Bool>(value: false)),
                dependency: XLTTCommonService())
            
            dialogReason.onDone = { text in
                dialogReason.dismiss(animated: true, completion: nil)
                self.viewModel.input?.noteFailure.accept(text ?? "")
                self.viewModel.input?.triggerFailure.accept(true)
            }
            self.parent?.present(dialogReason, animated: false, completion: nil)
            
        }
        
        self.customerInforView.addSubview(customerVC.view)
        customerVC.view.frame = self.customerInforView.bounds
    }
    
    private func productInfo(){
        productVC = XLTTCustomerProductVC(nibName: "XLTTCustomerProductVC", bundle: vnpXLTTBundle)
        self.productInfoView.addSubview(productVC.view)
        productVC.view.frame = self.productInfoView.bounds
        
        productVC.onSeeMore = { bool in
            self.updateViewProductInfo(data: self.viewModel.input?.dataLead.value.infoProduct, seeMore: bool)
        }
    }
    
    private func setupTab(){
        noteVC = XLTTNoteVC(nibName: "XLTTNoteVC", bundle: vnpXLTTBundle)
        noteVC.isHistory = self.isHistory
        infoSeekVC = XLTTInfoSeekVC(nibName: "XLTTInfoSeekVC", bundle: vnpXLTTBundle)
        historyTradeVC = XLTTHistoryTradeVC(nibName: "XLTTHistoryTradeVC", bundle: vnpXLTTBundle)
        if XLTTManager.shared.userProfileType == XLTTProfileType.SUPERVISOR.rawValue {
            historyAllocationVC = XLTTHistoryAllocationVC(nibName: "XLTTHistoryAllocationVC", bundle: vnpXLTTBundle)
        }
        
        self.tabInfoView.addSubview(tabController.view)
        
        self.tabController.view.frame = self.tabInfoView.bounds
        navigationController?.navigationBar.barTintColor = UIColor(hex: "EEF3FF")
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.barStyle = .black
        self.view.window?.makeKeyAndVisible()
        
        infoSeekVC.onChangeHeightView = { height in
            //            let heightView = height < 400 ? 400 : height
            //            self.tabInfoViewConstraintHeight.constant = CGFloat(heightView)
            //            self.tabInfoView.layoutIfNeeded()
            //            self.tabController.view.layoutIfNeeded()
        }
        
        noteVC.onAddNote =  {
            self.dialogAddNote = XLTTDialogAddNoteVC(nibName: "XLTTDialogAddNoteVC", bundle: self.vnpXLTTBundle)
            self.dialogAddNote.modalPresentationStyle = .overFullScreen
            self.dialogAddNote.onAddNote = { text in
                self.viewModel.input?.note.accept(text)
                self.viewModel.input?.triggerAddNote.accept(true)
            }
            
            self.parent?.present(self.dialogAddNote, animated: false, completion: nil)
        }
        
        tabController.addItem(item: infoSeekVC, title: "xltt_tab_seek".localized)
        tabController.addItem(item: noteVC, title: "xltt_tab_note".localized)
        tabController.addItem(item: historyTradeVC, title: "xltt_tab_history_trade".localized)
        if XLTTManager.shared.userProfileType == XLTTProfileType.SUPERVISOR.rawValue {
            tabController.addItem(item: historyAllocationVC, title: "xltt_tab_history_handing".localized)
        }
        tabController.setHeaderActiveColor(color: UIColor(hex: "127BCA"))
        tabController.setHeaderInActiveColor(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.6))
        tabController.setHeaderBackgroundColor(color: .white) // default white
        tabController.setIndicatorActiveColor(color: UIColor(hex: "127BCA"))
        tabController.setStyle(style: .flexible) // default fixed
        tabController.fontSize = 14
        tabController.build() // build
        
    }
}

extension XLTTCustomerDetailVC: XLTTViewControllerProtocal {
    func setupViewModel() {
        if let ip = self.viewModel.input {
            bindingInput(input: ip)
        }
        if let op = self.viewModel.output {
            bindingOutput(output: op)
        }
    }
    
    private func bindingInput(input: XLTTCustomerDetailVM.Input){
        input.isLoading.subscribe(onNext: { [weak self] isLoading in
            guard let self = self else {return}
            if isLoading {
                self.loading.startAnimating()
            } else {
                self.loading.stopAnimating()
            }
        }).disposed(by: disposeBag)
    }
    
    private func bindingOutput(output: XLTTCustomerDetailVM.Output){
        output.detailResult.subscribe(onNext: { [weak self] result in
            guard let self = self else {return}
            if result.success {
                self.viewModel.input?.dataLead.accept(result.data ?? XLTTLeadDetailModel())
            } else {
                self.showErrorDialog(errorMsg: result.message)
            }
        }).disposed(by: disposeBag)
        
        output.acceptResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.success {
                let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
                let dialogSuccess = XLTTDialogMessage(nibName: "XLTTDialogMessage", bundle: bundle)
                dialogSuccess.modalPresentationStyle = .overFullScreen
                dialogSuccess.titleDialog = BehaviorRelay<String>(value: "xltt_action_success".localized)
                dialogSuccess.messageDialog = BehaviorRelay<String>(value: "xltt_action_message_success".localized)
                dialogSuccess.status = BehaviorRelay<Bool>(value: true)
                dialogSuccess.onDoneActions = {
                    dialogSuccess.dismiss(animated: true, completion: nil)
                    self.viewModel.input?.triggerDetail.accept(true)
                }
                self.parent?.present(dialogSuccess, animated: false, completion: nil)
            } else {
                self.showErrorDialog(errorMsg: result.message)
            }
        }).disposed(by: disposeBag)
        
        output.denyResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.success {
                let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
                let dialogSuccess = XLTTDialogMessage(nibName: "XLTTDialogMessage", bundle: bundle)
                dialogSuccess.modalPresentationStyle = .overFullScreen
                dialogSuccess.titleDialog = BehaviorRelay<String>(value: "xltt_action_success".localized)
                dialogSuccess.messageDialog = BehaviorRelay<String>(value: "xltt_action_message_success".localized)
                dialogSuccess.status = BehaviorRelay<Bool>(value: true)
                dialogSuccess.onDoneActions = {
                    dialogSuccess.dismiss(animated: true, completion: nil)
                    self.viewModel.input?.triggerDetail.accept(true)
                }
                self.parent?.present(dialogSuccess, animated: false, completion: nil)
            } else {
                self.showErrorDialog(errorMsg: result.message)
            }
        }).disposed(by: disposeBag)
        
        output.denyResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.success {
                self.viewModel.input?.triggerDetail.accept(true)
            } else {
                self.showErrorDialog(errorMsg: result.message)
            }
        }).disposed(by: disposeBag)
        
        output.pinResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.success {
                self.viewModel.input?.triggerDetail.accept(true)
            } else {
                self.showErrorDialog(errorMsg: result.message)
            }
        }).disposed(by: disposeBag)
        
        output.successResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.success {
                self.viewModel.input?.triggerDetail.accept(true)
                let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
                let dialogSuccess = XLTTDialogMessage(nibName: "XLTTDialogMessage", bundle: bundle)
                dialogSuccess.modalPresentationStyle = .overFullScreen
                dialogSuccess.titleDialog = BehaviorRelay<String>(value: "xltt_action_success".localized)
                dialogSuccess.messageDialog = BehaviorRelay<String>(value: "xltt_action_message_success".localized)
                dialogSuccess.status = BehaviorRelay<Bool>(value: true)
                dialogSuccess.onDoneActions = {
                    dialogSuccess.dismiss(animated: true, completion: nil)
                }
                self.parent?.present(dialogSuccess, animated: false, completion: nil)
            } else {
                self.showErrorDialog(errorMsg: result.message)
            }
        }).disposed(by: disposeBag)
        
        output.failureResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.success {
                self.viewModel.input?.triggerDetail.accept(true)
                let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
                let dialogSuccess = XLTTDialogMessage(nibName: "XLTTDialogMessage", bundle: bundle)
                dialogSuccess.modalPresentationStyle = .overFullScreen
                dialogSuccess.titleDialog = BehaviorRelay<String>(value: "xltt_action_success".localized)
                dialogSuccess.messageDialog = BehaviorRelay<String>(value: "xltt_action_message_success".localized)
                dialogSuccess.status = BehaviorRelay<Bool>(value: true)
                dialogSuccess.onDoneActions = {
                    dialogSuccess.dismiss(animated: true, completion: nil)
                }
                self.parent?.present(dialogSuccess, animated: false, completion: nil)
            } else {
                self.showErrorDialog(errorMsg: result.message)
            }
        }).disposed(by: disposeBag)
        
        output.noteResult.subscribe(onNext: {[weak self] result in
            guard let self = self else {return}
            if result.success {
                self.viewModel.input?.triggerDetail.accept(true)
                self.dialogAddNote.dismiss(animated: false, completion: nil)
            } else {
                self.showErrorDialog(errorMsg: result.message)
            }
        }).disposed(by: disposeBag)
    }
    
    func setupListView() {
        
    }
    
    func updateViewCustomerInfo(data: XLTTCustomer?){
        var height = 160
        if !(self.viewModel.input?.isAction.value ?? false) {
            if self.viewModel.input?.dataLead.value.status == XLTTCustomerType.CUSTOMER_STATUS_SUCCESS.rawValue || self.viewModel.input?.dataLead.value.status == XLTTCustomerType.CUSTOMER_STATUS_FAIL.rawValue {
                height -= 40
            } else {
                height -= 60
            }
        } else {
            if self.viewModel.input?.type.value == XLTTPotenitalcus.HISTORY.rawValue {
                if self.viewModel.input?.dataLead.value.status != XLTTCustomerType.CUSTOMER_STATUS_SUCCESS.rawValue && self.viewModel.input?.dataLead.value.status != XLTTCustomerType.CUSTOMER_STATUS_FAIL.rawValue {
                    height -= 60
                }
            }
        }
        let widthView = self.view.frame.size.width
        let widthLine = ((widthView * 2) / 3) - 20
        if data?.customerName != "" {
            let nameLength = data?.customerName.count ?? 0
            var addNum = (nameLength * 9) / Int(widthLine)
            if addNum == 0 { addNum = 1 }
            height += (addNum * 50)
        }
        if data?.email != "" {
            let nameLength = data?.email.count ?? 0
            var addNum = (nameLength * 9) / Int(widthLine)
            if addNum == 0 { addNum = 1 }
            height += (addNum * 30)
        }
        if data?.phoneNumber != "" {
            let nameLength = data?.phoneNumber.count ?? 0
            var addNum = (nameLength * 9) / Int(widthLine)
            if addNum == 0 { addNum = 1 }
            height += (addNum * 30)
        }
        if data?.temporaryAddress != nil && data?.temporaryAddress?.getFullAddress() != "" {
            let lengthAddress = data?.temporaryAddress?.getFullAddress().count ?? 1
            var addNum = lengthAddress / 30
            addNum += 1
            if addNum == 1 {
                height += (addNum * 30)
            } else {
                height += (addNum * 25)
            }
        }
        self.customerInfoViewConstraintHeight.constant = CGFloat(height)
        self.customerInforView.layoutIfNeeded()
    }
    
    func updateViewProductInfo(data: XLTTInfoProduct?, seeMore: Bool){
        if data?.questionAnswer.count ?? 0 > 0 {
            let limit = self.productVC.itemLimit
            let widthView = self.view.frame.size.width
            let widthLine = (widthView / 2) - 20
            var height = 100
            if data?.questionAnswer.count ?? 0 <= limit {
                data?.questionAnswer.forEach({ (item) in
                    var qNum = (item.displayText.count * 9) / Int(widthLine)
                    qNum += 1
                    height += (qNum * 30)
                })
            } else {
                if seeMore {
                    data?.questionAnswer.forEach({ (item) in
                        var qNum = (item.displayText.count * 9) / Int(widthLine)
                        qNum += 1
                        height += (qNum * 30)
                    })
                } else {
                    for index in 0...limit {
                        let item = data?.questionAnswer[index]
                        var qNum = (item?.displayText.count ?? 1 * 9) / Int(widthLine)
                        qNum += 1
                        height += (qNum * 30)
                    }
                }
                height += 50
            }
            self.productInfoViewConstraintHeight.constant = CGFloat(height)
            self.productInfoView.layoutIfNeeded()
        }
        
    }
    
    private func updateViewTabInfoSeek(data: [XLTTInfoSeek]){
        var height = 0
        data.forEach { (item) in
            height += heightCellInfoSeek(data: item)
        }
        if height < 500 {
            height = 500
        } else {
            height += 80
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.tabInfoViewConstraintHeight.constant = CGFloat(height)
            self.tabInfoView.layoutIfNeeded()
//            self.tabController.updateViewConstraints()
            self.tabController.setCurrentPosition(position: self.tabController.currentPosition)
        }
        
    }
    
    private func heightCellInfoSeek(data: XLTTInfoSeek?) -> Int {
        var height = 60
        if data?.questionAnswer.count ?? 0 > 0 {
            let widthView = self.view.frame.size.width
            let widthLine = (widthView / 2) - 20
            data?.questionAnswer.forEach({ (item) in
                var qNum = (item.questionName.count * 9) / Int(widthLine)
                qNum += 1
                height += qNum == 1 ? (qNum * 30) : (qNum * 20)
            })
        }
        return height
    }
}
