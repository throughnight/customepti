//
//  XLTTMessageManager.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/29/20.
//

import Foundation
import SwiftMessages
import RxCocoa
import RxSwift

class XLTTMessageManager {
    
    static let shared = XLTTMessageManager()
    
    var sharedConfig: SwiftMessages.Config {
        var config = SwiftMessages.Config()
        config.presentationStyle = .bottom
        config.presentationContext = .window(windowLevel: UIWindow.Level(rawValue: UIWindow.Level(rawValue: UIWindow.Level.statusBar.rawValue).rawValue))
        config.duration = .forever
        config.dimMode = .gray(interactive: true)
        config.interactiveHide = true
        config.preferredStatusBarStyle = .lightContent
        return config
    }
    
}

// MARK: - Message notify
extension XLTTMessageManager {
    
    func showMessage(messageType type: Theme, withTitle title: String = "", message: String, completion: (() -> Void)? = nil, duration: SwiftMessages.Duration = .seconds(seconds: 2)) {
        
        var config = sharedConfig
        
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureTheme(type)
        view.button?.isHidden = true
        view.configureContent(title: title, body: message)
        view.configureDropShadow()
        config.duration = duration
        config.eventListeners = [{ event in
            switch event {
            case .didHide:
                completion?()
            default:
                break
            }}]
        
        SwiftMessages.show(config: config, view: view)
    }
    
}
