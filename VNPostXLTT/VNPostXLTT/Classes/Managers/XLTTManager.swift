//
//  XLTTManager.swift
//  VNPostXLTT
//
//  Created by m2 on 10/20/20.
//

import Foundation
import RxCocoa
import RxSwift
import UIKit
import IQKeyboardManagerSwift

public class XLTTManager {
    
    // MARK: Singleton
    public static let shared = XLTTManager()
    
    private init() {
        // Keyboard
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
        IQKeyboardManager.shared.toolbarManageBehaviour = .byPosition
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    }
    
    var accessToken: String?
    
    var userName: String?
    
    var userProfileType: Int?
    
    public func setEnviroment(_ enviroment: XLTTEnvironment) {
        XLTTConfiguration.shared.environment = enviroment
    }
    
    public func setAccessToken(_ token: String) {
        self.accessToken = token
    }
    
    public func setUserProfileType(_ profileType: Int){
        self.userProfileType = profileType
    }
    
    public func setUserName(_ userName: String) {
        self.userName = userName
    }
    
    public func openIPA(_ navigationController: UINavigationController?, token: String) {
        self.accessToken = token
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        let home = XLTTHomeVC(nibName: "XLTTHomeVC", bundle: bundle)
        home.viewModel = XLTTHomeVM(token: token);
        home.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(home, animated: false)
        
    }
}
