//
//  XLTTPotentialCus.swift
//  VNPostXLTT
//
//  Created by m2 on 10/27/20.
//

import Foundation
import Moya
import Alamofire

enum XLTTPotentialCus {
    case getMobileGetDataHomeAsync(TypeRequest)
}

extension XLTTPotentialCus : TargetType {
    
    var baseURL: URL {
        switch self {
        case .getMobileGetDataHomeAsync:
            return URL(string: "\(XLTTConfiguration.shared.environment.apiDomain)/PotentialCus/\(XLTTConfiguration.shared.environment.apiVersion)")!
        }
    }
    
    var path: String {
        switch self {
        case .getMobileGetDataHomeAsync:
            return "/Mobile_GetDataHome"
        }
    }
    
    var encoding : ParameterEncoding {
        switch method {
        case .get:
            return URLEncoding.default
        default:
            return JSONEncoding()
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getMobileGetDataHomeAsync:
            return Moya.Method.get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getMobileGetDataHomeAsync(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.default)
        default:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        var defaultHeaders = ["Content-Type":"application/json"]
        if let accessToken = XLTTManager.shared.accessToken {
            defaultHeaders["Authorization"] = "Bearer \(accessToken)"
        }
        
        return defaultHeaders
    }
    
    private func parseDictionaryToString(dict: Dictionary<String, Any>) -> String {
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dict,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .utf8)
            
            return theJSONText!
        }
        
        return ""
    }
    
}

struct TypeRequest : XLTTAPIRequestProtocol {
    
    var type : String
    
    func toParameters() -> [String : Any] {
        return ["type" : type]
    }
}
