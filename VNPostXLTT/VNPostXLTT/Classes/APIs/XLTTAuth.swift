//
//  XLTTAuth.swift
//  VNPostXLTT
//
//  Created by m2 on 10/27/20.
//

import Foundation
import Moya
import Alamofire

enum XLTTAuth {
    case getToken(TokenRequest)
}

extension XLTTAuth: TargetType{
    var baseURL: URL {
        return URL(string: "\(XLTTConfiguration.shared.environment.apiDomain)/Auth/\(XLTTConfiguration.shared.environment.apiVersion)")!
    }
    
    var path: String {
        switch self {
        case .getToken:
            return "/GetTokenMobile"
        }
    }
    
    var method: Moya.Method {
        return Moya.Method.get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getToken(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        let defaultHeaders = ["Content-Type":"application/json"]
        return defaultHeaders
    }
    
    var url : String {
        return "\(baseURL)\(path)"
    }
    
    var encoding : ParameterEncoding {
        switch method {
        case .get:
            return URLEncoding.default
        default:
            return JSONEncoding()
        }
    }
    
}

struct TokenRequest : XLTTAPIRequestProtocol {
    
    var token : String
    
    func toParameters() -> [String : Any] {
        return ["tokenMobile" : token]
    }
}
