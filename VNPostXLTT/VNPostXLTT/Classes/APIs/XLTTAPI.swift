//
//  XLTTAPI.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/26/20.
//

import Foundation
import Moya
import Alamofire

enum XLTTAPI {
    case getDemoApi
    case getCustomer(LeadCustomerRequest)
    case customerOfMeAction(CustomerActionRequest)
    case customerOfMeDeny(CustomerActionRequest)
    case getListUserAssign(AssignLeadRequest)
    case assignLead(data: XLTTAssignModel)
    case getListTeamSale
    case getListProduct(PagingRequest)
    case getProvinces
    case getDistrict(id: String)
    case getWards(id: String)
    case filterCustomer(FilterRequest)
    case getHistory(LeadCustomerRequest)
    case getEmployees(EmployeeRequest)
    case filterHistory(FilterRequest)
    case getCustomerDetail(CustomerDetailRequest)
    case pinLead(PinCustomerRequest)
    case getListReason(PagingRequest)
    case addNote(data: XLTTAssignModel)
}

extension XLTTAPI : TargetType {
    
    var baseURL: URL {
        switch self {
        case .getDemoApi:
            return URL(string: XLTTConfiguration.shared.environment.tokenDomain)!
        case .getCustomer, .customerOfMeAction, .customerOfMeDeny, .getListUserAssign, .assignLead, .filterCustomer, .getHistory, .filterHistory, .getCustomerDetail, .pinLead, .addNote:
            return URL(string: "\(XLTTConfiguration.shared.environment.apiDomain)/PotentialCus/\(XLTTConfiguration.shared.environment.apiVersion)")!
        case .getListTeamSale, .getListProduct, .getProvinces, .getDistrict, .getWards, .getEmployees, .getListReason:
            return URL(string: "\(XLTTConfiguration.shared.environment.apiDomain)/Common/\(XLTTConfiguration.shared.environment.apiVersion)")!
        }
    }
    
    var path: String {
        switch self {
        case .getDemoApi:
            return "/connect/token"
        case .getCustomer:
            return "/PotentialCustomer"
        case .customerOfMeAction:
            return "/Action"
        case .customerOfMeDeny:
            return "/Refuse"
        case .getListUserAssign:
            return "/Mobile_GetListUserTeamAssign"
        case .assignLead:
            return "/Mobile_AssignLeadUser"
        case .getListTeamSale:
            return "/GetListTeamSale"
        case .getListProduct:
            return "/ListProducts"
        case .getProvinces:
            return "/Provinces"
        case .getDistrict(let id):
            return "/Provinces/\(id)/Districts"
        case .getWards(let id):
            return "/Districts/\(id)/Wards"
        case .filterCustomer:
            return "/PotentialCustomer"
        case .getHistory:
            return "/TransactionHistory"
        case .getEmployees:
            return "/ListUsersInTeam"
        case .filterHistory:
            return "/TransactionHistory"
        case .getCustomerDetail:
            return "/Mobile_CustomerGetDetail"
        case .pinLead:
            return "/Flag"
        case .getListReason:
            return "/GetAllReason"
        case .addNote:
            return "/Mobile_CreateComment"
        }
    }
    
    var encoding : ParameterEncoding {
        switch method {
        case .get:
            return URLEncoding.default
        default:
            return JSONEncoding()
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getDemoApi, .customerOfMeDeny, .customerOfMeAction, .assignLead, .addNote:
            return Moya.Method.post
        case .pinLead:
            return Moya.Method.put
        default:
            return Moya.Method.get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getCustomer(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.default)
        case .customerOfMeAction(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.queryString)
        case .customerOfMeDeny(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.queryString)
        case .getListUserAssign(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.default)
        case .assignLead(let data):
            return .requestJSONEncodable(data)
        case .getListProduct(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.default)
        case .filterCustomer(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.default)
        case .getHistory(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.default)
        case .getEmployees(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.default)
        case .filterHistory(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.default)
        case .getCustomerDetail(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.default)
        case .pinLead(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.queryString)
        case .getListReason(let request):
            return .requestParameters(parameters: request.toParameters(), encoding: URLEncoding.queryString)
        case .addNote(let data):
            return .requestJSONEncodable(data)
        default:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        var defaultHeaders = ["Content-Type":"application/json"]
        if let accessToken = XLTTManager.shared.accessToken {
            defaultHeaders["Authorization"] = "Bearer \(accessToken)"
        }
        
        return defaultHeaders
    }
    
    private func parseDictionaryToString(dict: Dictionary<String, Any>) -> String {
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dict,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .utf8)
            
            return theJSONText!
        }
        
        return ""
    }
    
}

struct JsonArrayEncoding: Moya.ParameterEncoding {
    
    public static var `default`: JsonArrayEncoding { return JsonArrayEncoding() }
    
    
    /// Creates a URL request by encoding parameters and applying them onto an existing request.
    ///
    /// - parameter urlRequest: The request to have parameters applied.
    /// - parameter parameters: The parameters to apply.
    ///
    /// - throws: An `AFError.parameterEncodingFailed` error if encoding fails.
    ///
    /// - returns: The encoded request.
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: [String: Any]?) throws -> URLRequest {
        var req = try urlRequest.asURLRequest()
        let json = try JSONSerialization.data(withJSONObject: parameters!["jsonArray"]!, options: JSONSerialization.WritingOptions.prettyPrinted)
        req.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        req.httpBody = json
        return req
    }
    
}

struct LeadCustomerRequest : XLTTAPIRequestProtocol {
    
    var search : String = ""
    var type : Int
    var pageIndex : Int
    var pageSize : Int
    
    func toParameters() -> [String : Any] {
        return ["search" : search, "type" : type, "pageIndex": pageIndex, "pageSize": pageSize]
    }
}

struct CustomerActionRequest : XLTTAPIRequestProtocol {
    
    var id : String = ""
    var type: Int
    var note: String?
    
    func toParameters() -> [String : Any] {
        var params : [String:Any] = ["id" : id, "type" : type]
        if note != nil {
            params["note"] = note
        }
        return params
    }
}

struct AssignLeadRequest : XLTTAPIRequestProtocol {
    
    var leadAssignId : String
    var search : String
    var pageIndex : Int
    var pageSize : Int
    
    func toParameters() -> [String : Any] {
        return ["leadAssignId" : leadAssignId, "search" : search, "pageIndex" : pageIndex, "pageSize": pageSize]
    }
    
}

struct PagingRequest : XLTTAPIRequestProtocol {
    
    var search : String = ""
    var pageIndex : Int
    var pageSize : Int
    
    func toParameters() -> [String : Any] {
        return ["search" : search, "pageIndex": pageIndex, "pageSize": pageSize]
    }
}

struct FilterRequest : XLTTAPIRequestProtocol {
    
    var type : Int
    var pageIndex : Int
    var pageSize : Int
    var saleTeamId: String?
    var Productid: String?
    var provinceId: String?
    var districtId: String?
    var wardId: String?
    var sort1: Int?
    var sort2: Int?
    var sort3: Int?
    var status: Int?
    var UserId: String?
    var search: String?
    
    func toParameters() -> [String : Any] {
        var params : [String:Any] = ["type" : type, "pageIndex": pageIndex, "pageSize": pageSize]
        if saleTeamId != nil {
            params["saleTeamId"] = saleTeamId
        }
        if Productid != nil {
            params["Productid"] = Productid
        }
        if provinceId != nil {
            params["provinceId"] = provinceId
        }
        if districtId != nil {
            params["districtId"] = districtId
        }
        if wardId != nil {
            params["wardId"] = wardId
        }
        if sort1 != nil {
            params["sort1"] = sort1
        }
        if sort2 != nil {
            params["sort2"] = sort2
        }
        if sort3 != nil {
            params["sort3"] = sort3
        }
        if status != nil {
            params["status"] = status
        }
        if UserId != nil {
            params["UserId"] = UserId
        }
        if search != nil {
            params["search"] = search
        }
        return params
    }
}

struct EmployeeRequest : XLTTAPIRequestProtocol {
    
    var saleTeamId : String = ""
    var pageIndex : Int
    var pageSize : Int
    var search: String?
    
    func toParameters() -> [String : Any] {
        var params : [String:Any] = ["saleTeamId": saleTeamId, "pageIndex": pageIndex, "pageSize": pageSize]
        if search != nil {
            params["search"] = search
        }
        return params
    }
}

struct CustomerDetailRequest : XLTTAPIRequestProtocol {
    
    var leadId : String = ""
    var leadAssignId : String = ""
    
    func toParameters() -> [String : Any] {
        return ["leadId": leadId, "leadAssignId": leadAssignId]
    }
}

struct PinCustomerRequest : XLTTAPIRequestProtocol {
    
    var id : String = ""
    var flag : String = "false"
    
    func toParameters() -> [String : Any] {
        return ["id": id, "flag": flag]
    }
}
