//
//  XLTTQuestionAnswer.swift
//  VNPostXLTT
//
//  Created by m2 on 10/29/20.
//

import Foundation
import UIKit
import ObjectMapper

class XLTTQuestionAnswer: Mappable{
    var displayText = ""
    var dataType :Int = 0
    var value = ""
    
    required init?(map: Map) {
        
    }
    
    init(displayText:String, dataType:Int, value:String) {
        self.displayText = displayText
        self.dataType = dataType
        self.value = value
    }
    
    func mapping(map: Map) {
        displayText <- map["displayText"]
        dataType <- map["dataType"]
        value <- map["value"]
    }
}
