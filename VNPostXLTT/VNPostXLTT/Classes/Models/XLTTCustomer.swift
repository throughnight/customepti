//
//  XLTTCustomer.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/26/20.
//

import Foundation
import UIKit
import ObjectMapper

class XLTTCustomer: Mappable{
    var id = ""
    var leadId = ""
    var status = 0
    var assignType = 0
    var customerName = ""
    var phoneNumber = ""
    var product = ""
    var receivedDate : String?
    var deliveryDate: String?
    var transactionTime: String?
    var flag = false
    var email: String = ""
    var province: String?
    var district: String?
    var ward: String?
    var lead_Status: Int?
    var customerCode: String?
    var temporaryAddress: XLTTAddressModel?
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        leadId <- map["leadId"]
        status <- map["status"]
        assignType <- map["assignType"]
        customerName <- map["customerName"]
        phoneNumber <- map["phoneNumber"]
        product <- map["product"]
        receivedDate <- map["receivedDate"]
        deliveryDate <- map["deliveryDate"]
        transactionTime <- map["transactionTime"]
        flag <- map["flag"]
        email <- map["email"]
        province <- map["province"]
        district <- map["district"]
        ward <- map["ward"]
        lead_Status <- map["lead_Status"]
        customerCode <- map["customerCode"]
        temporaryAddress <- map["temporaryAddress"]
    }
}

extension XLTTCustomer {
    func getFullAddress() -> String {
        var fullAddress = ""
        if ward != nil && ward != "" {
            fullAddress += ((ward ?? "") + ", ")
        }
        if district != nil && district != "" {
            if province != nil && province != "" {
                fullAddress += ((district ?? "") + ", ")
            } else {
                fullAddress += (district ?? "")
            }
        }
        if province != nil && province != "" {
            fullAddress += (province ?? "")
        }
        return fullAddress
    }
}

class XLTTAddressModel : Mappable {
    
    var id: String = ""
    var province : String = ""
    var district : String = ""
    var ward : String = ""
    var street : String = ""
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        province <- map["province"]
        district <- map["district"]
        ward <- map["ward"]
        street <- map["street"]
    }
}

extension XLTTAddressModel {
    func getFullAddress() -> String {
        var address = ""
        if street != "" {
            address += street + ", "
        }
        if ward != "" {
            address += ward + ", "
        }
        if district != "" {
            address += district + ", "
        }
        if province != "" {
            address += province
        }
        return address
    }
}
