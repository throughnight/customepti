//
//  XLTTChartHome.swift
//  VNPostXLTT
//
//  Created by m2 on 10/27/20.
//

import Foundation
import UIKit
import ObjectMapper

class XLTTChartHome : Mappable {
    
    var all = 0
    var success = 0
    var fail = 0
    var processing = 0
    var listChart : [XLTTChart] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        all <- map["all"]
        success <- map["success"]
        fail <- map["fail"]
        processing <- map["processing"]
        listChart <- map["listChart"]
    }
    
}
