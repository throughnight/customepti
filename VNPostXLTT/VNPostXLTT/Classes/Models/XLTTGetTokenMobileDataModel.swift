//
//  XLTTGetTokenMobileDataModel.swift
//  VNPostXLTT
//
//  Created by m2 on 10/27/20.
//

import Foundation
import UIKit
import ObjectMapper

class XLTTGetTokenMobileDataModel : Mappable {
    
    var userName = ""
    var token = ""
    var expiresIn = 0
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        userName <- map["userName"]
        token <- map["token"]
        expiresIn <- map["expiresIn"]
    }
    
}
