//
//  XLTTSaleInfo.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/27/20.
//

import Foundation
import UIKit
import ObjectMapper

class XLTTSaleInfo: Mappable{
    var id = ""
    var employeeCode = ""
    var fullName = ""
    var email = ""
    var phoneNumber = ""
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        employeeCode <- map["employeeCode"]
        fullName <- map["fullName"]
        email <- map["email"]
        phoneNumber <- map["phoneNumber"]
    }
}
