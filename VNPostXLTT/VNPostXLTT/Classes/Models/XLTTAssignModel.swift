//
//  XLTTAssignModel.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/27/20.
//

import Foundation
import ObjectMapper

typealias Codable = Decodable & Encodable

class XLTTAssignModel: Codable {
    var leadAssignId: String = ""
    var userId: String = ""
    var leadId: String?
    var content: String?
    
    enum CodingKeys: String, CodingKey {
        case leadAssignId, userId, leadId, content
    }
    
    init(leadId: String, userId: String) {
        self.leadAssignId = leadId
        self.userId = userId
    }
    
    init(leadAssignId: String, leadId: String, content: String) {
        self.leadAssignId = leadAssignId
        self.leadId = leadId
        self.content = content
    }
    
    init() {
        
    }
}
