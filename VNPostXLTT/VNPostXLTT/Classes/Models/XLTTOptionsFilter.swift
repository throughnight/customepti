//
//  XLTTOptionsFilter.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/28/20.
//

import Foundation
import UIKit
import ObjectMapper

class XLTTOptionsFilter : Mappable {
    
    var id : String = ""
    var code : String = ""
    var name :String = ""
    var fullName: String = ""
    var employeeCode: String = ""
    var email: String = ""
    var title: String = ""
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        code <- map["code"]
        name <- map["name"]
        fullName <- map["fullName"]
        employeeCode <- map["employeeCode"]
        email <- map["email"]
        title <- map["title"]
    }
    
}
