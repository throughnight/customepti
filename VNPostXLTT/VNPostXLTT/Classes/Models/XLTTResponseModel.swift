//
//  XLTTResponseModel.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/26/20.
//

import Foundation
import UIKit
import ObjectMapper

class XLTTResponseModel<T> : Mappable {
    
    var statusCode = 0
    var success = false
    var message = ""
    var data : T?
    
    required init?(map: Map) {
        
    }
    
    init(statusCode:Int, success:Bool, message:String) {
        self.statusCode = statusCode
        self.success = success
        self.message = message
    }
    
    func mapping(map: Map) {
        statusCode <- map["statusCode"]
        success <- map["success"]
        message <- map["message"]
        data <- map["data"]
    }
    
}
