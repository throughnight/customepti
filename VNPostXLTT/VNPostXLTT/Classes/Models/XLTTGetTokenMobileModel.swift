//
//  XLTTGetTokenMobileModel.swift
//  VNPostXLTT
//
//  Created by m2 on 10/27/20.
//

import Foundation
import UIKit
import ObjectMapper

class XLTTGetTokenMobileModel : Mappable {
    
    var data : XLTTGetTokenMobileDataModel?
    var profiletype = 0
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        data <- map["data"]
        profiletype <- map["profiletype"]
    }
    
}
