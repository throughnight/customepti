//
//  XLTTLeadDetailModel.swift
//  VNPostXLTT
//
//  Created by AEGONA on 11/3/20.
//

import Foundation
import UIKit
import ObjectMapper


class XLTTLeadDetailModel : Mappable {
    
    var flag : Bool = false
    var status : Int = 0
    var detailInfoCus : XLTTCustomer?
    var infoProduct: XLTTInfoProduct?
    var infoRegister: [XLTTInfoSeek] = []
    var historyComment : [XLTTNote] = []
    var statusHistory : [XLTTStatusHistory] = []
    var historyAllocation : [XLTTHistoryAllocation] = []
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        flag <- map["flag"]
        status <- map["status"]
        detailInfoCus <- map["detailInfoCus"]
        infoProduct <- map["infoProduct"]
        infoRegister <- map["infoRegister"]
        historyComment <- map["historyComment"]
        statusHistory <- map["statusHistory"]
        historyAllocation <- map["historyAllocation"]
    }
}

class XLTTInfoProduct : Mappable{
    
    var code: String = ""
    var name: String = ""
    var questionAnswer: [XLTTQuestionAnswer] = []
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        name <- map["name"]
        questionAnswer <- map["questionAnswer"]
    }
    
}

class XLTTInfoSeek : Mappable {
    
    var surveySheetId: String = ""
    var surveySheetName: String = ""
    var questionAnswer: [XLTTQuestion] = []
    var isFull = true
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        surveySheetId <- map["surveySheetId"]
        surveySheetName <- map["surveySheetName"]
        questionAnswer <- map["questionAnswer"]
        isFull <- map["isFull"]
    }
    
}

class XLTTQuestion : Mappable {
    var questionName: String = ""
    var values: String = ""
    var dataType: Int = 0
    var order: Int = 0
    var step: Int = 0
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        questionName <- map["questionName"]
        values <- map["values"]
        dataType <- map["dataType"]
        order <- map["order"]
        step <- map["step"]
    }
}

class XLTTNote : Mappable {
    var addedStamp: String = ""
    var note: String = ""
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        addedStamp <- map["addedStamp"]
        note <- map["note"]
    }
}

class XLTTStatusHistory: Mappable {
    
    var addedStamp: String = ""
    var addedUser: String = ""
    var saleStatusBefore: String = "Mới"
    var saleStatusAfter: String = "Mới"
    var historyComment: String?
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        addedStamp <- map["addedStamp"]
        addedUser <- map["addedUser"]
        saleStatusBefore <- map["saleStatusBefore"]
        saleStatusAfter <- map["saleStatusAfter"]
        historyComment <- map["historyComment"]
    }
    
}

class XLTTHistoryAllocation: Mappable {
    
    var addedStamp: String = ""
    var addedUser: String = ""
    var saleUserName: String = ""
    var leadStatus: Int = 0
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        addedStamp <- map["addedStamp"]
        addedUser <- map["addedUser"]
        saleUserName <- map["saleUserName"]
        leadStatus <- map["leadStatus"]
    }
    
}
