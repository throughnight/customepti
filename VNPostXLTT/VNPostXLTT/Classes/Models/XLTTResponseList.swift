//
//  XLTTResponseList.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/26/20.
//

import Foundation
import UIKit
import ObjectMapper

class XLTTResponseList<T> : Mappable {
    
    var search = ""
    var pageIndex = 1
    var pageSize = 20
    var total = 0
    var data : [T] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        search <- map["search"]
        pageIndex <- map["pageIndex"]
        pageSize <- map["pageSize"]
        total <- map["total"]
        data <- map["data"]
    }
    
}
