//
//  XLTTChart.swift
//  VNPostXLTT
//
//  Created by m2 on 10/27/20.
//

import Foundation
import UIKit
import ObjectMapper

class XLTTChart : Mappable {
    
    var x = 0
    var success = 0
    var fail = 0
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        x <- map["x"]
        success <- map["success"]
        fail <- map["fail"]
    }
    
}
