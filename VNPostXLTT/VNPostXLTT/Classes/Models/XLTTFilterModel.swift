//
//  XLTTFilterModel.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/29/20.
//

import Foundation
import UIKit
import ObjectMapper

class XLTTFilterModel: Mappable{
    var id = ""
    var saleTeam : XLTTOptionsFilter?
    var product : XLTTOptionsFilter?
    var province : XLTTOptionsFilter?
    var district : XLTTOptionsFilter?
    var ward : XLTTOptionsFilter?
    var sort1 : Int?
    var sort2 : Int?
    var sort3 : Int?
    var status : Int?
    var user: XLTTOptionsFilter?
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        saleTeam <- map["saleTeam"]
        product <- map["product"]
        province <- map["province"]
        district <- map["district"]
        ward <- map["ward"]
        sort1 <- map["sort1"]
        sort2 <- map["sort2"]
        sort3 <- map["sort3"]
        status <- map["status"]
        user <- map["user"]
    }
}
