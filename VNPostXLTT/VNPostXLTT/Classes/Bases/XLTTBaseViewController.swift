//
//  XLTTBaseViewController.swift
//  VNPostXLTT
//
//  Created by m2 on 10/20/20.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

public class XLTTBaseViewController : UIViewController {
    
    let disposeBag : DisposeBag! = DisposeBag()
    
    let vnpXLTTBundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
    
    func showNavigationBar(animated: Bool = false) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func hideNavigationBar(animated: Bool = false) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isMovingFromParent {
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        }
    }
    
    public func showAlert(title:String?, msg: String?){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Đóng", style: .default, handler: { action in
                                        switch action.style{
                                        case .default:
                                            print("default")
                                            
                                        case .cancel:
                                            print("cancel")
                                            alert.dismiss(animated: false, completion: nil)
                                            
                                        case .destructive:
                                            print("destructive")
                                            
                                            
                                        }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    public func showErrorDialog(errorMsg: String, buttonTitle: String = "Ok"){
        let bundle = Bundle(identifier: XLTTConstants.bundleIdentifier)
        let errorDialog = XLTTDialogError(nibName: "XLTTDialogError", bundle: bundle)
        
        errorDialog.messageDialog = BehaviorRelay<String>(value: getMessageError(error: errorMsg))
        errorDialog.titleButton = BehaviorRelay<String>(value: buttonTitle)
        errorDialog.modalPresentationStyle = .overFullScreen
        errorDialog.onDoneActions = {
            errorDialog.dismiss(animated: false, completion: nil)
        }
        
        self.parent?.present(errorDialog, animated: false, completion: nil)
    }
    
    func getMessageError(error: String) -> String {
        var ms = error
        switch error {
        case "UNAUTHORIZED":
            ms = "xltt_base_service_error_server_occur".localized
            break
        case "LEAD_NOT_FOUND":
            ms = "xltt_lead_not_found".localized
            break
        default:
            ms = error
        }
        return ms
    }
    
}

extension String {
    var localized: String {
        let bundle = Bundle(url: Bundle.main.bundleURL.appendingPathComponent("Frameworks").appendingPathComponent("VNPostXLTT.framework")) ?? Bundle.main
        return NSLocalizedString(self, tableName: nil, bundle: bundle, value: "", comment: "")
    }
}

