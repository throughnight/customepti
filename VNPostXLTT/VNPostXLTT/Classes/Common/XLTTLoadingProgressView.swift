//
//  XLTTLoadingProgressView.swift
//  VNPostXLTT
//
//  Created by m2 on 10/27/20.
//

import Foundation
import UIKit

class  XLTTLoadingProgressView: UIView {
    
    let imageViewLoading = UIImageView()
    let imageViewLogo = UIImageView()
    
    init(uiScreen: UIScreen) {
        super.init(frame: CGRect(x: 0, y: 0, width: uiScreen.bounds.width, height: uiScreen.bounds.height))
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        imageViewLoading.frame.size.height = 80
        imageViewLoading.frame.size.width = 80
        imageViewLoading.image = UIImage(named: "ic_progressbar_spinner",
                                         in: Bundle(url: Bundle.main.bundleURL.appendingPathComponent("Frameworks").appendingPathComponent("VNPostXLTT.framework")) ?? Bundle.main,
                                         compatibleWith: nil)
        imageViewLoading.contentMode = .scaleAspectFit
        imageViewLoading.center = self.center
        
        imageViewLogo.frame.size.height = 40
        imageViewLogo.frame.size.width = 40
        imageViewLogo.image = UIImage(named: "ic_vnpost_logo_progressbar",
                                      in: Bundle(url: Bundle.main.bundleURL.appendingPathComponent("Frameworks").appendingPathComponent("VNPostXLTT.framework")) ?? Bundle.main,
                                      compatibleWith: nil)
        
        imageViewLogo.contentMode = .scaleAspectFit
        imageViewLogo.center = self.center
        
        addSubview(imageViewLoading)
        addSubview(imageViewLogo)
    }
    
    required init(coder: NSCoder) {
        fatalError()
    }
    
    func startAnimating() {
        isHidden = false
        rotate()
    }
    
    func stopAnimating() {
        isHidden = true
        removeRotation()
    }
    
    private func rotate() {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 1
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        self.imageViewLoading.layer.add(rotation, forKey: "rotationAnimation")
    }
    
    private func removeRotation() {
        self.imageViewLoading.layer.removeAnimation(forKey: "rotationAnimation")
    }
}
