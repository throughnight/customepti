//
//  XLTTUtils.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/26/20.
//

import Foundation
import UIKit

let startLoadingMoreOffset: CGFloat = 20.0

func isNearTheBottomEdge(contentOffset: CGPoint, _ tableView: UITableView) -> Bool {
    return contentOffset.y + tableView.frame.size.height + startLoadingMoreOffset > tableView.contentSize.height
}
