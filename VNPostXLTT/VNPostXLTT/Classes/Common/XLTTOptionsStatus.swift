//
//  XLTTOptionsStatus.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/28/20.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class XLTTOptionsStatus: UIViewController {
    
    private let collectionHeader = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
    private let collectionHeaderIdentifier = "COLLECTION_HEADER_IDENTIFIER"
    
    private var titles = [String]()
    
    private var colorHeaderActive = UIColor.blue
    private var colorHeaderInActive = UIColor.gray
    private var colorHeaderBackground = UIColor.white
    private var indicatorActive = UIColor.blue
    
    var currentPosition = -1
    private var tabStyle = OptionsStyle.fixed
    private let heightHeader = 48
    
    var onChangeValue  : ((Int?)->())?
    
    func addItem(title: String){
        titles.append(title)
    }
    
    func setCurrentPosition(position: Int){
        
        currentPosition = position
        let path = IndexPath(item: currentPosition, section: 0)
        DispatchQueue.main.async {
            if self.tabStyle == .flexible {
                self.collectionHeader.scrollToItem(at: path, at: .centeredHorizontally, animated: true)
            }
            
            self.collectionHeader.reloadData()
        }
        
        self.onChangeValue?(position)
    }
    
    func setStyle(style: OptionsStyle){
        tabStyle = style
    }
    
    func build(){
        // view
        view.addSubview(collectionHeader)
        
        // collectionHeader
        collectionHeader.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11.0, *) {
            collectionHeader.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
            collectionHeader.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            // Fallback on earlier versions
        }
        collectionHeader.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionHeader.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionHeader.heightAnchor.constraint(equalToConstant: CGFloat(heightHeader)).isActive = true
        collectionHeader.widthAnchor.constraint(equalToConstant: CGFloat(view.layer.frame.size.width - 20)).isActive = true
        (collectionHeader.collectionViewLayout as? UICollectionViewFlowLayout)?.scrollDirection = .horizontal
        collectionHeader.showsHorizontalScrollIndicator = false
        collectionHeader.backgroundColor = colorHeaderBackground
        collectionHeader.register(HeaderCell.self, forCellWithReuseIdentifier: collectionHeaderIdentifier)
        collectionHeader.delegate = self
        collectionHeader.dataSource = self
        collectionHeader.reloadData()
    }
    
    private class HeaderCell: UICollectionViewCell {
        
        private let label = UILabel()
        
        var text: String! {
            didSet {
                label.text = text
            }
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            setupUI()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func select(didSelect: Bool){
            if didSelect {
                label.textColor = .white
//                self.backgroundColor = UIColor(hex: "025BBB")
                self.xlttsetGradientBackground(colorStart: UIColor(hex: "3692F4"), colorEnd: UIColor(hex: "025BBB"))
            }else{
                label.textColor = .black
                self.backgroundColor = .white
                self.xlttRemoveGradient()
            }
        }
        
        private func setupUI(){
            // view
            self.layer.cornerRadius = 8
            self.layer.masksToBounds = true
            self.clipsToBounds = true
            self.addSubview(label)
            // label
            label.translatesAutoresizingMaskIntoConstraints = false
            label.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
            label.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
            label.font = UIFont.systemFont(ofSize: 14)
            label.textColor = .black
        }
        
    }
    
}

extension XLTTOptionsStatus: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if currentPosition == indexPath.row {
            setCurrentPosition(position: -1)
        } else {
            setCurrentPosition(position: indexPath.row)
        }
        print("collectionView: \(indexPath.row))")
    }
}

extension XLTTOptionsStatus: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionHeaderIdentifier, for: indexPath) as! HeaderCell
        cell.text = titles[indexPath.row]
        
        var didSelect = false
        
        if currentPosition == indexPath.row {
            didSelect = true
        }
        
        cell.select(didSelect: didSelect)
        
        return cell
    }
}

extension XLTTOptionsStatus: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if tabStyle == .fixed {
            let spacer = CGFloat(titles.count)
            return CGSize(width: view.frame.width / spacer, height: CGFloat(heightHeader))
        }else{
            return CGSize(width: view.frame.width * 20 / 100, height: CGFloat(heightHeader))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

enum OptionsStyle: String {
    case fixed
    case flexible
}
