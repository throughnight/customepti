//
//  XLTTProtocal.swift
//  VNPostXLTT
//
//  Created by AEGONA on 10/26/20.
//

import Foundation

protocol XLTTViewModelProtocal {
    associatedtype Input
    associatedtype Output
    associatedtype Dependency
    
    var input      : Input? { get set }
    var output     : Output? { get set }
    var dependency : Dependency? { get set }
    
    @discardableResult
    func transform(input: Input?, dependency: Dependency?) -> Output?
}

protocol XLTTAPIRequestProtocol {
    func toParameters() -> [String:Any]
}

protocol XLTTViewControllerProtocal {
    func setupViewModel()
    func setupListView()
}
