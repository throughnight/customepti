//
//  UIView+Extensions.swift
//  
//
//  Created by AEGONA on 9/10/20.
//

import Foundation

enum GradientColorDirection {
    case vertical
    case horizontal
}

extension UIView {
    
    @IBInspectable
    var xlttCornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var xlttBorderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var xlttBorderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var xlttShadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var xlttShadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var xlttShadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var xlttShadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    func xlttApplyGradient(withColours colours: [UIColor], locations: [NSNumber]? = nil,
                            direction: GradientColorDirection = .vertical) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        
        if direction == .horizontal {
            gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
        }
        
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func xlttRemoveGradient() {
        self.layer.sublayers = self.layer.sublayers?.filter { theLayer in
            !theLayer.isKind(of: CAGradientLayer.classForCoder())
        }
    }
    
    func xlttSetBlueGradient(){
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor(hex: "3692F4"), UIColor(hex: "025BBB")]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        layer.insertSublayer(gradientLayer, at: 0)

    }
    
    func xlttsetGradientBackground(colorStart: UIColor, colorEnd: UIColor) {
        let gradientLayer = CAGradientLayer()
        let updatedFrame = bounds
//        updatedFrame.size.height += 20
        gradientLayer.frame = updatedFrame
        gradientLayer.colors = [colorStart.cgColor, colorEnd.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func xlttsetGradientBackground(colorStart: UIColor, colorEnd: UIColor, radius : Int) {
        let gradientLayer = CAGradientLayer()
        let updatedFrame = bounds
//        updatedFrame.size.height += 20
        gradientLayer.frame = updatedFrame
        gradientLayer.cornerRadius = CGFloat(radius)
        gradientLayer.colors = [colorStart.cgColor, colorEnd.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
         let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
         let mask = CAShapeLayer()
         mask.path = path.cgPath
         layer.mask = mask
     }
    
    func xlttSetViewBackgroundGradientIsBlue() {
        let gradientLayer = CAGradientLayer()
        let updatedFrame = self.bounds
        gradientLayer.frame = updatedFrame
        gradientLayer.colors = [UIColor(hex: "3692F4").cgColor, UIColor(hex: "025BBB").cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        self.clipsToBounds = true
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func XLTTSetGradientOrangeBackground() {
        let gradientLayer = CAGradientLayer()
        let updatedFrame = bounds
        gradientLayer.frame = updatedFrame
        gradientLayer.colors = [UIColor(hex: "FCB71E").cgColor, UIColor(hex: "F07700").cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        layer.insertSublayer(gradientLayer, at: 0)
    }
}

