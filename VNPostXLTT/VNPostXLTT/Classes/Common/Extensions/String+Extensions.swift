//
//  String+Extensions.swift
//  
//
//  Created by AEGONA on 9/10/20.
//

import Foundation

extension String {
    
    func xlttToDate(format: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        
        return formatter.date(from: self)
    }
    
    static func xlttFormatCurrency(_ inputNumber: NSNumber, symbol: String = "VND") -> String? {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.currencySymbol = symbol
        currencyFormatter.currencyGroupingSeparator = ","
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.positiveFormat = "#,##0 ¤"
        // localize to your grouping and decimal separator
        currencyFormatter.locale = Locale.current
        
        return currencyFormatter.string(from: inputNumber)
    }
    
    func xlttIsValidEmail() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        let result = emailTest.evaluate(with: self)
        return result
    }
    

    func xlttIsValidIdentityNumber() -> Bool {
        let identityNumberRegex = "^[0-9]{9}$|^[0-9]{12}$"
        let identityNumberTest = NSPredicate(format: "SELF MATCHES %@", identityNumberRegex)
        let result =  identityNumberTest.evaluate(with: self)
        return result
    }
    
    func xlttIsValidText() -> Bool {
        let emailRegex = "[A-Za-z]"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        let result = emailTest.evaluate(with: self)
        return result
    }
    
    func xlttIsValidName() -> Bool {
        let emailRegex = "^[_A-z|àáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹýÀÁÃẠẢĂẮẰẲẴẶÂẤẦẨẪẬÈÉẸẺẼÊỀẾỂỄỆĐÌÍĨỈỊÒÓÕỌỎÔỐỒỔỖỘƠỚỜỞỠỢÙÚŨỤỦƯỨỪỬỮỰỲỴỶỸÝ]*((\\s)*[_A-z|àáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹýÀÁÃẠẢĂẮẰẲẴẶÂẤẦẨẪẬÈÉẸẺẼÊỀẾỂỄỆĐÌÍĨỈỊÒÓÕỌỎÔỐỒỔỖỘƠỚỜỞỠỢÙÚŨỤỦƯỨỪỬỮỰỲỴỶỸÝ])*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        var result = emailTest.evaluate(with: self)
        if result {
            if self.trimmingCharacters(in: .whitespacesAndNewlines).contains(" ") {
                result = true
            } else {
                result = false
            }
        }
        return result
    }
    
    func xlttFormatDateSubmit() -> String? {
        let date = self.xlttToDate(format: "dd/MM/yyyy")
        let formatString = DateFormatter()
        formatString.dateFormat = "yyyy-MM-dd"
        if date != nil {
            return formatString.string(from: date!)
        }
        return self
    }
    
    func xlttFormatDateInput() -> String? {
        let date = self.xlttToDate(format: "yyyy-MM-dd")
        let formatString = DateFormatter()
        formatString.dateFormat = "dd/MM/yyyy"
        if date != nil {
            return formatString.string(from: date!)
        }
        return self
    }
    
    func xlttFormatPrice(symbol: String = "VND") -> String? {
        var stringText = self.replacingOccurrences(of: ",", with: "")
        stringText = stringText.replacingOccurrences(of: ".", with: "")
        let number = NSNumber(value: Double(stringText) ?? 0)
        let currencyFormatter = NumberFormatter()
        currencyFormatter.currencySymbol = symbol
        currencyFormatter.currencyGroupingSeparator = ","
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.positiveFormat = "#,##0"
        // localize to your grouping and decimal separator
//        currencyFormatter.locale = Locale.current
        
        return currencyFormatter.string(from: number)
    }
    
    func xlttDateToStringCustomer(endFormat: String = "dd/MM/yyyy - HH:mm:ss") -> String? {
        let newDate = self.substring(0..<19)
        let date = newDate.xlttToDate(format: "yyyy-MM-dd'T'HH:mm:ss")
        let cal = Calendar.current.date(byAdding: .hour, value: 7, to: date ?? Date())
        let formatString = DateFormatter()
        formatString.dateFormat = endFormat
        if cal != nil {
            return formatString.string(from: cal!)
        }
        return self
    }
    
    func xlttFormatTime() -> String? {
        let indexOf = self.lastIndex(of: ".")?.utf16Offset(in: self) ?? self.count
        let newDate = self.substring(0..<indexOf)
        if newDate.contains(".") == true {
            let indexDay = newDate.firstIndex(of: ".")?.utf16Offset(in: newDate) ?? newDate.count
            let day = newDate.substring(0..<indexDay)
            let times = newDate.substring((indexDay + 1)..<newDate.count)
            return "\(day) ngày \(times)"
        }
        return newDate
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
}

extension StringProtocol {
    func index<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> Index? {
        range(of: string, options: options)?.lowerBound
    }
    func endIndex<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> Index? {
        range(of: string, options: options)?.upperBound
    }
    func indices<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> [Index] {
        var indices: [Index] = []
        var startIndex = self.startIndex
        while startIndex < endIndex,
              let range = self[startIndex...]
                .range(of: string, options: options) {
            indices.append(range.lowerBound)
            startIndex = range.lowerBound < range.upperBound ? range.upperBound :
                index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return indices
    }
    func ranges<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> [Range<Index>] {
        var result: [Range<Index>] = []
        var startIndex = self.startIndex
        while startIndex < endIndex,
              let range = self[startIndex...]
                .range(of: string, options: options) {
            result.append(range)
            startIndex = range.lowerBound < range.upperBound ? range.upperBound :
                index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
    
    func substring(_ r: Range<Int>) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
        let toIndex = self.index(self.startIndex, offsetBy: r.upperBound)
        let indexRange = Range<String.Index>(uncheckedBounds: (lower: fromIndex, upper: toIndex))
        return String(self[indexRange])
    }
}

extension String {
    subscript(value: Int) -> Character {
        self[index(at: value)]
    }
}

extension String {
    subscript(value: NSRange) -> Substring {
        self[value.lowerBound..<value.upperBound]
    }
}

extension String {
    subscript(value: CountableClosedRange<Int>) -> Substring {
        self[index(at: value.lowerBound)...index(at: value.upperBound)]
    }
    
    subscript(value: CountableRange<Int>) -> Substring {
        self[index(at: value.lowerBound)..<index(at: value.upperBound)]
    }
    
    subscript(value: PartialRangeUpTo<Int>) -> Substring {
        self[..<index(at: value.upperBound)]
    }
    
    subscript(value: PartialRangeThrough<Int>) -> Substring {
        self[...index(at: value.upperBound)]
    }
    
    subscript(value: PartialRangeFrom<Int>) -> Substring {
        self[index(at: value.lowerBound)...]
    }
}

private extension String {
    func index(at offset: Int) -> String.Index {
        index(startIndex, offsetBy: offset)
    }
}
