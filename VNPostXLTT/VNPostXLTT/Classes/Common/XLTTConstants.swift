//
//  XLTTConstants.swift
//  VNPostXLTT
//
//  Created by m2 on 10/20/20.
//

import Foundation
// MARK: Server URl
public enum XLTTEnvironment: String {
    
    case develop = "develop"
    case staging = "staging"
    case production = "production"
    
    var apiDomain: String {
        get {
            switch self {
            case .develop: return "https://inp02.vsit.com.vn"
            case .staging: return "https://pti-ipa-api-test.aegona.work"
            case .production: return "https://inp.vsit.com.vn"
            }
        }
    }
    
    var apiVersion : String {
        get {
            switch self {
            case .develop: return "v1.0"
            case .staging: return "v1.0"
            case .production : return "v1.0"
            }
        }
    }
    
    var tokenDomain: String {
        get {
            switch self {
            case .develop: return "https://inp02.vsit.com.vn"
            case .staging: return "https://inp01.vsit.com.vn"
            case .production: return "https://inp.vsit.com.vn"
            }
        }
    }
    
}

struct XLTTConfiguration {
    
    static var shared = XLTTConfiguration()
    
    private init() {}
    
    var environment: XLTTEnvironment = .develop
}

// MARK: SDK  Constants
enum XLTTConstants {
    static let bundleIdentifier = "org.cocoapods.VNPostXLTT"
    
    // MARK: - Color
    static let navigationBarMainColor = UIColor(hex: "FCB71E")
    static let navigationBarSubColor = UIColor(hex: "F07700")
    
    static let buttonMainColor = UIColor(hex: "025BBB")
    static let buttonSubColor = UIColor(hex: "3692F4")
    
    static let tabBarMainColor = UIColor(hex: "0064CB")
    static let tabBarSubColor = UIColor(hex: "2E8DF0")
    
    static let xltt_color_background = UIColor(hex: "EEF3FF")
    static let xltt_color_blue = UIColor(hex: "2E8DF0")
    
    static let xltt_color_blue_1 = UIColor(hex: "144E8C")
    static let xltt_color_green_1 = UIColor(hex: "00B16A")
    static let xltt_color_red_1 = UIColor(hex: "F64744")
    static let xltt_color_orange_1 = UIColor(hex: "ff9d3f")
    
    static let xltt_color_blue_2 = UIColor(hex: "127BCA")
}

enum XLTTCustomerType: Int {
    case CUSTOMER_STATUS_NEW = 0
    case CUSTOMER_STATUS_CONTACT = 1
    case CUSTOMER_STATUS_SUCCESS = 2
    case CUSTOMER_STATUS_FAIL = 3
}

enum XLTTCustomerActionType : Int {
    case CONTACT = 1
    case SUCCESS = 2
    case FAIL = 3
}

enum XLTTPotenitalcus : Int {
    case ME = 0
    case GROUP = 1
    case ATTACH = 2
    case HISTORY = 3
}

enum XLTTProfileType: Int{
    case MEMBER = 3
    case SUPERVISOR  = 5
}

enum XLTTChartType: String{
    case GROUP = "GROUP"
    case ME    = "ME"
}

enum XLTTCustomerDataType : Int {
    case CUSTOMER_STATUS2_REFUSE = 2
    case CUSTOMER_STATUS2_EXPIRED = 4
}

enum XLTTHistoryType : Int {
    case EXCHANGED = 1
    case PROCESSING = 2
    case COMPLETED = 3
    case ASSIGN_HISTORY = 4
}

enum XLTTDataType : Int {
    case TEXT = 0
    case NUMBER = 1
    case DOUBLE = 2
    case PERCENTAGE = 3
    case BOOLEAN = 4
    case DATE = 5
    case LIST = 6
}
